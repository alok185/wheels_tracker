var express = require('express');
var authRoute = require('../routes/auth.route');
var secureRoute = require('../routes/secure.route');
var secureRouteAndroid = require('../routes/secure_android.route');
var checkToken = require('./secureRoute');
var bodyParser = require('body-parser');
var commonFunctionWeb = require('../commons/commonFunctionWeb/commonFuncWeb');

//var dbfunc = require('./db-function');
var db = require('./database');
// var admin = require("firebase-admin");
// var serviceAccount = require("./task-manager-2eb7e-firebase-adminsdk-u6teg-d951d5f16e.json");

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://task-manager-2eb7e.firebaseio.com"
// });

//HTTTP2..........................................................STARTS.................

// const http2 = require('http2');
// const fs = require('fs');
// const server = http2.createSecureServer({
//   key: fs.readFileSync('localhost-privkey.pem'),
//   cert: fs.readFileSync('localhost-cert.pem')
// });

//HTTP2............................................................ENDS.....................

var app = express();


//SOCKET IO EXAMPLE++++++++++++++++++++++++++++++++++++++++++++++++++STARTS+++++++++++++++

var http = require('http').Server(app);
var io = require('socket.io')(http);

// app.get('/', function(req, res){
// res.sendFile(__dirname + '/index.html');
// });

http.listen(3002, function(){
console.log('listening on *:3002');
});

  io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
  console.log('user disconnected');
  });
  });

io.on('connection', function(socket){
  socket.on('notification', function(msg){
  //console.log("RRRRRRRRRRRRRRRRRRRROOOOOOOOOOOOMMMM-----",msg)
  io.emit('notification', msg);
  });
});

io.sockets.on('connection', (socket) => { /* socket object allows us to join specific clients 
  to chat rooms and also to catch
  and emit the events.*/
  // handle incoming connections from clients
  // once a client has connected, we expect to get a ping from them saying what room they want to join
  socket.on('room', function(data) {
  // console.log("JOINING the rrrrrrrrrrrrrrrrrrrrrrroooooooooooooooommmmmmmm",data)
  socket.join('user'+data.user_code);
  });

  socket.on('leave room', function(data) {
  console.log("leaving the rrrrrrrrrrrrrrrrrrrrrrroooooooooooooooommmmmmmm",data)
  socket.leave('user'+data.user_code);
  });
  
  socket.on('send notification', async (data) => {
    try{
  //emitting the 'new message' event to the clients in that room
  // console.log("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhinnnnnnnnnnnnnnnnntttt datatattatttttttttttttttt","dddddddddddddddddddddddddddddd",data)
  // let insertNotification = await commonFunctionWeb.insertNotificationFunctionForWeb(data.user_code,data.title,data.message,-1,data.target_panel,"",0)
  // "title":title,"message":message,"created":await currentDateTime()
  io.emit('user'+data.user_code, {user_code:data.user_code,title:data.title,message:data.message,created:data.created});
    }catch(e){
      console.log(e)
    }
  })
})

//SOCKET IO EXAMPLE++++++++++++++++++++++++++++++++++++++++++++++++++ENDSHERERERRERE++++++++++++++++++












app.use(function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
   res.header('Access-Control-Allow-Headers', 'Content-Type');
 next();
});



//access allow
app.all('*', function(req, res,next) {
  /**
   * Response settings
   * @type {Object}
   */
  var responseSettings = {
      "AccessControlAllowOrigin": req.headers.origin,
      "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
      "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
      "AccessControlAllowCredentials": true
  };

  /**
   * Headers
   */
  
  res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
  res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
  res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
  res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

  if ('OPTIONS' == req.method) {
      res.send(200);
  }
  else {
      next();
  }
});


//set static folder
app.use("/public", express.static('public'));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use((error, request, response, next) => {
  if (error !== null) {
    console.log('error', error);
    return response.json({"invalid": "json"});
  }
  return next();
});

var router = express.Router();
app.use('/api',router);
authRoute.init(router);

var secureApi = express.Router();
app.use('/secureApi',secureApi);
secureApi.use(checkToken);
secureRoute.init(secureApi);
secureRouteAndroid.init(secureApi);


module.exports = {app}
