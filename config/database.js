var mysql = require('mysql');

function specificConnection(db, multiStat) {
  var connect = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : db,
    dateStrings : true,
    multipleStatements: true,
    timezone : 'GMT+5:30'
  });
  return connect;
}

// var connection = mysql.createPool({
//   host     : 'localhost',
//   user     : 'root',
//   password : ''
// });

module.exports = {specificConnection};
