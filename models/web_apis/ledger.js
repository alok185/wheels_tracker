var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');

var ledgerModel = {

    fetchAllLedgerData :fetchAllLedgerData,
    fetchSpecificLedgerDetailsOfParty:fetchSpecificLedgerDetailsOfParty

};


function fetchAllLedgerData(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,name,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var ledgerData = [];
        for(var i =0;i<rows.length;i++){
        if(rows[i].type_of_ob == 0){
            var opening_balance = -(rows[i].opening_balance);
        }else{
        var opening_balance = rows[i].opening_balance;
        }
        var closing_balance = rows[i].opening_balance
        let partyClosingbalance = await commonFunctionWeb.calculateClosingbalanceOfParty(connection,rows[i].code)
        
        var item ={};
        item.code = rows[i].code,
        item.name = rows[i].name,
        item.closing_balance = partyClosingbalance,
        item.opening_balance = opening_balance
        item.party_type_code = rows[i].party_type_code
        item.created = rows[i].created
        
        ledgerData.push(item)
        }
        if(ledgerData.length > 0){
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction(ledgerData)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
        "success":true,
        "message":"Ledger data fetched successfully",
        "data":encriptedData,
        // "data":ledgerData,
        })
        }else{
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction([])
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
          "success":true,
          "message":"Ledger data fetched successfully",
          "data":encriptedData,
          // "data":ledgerData,
          })    
        }
        }else{
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction([])
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
          "success":true,
          "message":"Ledger data fetched successfully",
          "data":encriptedData,
          // "data":ledgerData,
          })    
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        let compressData = await commonFunctionWeb.compressDataFunction([])
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
          "success":true,
          "message":"Ledger data fetched successfully",
          "data":encriptedData,
          // "data":ledgerData,
          })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }




//function
function fetchPartyTotalPayments(connection,party_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT SUM(payments.amount) AS total_amount FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code  WHERE party_payments.status != 2 AND party_payments.party_code = '${party_code}' ORDER BY created DESC`, async (error, rowsPartnerPayments, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{
        console.log("+++++++++++++++++++fetchTotalFuelEntryAmountOfParty+++++++++++",rowsPartnerPayments)
        if(rowsPartnerPayments.length > 0){
        if(rowsPartnerPayments[0].total_amount != null){
        resolve(rowsPartnerPayments[0].total_amount)
        }else{
        resolve(0)
        }
        }else{
        resolve(0)
        }
        }catch(e){
        console.log(e)
        resolve(0)
        }
       }
       })
     })
    }

function fetchTotalFuelEntryAmountOfParty(connection,party_code){
    connection.query(`SELECT SUM(amount) AS total_amount FROM fuel_entry WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        console.log("+++++++++++++++++++fetchTotalFuelEntryAmountOfParty+++++++++++",rows)
        if(rows.length > 0){
        if(rows[0].total_amount != null){
        resolve(rows[0].total_amount)
        }else{
        resolve(0)
        }
        }else{
        resolve(0)
        }
        }catch(e){
        console.log(e)
        resolve(0)    
        } 
      }
    })
   }


   function fetchTotalAutoPartAmountOfParty(connection,party_code){
    connection.query(`SELECT SUM(amount) AS total_amount FROM auto_parts WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        console.log("+++++++++++++++++++fetchTotalAutoPartAmountOfParty+++++++++++",rows)
        if(rows[0].total_amount != null){
        resolve(rows[0].total_amount)
        }else{
        resolve(0)
        }
        }else{
        resolve(0)
        }
        }catch(e){
        console.log(e)
        resolve(0)    
        } 
      }
    })
   }



function fetchSpecificLedgerDetailsOfParty(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.user_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT * FROM parties WHERE status != 2 AND code = '${body.party_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{
       // console.log(`SELECT * FROM ledger WHERE status != 2 AND party_code = '${body.party_code}' ORDER BY created DESC `,rows)
        if(rows.length > 0){
        let auto_parts_data = await fetchALLAutoPartsDataOfParty(connection,body.party_code);
        let fuel_entry_data = await fetchAllFuelEntryDataOfParty(connection,body.party_code);
        let party_payment_data = await fetchAllPaymentDataOfparty(connection,body.party_code);
        //let billy_data = await fetchAllBilltyData()
        var item = {};
        item.party_code = rows[0].code
        item.party_type_code = rows[0].party_type_code
        item.opening_balance = rows[0].opening_balance,
        item.type_of_ob = rows[0].type_of_ob
        item.party_name = rows[0].name
        item.created = rows[0].created
        item.auto_parts_data = auto_parts_data,
        item.fuel_entry_data = fuel_entry_data,
        item.party_payment_data = party_payment_data;
        
        let compressData = await commonFunctionWeb.compressDataFunction(item)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Ledger data fetched successfully",
        "data":encriptedData,
        // "data":item,
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })  
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }




function fetchAllPaymentDataOfparty(connection,party_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT DISTINCT party_payments.code,payments.amount,payments.bill_no,payments.bill_date,payments.from_date,payments.to_date,payments.amount,payments.purpose,payments.remarks,payments.created FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code  WHERE party_payments.status != 2 AND party_payments.party_code = '${party_code}'`, async (error, rowsPartnerPayments, fields)=>{
        if(error){
        createErrorLogFile(error);sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{
        console.log("+++++++++++++++++++fetchTotalFuelEntryAmountOfParty+++++++++++",rowsPartnerPayments)
        if(rowsPartnerPayments.length > 0){
        resolve(rowsPartnerPayments)
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve([])
        }
        }
        })
        })
    }
      
function fetchAllFuelEntryDataOfParty(connection,party_code){
    return new Promise(async function(resolve, reject){
    connection.query(`SELECT code,challan_no,quantity,fuel_rate,amount,bill_date,remarks,created FROM fuel_entry WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        createErrorLogFile(error);sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        console.log("+++++++++++++++++++fetchAllFuelEntryDataOfParty+++++++++++")
        if(rows.length > 0){
        resolve(rows)
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve([])    
        } 
      }
    })
   })
  }
      
      
function fetchALLAutoPartsDataOfParty(connection,party_code){
    return new Promise(async function(resolve, reject){
    connection.query(`SELECT code,challan_no,bill_date,remarks,created,amount FROM auto_parts WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        createErrorLogFile(error);sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        console.log("+++++++++++++++++++fetchTotalAutoPartAmountOfParty+++++++++++",rows)
        resolve(rows)
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve([])    
        } 
      }
    })
  })
}
      
      
      





    function fetchSpecificLedgerDetailsOfPartyForFirstPage(connection,party_code){
        return new Promise(async function(resolve, reject){
            try{
            connection.query(`SELECT * FROM ledger WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
           // console.log(`SELECT * FROM ledger WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC `,rows)
            if(rows.length > 0){
            var item = {};
            item.opening_balance = rows[0].opening_balance,
            item.closing_balance = rows[0].closing_balance,
            resolve(item)
            }else{
            resolve({"opening_balance":0,"closing_balance":0});  
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
           }
         })
        }
    
    


module.exports = ledgerModel;
