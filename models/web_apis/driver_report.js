
var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');



var driverReportModel = {
    fetchAllDriverReport:fetchAllDriverReport,
    fetchAllNecesaryDataForFilteringDriverReport:fetchAllNecesaryDataForFilteringDriverReport,

};
 



function fetchAllNecesaryDataForFilteringDriverReport(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        console.log(req.params)
        //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        let vehicle_list = await fetchAllVehicle(connection);
        let driver_list = await fetchAllDriver(connection);
        var item = {"driver_list":driver_list,"vehicle_list":vehicle_list}
        connection.destroy()
        let compressData = await commonFunctionWeb.compressDataFunction(item)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        resolve({
        "success":true,
        "message":"Data fetched successfully",
        // "data":item,
        "data":encriptedData
        })
        }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
      }
    })
   }



function fetchAllVehicle(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,label AS name,vehicle_no FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
        //connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }
  
  

    function fetchAllDriver(connection){
        return new Promise(async function(resolve, reject){
            try{
            connection.query(`SELECT code,name,mobile,profile_image_url,profile_image_url_thumbnail FROM drivers WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
            if(error){
            resolve(error);
            }else{ 
            //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
            if(rows.length > 0){
            resolve(rows)
            }else{
            resolve([])
            }
            }
            })
            }catch(e){
            console.log(e)
            resolve([])
           }
         })
        }
      







function fetchAllDriverReport(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var fuelEntryChallan = [];
        let billtyData = [];
        let autPartChallan = [];
        let driver_salary = 0
        if(body.driver_code != 0 && body.vehicle_code == 0){
        billtyData = await fetchWhenDriverIsSelected(connection,from_date,to_date,body.driver_code)
        let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection, body.driver_code)
        if(driverDetails != null)
        driver_salary = driverDetails.salary
        }
        if(body.driver_code == 0 && body.vehicle_code != 0){
        billtyData = await fetchAllDataWhenOnlyVehicleIsSelected(connection,from_date,to_date,body.vehicle_code)
        fuelEntryChallan = await fechFuelEntryChallanOfVehicle(connection,from_date,to_date,body.vehicle_code)
        autPartChallan = await fetchAutoPartChallanOfVehicle(connection,from_date,to_date,body.vehicle_code)
        }

        if(body.driver_code != 0 && body.vehicle_code != 0){
        billtyData = await fetchAllDataWhenBothDataIsSelected(connection,from_date,to_date,body.driver_code,body.vehicle_code)
        }

        // console.log("driver_salary=>>>",driver_salary)
        var item = {"billty_data":billtyData,"fuel_entry_challan":fuelEntryChallan,"auto_part_challan":autPartChallan,"driver_salary":driver_salary}
        connection.destroy();
        
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(item);
        let compressData = await commonFunctionWeb.compressDataFunction(item)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        // console.log("Compressed Data Size =====>>>>>>",encriptedData.length)
        resolve({
        "success":true,
        "message":"Driver Report fetched successfully",
        // "data":item,
        "data":encriptedData,
        // "data":compressData
        })
        }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
       }  
      })
     }



function fetchWhenDriverIsSelected(connection,from_date,to_date,driver_code){
    return new Promise(async function(resolve, reject){
    connection.query(`SELECT *,@a:=@a+1 slno FROM billty, (SELECT @a:= 0) AS a WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND driver_code = '${driver_code}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        var billtyData = [];
        let startDateAndEndDateArray = await getDateArray(new Date(from_date),new Date(to_date))
        // console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRREACHES HERE",startDateAndEndDateArray,"fromdate",from_date,"END DAte",to_date,"CCCCCCONNNNDDDDDITION==>>")
        for(var j =0;j<startDateAndEndDateArray.length;j++){
        for(var i = 0;i<rows.length;i++){
            // console.log("rows[iiiiii==>>].bill_date",rows[i].bill_date,"==",startDateAndEndDateArray[j].toISOString().split("T")[0],"RRRESSSSULLTT",rows[i].bill_date == startDateAndEndDateArray[j].toISOString().split("T")[0])
            if(rows[i].bill_date == startDateAndEndDateArray[j].toISOString().split("T")[0]){
                var vehicle_label = '';
                var party_name = "";
                var vehicle_no = ''
                var driver_name = ''
                var mine_name = '';
                var driver_mobile = '';
                var driver_salary = 0
                var driver_commission = 0
                var route_details = {};
                let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                if(routeDetails != null){
                route_details = routeDetails
                driver_commission = routeDetails.driver_commission
                }
                if(vehicle_details != null){
                vehicle_label = vehicle_details.label
                vehicle_no = vehicle_details.vehicle_no
                }
                if(party_details != null){
                party_name = party_details.name
                }
                if(driverDetails != null){
                driver_name = driverDetails.name
                driver_mobile = driverDetails.driver_mobile
                driver_salary = driverDetails.salary
                }
                var item = {}
                item.slno = j+1
                item.code = rows[i].code
                item.billty_no = rows[i].billty_no
                item.attendance_value = 1
                item.party_code = rows[i].party_code
                item.party_name = party_name
                item.bill_date = rows[i].bill_date,
            // item.route_details = route_details
                item.route_code = rows[i].route_commission_code
                item.route_from = routeDetails.mine_name
                item.route_to = routeDetails.party_name
                item.vehicle_code = rows[i].vehicle_code
                item.vehicle_label = vehicle_label
                item.vehicle_no = vehicle_no
                item.driver_code = rows[i].driver_code
                // item.driver_name = driver_name
                // item.driver_salary = driver_salary
                item.driver_commission = driver_commission
                item.advance = rows[i].advance
                // item.remarks = rows[i].remarks,
                item.freight_rate = rows[i].freight_rate
                item.challan_weight = rows[i].challan_weight
                item.diesel = rows[i].diesel,
                item.received_weight = rows[i].received_weight,
                item.short_weight = rows[i].short_weight,
                
                // item.image_url_1 = rows[i].image_url_1,
                // item.image_url_2 = rows[i].	image_url_2,
                // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                // item.created = rows[i].created
                // item.selected = false
                billtyData.push(item)
                break;
            }else{
                if(i == rows.length -1){
                var attendanceData = await checkDriverWasPresentInThatDate(connection,startDateAndEndDateArray[j].toISOString().split("T")[0],driver_code)
                console.log("attendanceData",attendanceData)
                var item = {}
                item.slno = j+1
                item.code = ""
                item.billty_no = attendanceData.label
                item.attendance_value = attendanceData.value
                item.party_code = ""
                item.party_name = ""
                item.bill_date = startDateAndEndDateArray[j].toISOString().split("T")[0],
            // item.route_details = route_details
                item.route_from = ""
                item.route_code = ""
                item.route_to = ""
                item.vehicle_code = ""
                item.vehicle_label = ""
                item.vehicle_no = ""
                // item.driver_code = rows[i].driver_code
                // item.driver_name = driver_name
                // item.driver_mobile = driver_mobile
                item.advance = ""
                // item.remarks = rows[i].remarks,
                item.freight_rate = ""
                item.challan_weight = ""
                item.diesel = "",
                item.received_weight = ""
                item.short_weight = ""
                // item.driver_salary = ""
                item.driver_commission = ""
                // item.image_url_1 = rows[i].image_url_1,
                // item.image_url_2 = rows[i].	image_url_2,
                // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                // item.created = ""
                // item.selected = false
                billtyData.push(item)
                }
            }
          }
        }
        if(billtyData.length > 0){
        // connection.destroy();
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
        resolve(billtyData)
        }else{
        // connection.destroy();
        resolve([])    
        }
        }else{
        // connection.destroy();
        resolve([])
        }
        }catch(e){
        // connection.destroy();
        console.log(e)
        resolve([])    
       }    
      }
     })     
    })
    }


function getDateArray (start, end) {
        var arr = new Array();
        var dt = new Date(start);
        while (dt <= end) {
        arr.push(new Date(dt));
        dt.setDate(dt.getDate() + 1);
        }
     return arr;
   } 



function fetchAllDataWhenOnlyVehicleIsSelected(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT *,@a:=@a+1 slno FROM billty, (SELECT @a:= 0) AS a WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' ORDER BY bill_date DESC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
            // console.log(rows)
            if(rows.length > 0){
            var billtyData = [];
            for(var i = 0;i<rows.length;i++){
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var route_details = {};
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    route_details = routeDetails
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    }
                    var item = {}
                    item.slno = rows[i].slno
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,
                // item.route_details = route_details
                    item.route_code = rows[i].route_commission_code
                    item.route_from = routeDetails.mine_name
                    item.route_to = routeDetails.party_name
                    item.vehicle_code = rows[i].vehicle_code
                    item.vehicle_label = vehicle_label
                    item.vehicle_no = vehicle_no
                    item.driver_code = rows[i].driver_code
                    item.driver_name = driver_name
                    // item.driver_mobile = driver_mobile
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    item.freight_rate = rows[i].freight_rate
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    // item.created = rows[i].created
                    // item.selected = false
                    billtyData.push(item)
                }
            if(billtyData.length > 0){
            // connection.destroy();
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }else{
            resolve([])
            }
            }catch(e){
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
       }



function fetchAllDataWhenBothDataIsSelected(connection,from_date,to_date,driver_code,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT *,@a:=@a+1 slno FROM billty, (SELECT @a:= 0) AS a WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' AND driver_code = '${driver_code}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
            // console.log(rows)
            if(rows.length > 0){
            var billtyData = [];
            for(var i = 0;i<rows.length;i++){
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var route_details = {};
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    route_details = routeDetails
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    }
                    var item = {}
                    item.slno = rows[i].slno
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,
                // item.route_details = route_details
                    item.route_code = rows[i].route_commission_code

                    item.route_from = routeDetails.mine_name
                    item.route_to = routeDetails.party_name
                    item.vehicle_code = rows[i].vehicle_code
                    // item.vehicle_label = vehicle_label
                    // item.vehicle_no = vehicle_no
                    // item.driver_code = rows[i].driver_code
                    // item.driver_name = driver_name
                    // item.driver_mobile = driver_mobile
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    // item.freight_rate = rows[i].freight_rate
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    // item.created = rows[i].created
                    // item.selected = false
                    billtyData.push(item)
                }
            if(billtyData.length > 0){
            // connection.destroy();
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }else{
            resolve([])
            }
            }catch(e){
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
      }



function checkDriverWasPresentInThatDate(connection, attendance_date,driver_code){
    return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM attendance INNER JOIN drivers ON drivers.code = attendance.driver_code WHERE attendance.status != 2 AND attendance.attendance_date  = '${attendance_date}' AND attendance.driver_code = '${driver_code}' `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    resolve({"label":"No Trip","value":2})
    }else{
    resolve({"label":"Absent","value":0})
    }
    }
    })
    }catch(e){
    console.log(e)
    resolve({"label":"False data","value":0})
    }
   })
}

function fechFuelEntryChallanOfVehicle(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT *,@a:=@a+1 slno FROM fuel_entry, (SELECT @a:= 0) AS a WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' ORDER BY bill_date ASC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        var fuelEntryData = [];
        for(var i = 0;i<rows.length;i++){
        var vehicle_label = '';
        var party_name = "";
        vehicle_no = ''
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        var item = {}
        item.slno = rows[i].slno
        item.code = rows[i].code
        item.challan_no = rows[i].challan_no
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.bill_date = rows[i].bill_date,
    
        // item.vehicle_code = rows[i].vehicle_code
        // item.vehicle_label = vehicle_label
        // item.vehicle_no = vehicle_no
        item.fuel_rate = rows[i].fuel_rate
        item.quantity = rows[i].quantity
        item.remarks = rows[i].remarks,
        item.amount = rows[i].amount
        // item.image_url_1 = rows[i].image_url_1,
        // item.image_url_2 = rows[i].	image_url_2,
        // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
        // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
        // item.created = rows[i].created
        fuelEntryData.push(item)
        }
        if(fuelEntryData.length > 0){
    
        resolve(fuelEntryData)
        }else{
        resolve([])    
        }
        }else{
        resolve([])
        }
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve([])    
        } 
        }
        })
        }catch(e){
        console.log(e)
        resolve([])
       }
     })
    }







function fetchAutoPartChallanOfVehicle(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
            connection.query(`SELECT *,@a:=@a+1 slno FROM auto_parts, (SELECT @a:= 0) AS a WHERE status != 2 AND vehicle_code = '${vehicle_code}' AND bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY bill_date DESC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rows.length > 0){
            var autoPartsData = [];
            for(var i =0;i<rows.length;i++){
            var item = {};
            var party_name = ''
            var vehicle_label = ''
            var  vehicle_no = ''
            var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
            if(party_details != null){
            party_name = party_details.name
            }
            var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
            if(vehicle_details != null){
            vehicle_label = vehicle_details.label
            vehicle_no = vehicle_details.vehicle_no
            }
            item.slno = rows[i].slno
            item.code = rows[i].code,
            item.challan_no = rows[i].challan_no,
            item.party_code = rows[i].party_code,
            item.party_name = party_name,
            // item.vehicle_code = rows[i].vehicle_code,
            // item.vehicle_label = vehicle_label,
            // item.vehicle_no = vehicle_no,
            item.amount = rows[i].amount,
            item.bill_date = rows[i].bill_date,
            item.remarks = rows[i].remarks,
            // item.created = rows[i].created
            autoPartsData.push(item);
            }
            if(autoPartsData.length > 0){
            resolve(autoPartsData)
            }else{
            resolve([])  
            }
            }else{
            resolve([])
            }
            }
           })
          })
         }

module.exports = driverReportModel;
