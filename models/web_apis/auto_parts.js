var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');

var autoPartsModel = {
  addNewAutoPartChallan: addNewAutoPartChallan,
  fetchAllAutoPartChallan:fetchAllAutoPartChallan,
  updateAutoPartChallan:updateAutoPartChallan,
  deleteSpecificAutoPartChallanDetails:deleteSpecificAutoPartChallanDetails,
  deleteMultipleAutoPartsChallanDetails:deleteMultipleAutoPartsChallanDetails,
  fetchAllNecessaryDataForAddingAndUpdatingAutoPart:fetchAllNecessaryDataForAddingAndUpdatingAutoPart,
  undoDeleteMultipleAutoPartsChallanDetails:undoDeleteMultipleAutoPartsChallanDetails
};


function addNewAutoPartChallan(bodyEncripted){
    return new Promise(async function(resolve, reject){
     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`INSERT INTO auto_parts SET challan_no='${body.challan_no}',party_code = '${body.party_code}',vehicle_code='${body.vehicle_code}',amount = '${body.amount}',bill_date = '${body.bill_date}',	remarks='${body.remarks}',created_by='${body.database_code}'`,async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){
      let autoPartsData = await commonFunctionWeb.fetchSpecificAutoPartChallanDetails(connection,rows.insertId);
      // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>autoPartsData",autoPartsData)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(autoPartsData);
     // let addDataInLedger = await commonFunctionWeb.insertDataInLedger(connection,body.party_code,"Some remarks")
      connection.destroy();
      resolve({
      "success":true,
      "message":"Auto part challan added",
      "data":encriptedData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Auto part challan not added"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
  })
 }



function fetchAllAutoPartChallan(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM auto_parts WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      var autoPartsData = [];
      for(var i =0;i<rows.length;i++){
        var item = {};
        var party_name = ''
        var vehicle_label = ''
        var  vehicle_no = ''
        var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        item.code = rows[i].code,
        item.challan_no = rows[i].challan_no,
        item.party_code = rows[i].party_code,
        item.party_name = party_name,
        item.vehicle_code = rows[i].vehicle_code,
        item.vehicle_label = vehicle_label,
        item.vehicle_no = vehicle_no,
        item.amount = rows[i].amount,
        item.bill_date = rows[i].bill_date,
        item.remarks = rows[i].remarks,
        item.created = rows[i].created
        item.selected = false
        autoPartsData.push(item);
      }
      if(autoPartsData.length > 0){
      connection.destroy();

      let compressData = await commonFunctionWeb.compressDataFunction(autoPartsData)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":true,
      "message":"Auto part data fetched successfully",
      "data":encriptedData,
     // "data":autoPartsData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })  
      }
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
    }
  })
  }





function updateAutoPartChallan(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE auto_parts SET challan_no='${body.challan_no}',party_code = '${body.party_code}',vehicle_code='${body.vehicle_code}',amount = '${body.amount}',bill_date = '${body.bill_date}',	remarks='${body.remarks}',created_by='${body.database_code}' WHERE code ='${body.auto_part_challan_code}'`,async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    let autoPartsData = await commonFunctionWeb.fetchSpecificAutoPartChallanDetails(connection,body.auto_part_challan_code);
    console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>autoPartsData",autoPartsData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(autoPartsData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Auto part challan data updated successfully",
    "data":encriptedData
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Auto part challan data not updated"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })
}


function deleteSpecificAutoPartChallanDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE auto_parts SET status = 2 WHERE code = '${body.code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Auto part challan data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Auto part challan data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
    }
  })
  }




  function deleteMultipleAutoPartsChallanDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
      var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE auto_parts SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows  > 0){
      connection.destroy()
      resolve({
      "success":true,
      "message":"Auto part challan data deleted successfully"
      })
      }else{
      connection.destroy()      
      resolve({
      "success":false,
      "message":"Auto part challan data not deleted"
      })
      }
      }
      })
      }catch(e){
      connection.destroy()
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
      reject({"success": false, "message": "unknown db"});    
     }
    })
   }
  
  

   function fetchAllNecessaryDataForAddingAndUpdatingAutoPart(req,res){
    return new Promise(async function(resolve, reject){
      var database_code = req.params.code
      //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
      if(connection != false){
      try{
      let party_list = await fetchAllPartyList(connection);
      let vehicle_list = await fetchAllVehicle(connection);
      // if(party_list.length != 0 || vehicle_list.length != 0){
      var item = {"party_list":party_list,"vehicle_list":vehicle_list}
      let encriptedData = await commonFunctionWeb.encriptDataFunction(item);
      connection.destroy();
      resolve({
      "success":true,
      "message":"Data fetched sucessfully",
      "data": item,
      "data":encriptedData,
      //"data":item
      })
      // }else{
      // connection.destroy();
      // resolve({
      // "success":false,
      // "message":"No data found"
      // })
      // }
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      } 
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
  }
  
  function fetchAllPartyList(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name FROM parties WHERE status != 2 AND party_type_code = 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        //connection.release();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
       // connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }


    
function fetchAllVehicle(connection){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,label AS name FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    resolve(error);
    }else{ 
    //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
    if(rows.length > 0){
    //connection.destroy();
    resolve(rows)
    }else{
    //connection.destroy();
    resolve([])
    }
    }
    })
    }catch(e){
    // connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
  })
}


function undoDeleteMultipleAutoPartsChallanDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE auto_parts SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Auto part challan data undo successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Auto part challan data not undo"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
   }
  })
}

  


module.exports = autoPartsModel;
