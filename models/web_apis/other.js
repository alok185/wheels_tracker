var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");


var otherModel = {
    globalSearch:globalSearch,
    addUsersQuery:addUsersQuery,
    fetchAllQueriesOfUsers:fetchAllQueriesOfUsers,
    saveSubcriptionId:saveSubcriptionId
}


function globalSearch(bodyEncripted){
  return new Promise(async(resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
      let billty = await searchBillty(connection,body.keyword);
      let vehicle = await searchVehicle(connection,body.keyword);
      let driver = await searchDriverData(connection,body.keyword);
      let autoPartChallan = await searchAutoPart(connection,body.keyword);
      let fuelEntry = await searchFuelEntry(connection,body.keyword);
      let driver_payments = await searchDriverPayments(connection,body.keyword);
      let party_payments = await searchPartyPayments(connection,body.keyword);
      let item = {};
      item.billty_list = billty;
      item.fuel_entry_list = fuelEntry;
      item.vehicle_list = vehicle;
      item.driver_list = driver;
      item.auto_part_challan_list = autoPartChallan;
      item.driver_payments = driver_payments;
      item.party_payments =  party_payments;
      let encriptedData = await commonFunctionWeb.encriptDataFunction(item);

      connection.destroy();
      resolve({
      "success":true,
      "message":"Data matched",
      "data":encriptedData,
      // "data":item
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
      reject({"success": false, "message": "unknown db"});
      }
    })
  }


     

 function searchBillty(connection,keyword){
    return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties On parties.code = billty.party_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code INNER JOIN drivers ON drivers.code = billty.driver_code WHERE billty.status != 2 AND (billty.billty_no LIKE '${keyword}%' OR 	billty.challan_weight LIKE '%${keyword}%' OR 	billty.received_weight LIKE '%${keyword}%' OR 	billty.short_weight  LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' OR billty.advance LIKE '%${keyword}%')  ORDER BY billty.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var billtyData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let billty = await fetchSpecificBilltyDetailsForGlobalSearch(connection,rows[i].code)
        if(billty != null){
        billtyData.push(billty)
        }
        }
        if(billtyData.length > 0){
        resolve(billtyData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }
 
     

 function searchFuelEntry(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT fuel_entry.code FROM fuel_entry INNER JOIN parties ON parties.code = fuel_entry.party_code INNER JOIN vehicles ON vehicles.code = fuel_entry.vehicle_code WHERE fuel_entry.status != 2 AND (fuel_entry.amount  LIKE '%${keyword}%' OR fuel_entry.quantity LIKE '%${keyword}%' OR fuel_entry.challan_no LIKE '%${keyword}%' OR fuel_entry.fuel_rate LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' )  ORDER BY fuel_entry.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var fuelEntyData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let fuelEntry = await fetchSpecificfuelEntryDetailsForGlobalSearch(connection,rows[i].code)
        if(fuelEntry != null){
        fuelEntyData.push(fuelEntry)
        }
        }
        if(fuelEntyData.length > 0){
        resolve(fuelEntyData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }




function searchVehicle(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT vehicles.code FROM vehicles INNER JOIN drivers ON drivers.code = vehicles.driver_code  WHERE vehicles.status != 2 AND (vehicles.label LIKE '${keyword}%' OR vehicles.vehicle_no LIKE '${keyword}%' OR vehicles.owner_name LIKE '${keyword}%' OR drivers.name LIKE '${keyword}%' OR drivers.mobile LIKE '${keyword}%' )  ORDER BY vehicles.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var vehicleData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let vehicle = await fetchSpecificVehicleDetailsForGlobalSearch(connection,rows[i].code)
        if(vehicle != null){
        vehicleData.push(vehicle)
        }
        }
        if(vehicleData.length > 0){
        resolve(vehicleData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }
    

function searchDriverData(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT drivers.code FROM drivers WHERE status != 2 AND (drivers.name LIKE '${keyword}%' OR drivers.mobile LIKE '${keyword}%' OR drivers.address LIKE '${keyword}%' OR drivers.adhaar_no LIKE '${keyword}%' OR drivers.license_no LIKE '${keyword}%')  ORDER BY drivers.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var deriverData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let driver = await fetchSpecificDriverDetailsForGlobalSearch(connection,rows[i].code)
        if(driver != null){
        deriverData.push(driver)
        }
        }
        if(deriverData.length > 0){
        resolve(deriverData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }


function searchAutoPart(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT auto_parts.code FROM auto_parts INNER JOIN parties ON parties.code = auto_parts.party_code INNER JOIN vehicles ON vehicles.code = auto_parts.vehicle_code  WHERE auto_parts.status != 2 AND (auto_parts.challan_no LIKE '%${keyword}%' OR auto_parts.remarks LIKE '%${keyword}%' OR auto_parts.amount LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' )  ORDER BY auto_parts.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var autoPartChallanData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let aurto_parts = await fetchSpecificAutoPartChallanDetailsForGlobalSearch(connection,rows[i].code)
        if(aurto_parts != null){
        autoPartChallanData.push(aurto_parts)
        }
        }
        if(autoPartChallanData.length > 0){
        resolve(autoPartChallanData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }


function searchPartyPayments(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT party_payments.payment_code FROM party_payments INNER JOIN payments ON party_payments.payment_code = payments.code INNER JOIN parties ON parties.code = party_payments.party_code  WHERE party_payments.status != 2 AND (payments.bill_no LIKE '${keyword}%' OR payments.amount LIKE '%${keyword}%' OR payments.purpose LIKE '%${keyword}%' OR payments.amount LIKE '%${keyword}%' OR payments.payment_type LIKE '%${keyword}%' OR payments.ref_no LIKE '%${keyword}%' OR payments.remarks LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%')  ORDER BY party_payments.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var driverPaymentData = [];
        //console.log()
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let party_payments = await fetchSpecificPartyPaymentDetailsForGlobalSearch(connection,rows[i].payment_code)
        if(party_payments != null){
        driverPaymentData.push(party_payments)
        }
        }
        if(driverPaymentData.length > 0){
        resolve(driverPaymentData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }



function searchDriverPayments(connection,keyword){
  return new Promise(async(resolve, reject) => {  
    connection.query(`SELECT DISTINCT driver_payments.payment_code FROM driver_payments INNER JOIN drivers ON drivers.code = driver_payments.driver_code INNER JOIN payments ON driver_payments.payment_code = payments.code WHERE driver_payments.status != 2 AND (payments.bill_no LIKE '%${keyword}%' OR payments.amount LIKE '%${keyword}%' OR payments.purpose LIKE '%${keyword}%' OR payments.payment_type LIKE '%${keyword}%' OR payments.ref_no LIKE '%${keyword}%' OR payments.remarks LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' )   ORDER BY driver_payments.created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        var driverPaymentData = [];
        if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
        let driver_payments = await fetchSpecificDriverPaymentDetailsForGlobalSearch(connection,rows[i].payment_code)
        if(driver_payments != null){
        driverPaymentData.push(driver_payments)
        }
        }
        if(driverPaymentData.length > 0){
        resolve(driverPaymentData)
        }else{
        resolve([])
        }
        }else{
        resolve([])
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }
      })
     })
    }





function addUsersQuery(bodyEncripted){
  return new Promise(async(resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      connection.query(`INSERT INTO users_queries SET user_code = '${body.user_code}',query = '${body.query}' `,async(error,rowsUserData, fields)=>{
        if (error) {
        connection.destroy()
        throw error;
        }else{
        if(rowsUserData.affectedRows > 0){
        let fetchAdminEmail = await fetchContactUsDetails()
        if(fetchAdminEmail != null){
          let sendQueryToAdminMail = await commonFunctionWeb.sendUserQueryToAdminInEmail(fetchAdminEmail.email,body.query)
        }
        resolve({
        "success":true,
        "message":"Query submitted successfully"
        })
        }else{
        resolve({
        "success":false,
        "message":"Query not submitted"
        })
        }
        }
       })
       }else{
       resolve({"success": false, "message": "unknown db"});
      }
    })
  }




function fetchContactUsDetails(){
  return new Promise(async(resolve, reject) => {
    //var database_code = req.params.code
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM contact_us ORDER BY created DESC`,async (error,rows, fields)=>{
          if (error) {
          //commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
              if(rows.length > 0){
              //let encriptedData = await commonFunctionWeb.encriptDataFunction(rows[0])
              //let customer_query = await fetchAllCustomerQuery(body)
              //item = {"contact_us_details":rows[0]}
              // let encriptedData = await commonFunctionWeb.encriptDataFunction(rows[0]);
              // connection.destroy()
              resolve(rows[0])                    
              }else{
              connection.destroy()
              resolve({
              "success":false,
              "message":"No data found"
              })
              }
              }
             })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
           }
         })
        }

function fetchAllQueriesOfUsers(body){
  return new Promise(async(resolve, reject) => {
    //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      connection.query(`SELECT * FROM users_queries ORDER BY created DESC `,async(error,rowsQueries, fields)=>{
        if(error) {
        connection.destroy()
        throw error;
        }else{

        if(rowsQueries.length > 0){
        var queryData = []
        for(var i =0;i<rowsQueries.length;i++){
        var item = {};
        var user_name = '';
        var user_mobile = ''
        let user_details = await commonFunctionWeb.fetchSpecificUserDetails(connection,rowsQueries[i].user_code)
        if(user_details != null){ 
        user_name = user_details.name
        user_mobile = user_details.mobile
        }
        item.code = rowsQueries[i].code
        item.user_code = rowsQueries[i].code
        item.name = user_name
        item.mobile = user_mobile
        item.query = rowsQueries[i].query
        item.created = rowsQueries[i].created
        queryData.push(item)
        }
        if(queryData.length > 0){
        let encriptedData = await commonFunctionWeb.encriptDataFunction(queryData);
        resolve({
        "success":true,
        "message":"Query data fetched successfully",
        //"data":queryData,
        "data":encriptedData
        });
        }else{
        resolve({
        "success":false,
        "message":"No query found"
        })
        }
        }else{
        resolve({
        "success":false,
        "message":"No query found"
        })
        }
        }
       })
       }else{
       resolve({"success": false, "message": "unknown db"});
      }
    })
  }









  function fetchSpecificBilltyDetailsForGlobalSearch(connection,billty_code){
    return new Promise(async function(resolve, reject){
    try{
      connection.query(`SELECT * FROM billty  WHERE code = '${billty_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      var billtyData = [];
      var vehicle_label = '';
      var party_name = "";
      var vehicle_no = '';
      var driver_name = '';
      var mine_name = '';
      var route_details = {};
      let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
      let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[0].route_commission_code)
      let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[0].driver_code)
  
      if(routeDetails != null){
      route_details = routeDetails
      }
      if(vehicle_details != null){
      vehicle_label = vehicle_details.label
      vehicle_no = vehicle_details.vehicle_no
      }
      if(party_details != null){
      party_name = party_details.name
      }
      if(driverDetails != null){
        driver_name = driverDetails.name
        // driver_mobile = driverDetails.mobile
      }
      var item = {}
      item.code = rows[0].code
      item.route_code = rows[0].route_commission_code
      item.billty_no = rows[0].billty_no
      item.party_code = rows[0].party_code
      item.party_name = party_name
      item.bill_date = rows[0].bill_date,
      item.route_details = route_details
      item.vehicle_code = rows[0].vehicle_code
      item.vehicle_label = vehicle_label
      item.vehicle_no = vehicle_no
      item.driver_name = driver_name
      item.advance = rows[0].advance
      item.remarks = rows[0].remarks,
      item.freight_rate = rows[0].freight_rate
      item.challan_weight = rows[0].challan_weight
      item.diesel = rows[0].diesel,
      item.received_weight = rows[0].received_weight,
      item.short_weight = rows[0].short_weight,
      item.image_url_1 = rows[0].image_url_1,
      item.image_url_2 = rows[0].	image_url_2,
      item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
      item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
      item.created = rows[0].created
      item.router_link = "billty/bDetails/",
      item.label = "Billty"
      resolve(item)
      }else{
      resolve(null)
      }
      }catch(e){
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })    
      } 
      }
      })
      }catch(e){
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    })
  }
  
  


  function fetchSpecificVehicleDetailsForGlobalSearch(connection,vehicle_code){
    return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT * FROM vehicles WHERE code = '${vehicle_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      //connection.release();
      resolve(error);
      }else{ 
      if(rows.length > 0){
      //connection.destroy();
      var item = {}
      var driver_name = "";
      var driver_mobile = "";
      let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[0].driver_code);
      if(driver_details != null){
      driver_name = driver_details.name
      driver_mobile = driver_details.mobile
      }
      item.code = rows[0].code
      item.label = rows[0].label,
      item.vehicle_no = rows[0].vehicle_no,
      item.driver_code = rows[0].driver_code,
      item.driver_name = driver_name,
      item.driver_mobile = driver_mobile,
      item.owner_name = rows[0].owner_name
      item.router_link = "vehicle/vDetails/",
      item.label = "Vehicle"
      resolve(item)
      }else{
      //connection.destroy();
      resolve(null)
      }
      }
      })
      }catch(e){
      //connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }
  


  
function fetchSpecificDriverDetailsForGlobalSearch(connection,driver_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,name,mobile,address,adhaar_no,license_no,salary,profile_image_url,profile_image_url_thumbnail,document_image_url_1,document_image_url_2,document_image_url_thumbnail_1,document_image_url_thumbnail_2, created,"drivers/dDetails/" AS router_link, "Drivers" AS label FROM drivers WHERE  code = '${driver_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    //connection.destroy();
    createErrorLogFile(error);sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    //console.log(`SELECT code,name,mobile,address,adhaar_no,license_no,profile_image_url,profile_image_url_thumbnail,document_image_url,document_image_url_thumbnail,created FROM drivers WHERE status != 2 AND code = '${driver_code}' ORDER BY created DESC `,rows)
    if(rows.length > 0){
    //connection.destroy();item.router_link = "vehicle/vDetails/",

      // item.label = "Vehicle"
   // let encriptedData = await encriptDataFunction(rows);
    resolve(rows[0])
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
  })  
  }

  function fetchSpecificAutoPartChallanDetailsForGlobalSearch(connection,auto_part_challan_code){
    return new Promise(async function(resolve, reject){
      connection.query(`SELECT * FROM auto_parts WHERE code = '${auto_part_challan_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      // connection.destroy();
      resolve(error);
      }else{ 
      if(rows.length > 0){
          var item = {};
          var party_name = ''
          var vehicle_label = ''
          var  vehicle_no = ''
          var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[0].party_code)
          if(party_details != null){
          party_name = party_details.name
          }
          var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
          if(vehicle_details != null){
          vehicle_label = vehicle_details.label
          vehicle_no = vehicle_details.vehicle_no
          }
          item.code = rows[0].code,
          item.challan_no = rows[0].challan_no,
          item.party_code = rows[0].party_code,
          item.party_name = party_name,
          item.vehicle_code = rows[0].vehicle_code,
          item.vehicle_label = vehicle_label,
          item.vehicle_no = vehicle_no,
          item.amount = rows[0].amount,
          item.bill_date = rows[0].bill_date,
          item.remarks = rows[0].remarks,
          item.created = rows[0].created,
          item.router_link = "auto_part/aDetails/",
          item.label = "Auto Parts"
        resolve(item)
        }else{
        resolve(null)  
        }
        }
       })
     })
    }
  






    function fetchSpecificfuelEntryDetailsForGlobalSearch(connection,fuel_entry_code){
      return new Promise(async function(resolve, reject){
      try{
        connection.query(`SELECT * FROM fuel_entry WHERE code = '${fuel_entry_code}'`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        var vehicle_label = '';
        var party_name = "";
        var vehicle_no = '';
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
    
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        var item = {}
        item.code = rows[0].code
        item.challan_no = rows[0].challan_no
        item.party_code = rows[0].party_code
        item.party_name = party_name
        item.bill_date = rows[0].bill_date,
    
        item.vehicle_code = rows[0].vehicle_code
        item.vehicle_label = vehicle_label
        item.vehicle_no = vehicle_no
        item.fuel_rate = rows[0].fuel_rate
        item.quantity = rows[0].quantity
        item.remarks = rows[0].remarks,
        item.amount = rows[0].amount
        item.image_url_1 = rows[0].image_url_1,
        item.image_url_2 = rows[0].	image_url_2,
        item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
        item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
        item.created = rows[0].created,

        item.router_link = "fuel_entry/fDetails/",
        item.label = "Fuel Entry",
        resolve(item)
        }else{
        resolve(null)
        }
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })    
        } 
        }
      })
      }catch(e){
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })    
      }
    })
    }
    
    
    function fetchSpecificDriverPaymentDetailsForGlobalSearch(connection,payment_code){
      return new Promise(async function(resolve, reject){
       connection.query(`SELECT * FROM driver_payments WHERE payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var paymentData = [];
        var driver_name = '';
        let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[0].driver_code)
        if(driver_details != null){
        driver_name = driver_details.name
        }
        let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[0].payment_code)
        if(payment_details != null){
        var item = {}
        item.driver_code = rows[0].driver_code
        item.driver_name = driver_name
        item.payment_code = rows[0].payment_code
        item.bill_no = payment_details.bill_no,
        item.bill_date = payment_details.bill_date,
        item.from_date = payment_details.from_date,
        item.to_date = payment_details.to_date,
        item.amount = payment_details.amount,
        item.purpose = payment_details.purpose,
        item.payment_type = payment_details.payment_type,
        item.ref_no = payment_details.ref_no,
        item.remarks = payment_details.remarks,
        item.image_url_1 = payment_details.image_url_1,
        item.image_url_2 = payment_details.	image_url_2,
        item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail;
        item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;
        
        item.router_link = "driver_payments/dDetails/",
        item.label = "Driver Payments",
        resolve(item)
        }else{
        resolve(null)    
        }
        }else{
        resolve(null)
        }
      }
    })
  })
  }
  

  function fetchSpecificPartyPaymentDetailsForGlobalSearch(connection,payment_code){
    return new Promise(async function(resolve, reject){
     connection.query(`SELECT * FROM party_payments WHERE status != 2 AND payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
      connection.destroy();
      resolve(error);
      }else{ 
      if(rows.length > 0){
      var paymentData = [];
      var party_name = '';
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
      if(party_details != null){
        party_name = party_details.name
      }
      let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[0].payment_code)
      if(payment_details != null){
      var item = {}
      item.party_code = rows[0].party_code
      item.party_name = party_name
      item.payment_code = rows[0].payment_code
      item.bill_no = payment_details.bill_no,
      item.bill_date = payment_details.bill_date,
      item.from_date = payment_details.from_date,
      item.to_date = payment_details.to_date,
      item.amount = payment_details.amount,
      item.purpose = payment_details.purpose,
      item.payment_type = payment_details.payment_type,
      item.ref_no = payment_details.ref_no,
      item.remarks = payment_details.remarks,
      item.image_url_1 = payment_details.image_url_1,
      item.image_url_2 = payment_details.	image_url_2,
      item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail;
      item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;

      item.router_link = "party_payments/pDetails/",
      item.label = "Party Payments",
      resolve(item)
      }else{
      resolve(null)    
      }
      }else{
      resolve(null)
      }
    }
  })
  })
  }
  


function saveSubcriptionId(body){
    return new Promise(async(resolve, reject) => {
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getMasterDatabaseConnection()
      if(connection != false){
       try{
        connection.query(`INSERT INTO web_user_subscription_id set endpoint = '${body.endpoint}',auth_key = '${body.keys.auth}',	p256dh_key = '${body.keys.p256dh}',user_code = '${body.user_code}',client_system_id= '${body.client_system_id}' `,async(error,rows, fields)=>{
        if (error) {
        connection.destroy();
        throw error;
        }else{
        if(rows.affectedRows > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Subscription id saved successfully",
        })
        }else{
        connection.destroy()
        resolve({
        "success":false,
        "message":"Subscription id not saved",
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
       }
      })
     }
  



module.exports = otherModel;
