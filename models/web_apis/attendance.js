var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');

var attendanceModel = {
  addNewAttendance: addNewAttendance,
  fetchAttendanceWise:fetchAttendanceWise,
  updateAttendance:updateAttendance,
  fetchSpecificAttendanceDetails:fetchSpecificAttendanceDetails,
  deleteMultipleAttendanceDetails:deleteMultipleAttendanceDetails,
  undoDeleteMultipleAttendanceDetails:undoDeleteMultipleAttendanceDetails
};


function addNewAttendance(bodyEncripted){
    return new Promise(async function(resolve, reject){
     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      var falseData = [];
      for(var i =0;i<body.attendance_data.length;i++){
      let addAttendeceData = await addAttendanceDataFunction(connection,body.attendance_data[i].code,body.attendance_date,body.attendance_data[i].attendance_value,body.database_code)
      if(addAttendeceData == false){
      falseData.push(i)
      }
      }
      if(falseData.length > 0){      
      connection.destroy()
      resolve({
      "success":false,
      "message":"Attendance not added",
      })
      }else{
      let attendanceDetails = await fetchSpecificAttendanceDetailsAfterAddAndUpdate(connection,body.attendance_date)
      let compressData = await commonFunctionWeb.compressDataFunction(attendanceDetails)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",attendanceDetails)
      connection.destroy();
      resolve({
      "success":true,
      "message":"Attendance added successfully",
      // "data":attendanceDetails,
      "data":encriptedData
      })
      }
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
  })
 }





function addAttendanceDataFunction(connection,driver_code,attendance_date,attendance_value,created_by){
  return new Promise(async function(resolve, reject){ 
      try{   
        connection.query(`INSERT INTO attendance SET driver_code='${driver_code}',attendance_date = '${attendance_date}',	attendance_value='${attendance_value}',created_by = '${created_by}'`, (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        resolve(true)
        }else{
        resolve(false)
       }
      }
      })
      }catch(e){
      console.log(e)
      resolve(false)
      }
    })
   }



   


function fetchAttendanceWise(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT Distinct attendance_date FROM attendance WHERE status != 2 AND  attendance_date  BETWEEN '${body.from_date}' AND '${body.to_date}'  ORDER BY attendance_date ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      var attendanceData = [];
      for(var i =0;i<rows.length;i++){
      var item = {};
      item.code = rows[i].code;
      item.attendance_date = rows[i].attendance_date,
      item.present = await fetchAllPresentDriverCount(connection,rows[i].attendance_date)
      attendanceData.push(item)
      }
      if(attendanceData.length > 0){
      connection.destroy();
      let compressData = await commonFunctionWeb.compressDataFunction(attendanceData)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":true,
      "message":"Attendance Data fetched successfully",
      "data":encriptedData,
      // "data":attendanceData,
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })  
      }
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
    }
  })
  }



function fetchAllPresentDriverCount(connection,attendance_date){
  return new Promise(async function(resolve, reject){
    connection.query(`SELECT COUNT(DISTINCT driver_code) as present_driver_count FROM attendance WHERE attendance_date = '${attendance_date}' and attendance_value = 1`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    if(rows[0].present_driver_count != null){
    resolve(rows[0].present_driver_count)
    }else{
    resolve(0)
    }
    }else{
    resolve(0)
    }
    }
   })
  })
}





function updateAttendance(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
     if(connection != false){
     try{
     var falseData = [];
     let deletePreviousAttendanceData = await deletePreviousAttendanceDataFunction(connection, body.attendance_date)
     for(var i =0;i<body.attendance_data.length;i++){
     let addAttendeceData = await addAttendanceDataFunction(connection,body.attendance_data[i].code,body.attendance_date,body.attendance_data[i].attendance_value,body.database_code)
     if(addAttendeceData == false){
     falseData.push(i)
     }
     }
     if(falseData.length > 0){      
     connection.destroy()
     resolve({
     "success":false,
     "message":"Attendance not updated",
     })
     }else{
     let attendanceDetails = await fetchSpecificAttendanceDetailsAfterAddAndUpdate(connection,body.attendance_date)
     let compressData = await commonFunctionWeb.compressDataFunction(attendanceDetails)
     let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
     connection.destroy();
     resolve({
     "success":true,
     "message":"Attendance updated successfully",
    //  "data":attendanceDetails,
     "data":encriptedData
     })
     }
     }catch(e){
     connection.destroy();
     console.log(e)
     resolve({
     "success":false,
     "message":"Something went wrong"
     })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});
   }
 })
}



function deletePreviousAttendanceDataFunction(connection, attendance_date){
  return new Promise(async function(resolve, reject){
  connection.query(`DELETE FROM attendance WHERE attendance_date  = '${attendance_date}'`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    resolve(true)
    }
    resolve(false)
   }
  })
 })
}
  


function fetchSpecificAttendanceDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)    
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM attendance INNER JOIN drivers ON drivers.code = attendance.driver_code WHERE attendance.status != 2 AND attendance.attendance_date  = '${body.attendance_date}' `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      // let attendanceData = await fetchAllDriverList(connection,rows)
      // if(attendanceData.length > 0){
        // console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR",rows.length)
      var attendanceData = [];
      for(var i =0;i<rows.length;i++){
      var item = {};
      var driver_mobile = "";
      var driver_name = "";
      let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code);
      if(driver_details != null){
      driver_name = driver_details.name
      driver_mobile = driver_details.mobile
      }
      item.driver_code = rows[i].driver_code
      item.driver_name = driver_name,
      item.driver_mobile = driver_mobile
      item.attendance_value = rows[i].attendance_value
      attendanceData.push(item)
      }
      var item1 = {"attendance_date":body.attendance_date,"attendance_data":attendanceData}

      let compressData = await commonFunctionWeb.compressDataFunction(item1);
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      
      connection.destroy();
      resolve({
      "success":true,
      "message":"Attendance Data fetched successfully",
      "data":encriptedData,
      // "data":item1,
      })
      // }else{
      // connection.destroy();
      // resolve({
      // "success":false,
      // "message":"No data found"
      // })  
      // }
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
   }
  })
}
  



function fetchAllDriverList(connection,driverList){
  return new Promise(async function(resolve, reject){  
  connection.query(`SELECT * FROM drivers WHERE status != 2 ORDER BY name ASC  `, async (error, rowsDriver, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{
      var attendanceData = [];
      if(rowsDriver.length > 0){
      for(var i =0;i<rowsDriver.length;i++){
      var item = {};
      var driver_mobile = "";
      var driver_name = "";
      let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rowsDriver[i].code);
      if(driver_details != null){
      driver_name = driver_details.name
      driver_mobile = driver_details.mobile
      }
      item.driver_code = rowsDriver[i].code
      item.driver_name = driver_name;
      item.driver_mobile = driver_mobile;
      item.isPresent = await checkDriverPresentOrNot(rowsDriver[i].code,driverList)
      attendanceData.push(item)
      }
      resolve(attendanceData)
      }else{
      resolve(rowsDriver)
     }
    }
  })
})
}



function checkDriverPresentOrNot(driver_code,driverList){
  for(var i= 0;i<driverList.length;i++){
    // console.log(driverList[i].driver_code,"driver_code ARRAy <<<<<<<<<<<<<<<<<<< == >>>>>>>>>>>>>>>>>>driver_code", driver_code)
    if(driver_code == driverList[i].driver_code){
      return true;
     }
   }
  return false;
}



function fetchSpecificAttendanceDetailsAfterAddAndUpdate(connection,attendance_date){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM attendance INNER JOIN drivers ON drivers.code = attendance.driver_code WHERE attendance.status != 2 AND attendance.attendance_date  = '${attendance_date}'  ORDER BY drivers.name ASC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      // let attendanceData = await fetchAllDriverList(connection,rows)
      // if(attendanceData.length > 0){
        // console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR",rows.length)
      var attendanceData = [];
      for(var i =0;i<rows.length;i++){
        var item = {};
        var driver_mobile = "";
        var driver_name = "";
        let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code);
        if(driver_details != null){
        driver_name = driver_details.name
        driver_mobile = driver_details.mobile
        }
        item.driver_code = rows[i].driver_code
        item.driver_name = driver_name,
        item.driver_mobile = driver_mobile
        item.attendance_value = rows[i].attendance_value
        attendanceData.push(item)
      }
      var item1 = {"attendance_date":attendance_date,"attendance_data":attendanceData}
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(item1);
      resolve(item1)
      // }else{
      // connection.destroy();
      // resolve({
      // "success":false,
      // "message":"No data found"
      // })  
      // }
      }else{
      resolve(null)
      }
      }
      })
      }catch(e){
      console.log(e)
      resolve(null)
    }
    })
    }
  







    function deleteMultipleAttendanceDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE attendance SET status = 2 WHERE attendance_date IN (${await replaceArrayDataToString(body.attendance_date)}) `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        console.log(`UPDATE attendance SET status = 2 WHERE attendance_date IN (${await replaceArrayDataToString(body.attendance_date)}) `,rows)
        if(rows.affectedRows  > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Attendance data deleted successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Attendance data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
     })
    }
      
      
    
    function undoDeleteMultipleAttendanceDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE attendance SET status = 0 WHERE attendance_date IN (${await replaceArrayDataToString(body.attendance_date)}) `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        console.log(`UPDATE attendance SET status = 0 WHERE attendance_date IN (${await replaceArrayDataToString(body.attendance_date)}) `,rows)
        if(rows.affectedRows  > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Attendance data undo successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Attendance data not undo"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
     })
    }
      

    
    
function replaceArrayDataToString(myArray){
  return new Promise(async(resolve, reject) => {
    console.log("myARRAY_________------->>",myArray)
    var finalData = "";
    for(var i =0;i<myArray.length;i++){
    if(i == 0){
    finalData +=  "'"+myArray[i]+"'"
    }else{
    finalData += ","+"'"+myArray[i]+"'"
    }
    }
    resolve(finalData)
  })
}




module.exports = attendanceModel;
