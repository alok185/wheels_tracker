var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');

var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var driverModel = {
  addNewDriver: addNewDriver,
  fetchAllDrivers:fetchAllDrivers,
  updateDriverDetails:updateDriverDetails,
  deleteSpecificDriverDetails:deleteSpecificDriverDetails,
  deleteMultipleDriverDetails:deleteMultipleDriverDetails,
  fetchSpecificDriverDetails:fetchSpecificDriverDetails,
  undoDeleteMultipleDriverDetails:undoDeleteMultipleDriverDetails,
  fetchDriversData:fetchDriversData 
};



const storage = multer.diskStorage({
    destination: "./public/uploads/drivers",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()




function addNewDriver(req,res){
    return new Promise(function(resolve, reject) {
        upload(req, res, async (err) => {        
        if (err) {
        commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
        console.log(err);
        }else{
            var query = '';
            var data  = await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.driver_data));
            //var data = JSON.parse(req.body.driver_data);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
           /// console.log("BBBBODDYY",req.body)
            var imageLocation ='';
            var thumbImageLocation = '';
            var documentImageLocation1 = '';
            var documentThumbImageLocation1 = '';
            var documentImageLocation2 = '';
            var documentThumbImageLocation2 = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'profile_image_url'){
            imageLocation = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImageLocation = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'document_image_url_1') {
            documentImageLocation1 = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            documentThumbImageLocation1 = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            if(req.files[i].fieldname == 'document_image_url_2') {
            documentImageLocation2 = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            documentThumbImageLocation2 = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            connection.query(`INSERT INTO drivers SET created_by = '${data.database_code}',name='${data.name}',profile_image_url = '${imageLocation}',profile_image_url_thumbnail ='${thumbImageLocation}',document_image_url_1 = '${documentImageLocation1}',document_image_url_thumbnail_1 = '${documentThumbImageLocation1}',document_image_url_2 = '${documentImageLocation2}',document_image_url_thumbnail_2 = '${documentThumbImageLocation2}',mobile='${data.mobile}',salary='${data.salary}',address='${data.address}', adhaar_no = '${data.adhaar_no}',license_no = '${data.license_no}'`,async (error,rowsDriver, fields)=>{
            if (error) {
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
            if(rowsDriver.affectedRows > 0){
            let driverData = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rowsDriver.insertId);
            // console.log("DRIIIIIIIIIIIIIIIIVERRRDATA========??",driverData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(driverData);

            connection.destroy()
            resolve({
            "success":true,
            "message":"Driver added",
            "data":encriptedData,
            //"data":driverData
            })
            }else{
            connection.destroy()
            resolve({
            "success":false,
            "message":"Driver not added"
            })
           }
         }
      })
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
   }
  })
 })
}




function fetchAllDrivers(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,name,mobile,address,adhaar_no,license_no,profile_image_url,profile_image_url_thumbnail,document_image_url_1,document_image_url_2,document_image_url_thumbnail_1,document_image_url_thumbnail_2,created,salary, "false" AS "selected" FROM drivers WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.length > 0){
        
        let compressData = await commonFunctionWeb.compressDataFunction(rows)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
        connection.destroy();
        resolve({
        "success":true,
        "message":"Drivers data fetched successfully",
        "data":encriptedData,
        //"data":rows,
        })
        }else{
          let compressData = await commonFunctionWeb.compressDataFunction([])
          let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
          connection.destroy();
          resolve({
          "success":true,
          "message":"Drivers data fetched successfully",
          "data":encriptedData,
          //"data":rows,
          })
        }
        }
        })
        }catch(e){
        console.log(e)
        let compressData = await commonFunctionWeb.compressDataFunction([])
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)

        connection.destroy();

        resolve({
        "success":true,
        "message":"Drivers data fetched successfully",
        "data":encriptedData,
        //"data":rows,
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
      }
    })  
   }



function updateDriverDetails(req,res){
    return new Promise(function(resolve, reject) {
        upload(req, res, async (err) => {        
        if (err) {
        commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
        console.log(err);
        }else{
            var query = '';
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.driver_data));
            //var data  = JSON.parse(req.body.driver_data);
            
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
           /// console.log("BBBBODDYY",req.body)
            var imageLocation = data.profile_image_url;
            var thumbImageLocation = data.profile_image_url_thumbnail;
            var documentImageLocation1 = data.document_image_url_1;
            var documentThumbImageLocation1 =data.document_image_url_thumbnail_1;
            var documentImageLocation2 = data.document_image_url_2;
            var documentThumbImageLocation2 =data.document_image_url_thumbnail_2;
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'profile_image_url'){
            imageLocation = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImageLocation = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'document_image_url_1') {
            documentImageLocation1 = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            documentThumbImageLocation1 = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }

            if(req.files[i].fieldname == 'document_image_url_2') {
            documentImageLocation2 = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            documentThumbImageLocation2 = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            connection.query(`UPDATE drivers SET created_by = '${data.database_code}',name='${data.name}',profile_image_url = '${imageLocation}',profile_image_url_thumbnail ='${thumbImageLocation}',document_image_url_1 = '${documentImageLocation1}',document_image_url_thumbnail_1 = '${documentThumbImageLocation1}',document_image_url_2 = '${documentImageLocation2}',document_image_url_thumbnail_2 = '${documentThumbImageLocation2}',mobile='${data.mobile}',salary='${data.salary}',address='${data.address}', adhaar_no = '${data.adhaar_no}',license_no = '${data.license_no}' WHERE code ='${data.driver_code}'`,async (error,rowsDriver, fields)=>{
            if (error) {
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
            if(rowsDriver.affectedRows > 0){
                
            let driverData = await commonFunctionWeb.fetchSpecificDriverDetails(connection,data.driver_code);
            //console.log("DRIIIIIIIIIIIIIIIIVERRRDATA========??",driverData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(driverData);

            connection.destroy()
            resolve({
            "success":true,
            "message":"Driver details updated successfully",
            "data":encriptedData,
            // "data":driverData
            })
            }else{

            connection.destroy()
            resolve({
            "success":false,
            "message":"Driver details not updated"
            })
           }
         }
      })
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
   }
  })
 })
}


function deleteSpecificDriverDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
      var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE drivers SET status = 2 WHERE code = '${body.code}'`, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows  > 0){
      connection.destroy()
      resolve({
      "success":true,
      "message":"Driver data deleted successfully"
      })
      }else{
      connection.destroy()      
      resolve({
      "success":false,
      "message":"Driver data not deleted"
      })
      }
      }
      })
      }catch(e){
      connection.destroy()
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
      reject({"success": false, "message": "unknown db"});    
     }
    })
    }
  
  
  
  
    function deleteMultipleDriverDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE drivers SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Driver data deleted successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Driver data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});    
       }
      })
     }
    


     function undoDeleteMultipleDriverDetails(bodyEncripted){
        return new Promise(async function(resolve, reject){
          var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          connection.query(`UPDATE drivers SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows  > 0){
            var undoData = [];
            for(var i =0;i<body.code.length;i++){
              let undoDataDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection, body.code[i])
              if(undoDataDetails != null){
                undoData.push(undoDataDetails)
              }
            }
          connection.destroy()
         let encriptedData = await commonFunctionWeb.encriptDataFunction(undoData);

          resolve({
          "success":true,
          "message":"Driver data undo successfully",
          // "data":undoData,
          "data":encriptedData
          })
          }else{
          connection.destroy()      
          resolve({
          "success":false,
          "message":"Driver data not undo"
          })
          }
          }
          })
          }catch(e){
          connection.destroy()
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
          }
          }else{
          reject({"success": false, "message": "unknown db"});    
         }
        })
       }
      


    
function fetchSpecificDriverDetails(body){
    return new Promise(async function(resolve, reject){
       // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,name,mobile,address,adhaar_no,license_no,salary,profile_image_url,profile_image_url_thumbnail,document_image_url_1,document_image_url_2,document_image_url_thumbnail_1,document_image_url_thumbnail_2,created FROM drivers WHERE  code = '${body.driver_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name,mobile,address,adhaar_no,license_no,profile_image_url,profile_image_url_thumbnail,document_image_url,document_image_url_thumbnail,created FROM drivers WHERE status != 2 AND code = '${driver_code}' ORDER BY created DESC `,rows)
        if(rows.length > 0){
        connection.destroy();
         let encriptedData = await commonFunctionWeb.encriptDataFunction(rows[0]);
        resolve({
        "success":true,
        "message":"Driver data fetched successfully",
       // "data":rows[0]
       "data":encriptedData,
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});    
       }
      })
     }
  


function fetchDriversData(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    // var index = req.params.index
    // var limit = req.params.limit
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,adhaar_no,license_no,profile_image_url as image_url,profile_image_url_thumbnail as thumb_url,document_image_url_1 as license_image_url,document_image_url_2,document_image_url_thumbnail_1 as license_image_url_thumbnail,document_image_url_thumbnail_2,created,salary, "false" AS "selected" FROM drivers WHERE status != 2 ORDER BY name ASC`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    connection.destroy();
    resolve({
    "success":true,
    "message":"Drivers data fetched successfully",
    // "data":encriptedData,
    "drivers_data":rows,
    })
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Drivers data not fetched",
      })
    }
    }
    })
    }catch(e){
    console.log(e)
    connection.destroy();
    resolve({
    "success":false,
    "message":"Something went wrong",
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })  
}
  

module.exports = driverModel;
