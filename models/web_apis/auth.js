var db = require("../config/database");
var otplib = require('otplib');
var authenticator = require('otplib/authenticator');
const jwt = require('jsonwebtoken');
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';
var commonFunctionWeb = require('../commons/commonFunctionWeb/commonFuncWeb');


otplib.authenticator.options = {
  step: 60,
  window: 1,
};

var authModel = {  

  appInfo:appInfo,
  sendOtpForUser:sendOtpForUser,
  verifyOtpForUser:verifyOtpForUser,
  getEncriptedDataOfJson:getEncriptedDataOfJson,
  getDecriptedDataOfJson:getDecriptedDataOfJson,
  sendOtpForChangeMobileOfUser:sendOtpForChangeMobileOfUser,

}


 


function appInfo() {
  return new Promise(async(resolve, reject) => {
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
    connection.query(`SELECT * FROM app_info`, (error, rows, fields)=>{
    if(error) {
    connection.destroy();
    reject(error);
    } else {
    if(rows.length > 0){
    connection.destroy()
    resolve({
    "success": true,
    "message": "Data fetched successfully",
    "app_info": {"version":rows[0].version_code}
    });
    }else{
    connection.destroy()
    resolve({
    "success": false,
    "message": "No data found",
    });
    }
   }
  });
  }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
  }else{
  resolve({"success": false, "message": "unknown db"});
  }
 });
}

  


function sendOtpFunction(mobile,otp){
  return new Promise((resolve, reject) => {
    console.log("MOBILE------->>>>",mobile,"OTP--------------->>>>",otp)
    // const SendOtp = require('sendotp');
    // const sendOtp = new SendOtp('230364AslqNHOaR5b6932aa');
    // sendOtp.send("91"+mobile, "SRCOTP", otp, function (error, data) {
    // console.log(data);
    // });
    resolve({
    "success":true,
    "message":"otp sent success",
    })
  })
}



function sendOtpForUser(bodyEncripted) {
    return new Promise(async(resolve, reject) => {
      //console.log("ENCRYPTED DATA =============>>>>",bodyEncripted);
      var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);
     // console.log("DECRYPTED DATA =============>>>>===========",body);
        var mobile = body.mobile;
        const otp = authenticator.generate(secret); // 556443
        const token1 = jwt.sign({mobile,otp},'my_secret_key');
        var data  = {"otp":otp,"token":token1}
        sendOtpFunction(mobile,otp);
        let encriptedData = await commonFunctionWeb.encriptDataFunction(data);
        resolve({
        "success":true,
        "message":"otp sent success",
        //"data":data,
        "data":encriptedData
        })
       })
      }
  
  
  



      function verifyOtpForUser(bodyEncripted) {
        return new Promise(async (resolve, reject) => {
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getMasterDatabaseConnection()
        if(connection != false){
        try{
          var mobile = body.mobile;
          var otp = body.otp
          var secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
          const isValid = otplib.authenticator.check(otp, secret);
          var token = body.token;
          var decoded = jwt.decode(token);
          var finalOtp = decoded.otp
        //  console.log("cccccccccccccccccccrrrrrrrrrrrrrrrrrassshh",finalOtp,otp)
          if(isValid == true && otp==`${finalOtp}`){
            connection.query(`SELECT * FROM users WHERE mobile = '${mobile}'  `,async(error,rowsUserData, fields)=>{
                if (error) {
                connection.destroy()
                throw error;
                 }else{
                  try{
                  if(rowsUserData.length > 0){
                  let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,rowsUserData[0].code)
                  fetchUserData.notification_array = [];
                  fetchUserData.unseen_notification_count = 0;
                  connection.destroy();
                  let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
                  resolve({
                  "success":true,
                  "message":"user already exist",
                  "token":token,
                  //"data":fetchUserData,
                   "data":encriptedData,
                  })
                  }else{
                  var add = await addUser(connection,body.mobile);
                  }
                  if(add){
                  let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,add)
                  fetchUserData.notification_array = [];
                  fetchUserData.unseen_notification_count = 0;
                  let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
                  connection.destroy();
                  resolve({
                  "success":true,
                  "message":"User profile created",
                  "token":token,
                  //"data":fetchUserData,
                  "data":encriptedData

                  })
                  }else{
                  connection.destroy();
                  resolve({
                  "success":false,
                  "message":"User profile not created"
                  })
                  }
                  }catch(e){
                   connection.destroy();
                   console.log(e)
                   resolve({
                  "success":false,
                  "message":"Something went wrong"
                   })
                   }
                  }
                })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Wrong otp"
            })
            }
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
           "success":false,
           "message":"Something went wrong"
            })
            }
           }else{
            resolve({"success": false, "message": "unknown db"});
           }
          });
         }





function addUser(connection,mobile){
    return new Promise((resolve, reject) => {
        console.log(`INSERT INTO users SET mobile = '${mobile}'`)
        connection.query(`INSERT INTO users SET mobile = '${mobile}'`,async(error,rowsUser, fields)=>{
            if (error) {
            throw error;
             }else{
                if(rowsUser.affectedRows > 0){
                // var db_name = 'wheels_tracker'+rowsUser.insertId
                // connection.query(`UPDATE users SET db_name  = '${db_name}' , code = '${rowsUser.insertId}'`,async(error,rows, fields)=>{
                // if (error) {
                // throw error;
                // }else{
                // if(rows.affectedRows > 0){
                let createDatabaseForUser  = await createDatabaseForNewUser(connection,rowsUser.insertId)
                resolve(rowsUser.insertId)
                // }else{
                // resolve(false)
                // }
                // }
                // })
                }else{
                resolve(false)
                }
                }
              })
           })    
         }



function createDatabaseForNewUser(connection,user_code){
    return new Promise((resolve, reject) => {
    var db_name = 'wheels_tracker'+user_code
        // connection.query(`UPDATE users SET db_name  = '${db_name}' , code = '${user_code}'`,async(error,rows, fields)=>{
        // if (error) {
        // throw error;
        // }else{
        //if(rows.affectedRows > 0){
        connection.query(`CREATE DATABASE ${db_name}`, async(error0, rowsDatabase, fileds0)=>{
        if (error0) {
        connection.release();
        reject(error0);
        } else {
        if(rowsDatabase.affectedRows > 0){
        let connectionNewDbWithUserCode = await commonFunctionWeb.getSpecificDatabaseConnection(user_code)
        if(connectionNewDbWithUserCode != false){
        try{
        connectionNewDbWithUserCode.query(commonFunctionWeb.createTables, (error1, rowsDatabaseOfUser, fields1)=>{
        if (error1) {
        connectionNewDbWithUserCode.release();
        reject(error1);
        }else{
        if(rowsDatabaseOfUser.affectedRows > 0){
        resolve(true)
        }else{
        resolve(false)
        }
       }
      })
    }catch(e){
    console.log(e)
    resolve(false)
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
    }else{
    resolve(false)
    }
    }
    })
    // }else{
  //  resolve(false)
    // }
   //}
 //})
 })
}

function getEncriptedDataOfJson(body){
  return new Promise(async function(resolve, reject) {
    let encriptData = await commonFunctionWeb.encriptDataFunction(body)
    resolve(encriptData)
  })
}


function getDecriptedDataOfJson(body){
  return new Promise(async function(resolve, reject) {
    let decriptData = await commonFunctionWeb.decriptDataFunction(body.data)
    resolve(decriptData)
  })
}


function sendOtpForChangeMobileOfUser(bodyEncripted){
  return new Promise(async(resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    var mobile = body.mobile;
     const otp = authenticator.generate(secret); // 556443
     const token1 = jwt.sign({mobile,otp},'my_secret_key');
     try{
      connection.query(`SELECT code,NOW() AS created FROM users WHERE mobile = '${mobile}' `,async(error,rows, fields)=>{
       if (error) {
        connection.destroy()
        throw error;
       }else{
        if(rows.length > 0){
        connection.destroy()
        resolve({
        "success":false,
        "message":"Number is already in use",
        })
        }else{

        var data  = {"otp":otp,"token":token1}
        sendOtpFunction(mobile,otp);          
        let encriptedData = await commonFunctionWeb.encriptDataFunction(data);
        connection.destroy()
          resolve({
          "success":true,
          "message":"otp sent",
          "data":data,
          //"data":encriptedData,
          })
          }
         }
       })
       }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
    }






module.exports = authModel;
