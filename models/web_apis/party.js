var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');

var partyModel = {
  addNewParty: addNewParty,
  fetchAllPartyData:fetchAllPartyData,
  updateParty:updateParty,
  deleteSpecificPartyDetails:deleteSpecificPartyDetails,
  deleteMultiplePartyDetails:deleteMultiplePartyDetails,
  fetchAllTransporterPartyData :fetchAllTransporterPartyData, 
  undoDeleteMultiplePartyDetails:undoDeleteMultiplePartyDetails,
  fetchFuelParty:fetchFuelParty,
  fetchAutoPartsParty:fetchAutoPartsParty,
  fetchTransporterParty:fetchTransporterParty
};


function addNewParty(bodyEncripted){
    return new Promise(async function(resolve, reject){
      var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`INSERT INTO parties SET name='${body.name}',mobile = '${body.mobile}',address='${body.address}',party_type_code = '${body.party_type_code}',opening_balance = '${body.opening_balance}',type_of_ob='${body.type_of_ob}',created_by='${body.created_by}'`, async(error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){
      let partyData = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows.insertId);
      // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",partyData)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(partyData);
      connection.destroy();
      resolve({
      "success":true,
      "message":"Party added",
      "data":encriptedData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Party not added"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
  })
 }


function fetchAllPartyData(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();

    let compressData = await commonFunctionWeb.compressDataFunction(rows)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party data fetched successfully",
    "data":encriptedData,
    //"data":rows,
    })
    }else{
    connection.destroy();
    let compressData = await commonFunctionWeb.compressDataFunction([])
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party data fetched successfully",
    "data":encriptedData,
    //"data":rows,
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    let compressData = await commonFunctionWeb.compressDataFunction([])
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party data fetched successfully",
    "data":encriptedData,
    //"data":rows,
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function fetchAllTransporterPartyData(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 AND party_type_code = 1 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();

    let compressData = await commonFunctionWeb.compressDataFunction(rows)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party data fetched successfully",
    "data":encriptedData,
    //"data":rows,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}





function updateParty(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE parties SET name = '${body.name}',mobile = '${body.mobile}',address='${body.address}',party_type_code = '${body.party_type_code}',opening_balance = '${body.opening_balance}',type_of_ob='${body.type_of_ob}',created_by='${body.created_by}' WHERE code = '${body.party_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    let partyData = await commonFunctionWeb.fetchSpecificPartyDetails(connection,body.party_code);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",partyData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(partyData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Party updated",
    "encriptedData":encriptedData
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Party not updated"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
})
}


function deleteSpecificPartyDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE parties SET status = 2 WHERE code = '${body.code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Pary data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Pary data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
    }
  })
  }




function deleteMultiplePartyDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE parties SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Party data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
 })
}
  
  

function undoDeleteMultiplePartyDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE parties SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Party data undo successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party data not undo"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
 })
}
  
  


function fetchFuelParty(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    var party_type_code = req.params.party_type_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 AND party_type_code = 3 ORDER BY name ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
      var partyData = [];
      for(var i =0;i<rows.length;i++){
        var items = {}
        items["code"] = rows[i].code,
        items["name"] = rows[i].name
        items["mobile"] = rows[i].mobile
        items["party_type_code"] = rows[i].party_type_code,
        items["opening_balance"] = rows[i].opening_balance,
        items["type_of_ob"] =  rows[i].type_of_ob == 0 ? "Debit":"Credit",
        items["address"] = rows[i].address,
        items["created"] = rows[i].created,
        items["party_type_label"] = await commonFunctionAndroid.fetchPartyTypeLabel(rows[i].party_type_code)
        partyData.push(items)
      }
      if(partyData.length > 0){
        // connection.destroy(); connecttion already destroyed
        resolve({
          "success":true,
          "message":"Party data fetched successfully",
          // "data":encriptedData,
          "parties_data":partyData,
          })
      }else{
        // connection.destroy(); connecttion already destroyed
        resolve({
        "success":false,
        "message":"Party data not fetched",
        })
      }
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    }else{
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Party data not fetched",
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
      "success":false,
      "message":"Party data not fetched",
      })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}


function fetchAutoPartsParty(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    var party_type_code = req.params.party_type_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 AND party_type_code = '2' ORDER BY name ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
      var partyData = [];
      for(var i =0;i<rows.length;i++){
        var items = {}
        items["code"] = rows[i].code,
        items["name"] = rows[i].name
        items["mobile"] = rows[i].mobile
        items["party_type_code"] = rows[i].party_type_code,
        items["opening_balance"] = rows[i].opening_balance,
        items["type_of_ob"] =  rows[i].type_of_ob == 0 ? "Debit":"Credit",
        items["address"] = rows[i].address,
        items["created"] = rows[i].created,
        items["party_type_label"] = await commonFunctionAndroid.fetchPartyTypeLabel(rows[i].party_type_code)
        partyData.push(items)
      }
      if(partyData.length > 0){
        // connection.destroy(); connecttion already destroyed
        resolve({
          "success":true,
          "message":"Party data fetched successfully",
          // "data":encriptedData,
          "parties_data":partyData,
          })
      }else{
        // connection.destroy(); connecttion already destroyed
        resolve({
        "success":false,
        "message":"Party data not fetched",
        })
      }
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    }else{
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Party data not fetched",
    })
    }
    }
    })
    }catch(e){
      connection.destroy();
      console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":false,
      "message":"Party data not fetched",
      })
      }
     }else{
     reject({"success": false, "message": "unknown db"});
    }
  })
}

function fetchTransporterParty(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    var party_type_code = req.params.party_type_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,"false" AS selected FROM parties WHERE status != 2 AND party_type_code = 1 ORDER BY name ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
      var partyData = [];
      for(var i =0;i<rows.length;i++){
        var items = {}
        items["code"] = rows[i].code,
        items["name"] = rows[i].name
        items["mobile"] = rows[i].mobile
        items["party_type_code"] = rows[i].party_type_code,
        items["opening_balance"] = rows[i].opening_balance,
        items["type_of_ob"] =  rows[i].type_of_ob == 0 ? "Debit":"Credit",
        items["address"] = rows[i].address,
        items["created"] = rows[i].created,
        items["party_type_label"] = await commonFunctionAndroid.fetchPartyTypeLabel(rows[i].party_type_code)
        partyData.push(items)
      }
      if(partyData.length > 0){
        // connection.destroy(); connecttion already destroyed
        resolve({
          "success":true,
          "message":"Party data fetched successfully",
          // "data":encriptedData,
          "parties_data":partyData,
          })
      }else{
        // connection.destroy(); connecttion already destroyed
        resolve({
        "success":false,
        "message":"Party data not fetched",
        })
      }
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    }else{
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Party data not fetched",
    })
    }
    }
    })
    }catch(e){
      connection.destroy();
      console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":false,
      "message":"Party data not fetched",
      })
      }
     }else{
     reject({"success": false, "message": "unknown db"});
    }
  })
}
module.exports = partyModel;
