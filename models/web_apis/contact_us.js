var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");


var contactUsModel = {
    addContactUsDetails:addContactUsDetails,
    fetchContactUsDetails:fetchContactUsDetails,
    deleteContactUsData:deleteContactUsData,
    updateContactUsDetails:updateContactUsDetails,
    fetchContactUsDetailsForAndroid:fetchContactUsDetailsForAndroid
}


function addContactUsDetails(bodyEncripted){
  return new Promise(async(resolve, reject) => {
   var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`INSERT INTO contact_us SET name='${body.name}',mobile= '${body.mobile}',whats_app_number = '${body.whats_app_number}',paytm_number = '${body.paytm_number}',email='${body.email}',address='${body.address}'`,async (error,rows, fields)=>{
        if (error) {
        throw error;
        }else{
        if(rows.affectedRows > 0){
        let contactUsDetails = await fetchSpecificContactUsDetails(rows.insertId);
        let encriptedData = await commonFunctionWeb.encriptDataFunction(contactUsDetails);

        connection.destroy()
        resolve({
        "success":true,
        "message":"Contact us data added successfully",
        "data":encriptedData
        })                    
        }else{
        connection.destroy()
        resolve({
        "success":false,
        "message":"Contact us data added"
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
       }
     })
    }



    
function fetchContactUsDetails(body){
  return new Promise(async(resolve, reject) => {
    //var database_code = req.params.code
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM contact_us ORDER BY created DESC`,async (error,rows, fields)=>{
          if (error) {
          //commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
              if(rows.length > 0){
              //let encriptedData = await commonFunctionWeb.encriptDataFunction(rows[0])
              //let customer_query = await fetchAllCustomerQuery(body)
              //item = {"contact_us_details":rows[0]}

              let compressData = await commonFunctionWeb.compressDataFunction(rows[0])
              let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

              connection.destroy()
              resolve({
              "success":true,
              "message":"Contact us data fetched success",
              //"data":rows[0],
              "data":encriptedData
              })                    
              }else{

              connection.destroy()
              resolve({
              "success":false,
              "message":"No data found"
              })
              }
              }
             })
            }catch(e){

            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
           }
         })
        }


function fetchContactUsDetailsForAndroid(body){
  return new Promise(async(resolve, reject) => {
    //var database_code = req.params.code
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM contact_us ORDER BY created DESC`,async (error,rows, fields)=>{
          if (error) {
          //commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
              if(rows.length > 0){
              //let encriptedData = await commonFunctionWeb.encriptDataFunction(rows[0])
              //let customer_query = await fetchAllCustomerQuery(body)
              //item = {"contact_us_details":rows[0]}
              // let compressData = await commonFunctionWeb.compressDataFunction(rows[0])
              // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

              connection.destroy()
              resolve({
              "success":true,
              "message":"Contact us data fetched success",
              "data":rows[0],
              // "data":encriptedData
              })                    
              }else{
              connection.destroy()
              resolve({
              "success":false,
              "message":"No data found"
              })
              }
              }
              })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          })
        }

        


function deleteContactUsData(body){
    return new Promise(async(resolve, reject) => {
    let connection = await commonFunctionWeb.getMasterDatabaseConnection(body.database_code)
    if(connection != false){
    try{
        connection.query(`DELETE FROM contact_us WHERE code ='${body.contact_us_code}' `,async(error,rows, fields)=>{
        if (error) {
        resolve (error)
        }else{ 
          if(rows.affectedRows > 0){
          connection.destroy();
          resolve({
          "success":true,
          "message":"Contact us data deleted success",
          })  
          }else{
  
          connection.destroy();
          resolve({
          "success":false,
          "message":"Contact us data not deleted ",
          })  
          }
         }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
       }
       })
      }




function updateContactUsDetails(bodyEncripted){
    return new Promise(async(resolve, reject) => {
   var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM contact_us ORDER BY created DESC`,async (error,rows, fields)=>{
        if (error) {
        //commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
        connection.destroy()
          throw error;
          }else{
            if(rows.length > 0){
              connection.query(` UPDATE contact_us SET name='${body.name}',mobile= '${body.mobile}',whats_app_number = '${body.whats_app_number}',paytm_number = '${body.paytm_number}',email='${body.email}',address='${body.address}' WHERE code = '${body.contact_us_code}'`,async (error,rows, fields)=>{
              if (error) {
                throw error;
                }else{
                if(rows.affectedRows > 0){
                          
                let contactUsDetails = await fetchSpecificContactUsDetails(body.contact_us_code);
                let encriptedData = await commonFunctionWeb.encriptDataFunction(contactUsDetails);
                connection.destroy()
                resolve({
                "success":true,
                "message":"Contact us data updated successfully",
                "data":encriptedData
                })                    
                }else{
                connection.destroy()
                resolve({
                "success":false,
                "message":"Contact us data not updated"
                })
                }
               }
              })
             }else{
                connection.query(` INSERT INTO contact_us SET name='${body.name}',mobile= '${body.mobile}',whats_app_number = '${body.whats_app_number}',email='${body.email}',address='${body.address}'`,async (error,rows, fields)=>{
                if (error) {
                  throw error;
                  }else{
                  if(rows.affectedRows > 0){
                  connection.destroy()
                  resolve({
                  "success":true,
                  "message":"Contact us data updated",
                  })                    
                  }else{
                  connection.destroy()
                  resolve({
                  "success":false,
                  "message":"Contact us data not updated"
                  })
                  }
                 }
               })
             }
            }
          })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
        }
      })
     }





            
function fetchAllCustomerQuery(body){
  return new Promise( function(resolve, reject) {
      db.query(`SELECT * FROM customer_query order by created  DESC`,async (error,rows, fields)=>{
      if (error) {
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      throw error;
      }else{
      try{
         // let encriptedData = await commonFunctionWeb.encriptDataFunction(rows)
          resolve(rows)
          }catch(e){
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong",
          })
         }
        }
      })
    })
   }





function fetchSpecificContactUsDetails(contact_us_code){
  return new Promise(async(resolve, reject) => {
    //var database_code = req.params.code
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM contact_us WHERE code = '${contact_us_code}' ORDER BY created DESC`,async (error,rows, fields)=>{
          if (error) {
          //commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
            throw error;
            }else{
            if(rows.length > 0){
            // console.log(`SELECT * FROM contact_us WHERE code = '${contact_us_code}' ORDER BY created DESC`,rows)
            connection.destroy()
            resolve(rows[0])                    
            }else{
            connection.destroy()
            resolve(null)
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve(null)
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          })
        }

 
 
     
    
module.exports = contactUsModel;
