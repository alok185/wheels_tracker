var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var Buffer = require('buffer').Buffer;
var zlib = require('zlib');



var transporterReportModel = {
    fetchTransporterReport:fetchTransporterReport,
    fetchAllOwnerListOfVehicles:fetchAllOwnerListOfVehicles,
    fetchAllVehicleListOfSpecificOwner:fetchAllVehicleListOfSpecificOwner,
    fetchAllNecessaryDataForFilteringTransportReportData:fetchAllNecessaryDataForFilteringTransportReportData
};
 



function fetchTransporterReport(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        console.log("**********************************>>>>>>>>",body,"ENCRYPTED BOY=========>>",bodyEncripted)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var routeQuery = ""
        let billtyData = [];
        var finalBilltyData = [];
        if(body.party_code != 0){
        partyQuery = `AND party_code = '${body.party_code}'`
        }
        if(body.vehicle_code.length > 0){
        vehicleQuery = `AND vehicle_code IN('${body.vehicle_code}')`
        }
        if(body.route_code != 0){
        routeQuery = `AND route_commission_code = '${body.route_code}'`
        }
        if(body.vehicle_code.length == 0){
        let allOwnerWithVehicle = await fetchAllOwnerListWithVehicle(connection,body.database_code,body.party_code,from_date,to_date)
        for(var i =0;i<allOwnerWithVehicle.length;i++){
        billtyData = await fetchDistinctRouteFromBillty(connection,from_date,to_date,partyQuery,routeQuery,vehicleQuery,allOwnerWithVehicle[i].vehicle_codes)
        if(billtyData.length > 0){
        var item = {}
        item.owner_name = allOwnerWithVehicle[i].owner_name
        item.billty_data_of_owner = billtyData
        if(billtyData.length > 0){
        finalBilltyData.push(item)
        }
        }
        }
        let compressData = await commonFunctionWeb.compressDataFunction(finalBilltyData)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
       
        // let compressData = await commonFunctionWeb.compressDataFunction(finalBilltyData)
        

        // inp.pipe(gzip).pipe(out);
        // console.log("**************&&&&&&&&&&&&&&&&&&**********************")
        connection.destroy();
        resolve({
        "success":true,
        "message":"Transporter report fetched successfully",
        // "data":finalBilltyData,
        "data":encriptedData
        })
        // }
        }else if(body.vehicle_code.length >= 1){
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,body.vehicle_code[0])
        var owner_name = ""
        if(vehicle_details != null){
        owner_name = vehicle_details.owner_name
        }
        billtyData = await fetchDistinctRouteFromBillty(connection,from_date,to_date,partyQuery,vehicleQuery,routeQuery,body.vehicle_code)
        var item = {};
        item.owner_name = owner_name
        item.billty_data_of_owner = billtyData
        if(billtyData.length > 0){
        finalBilltyData.push(item)
        }
        let compressData = await commonFunctionWeb.compressDataFunction(finalBilltyData)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        connection.destroy();
        resolve({
        "success":true,
        "message":"Transporter report fetched successfully",
        // "data":finalBilltyData,
        "data":encriptedData
        })
       }
      }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
       }  
      })
     }





// function getDateArray (start, end) { for getting the list of date between dates....
//         var arr = new Array();
//         var dt = new Date(start);
//         while (dt <= end) {
//         arr.push(new Date(dt));
//         dt.setDate(dt.getDate() + 1);
//         }
//      return arr;
//    } 



function fetchDistinctRouteFromBillty(connection,from_date,to_date,partyQuery,routeQuery,vehicleQuery,vehicle_codes){
    return new Promise(async function(resolve, reject){
        // console.log("FINAL_QUERY",`SELECT DISTINCT route_commission_code FROM billty WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' ${partyQuery} ${vehicleQuery} ${routeQuery} ORDER BY bill_date ASC `)
        connection.query(`SELECT DISTINCT route_commission_code FROM billty WHERE status != 2 AND vehicle_code IN(${vehicle_codes}) AND bill_date BETWEEN '${from_date}' AND '${to_date}' ${partyQuery} ${routeQuery}  ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            try{
            // console.log("RRRRRRRRRRRRRRROOOOOOOOWSSSSSSSSSSSroute_commission_code",vehicle_codes,"===",rows)
            if(rows.length > 0){
            var billtyData = []
            for(var i = 0;i<rows.length;i++){
            var party_name = "";
            var mine_name = '';
            var expense = 0
            let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
            // console.log("routeDetailsrouteDetailsrouteDetails",routeDetails)
            if(routeDetails != null){
            party_name = routeDetails.party_name
            mine_name = routeDetails.mine_name
            expense = routeDetails.expense
            }
            let billty_data = await fetchAllDataOfBillty(connection,rows[i].route_commission_code,from_date,to_date,partyQuery,vehicleQuery,expense,vehicle_codes)
            // console.log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBILLOIOILDSIJDSADTADTADTDA",billtyData)
            if(billty_data.length > 0){
            var item = {}
            var itemFianl = {}
            item.route_code = rows[i].route_commission_code
            item.route_from = mine_name
            item.route_to = party_name
            item.billty_data = billty_data 
            // itemFianl.billty_data = item
            billtyData.push(item)
            }
            }
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }catch(e){
            // connection.destroy();
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
        }









function fetchAllDataOfBillty(connection,route_code,from_date,to_date,partyQuery,vehicleQuery,expense,vehicle_codes){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT *,@a:=@a+1 slno FROM billty, (SELECT @a:= 0) AS a WHERE status != 2 AND route_commission_code = '${route_code}' AND vehicle_code IN(${vehicle_codes}) AND  bill_date BETWEEN '${from_date}' AND '${to_date}' ${partyQuery} ${vehicleQuery} ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            try{
            // console.log("+++++++++++++++++++++++++++++++++++++++++++++>>>>>>>",`SELECT *,@a:=@a+1 slno FROM billty, (SELECT @a:= 0) AS a WHERE status != 2 AND route_commission_code = '${route_code}' AND bill_date BETWEEN DATE('${from_date}') AND DATE('${to_date}') ${partyQuery} ${vehicleQuery} ORDER BY bill_date ASC `,rows)
            if(rows.length > 0){
            var billtyData = [];
            for(var i = 0;i<rows.length;i++){
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var driver_salary = 0
                    var owner_name = ""
                    var min_qty = 0
                    var short_rate = 0
                    if(rows[i].challan_weight > rows[i].received_weight){
                        min_qty  = rows[i].received_weight
                    }else{
                        min_qty = rows[i].challan_weight
                    }
                    // var route_details = {};
                    var expense = 0
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    expense  = routeDetails.expense
                    short_rate = routeDetails.short_rate
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    owner_name = vehicle_details.owner_name
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    driver_salary = driverDetails.salary
                    }
                    var item = {}
                    item.slno = rows[i].slno
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,
                    // item.route_details = route_details
                    // item.route_from = routeDetails.mine_name
                    // item.route_to = routeDetails.party_name
                    item.vehicle_code = rows[i].vehicle_code
                    item.vehicle_label = vehicle_label
                    item.vehicle_no = vehicle_no
                    item.driver_code = rows[i].driver_code
                    item.driver_name = driver_name
                    // item.driver_mobile = driver_mobile
                    item.driver_salary = driver_salary
                    item.driver_mobile = rows[i].driver_commission
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    item.freight_rate = rows[i].freight_rate
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    item.short_rate = short_rate
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    item.created = rows[i].created
                    item.modified = rows[i].modified
                    item.expense = expense
                    item.owner_name = owner_name
                    item.fuel_rate = await fetchFuelRateOfVehicle(connection,rows[i].bill_date,rows[i].vehicle_code)
                    item.min_qty = min_qty
                    billtyData.push(item)
                }
            if(billtyData.length > 0){
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }else{
            resolve([])
            }
            }catch(e){
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
       }





function fetchFuelRateOfVehicle(connection,bill_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT fuel_rate FROM fuel_entry WHERE vehicle_code = '${vehicle_code}' AND bill_date = '${bill_date}' `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        resolve(rows[0].fuel_rate);
        }else{
        resolve(0);
        }
        }catch(e){
        console.log(e)
        resolve(0)
       }
      }
     })
   })
  }



function fetchAllOwnerListOfVehicles(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT DISTINCT owner_name  FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.release();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var vehicleData = [];
        for(var i = 0;i<rows.length;i++){
            
        var item = {}
        item.owner_name = rows[i].owner_name,
        vehicleData.push(item)
        }
        let encriptedData = await commonFunctionWeb.encriptDataFunction(vehicleData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Owner data fetched successfully",
        "data":encriptedData,
        // "data":vehicleData,
        })
        // }else{
        // resolve({
        // "success":false,
        // "message":"No data found",
        // })  
        // }
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }



function fetchAllVehicleListOfSpecificOwner(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT * FROM vehicles WHERE status != 2 AND owner_name = '${body.owner_name}' ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.release();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var vehicleData = [];
        for(var i = 0;i<rows.length;i++){
        var driver_name = "";
        var driver_mobile = "";
        let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code);
        if(driver_details != null){
        driver_name = driver_details.name
        driver_mobile = driver_details.mobile
        }
        var item = {}
        item.code = rows[i].code,
        item.label = rows[i].label,
        item.vehicle_no = rows[i].vehicle_no,
        // item.owner_name = rows[i].owner_name,
        // item.driver_code = rows[i].driver_code
        // item.driver_name = driver_name
        // item.driver_mobile = driver_mobile
        // item.created = rows[i].created
        // item.title = "vehicle"
        // item.selected = false;
        vehicleData.push(item)
        }
        // if(vehicleData.length > 0){
        let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Vehicle data fetched successfully",
        "data":encriptedData,
        // "data":vehicleData,
        })
        // }else{
        // resolve({
        // "success":false,
        // "message":"No data found",
        // })  
        // }
        }else{
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        connection.destroy();
        resolve({
        "success":true,
        "message":"Vehicle data fetched successfully",
        "data":encriptedData,
        // "data":vehicleData,
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }




function fetchAllNecessaryDataForFilteringTransportReportData(req,res){
    return new Promise(async function(resolve, reject){

        var database_code = req.params.code
        // console.log(req.params)
        //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        let vehicle_list = await fetchAllVehicle(connection);   
        let party_list = await fetchAllTransporterParty(connection);   
        let owner_list = await fetchAllOwnerList(connection);   
        let route_list = await fetchAllRouteList(connection);
        var item = {"party_list":party_list,"vehicle_list":vehicle_list,"owner_list":owner_list,"route_list":route_list}
        connection.destroy()

        let compressData = await commonFunctionWeb.compressDataFunction(item)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
        "success":true,
        "message":"Data fetched successfully",
        // "data":item,
        "data":encriptedData
        })
        }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
      }
    })
   }



   function fetchAllVehicle(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,label AS name,vehicle_no,owner_name FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
        //connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve([])
       }
     })
    }
  
  

function fetchAllTransporterParty(connection){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT code,name FROM parties WHERE status != 2 AND party_type_code = 1 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        resolve(rows)
        }else{
        resolve([])
        }
       }
      })
    })
   }

function fetchAllOwnerList(connection){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT DISTINCT owner_name  FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var vehicleData = [];
        for(var i = 0;i<rows.length;i++){
        var item = {}
        item.owner_name = rows[i].owner_name,
        vehicleData.push(item)
        }
        resolve(vehicleData)
        }else{
        resolve([])
        }
       }
      })
     })
     }


function fetchAllRouteList(connection){
    return new Promise(async function(resolve, reject){
    connection.query(`SELECT * FROM route_commission WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var routeData = [];
        for(var i = 0;i<rows.length;i++){
            var mine_name = ""
            var party_name = ""
            let mine_details = await commonFunctionWeb.fetchSpecificMineDetails(connection,rows[i].mine_code);
            let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
            if(mine_details != null){
            mine_name = mine_details.name
            }
            if(party_details != null){
            party_name = party_details.name
            }
            
            var item = {}
            item.code = rows[i].code,
            // item.short_rate = rows[i].short_rate,
            // item.freight_rate = rows[i].freight_rate,
            // item.driver_commission = rows[i].driver_commission,
            item.mine_code = rows[i].mine_code
            item.mine_name = mine_name
            item.party_code = rows[i].party_code
            item.party_name = party_name
            // item.created = rows[i].created
            // item.modified = rows[i].modified
    
            routeData.push(item)
        }
        if(routeData.length > 0){
        resolve(routeData)
        }else{
        resolve([])  
        }
        }else{
        resolve([])
        }
        }
      })
    })
   }




function fetchAllOwnerListWithVehicle(connection,user_code,party_code,from_date,to_date){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT DISTINCT vehicles.owner_name AS owner_name FROM billty INNER JOIN vehicles ON vehicles.code=billty.vehicle_code WHERE billty.created_by = '${user_code}' AND billty.status<>2 AND billty.party_code = '${party_code}' AND billty.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY owner_name`, async (error, rows, fields)=>{
            if(error){
            resolve(error);
            }else{
            // console.log(`SELECT DISTINCT billty.vehicle_code AS vehicle_code FROM billty INNER JOIN vehicles ON vehicles.code=billty.vehicle_code WHERE billty.created_by = '${user_code}' AND billty.status<>2 AND billty.party_code = '${party_code}'  AND billty.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY owner_name`,rows)
                if(rows.length > 0){
                vehicleDataOwnerWise = [];
                for(var i =0;i<rows.length;i++){
                let fetchAllVehicleFromOwnerName = await fetchAllVehicleFromOwnerNameFunction(connection,user_code,party_code,from_date,to_date,rows[i].owner_name)
                // console.log("fetchAllVehicleFromOwnerName",fetchAllVehicleFromOwnerName)
                if(fetchAllVehicleFromOwnerName.length > 0){
                    var item = {}
                    item.owner_name = rows[i].owner_name
                    item.vehicle_codes = fetchAllVehicleFromOwnerName
                    vehicleDataOwnerWise.push(item)
                   }
                 }
                resolve(vehicleDataOwnerWise)
              }else{
            resolve([])
              }
            }
          })
        })
      }



function fetchAllVehicleFromOwnerNameFunction(connection,user_code,party_code,from_date,to_date,owner_name){
    return new Promise(async function(resolve, reject){
        // console.log("owner_nameowner_name",owner_name)
        connection.query(`SELECT DISTINCT billty.vehicle_code AS vehicle_code FROM billty INNER JOIN vehicles ON vehicles.code=billty.vehicle_code WHERE billty.created_by = '${user_code}' AND billty.status<>2 AND billty.party_code = '${party_code}' AND vehicles.owner_name = '${owner_name}' AND billty.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY owner_name`, async (error, rows, fields)=>{
        if(error){
        resolve(error);
        }else{
            // console.log(`SELECT DISTINCT billty.vehicle_code AS vehicle_code FROM billty INNER JOIN vehicles ON vehicles.code=billty.vehicle_code WHERE billty.created_by = '${user_code}' AND billty.status<>2 AND billty.party_code = '${party_code}' AND vehicles.owner_name = '${owner_name}' AND billty.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY owner_name`,rows)
            var vehcileCodes = [];
            if(rows.length > 0){
            for(var i = 0;i<rows.length;i++){
            vehcileCodes.push(rows[i].vehicle_code)
            }
            resolve(vehcileCodes)
            }else{
            resolve([])
            }
        }
      })
    })
   }


module.exports = transporterReportModel;
