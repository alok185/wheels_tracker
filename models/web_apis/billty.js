var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var billtyModel = {
  addNewBillty:addNewBillty,
  fetchAllBillties:fetchAllBillties,
  updateBilltyDetails:updateBilltyDetails,
  deleteSpecificBilltyDetails:deleteSpecificBilltyDetails,
  deleteMultipleBilltyDetails:deleteMultipleBilltyDetails,
  fetchAllNecessaryDataForAddingAndUpdatingBillty:fetchAllNecessaryDataForAddingAndUpdatingBillty,
  searchBilltyData:searchBilltyData,
  fetchSpecificBilltyDetails:fetchSpecificBilltyDetails,
  filterBilltyData:filterBilltyData,
  undoDeleteMultipleBilltyDetails:undoDeleteMultipleBilltyDetails
};


const storage = multer.diskStorage({
    destination: "./public/uploads/fuelEntry",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()


function addNewBillty(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.billty_data));
            //var data =  await JSON.parse(req.body.billty_data);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image_url_1'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'image_url_2') {
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            try{
            connection.query(`INSERT INTO billty SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',billty_no = '${data.billty_no}',route_commission_code = '${data.route_code}',bill_date = '${data.bill_date}',challan_weight = '${data.challan_weight}',received_weight = '${data.received_weight}',short_weight = '${data.short_weight}',advance='${data.advance}',diesel='${data.diesel}',party_code = '${data.party_code}',vehicle_code = '${data.vehicle_code}',driver_code ='${data.driver_code}',freight_rate = '${data.freight_rate}',remarks = '${data.remarks}',created_by = '${data.database_code}'`,async (error, rows, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rows.affectedRows > 0){
            let billtyData = await commonFunctionWeb.fetchSpecificBilltyDetails(connection,rows.insertId);
            // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",billtyData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);

            connection.destroy();
            resolve({
            "success":true,
            "message":"Billty data added sucessfully",
            "data":encriptedData,
            // "data":billtyData,
            
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Billty data not added"
            })    
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }


function fetchAllBillties(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT * FROM billty WHERE status != 2 ORDER BY created DESC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var billtyData = [];
    for(var i = 0;i<rows.length;i++){
    var vehicle_label = '';
    var party_name = "";
    var vehicle_no = ''
    var driver_name = ''
    var mine_name = '';
    var driver_mobile = '';
    var route_details = {};
    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
    if(routeDetails != null){
    route_details = routeDetails
    }
    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    if(driverDetails != null){
      driver_name = driverDetails.name
      driver_mobile = driverDetails.driver_mobile
    }
    var item = {}
    item.code = rows[i].code
    item.billty_no = rows[i].billty_no
    item.party_code = rows[i].party_code
    item.party_name = party_name
    item.bill_date = rows[i].bill_date,
    item.route_details = route_details
    item.vehicle_code = rows[i].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.driver_code = rows[i].driver_code
    item.driver_name = driver_name
    item.driver_mobile = driver_mobile
    item.advance = rows[i].advance
    item.remarks = rows[i].remarks,
    item.freight_rate = rows[i].freight_rate
    item.challan_weight = rows[i].challan_weight
    item.diesel = rows[i].diesel,
    item.received_weight = rows[i].received_weight,
    item.short_weight = rows[i].short_weight,
    item.image_url_1 = rows[i].image_url_1,
    item.image_url_2 = rows[i].	image_url_2,
    item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
    item.created = rows[i].created
    item.selected = false
    billtyData.push(item)
    }
    if(billtyData.length > 0){
    connection.destroy();
    let compressData = await commonFunctionWeb.compressDataFunction(billtyData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    resolve({
    "success":true,
    "message":"Billty data fetched successfully",
    "data":{"bitty_data":encriptedData},
    //"data":{"bitty_data":billtyData},
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}





function updateBilltyDetails(req,res){
  return new Promise(async function(resolve, reject){
    upload(req, res, async (err) => {        
        if (err) {
        commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
        console.log(err);
        }else{
        var query = '';
        var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.billty_data));
       // var data =  await JSON.parse(req.body.billty_data); 
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
        if(connection != false){
        var image1Location = data.image_url_1;
        var thumbImage1Location = data.image_url_1_thumbnail;
        var image2Location = data.image_url_2;
        var thumbImage2Location = data.image_url_2_thumbnail;
        if(req.files.length > 0){
        for(var i = 0;i<req.files.length;i++){
        if(req.files[i].fieldname == 'image_url_1'){
        image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
        var myFile = req.files[i];
        //for comprressing the images........................STARTS.............
        Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
        return image.quality(40)        // set JPEG quality
        .resize(256, 256)
        .write(myFile.destination+"/thumb/"+myFile.filename); // save
        }).catch(function (err) {
        console.error(err);
        // resolve({"success": false, "message": "Error"});
        });
        thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
        }
        if(req.files[i].fieldname == 'image_url_2') {
        image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
        var myFile1 = req.files[i];
        //for comprressing the images........................STARTS.............
        Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
        return image.quality(40)        // set JPEG quality
        .resize(256, 256)
        .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
        }).catch(function (err) {
        console.error(err);
        });
        thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
        }
        }
        }
        try{
        connection.query(`UPDATE billty SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',billty_no = '${data.billty_no}',route_commission_code = '${data.route_code}',bill_date = '${data.bill_date}',challan_weight = '${data.challan_weight}',received_weight = '${data.received_weight}',short_weight = '${data.short_weight}',advance='${data.advance}',diesel='${data.diesel}',party_code = '${data.party_code}',vehicle_code = '${data.vehicle_code}',driver_code ='${data.driver_code}',freight_rate = '${data.freight_rate}',remarks = '${data.remarks}',created_by = '${data.database_code}' WHERE code = '${data.billty_code}'`, async(error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        let billtyData = await commonFunctionWeb.fetchSpecificBilltyDetails(connection,data.billty_code);
        console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",billtyData);

        let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Billty data added sucessfully",
        "data":encriptedData,
        //"data":billtyData,
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Billty data not added"
        })    
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
       }
      }
    })
   })
  }


function deleteSpecificBilltyDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE billty SET status = 2 WHERE code = '${body.billty_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Billty data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Billty data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
   }
  })
  }




  function deleteMultipleBilltyDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE billty SET status = 2 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.destroy();
        resolve({
        "success":true,
        "message":"Billty data deleted"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Billty data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});    
        }
      })
    }
  
  
    function undoDeleteMultipleBilltyDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
          var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          connection.query(`UPDATE billty SET status = 0 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows  > 0){
          connection.destroy();
          resolve({
          "success":true,
          "message":"Billty data undo successfully"
          })
          }else{
          connection.destroy()      
          resolve({
          "success":false,
          "message":"Billty data not undo"
          })
          }
          }
          })
          }catch(e){
          connection.destroy()
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
          }
          }else{
          reject({"success": false, "message": "unknown db"});    
          }
        })
      }
    
    



function fetchAllNecessaryDataForAddingAndUpdatingBillty(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    let party_list = await fetchAllPartyList(connection);
    let vehicle_list = await fetchAllVehicle(connection);
    let driver_list = await fetchAllDriver(connection);
    let route_list = await fetchAllRoute(connection)
    if(party_list.length != 0 || vehicle_list.length != 0 || driver_list.length != 0 || route_list.length != 0){
    var item = {"party_list":party_list,"vehicle_list":vehicle_list,"driver_list":driver_list,"route_list":route_list}

    let compressData = await commonFunctionWeb.compressDataFunction(item)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
 
    connection.destroy();
    resolve({
    "success":true,
    "message":"Data fetched sucessfully",
    "data": item,
    "data":encriptedData,
    //"data":item
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    } 
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
}






function fetchAllPartyList(connection){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      //connection.release();
      resolve(error);
      }else{ 
      if(rows.length > 0){
      //connection.destroy();
      resolve(rows)
      }else{
     // connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
     // connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }


function fetchAllVehicle(connection){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT code,label AS name,vehicle_no FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      resolve(error);
      }else{ 
      //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
      if(rows.length > 0){
      //connection.destroy();
      resolve(rows)
      }else{
      //connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
     // connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }




  function fetchAllDriver(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name,mobile,profile_image_url,profile_image_url_thumbnail FROM drivers WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
        //connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
      //  connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }
  
  function fetchAllRoute(connection){
    return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT * FROM route_commission WHERE status != 2 ORDER BY created DESC  `, async (error, rows, fields)=>{
      if(error){
      //connection.destroy();
      createErrorLogFile(error);sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.length > 0){
      var routeData = [];
      for(var i =0;i<rows.length;i++){
      var mine_name  = '';
      var party_name = '';
      let mine_details = await  commonFunctionWeb.fetchSpecificMineDetails(connection,rows[i].mine_code)
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
      if(party_details != null){
        party_name = party_details.name
      }
      if(mine_details != null){
        mine_name = mine_details.name
      }
      var item = {};
      item.code = rows[i].code,
      item.mine_code = rows[i].mine_code,
      item.mine_name = mine_name
      item.party_code = rows[i].party_code,
      item.party_name = party_name
      routeData.push(item)
      }
      if(routeData.length > 0){
      resolve(routeData)
      }else{
      resolve([])
      }
      }else{
      //connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
      //connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    })
  }




function searchBilltyData(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    connection.query(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${body.keyword}%' OR parties.name LIKE '%${body.keyword}%' OR billty.bill_date LIKE '%${body.keyword}%' OR  vehicles.label LIKE '%${body.keyword}%' OR vehicles.vehicle_no LIKE '%${body.keyword}%' OR drivers.name LIKE '%${body.keyword}%' Or billty.advance LIKE '%${body.keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    // console.log(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${body.keyword}%' OR parties.name LIKE '%${body.keyword}%' OR billty.bill_date LIKE '%${body.keyword}%' OR  vehicles.label LIKE '%${body.keyword}%' OR vehicles.vehicle_no LIKE '%${body.keyword}%' OR drivers.name LIKE '%${body.keyword}%' Or billty.advance LIKE '%${body.keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${body.range} OFFSET ${body.offset}`,rows)
    // if(rows.length > 0){
    var billtyData = [];
    for(var i = 0;i<rows.length;i++){
    let specificBilltyDetails = await commonFunctionWeb.fetchSpecificBilltyDetails(connection,rows[i].code)
    if(specificBilltyDetails != null){
    billtyData.push(specificBilltyDetails)
    }
    }
    // if(billtyData.length > 0){
    connection.destroy();
    let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
    resolve({
    "success":true,
    "message":"Billty data fetched successfully",
    "data":{"bitty_data":encriptedData},
    // "data":{"bitty_data":billtyData},
    })
    // }else{
    // connection.destroy();
    // resolve({
    // "success":true,
    // "message":"No data found"
    // })    
    // }
    // }else{
    // connection.destroy();
    // resolve({
    // "success":true,
    // "message":"No data found"
    // })
    // }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}




function fetchSpecificBilltyDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    var billty_code = body.billty_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
      connection.query(`SELECT * FROM billty  WHERE code = '${billty_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      var billtyData = [];
      var vehicle_label = '';
      var party_name = "";
      var vehicle_no = '';
      var driver_name = '';
      var driver_mobile = '';
      var mine_name = '';
      var route_details = {};
      let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
      let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[0].route_commission_code)
      let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[0].driver_code)

      if(routeDetails != null){
      route_details = routeDetails
      }
      if(vehicle_details != null){
      vehicle_label = vehicle_details.label
      vehicle_no = vehicle_details.vehicle_no
      }
      if(party_details != null){
      party_name = party_details.name
      }
      if(driverDetails != null){
        driver_name = driverDetails.name
        driver_mobile = driverDetails.mobile
      }
      var item = {}
      item.code = rows[0].code
      item.billty_no = rows[0].billty_no
      item.route_code = rows[0].route_commission_code
      item.party_code = rows[0].party_code
      item.party_name = party_name
      item.bill_date = rows[0].bill_date,
      item.route_details = route_details
      item.vehicle_code = rows[0].vehicle_code
      item.vehicle_label = vehicle_label
      item.vehicle_no = vehicle_no
      item.driver_code = rows[0].driver_code
      item.driver_name = driver_name
      // console.log("driver_mobiledriver_mobiledriver_mobiledriver_mobiledriver_mobiledriver_mobile",driver_mobile)
      item.driver_mobile = driver_mobile
      item.advance = rows[0].advance
      item.remarks = rows[0].remarks,
      item.freight_rate = rows[0].freight_rate
      item.challan_weight = rows[0].challan_weight
      item.diesel = rows[0].diesel,
      item.received_weight = rows[0].received_weight,
      item.short_weight = rows[0].short_weight,
      item.image_url_1 = rows[0].image_url_1,
      item.image_url_2 = rows[0].	image_url_2,
      item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
      item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
      item.created = rows[0].created

      let compressData = await commonFunctionWeb.compressDataFunction(item)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

      connection.destroy();
      resolve({
      "success":true,
      "message":"Billty data fetched successfully",
      //"data":item,
      "data":encriptedData
      })
      }else{
      
      connection.destroy();
      resolve({
      "success":false,
      "message":"Billty data not fetched",
      })
      }
      }catch(e){

      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })    
      } 
      }
      })
      }catch(e){

      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    }else{
    reject({"success": false, "message": "unknown db"});      
    }
   })
}




function filterBilltyData(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var month = body.month
    var year = body.year
    var finalString = year+"-"+month

    connection.query(`SELECT DISTINCT billty.code FROM billty WHERE status != 2 AND bill_date LIKE '%${finalString}%' ORDER BY billty.created DESC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    var billtyData = [];
    // console.log(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${body.keyword}%' OR parties.name LIKE '%${body.keyword}%' OR billty.bill_date LIKE '%${body.keyword}%' OR  vehicles.label LIKE '%${body.keyword}%' OR vehicles.vehicle_no LIKE '%${body.keyword}%' OR drivers.name LIKE '%${body.keyword}%' Or billty.advance LIKE '%${body.keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${body.range} OFFSET ${body.offset}`,rows)
    if(rows.length > 0){
    for(var i = 0;i<rows.length;i++){
    let specificBilltyDetails = await commonFunctionWeb.fetchSpecificBilltyDetails(connection,rows[i].code)
    if(specificBilltyDetails != null){
    billtyData.push(specificBilltyDetails)
    }
    }
    // if(billtyData.length > 0){
    connection.destroy();
    let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
    resolve({
    "success":true,
    "message":"Billty data fetched successfully",
    "data":{"bitty_data":encriptedData},
    // "data":{"bitty_data":billtyData},
    })
    // }else{
    // connection.destroy();
    // resolve({
    // "success":true,
    // "message":"No data found"
    // })    
    // }
    }else{
    connection.destroy();
    let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
    resolve({
    "success":true,
    "message":"No data found",
    "data":{"bitty_data":encriptedData},
    })
    }
    }catch(e){

    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}


module.exports = billtyModel;
