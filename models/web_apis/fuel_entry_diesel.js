var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var fuelEntryDieselModel = {
  addNewFuelEntryDieselRate:addNewFuelEntryDieselRate,
  fetchAllFuelEntryDieselRateDetailsOfSpecificParty:fetchAllFuelEntryDieselRateDetailsOfSpecificParty,
  updateFuelEntryDieselRate:updateFuelEntryDieselRate,
  deleteMultipleDieselRateDetails:deleteMultipleDieselRateDetails,
  undoDeleteMultipleDieselRateDetails:undoDeleteMultipleDieselRateDetails
};


function addNewFuelEntryDieselRate(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`INSERT INTO diesel_rate SET party_code='${body.party_code}',entry_date = '${body.entry_date}',rate='${body.rate}',	created_by = '${body.database_code}'`, async(error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    let fuelEntryDieselRate = await commonFunctionWeb.fetchSpecificFuelEntryDieselRateDetails(connection,rows.insertId);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",fuelEntryDieselRate)

    let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryDieselRate)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    connection.destroy();
    resolve({
    "success":true,
    "message":"Diesel rate added successfully",
    "data":encriptedData
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Diesel rate not added"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })
}


function fetchAllFuelEntryDieselRateDetailsOfSpecificParty(bodyEncripted){
  return new Promise(async function(resolve, reject){
  var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  var database_code = body.database_code 
  let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
  if(connection != false){
  try{
  var query = ""
  if(body.party_code != 0){
  query = `SELECT * FROM diesel_rate WHERE status != 2 AND party_code = '${body.party_code}' ORDER BY entry_date DESC `;
  }else{
  query = `SELECT * FROM diesel_rate WHERE status != 2 ORDER BY entry_date DESC `;
  }
  connection.query(query, async (error, rows, fields)=>{
  if(error){
  commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  connection.destroy();
  resolve(error);
  }else{ 
  if(rows.length > 0){
    var dieselRateData = [];
    
    for(var i =0 ; i<rows.length;i++){
      var party_name = '';
      var item = {};
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
      if(party_details != null){
      party_name = party_details.name
      }
      item.code = rows[i].code
      item.party_name = party_name
      item.party_code = rows[i].party_code
      item.entry_date = rows[i].entry_date
      item.rate = rows[i].rate
      dieselRateData.push(item)
    }
    
  if(dieselRateData.length > 0){

  connection.destroy();
  let compressData = await commonFunctionWeb.compressDataFunction(dieselRateData)
  let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
  resolve({
  "success":true,
  "message":"Diesel rate data fetched successfully",
  "data":encriptedData,
  //"data":dieselRateData,
  })
  }else{
  connection.destroy();
  resolve({
  "success":false,
  "message":"No data found"
  })  
  }
  }else{
  connection.destroy();
  resolve({
  "success":false,
  "message":"No data found"
  })
  }
  }
  })
  }catch(e){
  connection.destroy();
  console.log(e)
  resolve({
  "success":false,
  "message":"Something went wrong"
  })
 }
 }else{
 reject({"success": false, "message": "unknown db"});
 }
})
}



function updateFuelEntryDieselRate(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`Update diesel_rate SET party_code='${body.party_code}',entry_date = '${body.entry_date}',rate='${body.rate}',	created_by = '${body.database_code}' WHERE code = '${body.diesel_rate_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    let fuelEntryDieselRate = await commonFunctionWeb.fetchSpecificFuelEntryDieselRateDetails(connection,body.diesel_rate_code);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",fuelEntryDieselRate)

    let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryDieselRate)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Diesel rate updated successfully",
    "data":encriptedData
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Diesel rate not updated"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
})
}



function deleteMultipleDieselRateDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
  var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
  if(connection != false){
  try{
  connection.query(`UPDATE diesel_rate SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
  if(error){
  connection.destroy();
  commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  resolve(error);
  }else{ 
  if(rows.affectedRows  > 0){

  connection.destroy()
  resolve({
  "success":true,
  "message":"Diesel rate data deleted successfully"
  })
  }else{

  connection.destroy()      
  resolve({
  "success":false,
  "message":"Diesel rate data not deleted"
  })
  }
  }
  })
  }catch(e){
  connection.destroy()
  console.log(e)
  resolve({
  "success":false,
  "message":"Something went wrong"
  })
  }
  }else{
  resolve({"success": false, "message": "unknown db"});    
  }
})
}



function undoDeleteMultipleDieselRateDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
  var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
  if(connection != false){
  try{
  connection.query(`UPDATE diesel_rate SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
  if(error){
  connection.destroy();
  commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  resolve(error);
  }else{ 
  if(rows.affectedRows  > 0){
  connection.destroy()
  resolve({
  "success":true,
  "message":"Diesel rate data undo successfully"
  })
  }else{

  connection.destroy()      
  resolve({
  "success":false,
  "message":"Diesel rate data not undo"
  })
  }
  }
  })
  }catch(e){
  connection.destroy()
  console.log(e)
  resolve({
  "success":false,
  "message":"Something went wrong"
  })
  }
  }else{
  resolve({"success": false, "message": "unknown db"});    
  }
})
}





module.exports = fuelEntryDieselModel;
