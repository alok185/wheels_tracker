var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var partyPaymentModel = {
  addNewPartyPayment: addNewPartyPayment,
  fetchAllPartyPayments:fetchAllPartyPayments,
  updatePartyPaymentDetails:updatePartyPaymentDetails,
  deleteSpecificPartyPaymentDetails:deleteSpecificPartyPaymentDetails,
  deleteMultiplePartyPaymentDetails:deleteMultiplePartyPaymentDetails,
  fetchAllPartyForAddingPayment:fetchAllPartyForAddingPayment,
  fetchSpecificPartyPaymentDetails:fetchSpecificPartyPaymentDetails,
  undoDeleteMultiplePartyPaymentDetails:undoDeleteMultiplePartyPaymentDetails
};


const storage = multer.diskStorage({
    destination: "./public/uploads/Party/payments",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()


function addNewPartyPayment(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.party_payment_data));
          //  var data =  JSON.parse(req.body.party_payment_data);

            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image_url_1'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'image_url_2') {
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            try{
            connection.query(`INSERT INTO payments SET  image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',bill_no = '${data.bill_no}',bill_date = '${data.bill_date}',from_date='${data.from_date}',to_date = '${data.to_date}',amount = '${data.amount}',purpose='${data.purpose}',payment_type='${data.payment_type}',ref_no ='${data.ref_no}',remarks='${data.remarks}',created_by = '${data.database_code}'`, (error, rowsPayment, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsPayment.affectedRows > 0){
            connection.query(`INSERT INTO party_payments SET party_code = '${data.party_code}',	payment_code = '${rowsPayment.insertId}',created_by='${data.database_code}'`, async(error, rowsPartyPayment, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsPartyPayment.affectedRows > 0){
            let partyPaymentData = await commonFunctionWeb.fetchSpecificPartyPaymentDetails(connection,rowsPayment.insertId);
            // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyPaymentData",partyPaymentData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(partyPaymentData);

            connection.destroy();
            resolve({
            "success":true,
            "message":"Party payment added sucessfully",
            "data":encriptedData
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment not added"
            })    
            }
            }
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment not added"
            })
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            resolve({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }


function fetchAllPartyPayments(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM party_payments WHERE status != 2 ORDER BY created DESC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var paymentData = [];
    for(var i = 0;i<rows.length;i++){
    var party_name = '';
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
    if(party_details != null){
    party_name = party_details.name
    }
    let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[i].payment_code)
    if(payment_details != null){
    var item = {}
    item.party_code = rows[i].party_code
    item.party_name = party_name
    item.payment_code = rows[i].payment_code
    item.bill_no = payment_details.bill_no,
    item.bill_date = payment_details.bill_date,
    item.from_date = payment_details.from_date,
    item.to_date = payment_details.to_date,
    item.amount = payment_details.amount,
    item.purpose = payment_details.purpose,
    item.payment_type = payment_details.payment_type,
    item.ref_no = payment_details.ref_no,
    item.remarks = payment_details.remarks,
    item.image_url_1 = payment_details.image_url_1,
    item.image_url_2 = payment_details.	image_url_2,
    item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail,
    item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail,
    item.selected = false
    item.created = rows[i].created
    paymentData.push(item)

    }
    }
    if(paymentData.length > 0){
    
    connection.destroy();

    let compressData = await commonFunctionWeb.compressDataFunction(paymentData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party payement data fetched successfully",
    "data":encriptedData,
    //"data":paymentData,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){

    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}





function updatePartyPaymentDetails(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.party_payment_data));
           // var data =  JSON.parse(req.body.party_payment_data);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location =data.image_url_1;
            var thumbImage1Location = data.image_url_1_thumbnail;
            var image2Location = data.image_url_2;
            var thumbImage2Location = data.image_url_2_thumbnail;
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image_url_1'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'image_url_2') {
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            try{
            connection.query(`UPDATE payments SET  image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',bill_no = '${data.bill_no}',bill_date = '${data.bill_date}',from_date='${data.from_date}',to_date = '${data.to_date}',amount = '${data.amount}',purpose='${data.purpose}',payment_type='${data.payment_type}',ref_no ='${data.ref_no}',remarks='${data.remarks}',created_by = '${data.database_code}' WHERE code = '${data.payment_code}'`, (error, rowsPayment, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsPayment.affectedRows > 0){
            connection.query(`UPDATE party_payments SET  party_code = '${data.party_code}',	payment_code = '${data.payment_code}',created_by='${data.database_code}' WHERE  payment_code = '${data.payment_code}'`, async (error, rowsPartyPayment, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsPartyPayment.affectedRows > 0){
            let partyPaymentData = await commonFunctionWeb.fetchSpecificPartyPaymentDetails(connection,data.payment_code);
            // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyPaymentData",partyPaymentData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(partyPaymentData);

            connection.destroy();
            resolve({
            "success":true,
            "message":"Party payment data updated sucessfully",
            "data":encriptedData
            })
            }else{
              
            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment data not updated"
            })    
            }
            }
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment not added"
            })
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            resolve({"success": false, "message": "unknown db"});
            }
          }
        })
       })
     }


function deleteSpecificPartyPaymentDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE payments SET status = 2 WHERE code = '${body.payment_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.query(`UPDATE party_payments SET status = 2 WHERE payment_code = '${body.payment_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Party payment data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party payment data not deleted"
    })     
    }
    }
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party payment data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
  }




  function deleteMultiplePartyPaymentDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE payments SET status = 2 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.query(`UPDATE party_payments SET status = 2 WHERE payment_code IN(${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Party payment data deleted successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Party payment data not deleted"
        })     
        }
        }
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Party payment data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
      })
    }
  
  

    function undoDeleteMultiplePartyPaymentDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
          var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          connection.query(`UPDATE payments SET status = 0 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows  > 0){
          connection.query(`UPDATE party_payments SET status = 0 WHERE payment_code IN(${body.code})`, async (error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows > 0){
          connection.destroy()
          resolve({
          "success":true,
          "message":"Party payment data undo successfully"
          })
          }else{
          connection.destroy()      
          resolve({
          "success":false,
          "message":"Party payment data not undo"
          })     
          }
          }
          })
          }else{
          connection.destroy()      
          resolve({
          "success":false,
          "message":"Party payment data not undo"
          })
          }
          }
          })
          }catch(e){
          connection.destroy()
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
          }
          }else{
          resolve({"success": false, "message": "unknown db"});    
          }
        })
      }
    
    


    function fetchAllPartyForAddingPayment(req,res){
      return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY name ASC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction(rows)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        resolve({
        "success":true,
        "message":"Party data fetched successfully",
        "data":encriptedData,
        //"data":rows,
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       resolve({"success": false, "message": "unknown db"});
       }
     })
    }



function fetchSpecificPartyPaymentDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
   var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
     var database_code = body.database_code
     var payment_code = body.payment_code 
     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
     if(connection != false){
     try{
      connection.query(`SELECT * FROM party_payments WHERE payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var paymentData = [];
        var party_name = '';
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[0].payment_code)
        if(payment_details != null){
        var item = {}
        item.party_code = rows[0].party_code
        item.party_name = party_name
        item.payment_code = rows[0].payment_code
        item.bill_no = payment_details.bill_no,
        item.bill_date = payment_details.bill_date,
        item.from_date = payment_details.from_date,
        item.to_date = payment_details.to_date,
        item.amount = payment_details.amount,
        item.purpose = payment_details.purpose,
        item.payment_type = payment_details.payment_type,
        item.ref_no = payment_details.ref_no,
        item.remarks = payment_details.remarks,
        item.image_url_1 = payment_details.image_url_1,
        item.image_url_2 = payment_details.	image_url_2,
        item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail;
        item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;

        let compressData = await commonFunctionWeb.compressDataFunction(item)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Party payment data fetched successfully",
        // "data":item,
        "data":encriptedData
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Party payment data not fetched",
        })
        }
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Party payment data not fetched",
        })
        }
      }
     })
     }catch(e){
      console.log(e)
      connection.destroy();
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    }else{
    resolve({"success": false, "message": "unknown db"});
    }
  })
}

module.exports = partyPaymentModel;
