var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var fuelEntryModel = {
  addNewFuelEntry: addNewFuelEntry,
  fetchAllFuelEntryDetails:fetchAllFuelEntryDetails,
  updateFuelEntryDetails:updateFuelEntryDetails,
  deleteSpecificFuelEntryDetails:deleteSpecificFuelEntryDetails,
  deleteMultipleFuelEntryDetails:deleteMultipleFuelEntryDetails,
  fetchAllFuelEntryPartyData:fetchAllFuelEntryPartyData,
  fetchSpecificFuelEntryDetails:fetchSpecificFuelEntryDetails,
  filterFuelEntryData:filterFuelEntryData,
  undoDeleteMultipleFuelEntryDetails:undoDeleteMultipleFuelEntryDetails
};


const storage = multer.diskStorage({
    destination: "./public/uploads/fuelEntry",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()


function addNewFuelEntry(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.fuel_entry_data));
            //var data =  JSON.parse(req.body.fuel_entry_data);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image_url_1'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'image_url_2') {
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            try{
            connection.query(`INSERT INTO fuel_entry SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',challan_no = '${data.challan_no}',vehicle_code = '${data.vehicle_code}',bill_date = '${data.bill_date}',party_code = '${data.party_code}',fuel_rate = '${data.fuel_rate}',amount = '${data.amount}',quantity='${data.quantity}',remarks='${data.remarks}',created_by = '${data.database_code}'`,async (error, rows, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rows.affectedRows > 0){
            let fuelEntryData = await commonFunctionWeb.fetchSpecificfuelEntryDetails(connection,rows.insertId);
            // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",fuelEntryData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(fuelEntryData);

            connection.destroy();
            resolve({
            "success":true,
            "message":"Fuel entry data added sucessfully",
            "data":encriptedData,
            //"data":fuelEntryData,
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Fuel entry data not added"
            })    
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }


function fetchAllFuelEntryDetails(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    var limit = 1000
    var offset = 0
    if(connection != false){
    try{
    connection.query(`SELECT * FROM fuel_entry WHERE status != 2 ORDER BY created DESC LIMIT ${limit} OFFSET ${offset}` , async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var fuelEntryData = [];
    for(var i = 0;i<rows.length;i++){
    var vehicle_label = '';
    var party_name = "";
    vehicle_no = ''
    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)

    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    var item = {}
    item.code = rows[i].code
    item.challan_no = rows[i].challan_no
    item.party_code = rows[i].party_code
    item.party_name = party_name
    item.bill_date = rows[i].bill_date,

    item.vehicle_code = rows[i].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.fuel_rate = rows[i].fuel_rate
    item.quantity = rows[i].quantity
    item.remarks = rows[i].remarks,
    item.amount = rows[i].amount
    item.image_url_1 = rows[i].image_url_1,
    item.image_url_2 = rows[i].	image_url_2,
    item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
    item.created = rows[i].created
    item.selected = false
    fuelEntryData.push(item)
    }
    if(fuelEntryData.length > 0){

    connection.destroy();

    let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Driver payement data fetched successfully",
    "data":encriptedData,
    //"data":fuelEntryData,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}





function updateFuelEntryDetails(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
           var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.fuel_entry_data));
           // var data =  JSON.parse(req.body.fuel_entry_data);

            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location =data.image_url_1;
            var thumbImage1Location = data.image_url_1_thumbnail;
            var image2Location = data.image_url_2;
            var thumbImage2Location = data.image_url_2_thumbnail;
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image_url_1'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            if(req.files[i].fieldname == 'image_url_2') {
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            }).catch(function (err) {
            console.error(err);
            });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            }
            }
            }
            try{
            connection.query(`UPDATE fuel_entry SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',challan_no = '${data.challan_no}',vehicle_code = '${data.vehicle_code}',bill_date = '${data.bill_date}',party_code = '${data.party_code}',fuel_rate = '${data.fuel_rate}',amount = '${data.amount}',quantity='${data.quantity}',remarks='${data.remarks}',created_by = '${data.database_code}' WHERE code = '${data.fuel_entry_code}'`, async(error, rows, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rows.affectedRows > 0){
            let fuelEntryData = await commonFunctionWeb.fetchSpecificfuelEntryDetails(connection,data.fuel_entry_code);
            console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>fuelEntryData",fuelEntryData)
            let encriptedData = await commonFunctionWeb.encriptDataFunction(fuelEntryData);
            connection.destroy();
            resolve({
            "success":true,
            "message":"Fuel entry data updated sucessfully",
            "data":encriptedData
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Fuel entry data not updated"
            })    
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }


function deleteSpecificFuelEntryDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE fuel_entry SET status = 2 WHERE code = '${body.fuel_entry_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy();
    resolve({
    "success":true,
    "message":"Fuel entry data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Fuel entry data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
   reject({"success": false, "message": "unknown db"});    
   }
  })
 }




  function deleteMultipleFuelEntryDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE fuel_entry SET status = 2 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.destroy()      
        resolve({
        "success":true,
        "message":"Fuel entry data deleted successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Fuel entry data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});    
        }
      })
    }
  
  



    function undoDeleteMultipleFuelEntryDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
          var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          connection.query(`UPDATE fuel_entry SET status = 0 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows  > 0){
          connection.destroy()      
          resolve({
          "success":true,
          "message":"Fuel entry data undo successfully"
          })
          }else{
          connection.destroy()      
          resolve({
          "success":false,
          "message":"Fuel entry data not deleted"
          })
          }
          }
          })
          }catch(e){
          connection.destroy()
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
          }
          }else{
          reject({"success": false, "message": "unknown db"});    
          }
        })
      }
    
    
  



    function fetchAllFuelEntryPartyData(req,res){
      return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        // console.log(`SELECT * FROM parties WHERE status != 2 ORDER BY created DESC `)
        connection.query(`SELECT code,name,mobile FROM parties WHERE status != 2 AND party_type_code = 3 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction(rows)
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        resolve({
        "success":true,
        "message":"Party data fetched successfully",
        "data":encriptedData,
        //"data":rows
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }


    
function fetchSpecificFuelEntryDetails(bodyEncripted){
  return new Promise(async function(resolve, reject){
   var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    var fuel_entry_code = body.fuel_entry_code 
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
      connection.query(`SELECT * FROM fuel_entry WHERE code = '${fuel_entry_code}'`, async (error, rows, fields)=>{
      if(error){
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      var vehicle_label = '';
      var party_name = "";
      var vehicle_no = '';
      let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
  
      if(vehicle_details != null){
      vehicle_label = vehicle_details.label
      vehicle_no = vehicle_details.vehicle_no
      }
      if(party_details != null){
      party_name = party_details.name
      }
      var item = {}
      item.code = rows[0].code
      item.challan_no = rows[0].challan_no
      item.party_code = rows[0].party_code
      item.party_name = party_name
      item.bill_date = rows[0].bill_date,
  
      item.vehicle_code = rows[0].vehicle_code
      item.vehicle_label = vehicle_label
      item.vehicle_no = vehicle_no
      item.fuel_rate = rows[0].fuel_rate
      item.quantity = rows[0].quantity
      item.remarks = rows[0].remarks,
      item.amount = rows[0].amount
      item.image_url_1 = rows[0].image_url_1,
      item.image_url_2 = rows[0].	image_url_2,
      item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
      item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
      item.created = rows[0].created

      let compressData = await commonFunctionWeb.compressDataFunction(item)
      let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

      connection.destroy();
      resolve({
      "success":true,
      "message":"Fuel entry data fetched successfully",
      "data":encriptedData,
      // "data":item
      })
      }else{
      
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }catch(e){

      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })    
      } 
      }
    })
    }catch(e){

    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    }
   }else{
   resolve({"success": false, "message": "unknown db"});    
  }
 })
}



function filterFuelEntryData(bodyEncripted){
  return new Promise(async function(resolve, reject){
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    var month = body.month
    var year = body.year
    var finalString = year+"-"+month
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM fuel_entry WHERE status != 2 AND bill_date LIKE '%${finalString}%' ORDER BY created DESC`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    console.log(`SELECT * FROM fuel_entry WHERE status != 2 AND bill_date LIKE '%${finalString}%' ORDER BY created DESC`)
    var fuelEntryData = [];
    if(rows.length > 0){
    for(var i = 0;i<rows.length;i++){
    var vehicle_label = '';
    var party_name = "";
    vehicle_no = ''
    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)

    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    var item = {}
    item.code = rows[i].code
    item.challan_no = rows[i].challan_no
    item.party_code = rows[i].party_code
    item.party_name = party_name
    item.bill_date = rows[i].bill_date,

    item.vehicle_code = rows[i].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.fuel_rate = rows[i].fuel_rate
    item.quantity = rows[i].quantity
    item.remarks = rows[i].remarks,
    item.amount = rows[i].amount
    item.image_url_1 = rows[i].image_url_1,
    item.image_url_2 = rows[i].	image_url_2,
    item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
    item.created = rows[i].created
    item.selected = false
    fuelEntryData.push(item)
    }
    // if(fuelEntryData.length > 0){
    connection.destroy();
    let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Fuel entry data fetched successfully",
    "data":encriptedData,
    //"data":fuelEntryData,
    })
    // }else{
    // connection.destroy();
    // resolve({
    // "success":true,
    // "message":"No data found"
    // })    
    // }
    }else{
    connection.destroy();
    let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryData)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"No data found",
    "data":encriptedData,
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}


module.exports = fuelEntryModel;
