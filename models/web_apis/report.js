var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');



var reportModel = {
    fetchFuelEntryReport:fetchFuelEntryReport,
    fetchAutoPartReport:fetchAutoPartReport,
};
 



function fetchFuelEntryReport(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var fuelEntryData = [];

        if(body.party_code != 0){
        partyQuery = `AND party_code = '${body.party_code}'`
        partyPayment = await fetchSpecficPartyPaymentDetailsForReport(connection,body.party_code,body.from_date,body.to_date)
        }
        if(body.vehicle_code  != 0){
        vehicleQuery = `AND vehicle_code = '${body.vehicle_code}'`
        }
        var filterQuery = `SELECT @a:=@a+1 slno,code,challan_no,party_code,vehicle_code,quantity,fuel_rate,amount,bill_date,remarks,status,created,modified,image_url_1,image_url_2,	image_url_1_thumbnail,image_url_2_thumbnail,created_by FROM fuel_entry, (SELECT @a:= 0) AS a WHERE status != 2 ${partyQuery} ${vehicleQuery} AND  bill_date BETWEEN '${from_date}' AND '${to_date}'  ORDER BY bill_date ASC`
        console.log(filterQuery)
        connection.query(filterQuery, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        for(var i = 0;i<rows.length;i++){
        var vehicle_label = '';
        var party_name = "";
        vehicle_no = ''
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        var item = {}
        if(body.party_code != 0){
        item.vehicle_label = vehicle_label
        item.vehicle_no = vehicle_no
        }
        if(body.vehicle_code != 0){
            
        }
        item.slno = rows[i].slno
        item.code = rows[i].code
        item.challan_no = rows[i].challan_no
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.bill_date = rows[i].bill_date,
        item.vehicle_code = rows[i].vehicle_code
        
        item.fuel_rate = rows[i].fuel_rate
        item.quantity = rows[i].quantity
        item.remarks = rows[i].remarks,
        item.amount = rows[i].amount
        // item.image_url_1 = rows[i].image_url_1,
        // item.image_url_2 = rows[i].	image_url_2,
        // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
        // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
        item.created = rows[i].created
        // item.selected = false
        fuelEntryData.push(item)
        }
        if(fuelEntryData.length > 0 || partyPayment.length > 0){
        
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction({"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment})
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);  
        resolve({
        "success":true,
        "message":"Fuel entry challan report data fetched successfully",
        "data":encriptedData,
        // "data":{"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment},
        })
        }else{
        connection.destroy();
        let compressData = await commonFunctionWeb.compressDataFunction({"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment})
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);  
        resolve({
        "success":true,
        "message":"Fuel entry challan report data fetched successfully",
        // "data":{"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment},
        "data":encriptedData
        })
        }
        }else{
        // if(partyPayment.length > 0){
        let compressData = await commonFunctionWeb.compressDataFunction({"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment})
        let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);  
        connection.destroy();
        resolve({
        "success":true,
        "message":"Fuel entry challan report data fetched successfully",
        // "data":{"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment},
        "data":encriptedData
        })
        // }else{
        // connection.destroy();
        // resolve({
        // "success":false,
        // "message":"No data found"
        // })
        // }
        }
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })    
        } 
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
     }




function fetchSpecficPartyPaymentDetailsForReport(connection,party_code,from_date,to_date){ 
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT DISTINCT party_code,payment_code,@a:=@a+1 slno FROM (SELECT @a:= 0) AS a,party_payments  INNER JOIN payments ON party_payments.payment_code = payments.code WHERE party_payments.status != 2 And party_code = '${party_code}' AND  payments.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY payments.bill_date ASC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{

        if(rows.length > 0){
        var paymentData = [];
        for(var i = 0;i<rows.length;i++){
        var party_name = '';
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[i].payment_code)
        if(payment_details != null){
        var item = {}
        item.slno = rows[i].slno
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.payment_code = rows[i].payment_code
        item.bill_no = payment_details.bill_no,
        item.bill_date = payment_details.bill_date,
        item.from_date = payment_details.from_date,
        item.to_date = payment_details.to_date,
        item.amount = payment_details.amount,
        item.purpose = payment_details.purpose,
        item.payment_type = payment_details.payment_type,
        item.ref_no = payment_details.ref_no,
        item.remarks = payment_details.remarks,
        // item.image_url_1 = payment_details.image_url_1,
        // item.image_url_2 = payment_details.	image_url_2,
        // item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail,
        // item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail,
        // item.selected = false
        item.created = payment_details.created
        paymentData.push(item)
        }
        }
        if(paymentData.length > 0){
        resolve(paymentData)
        }else{
        resolve([])    
        }
        }else{
        resolve([])    
        }
        }catch(e){
        console.log(e)
        resolve([])        
        } 
        }
        })
        }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })   
    }



function fetchAutoPartReport(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var autoPartsData = [];
        if(body.party_code != 0){
        partyQuery = `AND party_code = '${body.party_code}'`
        partyPayment = await fetchSpecficPartyPaymentDetailsForReport(connection,body.party_code,body.from_date,body.to_date)
        }
        if(body.vehicle_code  != 0){
        vehicleQuery = `AND vehicle_code = '${body.vehicle_code}'`
        }
        var filterQuery = `SELECT @a:=@a+1 slno,code,challan_no,party_code,vehicle_code,amount,bill_date,remarks,status,created,modified,created_by FROM auto_parts, (SELECT @a:= 0) AS a WHERE status != 2 ${partyQuery} ${vehicleQuery} AND  bill_date BETWEEN '${from_date}' AND '${to_date}'  ORDER BY bill_date ASC`
        console.log(filterQuery)
        connection.query(filterQuery, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
            for(var i =0;i<rows.length;i++){
            var item = {};
            var party_name = ''
            var vehicle_label = ''
            var  vehicle_no = ''
            var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
            if(party_details != null){
            party_name = party_details.name
            }
            var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
            if(vehicle_details != null){
            vehicle_label = vehicle_details.label
            vehicle_no = vehicle_details.vehicle_no
            }
            item.slno = rows[i].slno
            item.code = rows[i].code,
            item.challan_no = rows[i].challan_no,
            item.party_code = rows[i].party_code,
            item.party_name = party_name,
            item.vehicle_code = rows[i].vehicle_code,
            item.vehicle_label = vehicle_label,
            item.vehicle_no = vehicle_no,
            item.amount = rows[i].amount,
            item.bill_date = rows[i].bill_date,
            item.remarks = rows[i].remarks,
            item.created = rows[i].created
            // item.selected = false
            autoPartsData.push(item);
            }
            connection.destroy();
            let compressData = await commonFunctionWeb.compressDataFunction({"auto_part_data":autoPartsData,"party_payment_data":partyPayment})
            let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
            resolve({
            "success":true,
            "message":"Auto part data report fetched successfully",
            // "data":{"auto_part_data":autoPartsData,"party_payment_data":partyPayment}
            "data":encriptedData,
           })
           }else{
            // if(partyPayment.length > 0){
            connection.destroy();
            let compressData = await commonFunctionWeb.compressDataFunction({"auto_part_data":autoPartsData,"party_payment_data":partyPayment})
            let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
            resolve({
            "success":true,
            "message":"Auto part data report fetched successfully",
            // "data":{"auto_part_data":autoPartsData,"party_payment_data":partyPayment}
            "data":encriptedData
            })
        //     }else{
        //     connection.destroy();
        //     resolve({
        //     "success":false,
        //     "message":"No data found"
        //     })   
        //     // resolve({
        //     //     "success":true,
        //     //     "message":"Auto part data fetched successfully",
        //     //     "data":{"auto_part_data":autoPartsData,"party_payment_data":partyPayment}
        //     //     // "data":autoPartsData
        //     //   })   
        //   }    
         }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })    
        } 
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     })
    }



module.exports = reportModel;
