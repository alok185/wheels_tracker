var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;
let date = require('date-and-time');



var usersModel = {

  updateUserDetails:updateUserDetails,
  fetchSpecificUserDetailsForUser:fetchSpecificUserDetailsForUser,
  verifyOtpForChangeMobileOfUser:verifyOtpForChangeMobileOfUser,
  rechargeUserAccount:rechargeUserAccount,
  updateProfileImageOfUser:updateProfileImageOfUser

};


const storage = multer.diskStorage({
    destination: "./public/uploads/Users",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
    }
 })

const upload = multer({ storage: storage}).any()


function updateUserDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
        let connection = await commonFunctionWeb.getMasterDatabaseConnection()
        if(connection != false){
        try{//`UPDATE users SET name = '${data.name}',profile_image_url = '${image1Location}',profile_image_url_thumbnail = '${thumbImage1Location}',password='${data.password}',email='${data.email}',address ='${data.address}',city='${data.city}',state = '${data.state}',dob= '${data.dob}' WHERE code = '${data.user_code}'`
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        connection.query(`UPDATE users SET name = '${body.name}',email='${body.email}',address ='${body.address}',city='${body.city}',city_code = '${body.city_code}',state = '${body.state}' WHERE code = '${body.user_code}'`, (error, rowsUser, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rowsUser.affectedRows > 0){
        connection.destroy();
        resolve({
        "success":true,
        "message":"User data updated sucessfully",
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"User data not updated"
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
        }
       })
      }





function fetchSpecificUserDetailsForUser(req,res){
    return new Promise(async function(resolve, reject){
    var user_code = req.params.code;
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
        connection.query(`SELECT * FROM users WHERE code = '${user_code}' ORDER BY created`,async (error,rows, fields)=>{
            if (error) {
            connection.destroy()
            resolve(error)
            }else {
                try{
                if(rows.length > 0){  
                //var delivery_boy = [];
                var item = {};
                item.code = rows[0].code
                item.name = rows[0].name,
                item.mobile = rows[0].mobile
                item.address = rows[0].address
                item.profile_image_url = rows[0].profile_image_url
                item.profile_image_url_thumbnail = rows[0].profile_image_url_thumbnail,
                //item.password = rows[0].password 
                //item.db_name = rows[0].db_name; 
                item.created = rows[0].created; 
                item.city = rows[0].city; 
                item.email = rows[0].email
                // item.state = rows[0].state,
                // item.dob = rows[0].dob,
                item.expiry_date = rows[0].expiry_date
                var last_recharge_details = await fetchLastRechargesDoneByUser(rows[0].code);
                item.recharge_done_list = last_recharge_details;
                var last_recharge_done_on = "0000-00-00";
                if(last_recharge_details.length > 0){
                last_recharge_done_on = last_recharge_details[0].created;
                }
                item.last_recharge_done_on = last_recharge_done_on;
                let compressData = await commonFunctionWeb.compressDataFunction(item)
                let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

                connection.destroy();
                resolve({
                "success":true,
                "message":"User data fetched successfully",
               "data":encriptedData,
                // "data":item
                })
                }else{

                connection.destroy();
                resolve({
                "success":false,
                "message":"User data not fetched",
                })
                }
                }catch(e){

                connection.destroy();
                console.log(e)
                reject({
                "success":false,
                "message":"Somthing went wrong",
                })
                }
                }
                })
                }catch(e){
                connection.destroy();
                console.log(e)
                reject({
                "success":false,
                "message":"Somthing went wrong",
                })
               }
              }else{
              resolve({"success": false, "message": "unknown db"});
              }
             })
            }







function verifyOtpForChangeMobileOfUser(bodyEncripted){
    return new Promise(async(resolve, reject) => {
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let verify_otp = await commonFunctionWeb.verifyOtp(body)
        if(verify_otp){
        connection.query(`UPDATE users SET mobile='${body.mobile}' WHERE code = '${body.user_code}'`,async (error,rows, fields)=>{
        if (error) {
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        throw error;
        }else{
        if(rows.affectedRows > 0 ){
        connection.destroy();
        resolve({
        "success":true,
        "message":"mobile number changed successfully",
        //"token":body.token
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"mobile number not changed",
        })
        }
        }
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"wrong otp ",
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});            
        }
     })
 }


function fetchLastRechargesDoneByUser(user_code){
    return new Promise(async function(resolve, reject){
        var database_code = user_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,expiry_date,amount,created FROM account_upgrade WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        connection.destroy();
        let encriptedData = await commonFunctionWeb.encriptDataFunction(rows);
        resolve(rows)
        }else{
        connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve([])
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     }) 
    }
          

    

function rechargeUserAccount(body){
    return new Promise(async function(resolve, reject){
        var database_code = body.user_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{

        var user_code = body.user_code
        let expiryDate = await fetchNextExpiryDateOfUserAfterRecharge(user_code)
        let expiry_date  = expiryDate.toISOString().split("T")[0]
        //console.log("expiry_dateexpiry_dateexpiry_date+++++++++++++++++++++>>",expiry_date)
        connection.query(`INSERT INTO account_upgrade SET expiry_date = '${expiry_date}', amount = '${body.amount}'`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  

        connection.destroy();

        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        let updateUserExpiryDate = await updateUserExpiryDateFunction(user_code,expiry_date)//have to check if this condition faild then delete the previous data stored in database

        connection.destroy();

        // let encriptedData = await commonFunctionWeb.encriptDataFunction(rows);
        var title = 'Account Recharged Successfully'
        var message = 'Your account has been recharged successfully with amount of Rs.'+body.amount
        commonFunctionWeb.insertNotificationFunctionForWeb(body.user_code,title,message,-1,'notification',"",0);
        resolve({
        "success":true,
        "message":"Recharge done successfully",
        "next_expiry_date":expiry_date
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Recharge failed"
        })
        }
        }
        })
        }catch(e){

        connection.destroy();
        
        console.log(e)
        resolve({
        "success":false,
        "message":"Recharge failed"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
       }
     }) 
    }



function fetchNextExpiryDateOfUserAfterRecharge(user_code){
    return new Promise(async function(resolve, reject){
        let connection = await commonFunctionWeb.getMasterDatabaseConnection()
        if(connection != false){
        try{
            connection.query(`SELECT * FROM users WHERE code = '${user_code}' ORDER BY created`,async (error,rows, fields)=>{
                if (error) {
                //connection.destroy()
                resolve(error)
                }else {
                try{
                if(rows.length > 0){  
                var expiry_date = rows[0].expiry_date
                var currentDate = await commonFunctionWeb.currentDateTime(connection)
                if(expiry_date > currentDate){
                let now = new Date(expiry_date);
                let expiry_date_final = date.addDays(now, 360);
                //  console.log("===================>>>>>>>>>>",expiry_date_final,"currentDate",currentDate.split(" ")[0],"expiry_date > new Date()==>>",expiry_date > currentDate)
                resolve(expiry_date_final)
                }else{
                let now = new Date();
                let expiry_date_final = date.addDays(now, 360);
                resolve(expiry_date_final)
                }
                resolve()
                }else{
                resolve("0000-00-00T")
                }
                }catch(e){
                console.log(e)
                reject({
                "success":false,
                "message":"Somthing went wrong",
                })
                }
                }
                })
                }catch(e){
                console.log(e)
                reject({
                "success":false,
                "message":"Somthing went wrong",
                })
                }
                }else{
                resolve({"success": false, "message": "unknown db"});
               }
             })    
            }





function updateUserExpiryDateFunction(user_code,expiry_date){
   return new Promise(async function(resolve, reject){
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    connection.query(`UPDATE users SET expiry_date = '${expiry_date}' WHERE code = '${user_code}'`, (error, rowsUser, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rowsUser.affectedRows > 0){
    connection.destroy();
    resolve(true)
    }else{
    resolve(false)
    }
    }   
    })
    }else{
    resolve({"success": false, "message": "unknown db"});
    }
   })
   }






   function updateProfileImageOfUser(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.user_data));
           // var data =JSON.parse(req.body.user_data);
            let connection = await commonFunctionWeb.getMasterDatabaseConnection()
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location =data.profile_image_url;
            var thumbImage1Location = data.profile_image_url_thumbnail;
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'profile_image_url'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            }
            }
            try{//`UPDATE users SET name = '${data.name}',profile_image_url = '${image1Location}',profile_image_url_thumbnail = '${thumbImage1Location}',password='${data.password}',email='${data.email}',address ='${data.address}',city='${data.city}',state = '${data.state}',dob= '${data.dob}' WHERE code = '${data.user_code}'`
            connection.query(`UPDATE users SET profile_image_url = '${image1Location}',profile_image_url_thumbnail = '${thumbImage1Location}' WHERE code = '${data.user_code}'`, (error, rowsUser, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsUser.affectedRows > 0){
            connection.destroy();
            resolve({
            "success":true,
            "message":"User profile image updated sucessfully",
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"User profile image not updated"
            })
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }






module.exports = usersModel;
