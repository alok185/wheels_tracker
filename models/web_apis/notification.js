var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var express = require('express');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);



var userNotificationModel = {
fetchAllNotificationOfUser:fetchAllNotificationOfUser,
seenAllNotificationOfUser:seenAllNotificationOfUser,
seenOneNotificationOfUser:seenOneNotificationOfUser
}


function fetchAllNotificationOfUser(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        connection.query(`SELECT code,title,message,icon_url,created FROM notification  WHERE user_code = '${database_code}' ORDER BY created DESC`,async(error,rows, fields)=>{
        if (error) {
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error)
        }else{
            if(rows.length > 0){
            let connectionMasterDatabase = await commonFunctionWeb.getMasterDatabaseConnection(database_code)
            if(connectionMasterDatabase != false){
            let specificUserDetails = await commonFunctionWeb.fetchSpecificUserDetails(connectionMasterDatabase,database_code);
            let encriptedData = await commonFunctionWeb.encriptDataFunction(rows);
            let encriptedExpiryStatusData = await commonFunctionWeb.encriptDataFunction(specificUserDetails.account_expiry_status);

            connectionMasterDatabase.destroy()
            connection.destroy();

            resolve({
            "success":true,
            "message":"Notification fetched successfully",
            // "data":rows
            "data":encriptedData,
            "account_expiry_status":encriptedExpiryStatusData//specificUserDetails.account_expiry_status
            });
            }else{
            connectionMasterDatabase.destroy()
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })        
            }
          }else{
            resolve({
            "success":false,
            "message":"No data found"
           })
          }
         }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
       }else{
        resolve({"success": false, "message": "unknown db"});
       }
      })   
     }




function seenAllNotificationOfUser(req,res){
   return new Promise(async(resolve, reject) => {
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
        connection.query(`UPDATE notification  SET seen = 1 WHERE user_code =  '${database_code}'`,(error,rows, fields)=>{
        if (error) {
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error)
        }else{
        if(rows.affectedRows > 0){
        connection.destroy();
        resolve({
        "success":true,
        "message":"Notification seen successfully",
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Notification not seen",
        })
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});            
        }
       })
      }
    


function seenOneNotificationOfUser(body){
    return new Promise(async(resolve, reject) =>{
        var database_code = body.database_code
        var notification_code = body.notification_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
            connection.query(`UPDATE notification SET seen = 1 WHERE code =  '${notification_code}'`,(error,rows, fields)=>{
            if (error) {
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error)
            }else{
            if(rows.affectedRows > 0){
            connection.destroy();
            resolve({
            "success":true,
            "message":"Notification seen successfully",
            })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Notification not seen",
            })
            }
            }
            })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
           resolve({"success": false, "message": "unknown db"});            
          }
         })
        }
         

module.exports = userNotificationModel;
