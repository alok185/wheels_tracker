var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');


var voucherModel = {
  
  addNewVoucherReceipt: addNewVoucherReceipt,
  fetchAllVoucherReceipt:fetchAllVoucherReceipt,
  fetchSpecificVoucherReceiptDetails:fetchSpecificVoucherReceiptDetails,
  deleteVoucherReceipt:deleteVoucherReceipt,
  updateVoucherReceipt:updateVoucherReceipt,
  searchVoucherReceipt:searchVoucherReceipt,
  filterVoucherReceipt:filterVoucherReceipt,
  fetchDistinctYearFromVoucherReceipt:fetchDistinctYearFromVoucherReceipt
};


const storage = multer.diskStorage({
    destination: "./public/uploads/drivers/payments",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()




function addNewVoucherReceipt(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
          // var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.driver_payment_data));
            var data = JSON.parse(req.body.postData);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            // if(req.files[i].fieldname == 'image_url_2') {
            // image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            // var myFile1 = req.files[i];
            // //for comprressing the images........................STARTS.............
            // Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            // return image.quality(40)        // set JPEG quality
            // .resize(256, 256)
            // .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            // }).catch(function (err) {
            // console.error(err);
            // });
            // thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            // }
            }
            }
            try{              
            connection.query(`INSERT INTO voucher_receipt 
            SET  
            image_url = '${image1Location}',
            party_code = '${data.party_code}', 
            challan_no = '${data.challan_no}',
            voucher_date = '${data.voucher_date}',
            amount = '${data.amount}',
            payment_type='${data.payment_type}',
            ref_no ='${data.ref_no}',
            purpose='${data.purpose}',
            remarks='${data.remarks}',
            created_by = '${data.database_code}'`, (error, rowsVoucher, fields)=>{
            if(error){

            connection.destroy();

            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsVoucher.affectedRows > 0){

              connection.destroy();

              resolve({
              "success":true,
              "message":"Voucher Receipt Added sucessfully",
              })
              }else{

              connection.destroy();

              resolve({
              "success":false,
              "message":"Voucher Not Receipt Added"
              })    
              }
              }
              })
            }catch(e){

            connection.destroy();

            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
       })
      }










function updateVoucherReceipt(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
          // var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.driver_payment_data));
            var data = JSON.parse(req.body.postData);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            /// console.log("BBBBODDYY",req.body)
            var image1Location =data.image_url;
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
            for(var i = 0;i<req.files.length;i++){
            if(req.files[i].fieldname == 'image'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            return image.quality(40)        // set JPEG quality
            .resize(256, 256)
            .write(myFile.destination+"/thumb/"+myFile.filename); // save
            }).catch(function (err) {
            console.error(err);
            // resolve({"success": false, "message": "Error"});
            });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
            }
            // if(req.files[i].fieldname == 'image_url_2') {
            // image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            // var myFile1 = req.files[i];
            // //for comprressing the images........................STARTS.............
            // Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            // return image.quality(40)        // set JPEG quality
            // .resize(256, 256)
            // .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            // }).catch(function (err) {
            // console.error(err);
            // });
            // thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
            // }
            }
            }
            try{              
            connection.query(`UPDATE voucher_receipt 
            SET  
            image_url = '${image1Location}',
            party_code = '${data.party_code}', 
            challan_no = '${data.challan_no}',
            voucher_date = '${data.voucher_date}',
            amount = '${data.amount}',
            payment_type='${data.payment_type}',
            ref_no ='${data.ref_no}',
            purpose='${data.purpose}',
            remarks='${data.remarks}',
            created_by = '${data.database_code}' WHERE code = '${data.voucher_receipt_code}'`, (error, rowsVoucher, fields)=>{
            if(error){
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rowsVoucher.affectedRows > 0){

              connection.destroy();

              resolve({
              "success":true,
              "message":"Voucher Receipt Updated sucessfully",
              })
              }else{

              connection.destroy();

              resolve({
              "success":false,
              "message":"Voucher Receipt Not Updated"
              })    
              }
              }
              })
            }catch(e){

            connection.destroy();

            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
        })
      }

    






function fetchAllVoucherReceipt(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var limit = commonFunctionAndroid.limit
    var offset = body.last_index_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM voucher_receipt WHERE status != 2 ORDER BY created DESC LIMIT ${limit} OFFSET ${offset}`, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var voucherData = [];
    for(var i = 0;i<rows.length;i++){
    var party_name = "";
    // var party_mobile = "";
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code);
    if(party_details != null){
    party_name = party_details.name
    // party_mobile = party_details.mobile
    }
    var item = {}
    item.code = rows[i].code
    item.challan_no = rows[i].challan_no
    item.voucher_date = rows[i].voucher_date
    item.amount = rows[i].amount
    item.party_code = rows[i].party_code
    item.party_name = party_name
    // item.party_mobile = party_mobile
    item.purpose = rows[i].purpose
    item.payment_type = rows[i].payment_type,
    item.ref_no = rows[i].ref_no
    item.remarks = rows[i].remarks
    item.image_url = rows[i].image_url
    item.created_by = rows[i].created_by
    item.created = rows[i].created
    voucherData.push(item)
    }
    if(voucherData.length > 0){
   
    // let compressData = await commonFunctionWeb.compressDataFunction(voucherData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    
  var receiptItems = {
    "success":true,
    "message":"Voucher data fetched successfully",
    "voucher_receipt_data":voucherData
    }

    if(offset <= 0){
    var  count = await fetchVoucherReceiptCount(connection,`SELECT COUNT(*) as total_count FROM voucher_receipt WHERE status != 2`)
    receiptItems.count = count
    }


    connection.destroy();

    resolve(receiptItems)
    }else{

      connection.destroy();

      resolve({
      "success":false,
      "message":"Voucher data not fetched",
      })  
    }
    }else{

      connection.destroy();

      resolve({
        "success":false,
        "message":"Voucher data not fetched",
        })
      }
     }
    })
    }catch(e){
      console.log(e)

      connection.destroy();

      resolve({
      "success":false,
      "message":"Something went wrong",
      })
     }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}


function fetchSpecificVoucherReceiptDetails(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var voucher_receipt_code = body.voucher_receipt_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM voucher_receipt WHERE status != 2 AND code = '${voucher_receipt_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
      if(rows.length > 0){
        var item = {}
        var party_name = "";
        // var party_mobile = "";
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code);
        if(party_details != null){
        party_name = party_details.name
        // party_mobile = party_details.mobile
        }
        item.code = rows[0].code
        item.challan_no = rows[0].challan_no
        item.voucher_date = rows[0].voucher_date
        item.amount = rows[0].amount
        item.party_code = rows[0].party_code
        item.party_name = party_name
        // item.party_mobile = party_mobile
        item.purpose = rows[0].purpose
        item.payment_type = rows[0].payment_type,
        item.ref_no = rows[0].ref_no
        item.remarks = rows[0].remarks
        item.image_url = rows[0].image_url
        item.created_by = rows[0].created_by
        item.created = rows[0].created

      connection.destroy();
      
      resolve({
      "success":true,
      "message":"Voucher Receipt data fetche successfully",
      "voucher_receipt_data":item
     })
    }else{

        connection.destroy();

        resolve({
        "success": false,
         "message": "No Data Found"
        });
       }
      }
    })
    }catch(e){
    console.log(e)

    connection.destroy();

    resolve({
    "success":false,
    "message":"Something went wrong",
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});
  }
 })
}


function deleteVoucherReceipt(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE voucher_receipt SET status = 2 WHERE code = '${body.voucher_receipt_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){

    connection.destroy()

    resolve({
    "success":true,
    "message":"Voucher Receipt data deleted successfully"
    })
    }else{

    connection.destroy() 

    resolve({
    "success":false,
    "message":"Voucher Receipt data not deleted"
    })
    }
    }
    })
    }catch(e){

    connection.destroy()

    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
}




  function fetchVoucherReceiptCount(connection,condition){
    return new Promise(async function(resolve, reject){
      connection.query(condition, async (error, rowsCount, fields)=>{
      if(error){
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
        // console.log("****************************************",rowsCount)
        resolve(rowsCount[0].total_count)
      }
    })
  })
 }
 
 

 function searchVoucherReceipt(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var limit = commonFunctionAndroid.limit
    var offset = body.last_index_code
    var keyword = body.search_text
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT DISTINCT(voucher_receipt.code) FROM voucher_receipt INNER JOIN parties ON parties.code = voucher_receipt.party_code WHERE voucher_receipt.status != 2 AND (parties.name LIKE '%${keyword}%' || voucher_receipt.challan_no LIKE '%${keyword}%' || 	voucher_receipt.purpose LIKE '%${keyword}%' || 	voucher_receipt.ref_no LIKE '%${keyword}%' || 	voucher_receipt.payment_type LIKE '%${keyword}%' || 	voucher_receipt.remarks LIKE '%${keyword}%') ORDER BY voucher_receipt.created DESC LIMIT ${limit} OFFSET ${offset}`, async (error, rows, fields)=>{
    if(error){

    connection.destroy();

    resolve(error);
    }else{ 
        if(rows.length > 0){
        var voucherData = [];
        for(var i = 0;i<rows.length;i++){
            let voucherDetails = await commonFunctionAndroid.fetchSpecificVoucherReceipt(connection,rows[i].code)
            if(voucherDetails != null){
              voucherData.push(voucherDetails)
            }
        }

        if(voucherData.length > 0){   
        var receiptItems = {
        "success":true,
        "message":"Voucher data fetched successfully",
        "voucher_receipt_data":voucherData
        }

        if(offset <= 0){
        var  count = await fetchVoucherReceiptCount(connection,`SELECT COUNT(DISTINCT(voucher_receipt.code)) As total_count FROM voucher_receipt INNER JOIN parties ON parties.code = voucher_receipt.party_code WHERE voucher_receipt.status != 2 AND (parties.name LIKE '%${keyword}%' || voucher_receipt.challan_no LIKE '%${keyword}%' || 	voucher_receipt.purpose LIKE '%${keyword}%' || 	voucher_receipt.ref_no LIKE '%${keyword}%' || 	voucher_receipt.payment_type LIKE '%${keyword}%' || 	voucher_receipt.remarks LIKE '%${keyword}%') `)
        receiptItems.count = count
        }

        connection.destroy();

        resolve(receiptItems)
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"No Match Found"
        })
        }
        }else{

        connection.destroy();

          resolve({
          "success":false,
          "message":"No Match Found"
          })
        }
       }
      })
      }catch(e){

      connection.destroy();

      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});       
   }
  })
 }


function filterVoucherReceipt(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var month = body.month_value
    var year = body.year_value
    var finalString = year+"-"+month
    var limit = commonFunctionAndroid.limit
    var offset = body.last_index_code


    
    var keyword = ""
    var query = "";
    var queryTotalCount = ""
    if(body.keyword != undefined){
      var keyword = body.keyword
      query = `SELECT DISTINCT(voucher_receipt.code) FROM voucher_receipt INNER JOIN parties ON parties.code = voucher_receipt.party_code WHERE voucher_receipt.status != 2 AND voucher_date LIKE '%${finalString}%' AND (parties.name LIKE '%${keyword}%' || voucher_receipt.challan_no LIKE '%${keyword}%' || 	voucher_receipt.purpose LIKE '%${keyword}%' || 	voucher_receipt.ref_no LIKE '%${keyword}%' || 	voucher_receipt.payment_type LIKE '%${keyword}%' || 	voucher_receipt.remarks LIKE '%${keyword}%') ORDER BY voucher_receipt.created DESC LIMIT ${limit} OFFSET ${offset}`
      queryTotalCount = `SELECT DISTINCT COUNT(voucher_receipt.code) as total_count FROM voucher_receipt INNER JOIN parties ON parties.code = voucher_receipt.party_code WHERE voucher_receipt.status != 2 AND voucher_date LIKE '%${finalString}%' AND (parties.name LIKE '%${keyword}%' || voucher_receipt.challan_no LIKE '%${keyword}%' || 	voucher_receipt.purpose LIKE '%${keyword}%' || 	voucher_receipt.ref_no LIKE '%${keyword}%' || 	voucher_receipt.payment_type LIKE '%${keyword}%' || 	voucher_receipt.remarks LIKE '%${keyword}%') ORDER BY voucher_receipt.created DESC`
    }else{
      query = `SELECT DISTINCT (code) FROM voucher_receipt WHERE status != 2 AND voucher_date LIKE '%${finalString}%' ORDER BY created DESC limit ${limit} OFFSET ${offset}`
      queryTotalCount = `SELECT DISTINCT COUNT (code) AS total_count FROM voucher_receipt WHERE status != 2 AND voucher_date LIKE '%${finalString}%'`
    }

    connection.query(query, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
      // console.log(`SELECT DISTINCT (code) FROM voucher_receipt WHERE status != 2 AND voucher_date LIKE '%${finalString}%' ORDER BY created DESC limit ${limit} OFFSET ${offset}`,rows.length)
    if(rows.length > 0){
      var voucherData = [];
      for(var i =0;i<rows.length;i++){
        let voucherReceiptDetails = await commonFunctionAndroid.fetchSpecificVoucherReceipt(connection ,rows[i].code)
        if(voucherReceiptDetails != null){
          voucherData.push(voucherReceiptDetails)
        }
      }
      if(voucherData.length > 0){

        var receiptItems = {
          "success":true,
          "message":"Voucher data fetched successfully",
          "voucher_receipt_data":voucherData
          }
          if(offset <= 0){
          var  count = await fetchVoucherReceiptCount(connection,queryTotalCount)
          receiptItems.count = count
          }
          
        connection.destroy()

        resolve(receiptItems)
      }else{

        connection.destroy()

        resolve({
          "success":false,
          "message":"No data found"
        })  
      }
    }else{

    connection.destroy();
    
    resolve({
      "success":false,
      "message":"No data found"
      })    
    }
    }catch(e){

    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){

    connection.destroy();

    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function fetchDistinctYearFromVoucherReceipt(req,res){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = req.params.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    // var limit = commonFunctionAndroid.limit;
    // var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT DISTINCT(YEAR(voucher_date)) As year FROM voucher_receipt WHERE status != 2 `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      var yearList = []
      if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
          if(rows[i].year != 0)
          yearList.push(rows[i].year)
        }

        connection.destroy();

        resolve({
        "success":true,
        "message":"Year Data Fetched Successfully",
        "data":yearList
        })
      }else{
        yearList.push((new Date()).getFullYear())

        connection.destroy();

        resolve({
          "success":true,
          "message":"Year Data Fetched Successfully",
          "data":yearList
        })
      }
    }
    })
    }catch(e){

    connection.destroy();
    
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}




module.exports = voucherModel;
