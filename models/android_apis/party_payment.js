var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');


var partyPaymentModel = {
  addNewPartyPaymentForAndroid: addNewPartyPaymentForAndroid,
  fetchAllPartyPaymentsForAndroid:fetchAllPartyPaymentsForAndroid,
  updatePartyPaymentDetailsForAndroid:updatePartyPaymentDetailsForAndroid,
  deleteSpecificPartyPaymentDetailsForAndroid:deleteSpecificPartyPaymentDetailsForAndroid,
  deleteMultiplePartyPaymentDetails:deleteMultiplePartyPaymentDetails,
  // fetchAllPartyForAddingPaymentForAndroid:fetchAllPartyForAddingPaymentForAndroid,
  fetchSpecificPartyPaymentDetailsForAndroid:fetchSpecificPartyPaymentDetailsForAndroid,
  // undoDeleteMultiplePartyPaymentDetails:undoDeleteMultiplePartyPaymentDetails,
  searchPartyPaymentDataForAndroid:searchPartyPaymentDataForAndroid,
  filterPartyPaymentDataForAndroid:filterPartyPaymentDataForAndroid,
  fetchAllPartyType:fetchAllPartyType,
  fetchDistinctYearFromPartyPayments:fetchDistinctYearFromPartyPayments
};


const storage = multer.diskStorage({
    destination: "./public/uploads/Party/payments",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()


function addNewPartyPaymentForAndroid(req,res){
    return new Promise(async function(resolve, reject){
      console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 111")
      try{
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
              try{
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 222");

            var query = '';
            // var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.party_payment_data));
            console.log("################################&&&&&&",req.body.postData,"\n\n\nFFFFFFFFFFFFFFILLLLLESSS",req.files);
            var data =  JSON.parse(req.body.postData);
            console.log("################################&&&&&&",data)
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 11!!@@!!!",connection)

            if(connection != false){
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 12!!@@!!!")
            /// console.log("BBBBODDYY",req.body)
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            if(req.files.length > 0){
                  try{
                  console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 13!!@@!!!");
                      if(req.files[0].fieldname == 'image_url'){
                        console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 14!!@@!!!");

                          image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[0].path.replace(/\\/g ,"/");
                          var myFile = req.files[0];
                          //for comprressing the images........................STARTS.............
                          Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
                          return image.quality(40)        // set JPEG quality
                          .resize(256, 256)
                          .write(myFile.destination+"/thumb/"+myFile.filename); // save
                          }).catch(function (err) {
                          console.error(err);
                          // resolve({"success": false, "message": "Error"});
                          });
                          thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[0].destination.replace("." ,"")+"/thumb/"+req.files[0].filename; 
                      }
                      // if(req.files[i].fieldname == 'image_url_2') {
                      //   console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 15!!@@!!!");
                      //     image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
                      //     var myFile1 = req.files[i];
                      //     //for comprressing the images........................STARTS.............
                      //     Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
                      //     return image.quality(40)        // set JPEG quality
                      //     .resize(256, 256)
                      //     .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
                      //     }).catch(function (err) {
                      //     console.error(err);
                      //     });
                      //     thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
                      // }
                  }catch(e){
                    console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 16!!@@!!!");
                    console.log(e)
                    resolve({
                      "success":false,
                      "message":"Something went wrong"
                    })
                  }
                }
            try{
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 15!!!!!!!!!!",`INSERT INTO payments SET  image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',bill_no = '${data.bill_no}',bill_date = '${data.bill_date}',from_date='${data.from_date}',to_date = '${data.to_date}',amount = '${data.amount}',purpose='${data.purpose}',payment_type='${data.payment_type}',ref_no ='${data.ref_no}',remarks='${data.remarks}',created_by = '${data.database_code}'`)

            connection.query(`INSERT INTO payments SET  image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',bill_no = '${data.bill_no}',bill_date = '${data.bill_date}',from_date='${data.from_date}',to_date = '${data.to_date}',amount = '${data.amount}',purpose='${data.purpose}',payment_type='${data.payment_type}',ref_no ='${data.ref_no}',remarks='${data.remarks}',created_by = '${data.database_code}'`, (error, rowsPayment, fields)=>{
            console.log("error+++++++++++++",error, "\nrowsPayment+++++++++",rowsPayment, "\nfields++++++++++",fields);
            if(error){
            console.log(error)
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 

            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 333",rowsPayment)

            if(rowsPayment.affectedRows > 0){
            connection.query(`INSERT INTO party_payments SET party_code = '${data.party_code}',	payment_code = '${rowsPayment.insertId}',created_by='${data.database_code}'`, async(error, rowsPartyPayment, fields)=>{
            if(error){
            console.log(error)
            connection.destroy();
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 

            
            if(rowsPartyPayment.affectedRows > 0){
            // let partyPaymentData = await commonFunctionAndroid.fetchSpecificPartyPaymentDetailsForAndroid(connection,rowsPayment.insertId);
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 5555")
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(partyPaymentData);

            connection.destroy();
            resolve({
            "success":true,
            "message":"Party payment added sucessfully",
            // "data":encriptedData,
            // "data":partyPaymentData
            })
            }else{
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 6666")

            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment not added"
            })    
            }
            }
            })
            }else{
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 7777")

            connection.destroy();
            resolve({
            "success":false,
            "message":"Party payment not added"
            })
            }
            }
            })
            }catch(e){
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 888");
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
            }else{
            resolve({"success": false, "message": "unknown db"});
            }
            }catch(e){
            console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 999")
            console.log(e)
            resolve({
              "success":false,
              "message":"Something went wrong"
            })
           }
          }
        })
      }catch(e){
        console.log("RRRRRRRRRRRRRRRRRRRCHEESS HETETERERRE 1010")
        console.log(e)
        resolve({
          "success":false,
          "message":"Something went wrong"
        })
      }
       })
      }


function fetchAllPartyPaymentsForAndroid(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.body.database_code
    var limit = commonFunctionAndroid.limit
    var offset = req.body.last_index_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM party_payments WHERE status != 2 ORDER BY created DESC LIMIT ${limit} OFFSET ${offset}`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var paymentData = [];
    for(var i = 0;i<rows.length;i++){
    var party_name = '';
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
    if(party_details != null){
    party_name = party_details.name
    }
    let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[i].payment_code)
    if(payment_details != null){
    var item = {}
    item.party_code = rows[i].party_code
    item.party_name = party_name
    item.code = rows[i].payment_code
    item.bill_no = payment_details.bill_no,
    item.bill_date = payment_details.bill_date,
    item.from_date = payment_details.from_date,
    item.to_date = payment_details.to_date,
    item.amount = payment_details.amount,
    item.purpose = payment_details.purpose,
    item.payment_type = payment_details.payment_type,
    item.ref_no = payment_details.ref_no,
    item.remarks = payment_details.remarks,
    item.image_url = payment_details.image_url_1,
    item.thumb_image_url = payment_details.image_url_1_thumbnail,
    // item.image_url_2 = payment_details.	image_url_2,
    // item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail,
    item.created = rows[i].created
    paymentData.push(item)

    }
    }
    if(paymentData.length > 0){
    
    connection.destroy();

    // let compressData = await commonFunctionWeb.compressDataFunction(paymentData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Party payement data fetched successfully",
    // "data":encriptedData,
    "payments_data":paymentData,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}





function updatePartyPaymentDetailsForAndroid(req,res){
    return new Promise(async function(resolve, reject){
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            var query = '';
            // var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.party_payment_data));
            var data =  JSON.parse(req.body.postData);
            // console.log("#############################################data",data);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
              /// console.log("BBBBODDYY",req.body)
              var image1Location =data.image_url;
              var thumbImage1Location = data.image_url_thumbnail;
              var image2Location = data.image_url_2;
              var thumbImage2Location = data.image_url_2_thumbnail;
              if(req.files.length > 0){
                  for(var i = 0;i<req.files.length;i++){
                  if(req.files[i].fieldname == 'image_url'){
                  image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
                  var myFile = req.files[i];
                  //for comprressing the images........................STARTS.............
                  Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
                  return image.quality(40)        // set JPEG quality
                  .resize(256, 256)
                  .write(myFile.destination+"/thumb/"+myFile.filename); // save
                  }).catch(function (err) {
                  console.error(err);
                  // resolve({"success": false, "message": "Error"});
                  });
                  thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
                  }
                  if(req.files[i].fieldname == 'image_url_1') {
                  image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
                  var myFile1 = req.files[i];
                  //for comprressing the images........................STARTS.............
                  Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
                  return image.quality(40)        // set JPEG quality
                  .resize(256, 256)
                  .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
                  }).catch(function (err) {
                  console.error(err);
                  });
                  thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
                  }
                }
              }
              try{
              console.log("!!!!!!!!!!!!!!!!!1111111111111111111111","image1Location",image1Location,"image2Location",image2Location)
              connection.query(`UPDATE payments SET  image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',bill_no = '${data.bill_no}',bill_date = '${data.bill_date}',from_date='${data.from_date}',to_date = '${data.to_date}',amount = '${data.amount}',purpose='${data.purpose}',payment_type='${data.payment_type}',ref_no ='${data.ref_no}',remarks='${data.remarks}',created_by = '${data.database_code}' WHERE code = '${data.party_payment_code}'`, (error, rowsPayment, fields)=>{
              if(error){
              console.log(error)
              connection.destroy();
              commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
              resolve(error);
              }else{ 
              console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRESSSULT",rowsPayment,"error",error)
              if(rowsPayment.affectedRows > 0){
              connection.query(`UPDATE party_payments SET  party_code = '${data.party_code}',	payment_code = '${data.party_payment_code}',created_by='${data.database_code}' WHERE  payment_code = '${data.party_payment_code}'`, async (error, rowsPartyPayment, fields)=>{
              if(error){
              connection.destroy();
              commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
              resolve(error);
              }else{ 
              if(rowsPartyPayment.affectedRows > 0){
              // let partyPaymentData = await commonFunctionAndroid.fetchSpecificPartyPaymentDetailsForAndroid(connection,data.party_payment_code);
              // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyPaymentData",partyPaymentData)
              // let encriptedData = await commonFunctionWeb.encriptDataFunction(partyPaymentData);

              connection.destroy();
              resolve({
              "success":true,
              "message":"Party payment data updated sucessfully",
              // "data":encriptedData
              // "data":partyPaymentData
              })
              }else{
                
              connection.destroy();
              resolve({
              "success":false,
              "message":"Party payment data not updated"
              })    
              }
              }
              })
              }else{
              connection.destroy();
              resolve({
              "success":false,
              "message":"Party payment not added"
              })
              }
              }
              })
              }catch(e){
              connection.destroy();
              console.log(e)
              resolve({
              "success":false,
              "message":"Something went wrong"
              })
              }
            }else{
            resolve({"success": false, "message": "unknown db"});
            }
          }
        })
       })
     }


function deleteSpecificPartyPaymentDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE payments SET status = 2 WHERE code = '${body.party_payment_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.query(`UPDATE party_payments SET status = 2 WHERE payment_code = '${body.party_payment_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Party payment data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party payment data not deleted"
    })     
    }
    }
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Party payment data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
  }




  function deleteMultiplePartyPaymentDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE payments SET status = 2 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.query(`UPDATE party_payments SET status = 2 WHERE payment_code IN(${body.code})`, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Party payment data deleted successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Party payment data not deleted"
        })     
        }
        }
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Party payment data not deleted"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
      })
    }
  
  

    // function undoDeleteMultiplePartyPaymentDetails(bodyEncripted){
    //   return new Promise(async function(resolve, reject){
    //       var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    //       let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    //       if(connection != false){
    //       try{
    //       connection.query(`UPDATE payments SET status = 0 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
    //       if(error){
    //       connection.destroy();
    //       commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    //       resolve(error);
    //       }else{ 
    //       if(rows.affectedRows  > 0){
    //       connection.query(`UPDATE party_payments SET status = 0 WHERE payment_code IN(${body.code})`, async (error, rows, fields)=>{
    //       if(error){
    //       connection.destroy();
    //       commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    //       resolve(error);
    //       }else{ 
    //       if(rows.affectedRows > 0){
    //       connection.destroy()
    //       resolve({
    //       "success":true,
    //       "message":"Party payment data undo successfully"
    //       })
    //       }else{
    //       connection.destroy()      
    //       resolve({
    //       "success":false,
    //       "message":"Party payment data not undo"
    //       })     
    //       }
    //       }
    //       })
    //       }else{
    //       connection.destroy()      
    //       resolve({
    //       "success":false,
    //       "message":"Party payment data not undo"
    //       })
    //       }
    //       }
    //       })
    //       }catch(e){
    //       connection.destroy()
    //       console.log(e)
    //       resolve({
    //       "success":false,
    //       "message":"Something went wrong"
    //       })
    //       }
    //       }else{
    //       resolve({"success": false, "message": "unknown db"});    
    //       }
    //     })
    //   }
    
    


    // function fetchAllPartyForAddingPaymentForAndroid(req,res){
    //   return new Promise(async function(resolve, reject){
    //     var database_code = req.params.code
    //     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    //     if(connection != false){
    //     try{
    //     connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY name ASC `, async (error, rows, fields)=>{
    //     if(error){
    //     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    //     connection.destroy();
    //     resolve(error);
    //     }else{ 
    //     if(rows.length > 0){
    //     connection.destroy();
    //     // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    //     // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    //     resolve({
    //     "success":true,
    //     "message":"Party data fetched successfully",
    //     // "data":encriptedData,
    //     "data":rows,
    //     })
    //     }else{
    //     connection.destroy();
    //     resolve({
    //     "success":false,
    //     "message":"No data found"
    //     })
    //     }
    //     }
    //     })
    //     }catch(e){
    //     connection.destroy();
    //     console.log(e)
    //     resolve({
    //     "success":false,
    //     "message":"Something went wrong"
    //     })
    //    }
    //    }else{
    //    resolve({"success": false, "message": "unknown db"});
    //    }
    //  })
    // }



function fetchSpecificPartyPaymentDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
  //  var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
     var database_code = body.database_code
     var payment_code = body.party_payment_code 
     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
     if(connection != false){
     try{
      connection.query(`SELECT * FROM party_payments WHERE payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);
        connection.destroy();
        resolve(error);
        }else{ 
        if(rows.length > 0){
        var paymentData = [];
        var party_name = '';
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[0].payment_code)
        if(payment_details != null){
        var item = {}
        item.party_code = rows[0].party_code
        item.party_name = party_name
        item.code = rows[0].payment_code
        item.bill_no = payment_details.bill_no,
        item.bill_date = payment_details.bill_date,
        item.from_date = payment_details.from_date,
        item.to_date = payment_details.to_date,
        item.amount = payment_details.amount,
        item.purpose = payment_details.purpose,
        item.payment_type = payment_details.payment_type,
        item.ref_no = payment_details.ref_no,
        item.remarks = payment_details.remarks,
        item.image_url = payment_details.image_url_1,
        item.thumb_image_url = payment_details.image_url_1_thumbnail;
        item.created = payment_details.created;
        // item.image_url_2 = payment_details.	image_url_2,
        // item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;

        // let compressData = await commonFunctionWeb.compressDataFunction(item)
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
        connection.destroy();
        resolve({
        "success":true,
        "message":"Party payment data fetched successfully",
        "payments_data":item,
        // "data":encriptedData
        })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Party payment data not fetched",
        })
        }
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Party payment data not fetched",
        })
        }
      }
     })
     }catch(e){
      console.log(e)
      connection.destroy();
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    }else{
    resolve({"success": false, "message": "unknown db"});
    }
  })
}


function searchPartyPaymentDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    var offset = body.last_index_code
    var limit = commonFunctionAndroid.limit
    var database_code = body.database_code
    var keyword  = body.search_text
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT DISTINCT(party_payments.payment_code) FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code INNER JOIN parties ON parties.code = party_payments.party_code  WHERE party_payments.status <> 2 AND payments.status <> 2 AND (payments.bill_no LIKE '%${keyword}%' OR payments.amount LIKE '%${keyword}%' OR payments.purpose LIKE '%${keyword}%' OR payments.payment_type LIKE '%${keyword}%' OR  payments.ref_no LIKE '%${keyword}%' OR payments.remarks LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%')   ORDER BY party_payments.created DESC LIMIT ${limit} OFFSET ${offset}`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
          if(rows.length > 0){
            var paymentData = [];
            for(var i =0;i<rows.length;i++){
              let specificpaymentDetails = await commonFunctionAndroid.fetchSpecificPartyPaymentDetails(connection ,rows[i].payment_code)
              if(specificpaymentDetails != null){
                paymentData.push(specificpaymentDetails)
              }
            }
            if(paymentData.length > 0){
              connection.destroy()
              resolve({
                "success":true,
                "message":"Payment data fetched successfully",
                "payments_data":paymentData
              })
            }else{
              connection.destroy()
              resolve({
                "success":false,
                "message":"No data found"
              })  
            }
          }else{
            connection.destroy()
            resolve({
            "success":false,
            "message":"No data found"
            })
          }
       }
    })
    }catch(e){
      console.log(e)
      connection.destroy()
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    }else{
    resolve({"success": false, "message": "unknown db"});
    }
  })
 }







 
function filterPartyPaymentDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var month = body.month_value
    var year = body.year_value
    var finalString = year+"-"+month
    var limit = 30
    var offset = body.last_index_code


    var keyword = ""
    var query = "";
    // var queryTotalCount = ""
    if(body.keyword != undefined){
      var keyword = body.keyword
      query = `SELECT DISTINCT(party_payments.payment_code) FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code INNER JOIN parties ON parties.code = party_payments.party_code  WHERE party_payments.status <> 2 AND payments.status <> 2 AND payments.bill_date LIKE '%${finalString}%' AND (payments.bill_no LIKE '%${keyword}%' OR payments.amount LIKE '%${keyword}%' OR payments.purpose LIKE '%${keyword}%' OR payments.payment_type LIKE '%${keyword}%' OR  payments.ref_no LIKE '%${keyword}%' OR payments.remarks LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%')   ORDER BY payments.bill_date DESC LIMIT ${limit} OFFSET ${offset}`
      // queryTotalCount = 
    }else{
      query = `SELECT DISTINCT (party_payments.payment_code) FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code   WHERE party_payments.status != 2 AND payments.bill_date LIKE '%${finalString}%' ORDER BY payments.bill_date DESC limit ${limit} OFFSET ${offset}`
      // queryTotalCount = 
    }


    connection.query(query, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
      var paymentData = [];
      for(var i =0;i<rows.length;i++){
        let specificpaymentDetails = await commonFunctionAndroid.fetchSpecificPartyPaymentDetails(connection ,rows[i].payment_code)
        if(specificpaymentDetails != null){
          paymentData.push(specificpaymentDetails)
        }
      }
      if(paymentData.length > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Payment data fetched successfully",
        "payments_data":paymentData
        })
      }else{
        connection.destroy()
        resolve({
        "success":false,
        "message":"No data found"
       })  
     }
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })    
     }
    }catch(e){

    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function fetchAllPartyType(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code, label FROM party_type WHERE status != 2 ORDER BY code ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    resolve({
    "success":true,
    "message":"Party Type fetched successfully",
    // "data":encriptedData,
    "party_type_data":rows,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}



function fetchDistinctYearFromPartyPayments(req,res){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = req.params.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    // var limit = commonFunctionAndroid.limit;
    // var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT DISTINCT(YEAR(payments.bill_date)) As year   FROM payments INNER JOIN party_payments ON payments.code = party_payments.payment_code  WHERE payments.status != 2 `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      var yearList = []
      if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
          if(rows[i].year != 0)
          yearList.push(rows[i].year)
        }

        connection.destroy();

        resolve({
        "success":true,
        "message":"Year Data Fetched Successfully",
        "data":yearList
        })
      }else{
        yearList.push((new Date()).getFullYear())

        connection.destroy();

        resolve({
          "success":true,
          "message":"Year Data Fetched Successfully",
          "data":yearList
        })
      }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}




module.exports = partyPaymentModel;
