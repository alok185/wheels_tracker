var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');

var autoPartsModel = {
  addNewAutoPartChallanForAndroid: addNewAutoPartChallanForAndroid,
  fetchAllAutoPartChallanForAndroid:fetchAllAutoPartChallanForAndroid,
  updateAutoPartChallanForAndroid:updateAutoPartChallanForAndroid,
  deleteSpecificAutoPartChallanDetailsForAndroid:deleteSpecificAutoPartChallanDetailsForAndroid,
  // deleteMultipleAutoPartsChallanDetails:deleteMultipleAutoPartsChallanDetails,
  fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid:fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid,
  // undoDeleteMultipleAutoPartsChallanDetails:undoDeleteMultipleAutoPartsChallanDetails,
  filterAutoPartsDataForAndroid :filterAutoPartsDataForAndroid,
  searchAutoPartsDataForAndroid:searchAutoPartsDataForAndroid,
  fetchSpecificAutoPartChallanDetailsForAndroid:fetchSpecificAutoPartChallanDetailsForAndroid,
  fetchDistinctYearFromAutoParts:fetchDistinctYearFromAutoParts
};


function addNewAutoPartChallanForAndroid(body){
    return new Promise(async function(resolve, reject){
    //  var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`INSERT INTO auto_parts SET challan_no='${body.challan_no}',party_code = '${body.party_code}',vehicle_code='${body.vehicle_code}',amount = '${body.amount}',bill_date = '${body.bill_date}',	remarks='${body.remarks}',created_by='${body.database_code}'`,async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){
      // let autoPartsData = await commonFunctionWeb.fetchSpecificAutoPartChallanDetails(connection,rows.insertId);
       // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>autoPartsData",autoPartsData)
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(autoPartsData);
     // let addDataInLedger = await commonFunctionWeb.insertDataInLedger(connection,body.party_code,"Some remarks")
      connection.destroy();
      resolve({
      "success":true,
      "message":"Auto part challan added",
      // "data":encriptedData,
      // "auto_parts_data":autoPartsData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Auto part challan not added"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
  })
 }



function fetchAllAutoPartChallanForAndroid(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var offset = body.last_index_code
    var limit = commonFunctionAndroid.limit
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM auto_parts WHERE status != 2 ORDER BY created DESC LIMIT ${limit} Offset ${offset} `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      var autoPartsData = [];
      for(var i =0;i<rows.length;i++){
        var item = {};
        var party_name = ''
        var vehicle_label = ''
        var  vehicle_no = ''
        var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        item.auto_part_code = rows[i].code,
        item.challan_no = rows[i].challan_no,
        item.party_code = rows[i].party_code,
        item.party_name = party_name,
        item.vehicle_code = rows[i].vehicle_code,
        item.vehicle_label = vehicle_label,
        item.vehicle_number = vehicle_no,
        item.amount = rows[i].amount,
        item.bill_date = rows[i].bill_date,
        item.remarks = rows[i].remarks,
        item.created = rows[i].created
        // item.selected = false
        autoPartsData.push(item);
      }
      if(autoPartsData.length > 0){

        var autoPartItems = {
          "success":true,
          "message":"Auto part data fetched successfully",
          "auto_parts_data":autoPartsData
          };

        if(offset <= 0){
        autoPartItems["count"] = await fetchTotalAutoPartCount(connection,`SELECT COUNT(*) AS total_count FROM auto_parts WHERE status != 2`)
        }

      connection.destroy();
      // let compressData = await commonFunctionWeb.compressDataFunction(autoPartsData)
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve(autoPartItems)
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })  
      }
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
    }
  })
  }





function updateAutoPartChallanForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE auto_parts SET challan_no='${body.challan_no}',party_code = '${body.party_code}',vehicle_code='${body.vehicle_code}',amount = '${body.amount}',bill_date = '${body.bill_date}',	remarks='${body.remarks}',created_by='${body.database_code}' WHERE code ='${body.auto_part_code}'`,async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    // let autoPartsData = await commonFunctionWeb.fetchSpecificAutoPartChallanDetails(connection,body.auto_part_code);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>autoPartsData",autoPartsData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(autoPartsData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Auto part challan data updated successfully",
    // "data":encriptedData,
    // "auto_parts_data":autoPartsData
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Auto part challan data not updated"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })
}


function deleteSpecificAutoPartChallanDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE auto_parts SET status = 2 WHERE code = '${body.auto_part_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Auto part challan data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Auto part challan data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
    }
  })
  }




  // function deleteMultipleAutoPartsChallanDetails(bodyEncripted){
  //   return new Promise(async function(resolve, reject){
  //     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  //     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
  //     if(connection != false){
  //     try{
  //     connection.query(`UPDATE auto_parts SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
  //     if(error){
  //     connection.destroy();
  //     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  //     resolve(error);
  //     }else{ 
  //     if(rows.affectedRows  > 0){
  //     connection.destroy()
  //     resolve({
  //     "success":true,
  //     "message":"Auto part challan data deleted successfully"
  //     })
  //     }else{
  //     connection.destroy()      
  //     resolve({
  //     "success":false,
  //     "message":"Auto part challan data not deleted"
  //     })
  //     }
  //     }
  //     })
  //     }catch(e){
  //     connection.destroy()
  //     console.log(e)
  //     resolve({
  //     "success":false,
  //     "message":"Something went wrong"
  //     })
  //     }
  //     }else{
  //     reject({"success": false, "message": "unknown db"});    
  //    }
  //   })
  //  }
  
  

   function fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid(req,res){
    return new Promise(async function(resolve, reject){
      var database_code = req.params.code
      //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
      if(connection != false){
      try{
      let party_list = await fetchAllPartyList(connection);
      let vehicle_list = await fetchAllVehicle(connection);
      // if(party_list.length != 0 || vehicle_list.length != 0){
      var item = {"party_list":party_list,"vehicle_list":vehicle_list}
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(item);
      connection.destroy();
      resolve({
      "success":true,
      "message":"Data fetched sucessfully1111111",
      "data": item,
      })
      // }else{
      // connection.destroy();
      // resolve({
      // "success":false,
      // "message":"No data found"
      // })
      // }
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      } 
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
  }
  
  function fetchAllPartyList(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name,mobile,address FROM parties WHERE status != 2 AND party_type_code = 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        //connection.destroy()
        resolve(error);
        }else{ 
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
       // connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }


    
function fetchAllVehicle(connection){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,label FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    resolve(error);
    }else{ 
    //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
    if(rows.length > 0){
    //connection.destroy();
    resolve(rows)
    }else{
    //connection.destroy();
    resolve([])
    }
    }
    })
    }catch(e){
    // connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
  })
}


// function undoDeleteMultipleAutoPartsChallanDetails(bodyEncripted){
//   return new Promise(async function(resolve, reject){
//     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
//     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
//     if(connection != false){
//     try{
//     connection.query(`UPDATE auto_parts SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
//     if(error){
//     connection.destroy();
//     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
//     resolve(error);
//     }else{ 
//     if(rows.affectedRows  > 0){
//     connection.destroy()
//     resolve({
//     "success":true,
//     "message":"Auto part challan data undo successfully"
//     })
//     }else{
//     connection.destroy()      
//     resolve({
//     "success":false,
//     "message":"Auto part challan data not undo"
//     })
//     }
//     }
//     })
//     }catch(e){
//     connection.destroy()
//     console.log(e)
//     resolve({
//     "success":false,
//     "message":"Something went wrong"
//     })
//     }
//     }else{
//     reject({"success": false, "message": "unknown db"});    
//    }
//   })
// }


function filterAutoPartsDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var month = body.month_value
    var year = body.year_value
    var finalString = year+"-"+month
    var limit = commonFunctionAndroid.limit
    var offset = body.last_index_code
    
    var keyword = ""
    var query = "";
    var queryTotalCount = ""
    if(body.keyword != undefined){
      var keyword = body.keyword
      query = `SELECT DISTINCT(auto_parts.code) FROM auto_parts INNER JOIN parties ON parties.code = auto_parts.party_code WHERE auto_parts.status != 2 AND auto_parts.bill_date LIKE '%${finalString}%' AND (parties.name LIKE '%${keyword}%' || challan_no LIKE '%${keyword}%' || amount LIKE '%${keyword}%') ORDER BY auto_parts.created DESC LIMIT ${limit} Offset ${offset} `
      queryTotalCount = `SELECT DISTINCT COUNT(auto_parts.code) AS total_count FROM auto_parts INNER JOIN parties ON parties.code = auto_parts.party_code WHERE auto_parts.status != 2 AND auto_parts.bill_date LIKE '%${finalString}%' AND (parties.name LIKE '%${keyword}%' || challan_no LIKE '%${keyword}%' || amount LIKE '%${keyword}%') ORDER BY auto_parts.created DESC`
    }else{
      query = `SELECT DISTINCT(auto_parts.code) FROM auto_parts WHERE status != 2 AND bill_date LIKE '%${finalString}%' ORDER BY created DESC LIMIT ${limit} Offset ${offset} `
      queryTotalCount = `SELECT DISTINCT COUNT(auto_parts.code) AS total_count FROM auto_parts WHERE status != 2 AND bill_date LIKE '%${finalString}%'`
    }
    
    connection.query(query, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      if(rows.length > 0){
        var autoPartsData = [];
        for(var i =0;i<rows.length;i++){
          let specificAutoParts = await commonFunctionAndroid.fetchSpecificAutoPartChallanDetails(connection,rows[i].code)
          if(specificAutoParts != null){
          autoPartsData.push(specificAutoParts);
          }
        }
        if(autoPartsData.length > 0){
          var autoPartItems = {
            "success":true,
            "message":"Auto part data fetched successfully",
            "auto_parts_data":autoPartsData,
            }
          if(offset <= 0){
          autoPartItems["count"] = await fetchTotalAutoPartCount(connection,queryTotalCount)
          }
  
        connection.destroy();
        // let compressData = await commonFunctionWeb.compressDataFunction(autoPartsData)
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
  
        resolve(autoPartItems)
        
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })  
        }
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"No data found"
        })
        } 
    }
  })
  }catch(e){
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
  }
  }else{
   resolve({"success": false, "message": "unknown db"});      
  }
 })
}

  

function fetchTotalAutoPartCount(connection,condition){
  return new Promise(async function(resolve, reject){
    connection.query(condition, async (error, rowsCount, fields)=>{
      if(error){
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
        // console.log("****************************************",rowsCount)
        resolve(rowsCount[0].total_count)
      }
    })
  })
}


function searchAutoPartsDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var offset = body.last_index_code
    var limit = commonFunctionAndroid.limit
    var keyword = body.keyword
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT DISTINCT(auto_parts.code) FROM auto_parts INNER JOIN parties ON parties.code = auto_parts.party_code WHERE auto_parts.status != 2 AND (parties.name LIKE '%${keyword}%' || challan_no LIKE '%${keyword}%' || amount LIKE '%${keyword}%') ORDER BY auto_parts.created DESC LIMIT ${limit} Offset ${offset} `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      var autoPartsData = [];
      for(var i =0;i<rows.length;i++){
        let specificAutoParts = await commonFunctionAndroid.fetchSpecificAutoPartChallanDetails(connection,rows[i].code)
        if(specificAutoParts != null){
        autoPartsData.push(specificAutoParts);
        }
      }
      if(autoPartsData.length > 0){
        var autoPartItems = {
          "success":true,
          "message":"Auto part data fetched successfully",
         "auto_parts_data":autoPartsData,
          }
        if(offset <= 0){
        autoPartItems["count"] = await fetchTotalAutoPartCount(connection,`SELECT COUNT(auto_parts.code) AS total_count FROM auto_parts INNER JOIN parties ON parties.code = auto_parts.party_code WHERE auto_parts.status != 2 AND (parties.name LIKE '%${keyword}%' || challan_no LIKE '%${keyword}%' || amount LIKE '%${keyword}%')`)
        }

      connection.destroy();
      // let compressData = await commonFunctionWeb.compressDataFunction(autoPartsData)
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

      resolve(autoPartItems)
      
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })  
      }
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
    }
  })
}


function fetchSpecificAutoPartChallanDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var auto_part_code = body.auto_part_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM auto_parts WHERE code = '${auto_part_code}' `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
      var autoPartsData = [];
        var item = {};
        var party_name = ''
        var vehicle_label = ''
        var  vehicle_no = ''
        var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[0].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        item.auto_part_code = rows[0].code,
        item.challan_no = rows[0].challan_no,
        item.party_code = rows[0].party_code,
        item.party_name = party_name,
        item.vehicle_code = rows[0].vehicle_code,
        item.vehicle_label = vehicle_label,
        item.vehicle_number = vehicle_no,
        item.amount = rows[0].amount,
        item.bill_date = rows[0].bill_date,
        item.remarks = rows[0].remarks,
        item.created = rows[0].created
        // item.selected = false

        connection.destroy();


        resolve({ 
        "success":true,
        "message":"Auto part data fetched successfully",
        "auto_parts_data":item
        })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
    }
    }else{
    reject({"success": false, "message": "unknown db"});
    }
  })
}


function fetchDistinctYearFromAutoParts(req,res){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = req.params.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    // var limit = commonFunctionAndroid.limit;
    // var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT DISTINCT(YEAR(bill_date)) As year FROM auto_parts WHERE status != 2 `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      var yearList = []
      if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
          if(rows[i].year != 0)
          yearList.push(rows[i].year)
        }

        connection.destroy();

        resolve({
        "success":true,
        "message":"Year Data Fetched Successfully",
        "data":yearList
        })
      }else{
        yearList.push((new Date()).getFullYear())

        connection.destroy();
        
        resolve({
          "success":true,
          "message":"Year Data Fetched Successfully",
          "data":yearList
        })
      }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}


module.exports = autoPartsModel;
