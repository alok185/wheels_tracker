
var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');
var DateDiff = require('date-diff');


var driverReportModel = {
    fetchAllDriverReportForWeb:fetchAllDriverReportForWeb,
    fetchAllNecesaryDataForFilteringDriverReportForWeb:fetchAllNecesaryDataForFilteringDriverReportForWeb,
};
 



function fetchAllNecesaryDataForFilteringDriverReportForWeb(req,res){
    return new Promise(async function(resolve, reject){
        var database_code = req.params.code
        // console.log(req.params)
        //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        let vehicle_list = await fetchAllVehicle(connection);
        let driver_list = await fetchAllDriver(connection);
        var item = {"driver_list":driver_list,"vehicle_list":vehicle_list}
        connection.destroy()
        // let compressData = await commonFunctionWeb.compressDataFunction(item)
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        resolve({
        "success":true,
        "message":"Data fetched successfully",
        "data":item,
        // "data":encriptedData
        })
        }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
      }
    })
   }



function fetchAllVehicle(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,label,vehicle_no FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
        //connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }
  
  

    function fetchAllDriver(connection){
        return new Promise(async function(resolve, reject){
            try{
            connection.query(`SELECT code,name,mobile,profile_image_url,profile_image_url_thumbnail FROM drivers WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
            if(error){
            resolve(error);
            }else{ 
            //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
            if(rows.length > 0){
            resolve(rows)
            }else{
            resolve([])
            }
            }
            })
            }catch(e){
            console.log(e)
            resolve([])
           }
         })
        }
      







function fetchAllDriverReportForWeb(body){
    return new Promise(async function(resolve, reject){
        // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){

        var driver_code = body.driver_code
        var vehicle_code = body.vehicle_code
        
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var attendance_message = "";
        var trips_count = [];
        var fuelEntryChallan = [];
        let billtyData = [];
        let autPartChallan = [];
        let driver_salary = 0
        var absent_count = 0
        var driverBilltyData = [];
        let driverDetails = null
        if(driver_code != 0 && vehicle_code == 0){
        billtyData = await fetchWhenDriverIsSelected(connection,from_date,to_date,body.driver_code)
        trips_count = await fetchTripCountOfVehicle(connection,from_date,to_date,body.driver_code)
        driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection, body.driver_code)
        
        if(driverDetails != null)
        driver_salary = driverDetails.salary

        absent_count = await fetchAbsentCountOfDriver(connection,from_date,to_date,body.driver_code)

        var date1 = new Date(to_date); // 2015-12-1
        var date2 = new Date(from_date); // 2014-01-1
        var diff = new DateDiff(date1, date2).days()
        // console.log("diff=======================>>",diff)
        var attendance_message =  await fetchCountOfAttendance(connection,from_date,to_date,body.driver_code) < diff ? "Attendance data is incomplete. So Salary amount may be affected. Please fill attendance completely between this date range first.":""

        }else if(driver_code == 0 && vehicle_code != 0){

        billtyData = await fetchAllDataWhenOnlyVehicleIsSelected(connection,from_date,to_date,body.vehicle_code)
        fuelEntryChallan = await fechFuelEntryChallanOfVehicle(connection,from_date,to_date,body.vehicle_code)
        autPartChallan = await fetchAutoPartChallanOfVehicle(connection,from_date,to_date,body.vehicle_code)
        driverBilltyData = await fetchDriverbilltyData(connection,from_date,to_date,body.vehicle_code)
        
        }else if(driver_code != 0 && vehicle_code != 0){
        billtyData = await fetchAllDataWhenBothDataIsSelected(connection,from_date,to_date,body.driver_code,body.vehicle_code)
        }else{
            resolve({
                "success":false,
                "message":"Vehicle Or Party Code Is Required"
            })
        }
        // console.log("driver_salary=>>>",driver_salary)
        // var item = {"billty_data":billtyData,"fuel_entry_challan":fuelEntryChallan,"auto_part_challan":autPartChallan,"driver_salary":driver_salary,"trips_count":trips_count}
       
        connection.destroy();
        
        var driverReportItem = {};
        if(driver_code != 0){
            driverReportItem = {
                "success":true,
                "message":"Driver Report fetched successfully",
                "billty_data":billtyData,
                "trips_count":trips_count,
                "absent_count":absent_count,
                "attendance_message":attendance_message,
                "driver_salary":driverDetails != null ? driverDetails.salary: 0
                // "data":encriptedData,
                // "data":compressData
                }
        }else if(vehicle_code != 0){
            driverReportItem = {
                "success":true,
                "message":"Driver Report fetched successfully",
                "billty_data":billtyData,
                "fuel_entry_data":fuelEntryChallan,
                "driver_billty_data":driverBilltyData,
                "auto_parts_data":autPartChallan
                }
        }else{
            resolve({
            "success":false,
            "message":"Driver code Or Vehicle code is required"
            })
        }

        resolve(driverReportItem)
        }else{
        resolve({
        "success":false,
        "message":"Unknown db"
        })
       }  
      })
     }



function fetchWhenDriverIsSelected(connection,from_date,to_date,driver_code){
    return new Promise(async function(resolve, reject){
        var startDateAndEndDateArray = await getDateArray(new Date(from_date),new Date(to_date))
        var finalBilltyData = [] 
        for(var j =0;j<startDateAndEndDateArray.length;j++){
            var date = startDateAndEndDateArray[j]
            let billtyData = await fetchBiltyDataDateWise(connection,date.toISOString().split("T")[0],driver_code)
            if(billtyData.length > 0){
                for(var i =0;i<billtyData.length;i++){
                    finalBilltyData.push(billtyData[i])
                }
            }
         }
         resolve(finalBilltyData)
      })
    }


function fetchBiltyDataDateWise(connection,date,driver_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT * FROM billty WHERE status != 2 AND bill_date = '${date}'AND driver_code = '${driver_code}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
            var billtyData = [];
            console.log(`SELECT * FROM billty WHERE status != 2 AND bill_date = '${date}'AND driver_code = '${driver_code}' ORDER BY bill_date ASC `,"\n\n ROWSSSSSSSSSSSSSSSSSSSSSSS",rows)
            if(rows.length > 0){
            for(var i = 0;i<rows.length;i++){
                // console.log("rows[iiiiii==>>].bill_date",rows[i].bill_date,"==",startDateAndEndDateArray[j].toISOString().split("T")[0],"RRRESSSSULLTT",rows[i].bill_date == startDateAndEndDateArray[j].toISOString().split("T")[0])
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var driver_salary = 0
                    var driver_commission = 0
                    var route_details = {};
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    route_details = routeDetails
                    driver_commission = routeDetails.driver_commission
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    driver_salary = driverDetails.salary
                    }
                    var item = {}
                    // item.slno = j+1
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.is_absent = 0
                    item.is_trip = 1
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,
                // item.route_details = route_details
                    item.route_code = rows[i].route_commission_code
                    item.route_from = routeDetails.mine_name
                    item.route_to = routeDetails.party_name
                    item.vehicle_code = rows[i].vehicle_code
                    item.vehicle_label = vehicle_label
                    item.vehicle_no = vehicle_no
                    item.driver_code = rows[i].driver_code
                    // item.driver_name = driver_name
                    // item.driver_salary = driver_salary
                    item.driver_commission = driver_commission
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    item.freight_rate = rows[i].freight_rate
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    // item.created = rows[i].created
                    // item.selected = false
                    billtyData.push(item)
              }
            resolve(billtyData)
            }else{
            var attendanceData = await checkDriverWasPresentInThatDate(connection,date,driver_code)
            // console.log("attendanceData",attendanceData)
            var item = {}
            // item.slno = j+1
            // item.billty_no = attendanceData.label // attendanceData.value = 1 = present, 0 = absent
            item.is_absent = attendanceData.value == 1 ? 0 : 1 // is_absent 0 = present 1 = absent
            item.is_trip = "0"
            // item.party_name = ""
            item.bill_date = date
        
            billtyData.push(item)
            resolve(billtyData)
            }
            }catch(e){
            // connection.destroy();
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
      }


function getDateArray (start, end) {
        var arr = new Array();
        var dt = new Date(start);
        while (dt <= end) {
        arr.push(new Date(dt));
        dt.setDate(dt.getDate() + 1);
        }
     return arr;
   } 



function fetchAllDataWhenOnlyVehicleIsSelected(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT * FROM billty WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
            // console.log(rows)
            if(rows.length > 0){
            var billtyData = [];
            for(var i = 0;i<rows.length;i++){
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var route_details = {};
                    var expense = 0
                    var short_rate = 0
                    var freight_rate = 0
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    route_details = routeDetails
                    expense = route_details.expense
                    short_rate = routeDetails.short_rate
                    freight_rate = routeDetails.freight_rate
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    }
                    var item = {}
                    // item.slno = rows[i].slno
                    item.code = rows[i].code
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,

                    item.route_commision_code = rows[i].route_commission_code
                    


                    item.route_code = rows[i].route_commission_code
                    item.route_from = routeDetails.mine_name
                    item.route_to = routeDetails.party_name
                    item.expense = expense
                    item.freight_rate = freight_rate
                    item.short_rate = short_rate

                    item.vehicle_code = rows[i].vehicle_code
                    item.vehicle_label = vehicle_label
                    item.vehicle_no = vehicle_no
                    item.driver_code = rows[i].driver_code
                    item.driver_name = driver_name
                    // item.driver_mobile = driver_mobile
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    item.fuel_rate = await fetchFuelRateOfVehicle(connection,rows[i].bill_date,rows[i].vehicle_code)
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    // item.created = rows[i].created
                    // item.selected = false
                    billtyData.push(item)
                }
            if(billtyData.length > 0){
            // connection.destroy();
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }else{
            resolve([])
            }
            }catch(e){
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
       }



function fetchAllDataWhenBothDataIsSelected(connection,from_date,to_date,driver_code,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT * FROM billty WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' AND driver_code = '${driver_code}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
            // console.log(rows)
            if(rows.length > 0){
            var billtyData = [];
            for(var i = 0;i<rows.length;i++){
                    var vehicle_label = '';
                    var party_name = "";
                    var vehicle_no = ''
                    var driver_name = ''
                    var mine_name = '';
                    var driver_mobile = '';
                    var route_details = {};
                    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
                    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
                    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
                    if(routeDetails != null){
                    route_details = routeDetails
                    }
                    if(vehicle_details != null){
                    vehicle_label = vehicle_details.label
                    vehicle_no = vehicle_details.vehicle_no
                    }
                    if(party_details != null){
                    party_name = party_details.name
                    }
                    if(driverDetails != null){
                    driver_name = driverDetails.name
                    driver_mobile = driverDetails.driver_mobile
                    }
                    var item = {}
                    // item.slno = rows[i].slno
                    item.code = rows[i].code
                    item.billty_no = rows[i].billty_no
                    item.party_code = rows[i].party_code
                    item.party_name = party_name
                    item.bill_date = rows[i].bill_date,
                // item.route_details = route_details
                    item.route_code = rows[i].route_commission_code

                    item.route_from = routeDetails.mine_name
                    item.route_to = routeDetails.party_name
                    item.vehicle_code = rows[i].vehicle_code
                    // item.vehicle_label = vehicle_label
                    // item.vehicle_no = vehicle_no
                    // item.driver_code = rows[i].driver_code
                    // item.driver_name = driver_name
                    // item.driver_mobile = driver_mobile
                    item.advance = rows[i].advance
                    // item.remarks = rows[i].remarks,
                    // item.freight_rate = rows[i].freight_rate
                    item.challan_weight = rows[i].challan_weight
                    item.diesel = rows[i].diesel,
                    item.received_weight = rows[i].received_weight,
                    item.short_weight = rows[i].short_weight,
                    // item.image_url_1 = rows[i].image_url_1,
                    // item.image_url_2 = rows[i].	image_url_2,
                    // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
                    // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
                    // item.created = rows[i].created
                    // item.selected = false
                    billtyData.push(item)
                }
            if(billtyData.length > 0){
            // connection.destroy();
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
            resolve(billtyData)
            }else{
            resolve([])    
            }
            }else{
            resolve([])
            }
            }catch(e){
            console.log(e)
            resolve([])    
           }    
          }
         })     
        })
      }



function checkDriverWasPresentInThatDate(connection, attendance_date,driver_code){
    return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM attendance  WHERE attendance.status != 2 AND attendance.attendance_date  = '${attendance_date}' AND driver_code = '${driver_code}' `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    resolve({"label":"No Trip","value":rows[0].attendance_value})
    }else{
    resolve({"label":"Absent","value":1})
    }
    }
    })
    }catch(e){
    console.log(e)
    resolve({"label":"False data","value":1})
    }
   })
}

function fechFuelEntryChallanOfVehicle(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT * FROM fuel_entry WHERE status != 2 AND bill_date BETWEEN '${from_date}' AND '${to_date}' AND vehicle_code = '${vehicle_code}' ORDER BY bill_date ASC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        var fuelEntryData = [];
        for(var i = 0;i<rows.length;i++){
        var vehicle_label = '';
        var party_name = "";
        vehicle_no = ''
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        var item = {}
        // item.slno = rows[i].slno
        item.code = rows[i].code
        item.challan_no = rows[i].challan_no
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.bill_date = rows[i].bill_date,
    
        item.vehicle_code = rows[i].vehicle_code
        // item.vehicle_label = vehicle_label
        // item.vehicle_no = vehicle_no
        item.rate = rows[i].fuel_rate
        item.quantity = rows[i].quantity
        item.remarks = rows[i].remarks,
        item.amount = rows[i].amount
        // item.image_url_1 = rows[i].image_url_1,
        // item.image_url_2 = rows[i].	image_url_2,
        // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
        // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
        item.created = rows[i].created
        fuelEntryData.push(item)
        }
        if(fuelEntryData.length > 0){
    
        resolve(fuelEntryData)
        }else{
        resolve([])    
        }
        }else{
        resolve([])
        }
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve([])    
        } 
        }
        })
        }catch(e){
        console.log(e)
        resolve([])
       }
     })
    }







function fetchAutoPartChallanOfVehicle(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
            connection.query(`SELECT * FROM auto_parts WHERE status != 2 AND vehicle_code = '${vehicle_code}' AND bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY bill_date ASC `, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            resolve(error);
            }else{ 
            if(rows.length > 0){
            var autoPartsData = [];
            for(var i =0;i<rows.length;i++){
            var item = {};
            var party_name = ''
            var vehicle_label = ''
            var  vehicle_no = ''
            var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
            if(party_details != null){
            party_name = party_details.name
            }
            var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
            if(vehicle_details != null){
            vehicle_label = vehicle_details.label
            vehicle_no = vehicle_details.vehicle_no
            }
            item.code = rows[i].code,
            item.challan_no = rows[i].challan_no,
            item.party_code = rows[i].party_code,
            item.party_name = party_name,
            item.vehicle_code = rows[i].vehicle_code,
            // item.vehicle_label = vehicle_label,
            // item.vehicle_no = vehicle_no,
            item.amount = rows[i].amount,
            item.bill_date = rows[i].bill_date,
            item.remarks = rows[i].remarks,
            // item.created = rows[i].created
            autoPartsData.push(item);
            }
            if(autoPartsData.length > 0){
            resolve(autoPartsData)
            }else{
            resolve([])  
            }
            }else{
            resolve([])
            }
            }
           })
          })
         }

//
function fetchTripCountOfVehicle(connection,from_date,to_date,driver_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT route_commission_code, COUNT(*) AS trips_count FROM billty WHERE billty.status<>2 AND billty.driver_code = '${driver_code}' AND billty.bill_date>='${from_date}' AND billty.bill_date<='${to_date}' GROUP BY route_commission_code`, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
            try{
                var tripCount = [];
                for(var i =0;i<rows.length;i++){
                    var item = {};
                    var to_label = "";
                    var from_label = "";
                    var commision = 0;
                    let routeDetails = await commonFunctionAndroid.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
                    if(routeDetails != null){
                        to_label = routeDetails.party_name
                        from_label = routeDetails.mine_name
                        commision = routeDetails.commission
                    }

                    item.route_commision_code = rows[i].route_commission_code
                    item.trips_count = rows[i].trips_count
                    item.commision = commision
                    item.from_label = from_label
                    item.to_label = to_label
                    tripCount.push(item)
                }
                resolve(tripCount)    
            }catch(e){
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
          }
        }
      })
    })
   }


function fetchAbsentCountOfDriver(connection,from_date,to_date,driver_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT COUNT(*) as AbsentCount FROM attendance WHERE attendance_date>='${from_date}' AND attendance_date<='${to_date}' AND driver_code='${driver_code}' AND attendance_value = 0`, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
                // console.log("CCCCCCCCCCCCOUNTT",rows);
                if(rows.length > 0){
                resolve(rows[0].AbsentCount)
                }else{
                resolve(0)
                }
            }
        })
    })
}


function fetchCountOfAttendance(connection,from_date,to_date,driver_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT COUNT(*) AS total_attendance FROM attendance WHERE attendance_date>='${from_date}' AND attendance_date<='${to_date}' AND driver_code='${driver_code}' AND status<>2`, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
                // console.log("total_attendance",rows)
                if(rows.length > 0){
                resolve(rows[0].total_attendance)
                }else{
                resolve(0)
                }
            }
        })
    })
}
//removed this as providing connection of user_code in that ==> billty.created_by = ${user_code} AND
function fetchDriverbilltyData(connection,from_date,to_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT DISTINCT(billty.driver_code) , drivers.name AS driver_name , drivers.salary AS driver_salary FROM billty INNER JOIN drivers ON drivers.code = billty.driver_code WHERE  billty.status<>2  AND billty.vehicle_code = ${vehicle_code}  AND billty.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY billty.bill_date ASC`, async (error, rows, fields)=>{
            if(error){
            commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
            connection.destroy();
            resolve(error);
            }else{ 
                if(rows.length > 0){
                    for(var i =0;i<rows.length;i++){
                        rows[i].trips_count =  await fetchTripCountOfVehicle(connection,from_date,to_date,rows[i].driver_code);
                    }
                    resolve(rows)
                }else{
                    resolve([])
                }
            }
         })
     })
}


function fetchFuelRateOfVehicle(connection,bill_date,vehicle_code){
    return new Promise(async function(resolve, reject){
        connection.query(`SELECT fuel_rate FROM fuel_entry WHERE vehicle_code = '${vehicle_code}' AND bill_date = '${bill_date}' `, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        resolve(rows[0].fuel_rate);
        }else{
        resolve(0);
        }
        }catch(e){
        console.log(e)
        resolve(0)
       }
      }
     })
   })
  }


module.exports = driverReportModel;
