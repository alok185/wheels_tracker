var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;


var fuelEntryDieselModel = {
  addNewFuelEntryDieselRateForAndroid:addNewFuelEntryDieselRateForAndroid,
  fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid:fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid,
  updateFuelEntryDieselRateForAndroid:updateFuelEntryDieselRateForAndroid,
  fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid:fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid
  // deleteMultipleDieselRateDetails:deleteMultipleDieselRateDetails,
  // undoDeleteMultipleDieselRateDetails:undoDeleteMultipleDieselRateDetails
};


function addNewFuelEntryDieselRateForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`INSERT INTO diesel_rate SET party_code='${body.party_code}',entry_date = '${body.entry_date}',rate='${body.rate}',	created_by = '${body.database_code}'`, async(error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    // let fuelEntryDieselRate = await commonFunctionWeb.fetchSpecificFuelEntryDieselRateDetails(connection,rows.insertId);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",fuelEntryDieselRate)
    // let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryDieselRate)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Diesel rate added successfully",
    // "data":encriptedData,
    // "data":fuelEntryDieselRate
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Diesel rate not added"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })
}


function fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid(body){
  return new Promise(async function(resolve, reject){
  // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  var database_code = body.database_code 
  let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
  if(connection != false){
  try{
  var query = ""
  var month = body.month_value
  var year = body.year_value
  var finalString = year+"-"+month
  if(body.party_code != 0 && body.party_code != undefined ){
  query = `SELECT * FROM diesel_rate WHERE status != 2 AND party_code = '${body.party_code}' AND entry_date LIKE '%${finalString}%' ORDER BY entry_date ASC `;
  }else{
    resolve({
      "success":false,
      "message":"No Data Found"
    })
  }
  connection.query(query, async (error, rows, fields)=>{
  if(error){
  commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  connection.destroy();
  resolve(error);
  }else{ 
  if(rows.length > 0){
    var dieselRateData = [];
    
    for(var i =0 ; i<rows.length;i++){
      var party_name = '';
      var item = {};
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
      if(party_details != null){
      party_name = party_details.name
      }
      item.code = rows[i].code
      item.party_name = party_name
      item.party_code = rows[i].party_code
      item.entry_date = rows[i].entry_date
      item.rate = rows[i].rate
      item.created = rows[i].created
      dieselRateData.push(item)
    }
    
  if(dieselRateData.length > 0){

  connection.destroy();
  // let compressData = await commonFunctionWeb.compressDataFunction(dieselRateData)
  // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
  resolve({
  "success":true,
  "message":"Diesel rate data fetched successfully",
  // "data":encriptedData,
  "diesels_rate":dieselRateData,
  })
  }else{
  connection.destroy();
  resolve({
  "success":false,
  "message":"No data found"
  })  
  }
  }else{
  connection.destroy();
  resolve({
  "success":false,
  "message":"No data found"
  })
  }
  }
  })
  }catch(e){
  connection.destroy();
  console.log(e)
  resolve({
  "success":false,
  "message":"Something went wrong"
  })
 }
 }else{
 reject({"success": false, "message": "unknown db"});
 }
})
}



function updateFuelEntryDieselRateForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`Update diesel_rate SET party_code='${body.party_code}',entry_date = '${body.entry_date}',rate='${body.rate}',	created_by = '${body.database_code}' WHERE code = '${body.diesel_rate_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows > 0){
    // let fuelEntryDieselRate = await commonFunctionWeb.fetchSpecificFuelEntryDieselRateDetails(connection,body.diesel_rate_code);
    // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",fuelEntryDieselRate)

    // let compressData = await commonFunctionWeb.compressDataFunction(fuelEntryDieselRate)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    connection.destroy();
    resolve({
    "success":true,
    "message":"Diesel rate updated successfully",
    // "data":encriptedData
    // "data":fuelEntryDieselRate
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Diesel rate not updated"
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
  }
})
}



function fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid(body){
  return new Promise(async function(resolve, reject){
  try{
  var database_code = body.database_code
  // var fuel_entry_diesel_rate_code = body.diesel_rate_code
  var entry_date = body.entry_date
  var party_code = body.party_code
  let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
  if(connection != false){
  connection.query(`SELECT * FROM diesel_rate WHERE party_code = '${party_code}' AND entry_date = '${entry_date}' ORDER BY entry_date DESC `, async (error, rows, fields)=>{
  if(error){
  createErrorLogFile(error);sendErrorsInEmail(error);  
  connection.destroy();
  resolve(error);
  }else{ 
    // console.log(`SELECT * FROM diesel_rate WHERE code = '${fuel_entry_diesel_rate_code}' ORDER BY entry_date DESC `,rows)
  if(rows.length > 0){
    var party_name = '';
    var item = {};
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[0].party_code)
    if(party_details != null){
    party_name = party_details.name
    }
    item.code = rows[0].code
    item.party_name = party_name
    item.party_code = rows[0].party_code
    item.entry_date = rows[0].entry_date
    item.rate = rows[0].rate

    connection.destroy();

    resolve({
      "success":true,
      "message":"Fuel entry disel data fetched successfully",
      "data":item
      })
  }else{
    connection.destroy();
      resolve({
      "success":false,
      "message":"No data found"
      })
     }
   }
  })
  }else{
  resolve({"success": false, "message": "unknown db"});
  }
  }catch(e){
  connection.destroy();
  console.log(e)
  resolve({
    "success":false,
    "message":"Something went wrong"
  })
 }
})
}



module.exports = fuelEntryDieselModel;
