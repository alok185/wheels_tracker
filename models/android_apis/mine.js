var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');






var mineModel = {
  addNewMineForAndroid :addNewMineForAndroid,
  fetchAllMineDetailsForAndroid:fetchAllMineDetailsForAndroid,
  updateMineForAndroid:updateMineForAndroid,
  deleteSpecificMineDetailsForAndroid:deleteSpecificMineDetailsForAndroid,
  deleteMultipleMineDetails:deleteMultipleMineDetails,
  undoDeleteMultipleMineDetails:undoDeleteMultipleMineDetails
};


function addNewMineForAndroid(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      //console.log("CONNNNNNNNNNNNEEEEEEEEEEEEEECTION------------>>",connection)
      if(connection != false){
      try{
      connection.query(`INSERT INTO mines SET name='${body.name}',expense = '${body.expense}',address='${body.address}',created_by='${body.database_code}'`,async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){
      // let mineData = await commonFunctionWeb.fetchSpecificMineDetails(connection,rows.insertId);
      // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",mineData)
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(mineData);
      connection.destroy();
      resolve({
      "success":true,
      "message":"Mine added",
      // "data":mineData,
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Mine not added"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
      resolve({"success": false, "message": "unknown db"});
    }
  })
 }


function fetchAllMineDetailsForAndroid(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log(`SELECT * FROM parties WHERE status != 2 ORDER BY created DESC `)
    connection.query(`SELECT code,name,expense,address,created FROM mines WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":true,
      "message":"Mine data fetched successfully",
      // "data":encriptedData,
      "mines_data":rows,
      })
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Mine data not fetched",
      })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Mine data fetched successfully",
    // "data":encriptedData,
    "data":rows
    })
   }
   }else{
    resolve({"success": false, "message": "unknown db"});
   }
 })
}




function updateMineForAndroid(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE mines SET name='${body.name}',expense = '${body.expense}',address='${body.address}',created_by='${body.database_code}' WHERE code = '${body.mine_code}'`, async(error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){
      // let mineData = await commonFunctionWeb.fetchSpecificMineDetails(connection,body.mine_code);
      // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",mineData)
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(mineData);
      connection.destroy();
      resolve({
      "success":true,
      "message":"Mine details updated",
      // "data":encriptedData,
      // "data":mineData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Mine details not updated"
      })
      }
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     resolve({"success": false, "message": "unknown db"});
    }
  })
 }


function deleteSpecificMineDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE mines SET status = 2 WHERE code = '${body.mine_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Mine data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Mine data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
  }




  function deleteMultipleMineDetails(bodyEncripted){
    return new Promise(async function(resolve, reject){
     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE mines SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows  > 0){
      connection.destroy()
      resolve({
      "success":true,
      "message":"Mine data deleted successfully"
      })
      }else{
      connection.destroy()      
      resolve({
      "success":false,
      "message":"Mine data not deleted"
      })
      }
      }
      })
      }catch(e){
      connection.destroy()
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
    }
  
  

    function undoDeleteMultipleMineDetails(bodyEncripted){
      return new Promise(async function(resolve, reject){
       var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE mines SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows  > 0){
        connection.destroy()
        resolve({
        "success":true,
        "message":"Mine data undo successfully"
        })
        }else{
        connection.destroy()      
        resolve({
        "success":false,
        "message":"Mine data not undo"
        })
        }
        }
        })
        }catch(e){
        connection.destroy()
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
      })
      }
    



module.exports = mineModel;
