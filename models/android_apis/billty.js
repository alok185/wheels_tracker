var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var CronJob = require('cron').CronJob;
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');


var billtyModel = {
  addNewBilltyForAndroid:addNewBilltyForAndroid,
  fetchAllBilltiesForAndroid:fetchAllBilltiesForAndroid,
  updateBilltyDetailsForAndroid:updateBilltyDetailsForAndroid,
  deleteSpecificBilltyDetailsForAndroid:deleteSpecificBilltyDetailsForAndroid,
  // deleteMultipleBilltyDetails:deleteMultipleBilltyDetails,
  fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid:fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid,
  searchBilltyDataForAndroid:searchBilltyDataForAndroid,
  fetchSpecificBilltyDetailsForAndroid:fetchSpecificBilltyDetailsForAndroid,
  filterBilltyDataForAndroid:filterBilltyDataForAndroid,
  validateBilltyNo:validateBilltyNo,
  fetchBilltiesForVehicleDate:fetchBilltiesForVehicleDate,
  fetchDistinctYearFromBillties:fetchDistinctYearFromBillties
  // undoDeleteMultipleBilltyDetails:undoDeleteMultipleBilltyDetails
};


const storage = multer.diskStorage({
    destination: "./public/uploads/billty",
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
   }
 })

const upload = multer({ storage: storage}).any()


function addNewBilltyForAndroid(req,res){
    return new Promise(async function(resolve, reject){
      try{
        upload(req, res, async (err) => {        
            if (err) {
            commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
            console.log(err);
            }else{
            // var data =  await commonFunctionWeb.decriptDataFunction(JSON.parse(req.body.billty_data));
            var data =  await JSON.parse(req.body.postData);
            let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
            if(connection != false){
            var image1Location ='';
            var thumbImage1Location = '';
            var image2Location = '';
            var thumbImage2Location = '';
            // console.log("&&&&&&&&&&&&&+++++++++++++++++++++++++++&&&&&&&&&&&&&&&&&",req.files)
            try{

            if(req.files.length > 0){

                for(var i = 0;i<req.files.length;i++){
                // console.log("req.files[i].fieldname============>>",req.files[i].fieldname)
                if(req.files[i].fieldname == 'image'){
                image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
                var myFile = req.files[i];
                //for comprressing the images........................STARTS.............
                // Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
                // return image.quality(40)        // set JPEG quality
                //   .resize(256, 256)
                //   .write(myFile.destination+"/thumb/"+myFile.filename); // save
                //   }).catch(function (err) {
                //  console.log(err);
                // resolve({"success": false, "message": "Error"});
                // });

                thumbImage1Location ="http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");// "http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 

                }
                if(req.files[i].fieldname == 'image_1') {
                image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
                var myFile1 = req.files[i];
                //for comprressing the images........................STARTS.............
                // Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
                // return image.quality(40)        // set JPEG quality
                // .resize(256, 256)
                // .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
                // }).catch(function (err) {
                // console.log(err);
                // resolve({"success": false, "message": "Error"});
                // });
                thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");//"http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename;
                }
                }
            }
            try{
                connection.query(`INSERT INTO billty SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',billty_no = '${data.billty_no}',route_commission_code = '${data.route_commision_code}',bill_date = '${data.bill_date}',challan_weight = '${data.challan_weight}',received_weight = '${data.received_weight}',short_weight = '${data.short_weight}',advance='${data.advance}',diesel='${data.diesel}',party_code = '${data.party_code}',vehicle_code = '${data.vehicle_code}',driver_code ='${data.driver_code}',freight_rate = '${data.freight_rate}',remarks = '${data.remarks}',created_by = '${data.database_code}'`,async (error, rows, fields)=>{
                if(error){
                console.log(error)
                connection.destroy();
                commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
                resolve(error);
                }else{ 
                if(rows.affectedRows > 0){
                // let billtyData = await commonFunctionAndroid.fetchSpecificBilltyDetails(connection,rows.insertId);
                // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>partyData",billtyData)
                // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);

                connection.destroy();

                resolve({
                "success":true,
                "message":"Billty data added sucessfully",
                // "data":encriptedData,
                // "billty_data":billtyData,
                
                })
                }else{

                connection.destroy();

                resolve({
                "success":false,
                "message":"Billty data not added"
                })    
                }
                }
                })
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
            }
          }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
            "success":false,
            "message":"Something went wrong"
            })
          }
            }else{
            reject({"success": false, "message": "unknown db"});
            }
          }
        })
        }catch(e){
          console.log(e)
          resolve({
            "success":false,
            "message":"Something went wrong"
            })
        }
       })
      }


function fetchAllBilltiesForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    var limit = commonFunctionAndroid.limit;
    var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT * FROM billty WHERE status != 2 ORDER BY created DESC LIMIT ${limit} Offset ${offset}`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var billtyData = [];
    for(var i = 0;i<rows.length;i++){
    var vehicle_label = '';
    var party_name = "";
    var vehicle_no = ''
    var driver_name = ''
    var mine_name = '';
    var driver_mobile = '';
    var from_code = 0;
    var to_code = 0;
    var from_label = "";
    var to_label = ""
    var route_details = {};
    let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
    let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
    let routeDetails = await commonFunctionWeb.fetchSpecificRouteDetails(connection,rows[i].route_commission_code)
    let driverDetails = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code)
    if(routeDetails != null){
      from_code = routeDetails.mine_code
      to_code = routeDetails.party_code
      from_label = routeDetails.mine_name
      to_label = routeDetails.party_name
    }
    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    if(driverDetails != null){
      driver_name = driverDetails.name
      driver_mobile = driverDetails.driver_mobile
    }
    var item = {}
    item.billty_code = rows[i].code
    item["route_commision_code"] = rows[i].route_commission_code
    item.billty_no = rows[i].billty_no
    item.party_code = rows[i].party_code
    item.transporter_party_name = party_name
    item.bill_date = rows[i].bill_date,
    // item.route_details = route_details
    item.from_code = from_code
    item.to_code = to_code
    item.from_label = from_label
    item.to_label = to_label
    item.vehicle_code = rows[i].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.driver_code = rows[i].driver_code
    item.driver_name = driver_name
    item.driver_mobile = driver_mobile
    item.advance = rows[i].advance
    item.remarks = rows[i].remarks,
    item.freight_rate = rows[i].freight_rate
    item.challan_weight = rows[i].challan_weight
    item.diesel = rows[i].diesel,
    item.received_weight = rows[i].received_weight,
    item.short = rows[i].short_weight,
    item.image_url = rows[i].image_url_1,
    item.image_url_1 = rows[i].	image_url_2,
    item.thumb_url = rows[i].image_url_1_thumbnail,
    item.thumb_url_1 = rows[i].image_url_2_thumbnail,
    item.created = rows[i].created
    billtyData.push(item)
    }
    if(billtyData.length > 0){
    // let compressData = await commonFunctionWeb.compressDataFunction(billtyData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    var billtyItems = {
      "success":true,
      "message":"Billty data fetched successfully",
      "billty_data":billtyData
      }

      if(offset <= 0){
      var  count = await fetchTotalBilltyCount(connection,`SELECT COUNT(*) AS total_count FROM billty WHERE status != 2`)
      billtyItems.count = count
      }

    connection.destroy();
    resolve(billtyItems)
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}


function fetchTotalBilltyCount(connection,condition){
  return new Promise(async function(resolve, reject){
    connection.query(condition, async (error, rowsCount, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      // console.log("****************************************",rowsCount)
      resolve(rowsCount[0].total_count)
    }
  })
})
}




function updateBilltyDetailsForAndroid(req,res){
  return new Promise(async function(resolve, reject){
    upload(req, res, async (err) => {        
        if (err) {
        commonFunctionWeb.createErrorLogFile(err);commonFunctionWeb.sendErrorsInEmail(err);
        console.log(err);
        }else{
        try{
       var data =  await JSON.parse(req.body.postData); 
      // console.log("data++++++++++++",data)
      console.log("111111111111111111111111111111111111111111111111111111111111111111111111111111",req.files)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(data.database_code)
        if(connection != false){
        var image1Location = data.image_url;
        var thumbImage1Location = data.image_url_thumbnail != undefined ?data.image_url_thumbnail :"";
        var image2Location = data.image_url_1 != undefined ?data.image_url_1 : "";
        var thumbImage2Location = data.image_url_1_thumbnail != undefined ?data.image_url_1_thumbnail:"";
        if(req.files.length > 0){
        for(var i = 0;i<req.files.length;i++){
        if(req.files[i].fieldname == 'image'){
            image1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile = req.files[i];
            //for comprressing the images........................STARTS.............
            // Jimp.read(myFile.destination+"/"+myFile.filename).then(function (image) {
            // return image.quality(40)        // set JPEG quality
            // .resize(256, 256)
            // .write(myFile.destination+"/thumb/"+myFile.filename); // save
            // }).catch(function (err) {
            // console.log(err);
            // resolve({"success": false, "message": "Error"});
            // });
            thumbImage1Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/"); //"http://"+ip.address()+commonFunctionWeb.thumbPort+req.files[i].destination.replace("." ,"")+"/thumb/"+req.files[i].filename; 
        }
        if(req.files[i].fieldname == 'image_1') {
          
            image2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
            var myFile1 = req.files[i];
            //for comprressing the images........................STARTS.............
            // Jimp.read(myFile1.destination+"/"+myFile1.filename).then(function (image) {
            // return image.quality(40)        // set JPEG quality
            // .resize(256, 256)
            // .write(myFile1.destination+"/thumb/"+myFile1.filename); // save
            // }).catch(function (err) {
            // console.log("EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRORORO",err);
            // console.log("EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRORORO");
            // resolve({"success": false, "message": "Error"});
            // });
            thumbImage2Location = "http://"+ip.address()+commonFunctionWeb.port+req.files[i].path.replace(/\\/g ,"/");
           }
          }
         }
        try{
        console.log("222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
        connection.query(`UPDATE billty SET image_url_1 = '${image1Location}',image_url_1_thumbnail = '${thumbImage1Location}',image_url_2 ='${image2Location}',image_url_2_thumbnail = '${thumbImage2Location}',billty_no = '${data.billty_no}',route_commission_code = '${data.route_commision_code}',bill_date = '${data.bill_date}',challan_weight = '${data.challan_weight}',received_weight = '${data.received_weight}',short_weight = '${data.short_weight}',advance='${data.advance}',diesel='${data.diesel}',party_code = '${data.party_code}',vehicle_code = '${data.vehicle_code}',driver_code ='${data.driver_code}',freight_rate = '${data.freight_rate}',remarks = '${data.remarks}',created_by = '${data.database_code}' WHERE code = '${data.billty_code}'`, async(error, rows, fields)=>{
        console.log("33333333333333333333333333333333333333333333333333333333333333333333333333333333333")
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{
        console.log("44444444444444444444444444444444444444444444444444444444444444444444444")
        if(rows.affectedRows > 0){
        console.log("5555555555555555555555555555555555555555555555555555555555555")
        connection.destroy();
        resolve({
        "success":true,
        "message":"Billty data updated sucessfully",
        })
        }else{
        console.log("666666666666666666666666666666666666666666666666666666666666666666666");
        connection.destroy();
        resolve({
        "success":false,
        "message":"Billty data not updated"
        })    
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
        }
        }else{
        reject({"success": false, "message": "unknown db"});
       }
      }catch(e){
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
      }
      }
    })
   })
  }


function deleteSpecificBilltyDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`UPDATE billty SET status = 2 WHERE code = '${body.billty_code}'`, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.affectedRows  > 0){
    connection.destroy()
    resolve({
    "success":true,
    "message":"Billty data deleted successfully"
    })
    }else{
    connection.destroy()      
    resolve({
    "success":false,
    "message":"Billty data not deleted"
    })
    }
    }
    })
    }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    reject({"success": false, "message": "unknown db"});    
   }
  })
  }




  
  
    // function undoDeleteMultipleBilltyDetails(bodyEncripted){
    //   return new Promise(async function(resolve, reject){
    //       var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    //       let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    //       if(connection != false){
    //       try{
    //       connection.query(`UPDATE billty SET status = 0 WHERE code IN (${body.code})`, async (error, rows, fields)=>{
    //       if(error){
    //       connection.destroy();
    //       commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    //       resolve(error);
    //       }else{ 
    //       if(rows.affectedRows  > 0){
    //       connection.destroy();
    //       resolve({
    //       "success":true,
    //       "message":"Billty data undo successfully"
    //       })
    //       }else{
    //       connection.destroy()      
    //       resolve({
    //       "success":false,
    //       "message":"Billty data not undo"
    //       })
    //       }
    //       }
    //       })
    //       }catch(e){
    //       connection.destroy()
    //       console.log(e)
    //       resolve({
    //       "success":false,
    //       "message":"Something went wrong"
    //       })
    //       }
    //       }else{
    //       reject({"success": false, "message": "unknown db"});    
    //       }
    //     })
    //   }
    
    



function fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    let party_list = await fetchAllPartyList(connection);
    let vehicle_list = await fetchAllVehicle(connection);
    let driver_list = await fetchAllDriver(connection);
    let route_list = await fetchAllRoute(connection)
    if(party_list.length != 0 || vehicle_list.length != 0 || driver_list.length != 0 || route_list.length != 0){
    var item = {"party_list":party_list,"vehicle_list":vehicle_list,"driver_list":driver_list,"route_list":route_list}

    // let compressData = await commonFunctionWeb.compressDataFunction(item)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
 
    connection.destroy();
    resolve({
    "success":true,
    "message":"Data fetched sucessfully",
    "data": item,
    // "data":encriptedData,
    "data":item
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    } 
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
}






function fetchAllPartyList(connection){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      //connection.destroy()
      resolve(error);
      }else{ 
      if(rows.length > 0){
      //connection.destroy();
      resolve(rows)
      }else{
     // connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
     // connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }


function fetchAllVehicle(connection){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT code,label AS name,vehicle_no FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      resolve(error);
      }else{ 
      //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
      if(rows.length > 0){
      //connection.destroy();
      resolve(rows)
      }else{
      //connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
     // connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }




  function fetchAllDriver(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name,mobile,profile_image_url,profile_image_url_thumbnail FROM drivers WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
        //connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
      //  connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }
  
  function fetchAllRoute(connection){
    return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT * FROM route_commission WHERE status != 2 ORDER BY created DESC  `, async (error, rows, fields)=>{
      if(error){
      //connection.destroy();
      createErrorLogFile(error);sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.length > 0){
      var routeData = [];
      for(var i =0;i<rows.length;i++){
      var mine_name  = '';
      var party_name = '';
      let mine_details = await  commonFunctionWeb.fetchSpecificMineDetails(connection,rows[i].mine_code)
      let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
      if(party_details != null){
        party_name = party_details.name
      }
      if(mine_details != null){
        mine_name = mine_details.name
      }
      var item = {};
      item.code = rows[i].code,
      item.mine_code = rows[i].mine_code,
      item.mine_name = mine_name
      item.party_code = rows[i].party_code,
      item.party_name = party_name
      routeData.push(item)
      }
      if(routeData.length > 0){
      resolve(routeData)
      }else{
      resolve([])
      }
      }else{
      //connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
      //connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    })
  }




function searchBilltyDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var keyword = body.search_text
    var limit = commonFunctionAndroid.limit;
    var offset = body.last_index_code
    connection.query(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR billty.bill_date LIKE '%${keyword}%' OR  vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' Or billty.advance LIKE '%${keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${limit} OFFSET ${offset}`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    //console.log(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR billty.bill_date LIKE '%${keyword}%' OR  vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' Or billty.advance LIKE '%${keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${body.range} OFFSET ${body.offset}`,rows)
    //if(rows.length > 0){
    var billtyData = [];
    for(var i = 0;i<rows.length;i++){
    let specificBilltyDetails = await commonFunctionAndroid.fetchSpecificBilltyDetails(connection,rows[i].code)
    if(specificBilltyDetails != null){
    billtyData.push(specificBilltyDetails)
    }
    }
    if(billtyData.length > 0){
        var billtyItems = {
          "success":true,
          "message":"Billty data fetched successfully",
          "billty_data":billtyData
          }
        
        if(offset <= 0){
        billtyItems["count"] = await fetchTotalBilltyCount(connection,`SELECT DISTINCT COUNT(billty.code) AS total_count FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR billty.bill_date LIKE '%${keyword}%' OR  vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' Or billty.advance LIKE '%${keyword}%' ) AND  billty.status != 2`)
        }
    connection.destroy();
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
    resolve(billtyItems)
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    // }else{
    // connection.destroy();
    // resolve({
    // "success":true,
    // "message":"No data found"
    // })
    // }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
    resolve({"success": false, "message": "unknown db"});
   }
 })
}




function fetchSpecificBilltyDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    var billty_code = body.billty_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
      connection.query(`SELECT * FROM billty  WHERE code = '${billty_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      
        var vehicle_label = '';
        var party_name = "";
        var vehicle_no = ''
        var driver_name = ''
        var mine_name = '';
        var driver_mobile = '';
        var from_code = 0;
        var to_code = 0;
        var from_label = "";
        var to_label = ""
        // var route_details = {};
        let vehicle_details = await commonFunctionAndroid.fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
        let party_details = await commonFunctionAndroid.fetchSpecificPartyDetails(connection,rows[0].party_code)
        let routeDetails = await commonFunctionAndroid.fetchSpecificRouteDetails(connection,rows[0].route_commission_code)
        let driverDetails = await commonFunctionAndroid.fetchSpecificDriverDetails(connection,rows[0].driver_code)
        if(routeDetails != null){
          from_code = routeDetails.mine_code
          to_code = routeDetails.party_code
          from_label = routeDetails.mine_name
          to_label = routeDetails.party_name
        }
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        if(driverDetails != null){
          driver_name = driverDetails.name
          driver_mobile = driverDetails.driver_mobile
        }
        var item = {}
        item.billty_code = rows[0].code
        item["route_commision_code"] = rows[0].route_commission_code
        item.billty_no = rows[0].billty_no
        item.party_code = rows[0].party_code
        item.transporter_party_name = party_name
        item.bill_date = rows[0].bill_date,
        // item.route_details = route_details
        item.from_code = from_code
        item.to_code = to_code
        item.from_label = from_label
        item.to_label = to_label
        item.vehicle_code = rows[0].vehicle_code
        item.vehicle_label = vehicle_label
        item.vehicle_no = vehicle_no
        item.driver_code = rows[0].driver_code
        item.driver_name = driver_name
        item.driver_mobile = driver_mobile
        item.advance = rows[0].advance
        item.remarks = rows[0].remarks,
        item.freight_rate = rows[0].freight_rate
        item.challan_weight = rows[0].challan_weight
        item.diesel = rows[0].diesel,
        item.received_weight = rows[0].received_weight,
        item.short = rows[0].short_weight,
        item.image_url = rows[0].image_url_1,
        item.image_url_1 = rows[0].	image_url_2,
        item.thumb_url = rows[0].image_url_1_thumbnail,
        item.thumb_url_1 = rows[0].image_url_2_thumbnail,
        item.created = rows[0].created

      connection.destroy();
      resolve({
      "success":true,
      "message":"Billty data fetched successfully",
      "billty_data":item,
      // "data":encriptedData
      })
      }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Billty data not fetched",
      })
      }
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })    
      } 
      }
      })
      }catch(e){
      connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
    }else{
      resolve({"success": false, "message": "unknown db"});
    }
   })
}




function filterBilltyDataForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log()
    var month = body.month_value
    var year = body.year_value
    var finalString = year+"-"+month
    var limit = commonFunctionAndroid.limit
    var offset = body.last_index_code

    var keyword = ""
    var query = "";
    var queryTotalCount = ""
    if(body.keyword != undefined){
      var keyword = body.keyword
      query = `SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR  vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' Or billty.advance LIKE '%${keyword}%' ) AND  billty.status != 2 AND billty.bill_date LIKE '%${finalString}%' ORDER BY billty.created DESC LIMIT ${limit} OFFSET ${offset}`
      queryTotalCount = `SELECT DISTINCT COUNT(billty.code) as total_count FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${keyword}%' OR parties.name LIKE '%${keyword}%' OR  vehicles.label LIKE '%${keyword}%' OR vehicles.vehicle_no LIKE '%${keyword}%' OR drivers.name LIKE '%${keyword}%' Or billty.advance LIKE '%${keyword}%' ) AND  billty.status != 2 AND billty.bill_date LIKE '%${finalString}%' ORDER BY billty.created DESC`
    }else{
      query = `SELECT DISTINCT billty.code FROM billty WHERE status != 2 AND bill_date LIKE '%${finalString}%' ORDER BY billty.created DESC limit ${limit} OFFSET ${offset}`
      queryTotalCount = `SELECT DISTINCT COUNT(billty.code) as total_count FROM billty WHERE status != 2 AND bill_date LIKE '%${finalString}%'`
    }

    connection.query(query, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    var billtyData = [];
    // console.log(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${body.keyword}%' OR parties.name LIKE '%${body.keyword}%' OR billty.bill_date LIKE '%${body.keyword}%' OR  vehicles.label LIKE '%${body.keyword}%' OR vehicles.vehicle_no LIKE '%${body.keyword}%' OR drivers.name LIKE '%${body.keyword}%' Or billty.advance LIKE '%${body.keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${body.range} OFFSET ${body.offset}`,rows)
    if(rows.length > 0){
    for(var i = 0;i<rows.length;i++){
    let specificBilltyDetails = await commonFunctionAndroid.fetchSpecificBilltyDetails(connection,rows[i].code)
    if(specificBilltyDetails != null){
    billtyData.push(specificBilltyDetails)
    }
    }
    if(billtyData.length > 0){
      var billtyItems = {
        "success":true,
        "message":"Billty data fetched successfully",
        "billty_data":billtyData
        }
      // console.log("CCCCCCCCCCCCONDITION",offset <= 0,"DDDDDDDDDDDDDDDDDDDATAT",await fetchTotalBilltyCount(connection,query))
      if(offset <= 0){
      billtyItems["count"] = await fetchTotalBilltyCount(connection,queryTotalCount)
      }
    connection.destroy();

    // let encriptedData = await commonFunctionWeb.encriptDataFunction(billtyData);
    resolve(billtyItems)
    }else{

    connection.destroy();

    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{

    connection.destroy();
    
    resolve({
      "success":false,
      "message":"No data found"
      })    
    }
    }catch(e){

    connection.destroy();

    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){

    connection.destroy();

    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}


function validateBilltyNo(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        var billty_no = body.billty_no;
        //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
        connection.query(`SELECT * FROM billty WHERE billty_no = '${billty_no}' AND status<>2`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        connection.destroy();
        resolve(error);
        }else{ 
           if(rows.length > 0){
            resolve({
              "success":false,
              "message":"Billty With Same Billty Number Already Exists. Choose Different Billty Number."
            })
           }else{
            resolve({
              "success":true,
              "message":"No Billty Found."
            })
           }
        }
      })
    }catch(e){
      console.log(e)
      resolve({
        "success":false,
        "message":"Something went wrong"
      })
    }
  }else{
    resolve({"success": false, "message": "unknown db"});
  }
 })
}


function fetchBilltiesForVehicleDate(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    // var limit = commonFunctionAndroid.limit;
    // var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT DISTINCT(billty.code) FROM billty WHERE billty.created_by = ${body.database_code} AND billty.status<>2 AND  billty.bill_date='${body.challan_date}' AND vehicle_code = ${body.vehicle_code} ORDER BY created DESC;`, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
      var billtyData = [];
    // console.log(`SELECT DISTINCT billty.code FROM billty INNER JOIN parties ON parties.code = billty.party_code Inner JOIN drivers On drivers.code =  billty.driver_code INNER JOIN vehicles ON vehicles.code = billty.vehicle_code WHERE (billty.billty_no LIKE '%${body.keyword}%' OR parties.name LIKE '%${body.keyword}%' OR billty.bill_date LIKE '%${body.keyword}%' OR  vehicles.label LIKE '%${body.keyword}%' OR vehicles.vehicle_no LIKE '%${body.keyword}%' OR drivers.name LIKE '%${body.keyword}%' Or billty.advance LIKE '%${body.keyword}%' ) AND  billty.status != 2 ORDER BY billty.created DESC LIMIT ${body.range} OFFSET ${body.offset}`,rows)
    if(rows.length > 0){
    for(var i = 0;i<rows.length;i++){
    let specificBilltyDetails = await commonFunctionAndroid.fetchSpecificBilltyDetails(connection,rows[i].code)
    if(specificBilltyDetails != null){
    billtyData.push(specificBilltyDetails)
    }
    }

    if(billtyData.length > 0){
    var billtyItems = {
      "success":true,
      "message":"Billty data fetched successfully",
      "billty_data":billtyData
      }
    connection.destroy();
    resolve(billtyItems)


    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"No data found"
    })    
    }
    // }else{
    // connection.destroy();
    // resolve({
    // "success":false,
    // "message":"No data found"
    // })
    // }
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })    
    } 
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}

function fetchDistinctYearFromBillties(req,res){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    var database_code = req.params.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    // var limit = commonFunctionAndroid.limit;
    // var offset = body.last_index_code;
    //console.log()// LIMIT ${body.range} OFFSET ${body.offset}
    connection.query(`SELECT DISTINCT(YEAR(bill_date)) As year FROM billty WHERE status != 2 `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
      var yearList = []
      if(rows.length > 0){
        for(var i =0;i<rows.length;i++){
          if(rows[i].year != 0)
          yearList.push(rows[i].year)
        }

        connection.destroy();

        resolve({
        "success":true,
        "message":"Year Data Fetched Successfully",
        "data":yearList
        })
      }else{
        yearList.push((new Date()).getFullYear())
        
        connection.destroy();

        resolve({
          "success":true,
          "message":"Year Data Fetched Successfully",
          "data":yearList
        })
      }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}

module.exports = billtyModel;
