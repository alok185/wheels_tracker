var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');

var vehicleModel = {
    addNewVehicleForAndroid:addNewVehicleForAndroid,
    fetchAllVehicleForAndroid:fetchAllVehicleForAndroid,
    updateVehicleForAndroid:updateVehicleForAndroid,
    deleteSpecificVehicleDetailsForAndroid:deleteSpecificVehicleDetailsForAndroid,

    addNewMaintenanceDataOfVehicleForAndroid:addNewMaintenanceDataOfVehicleForAndroid,
    updateMaintenanceDataOfVehicleForAndroid:updateMaintenanceDataOfVehicleForAndroid,
    fetchMaintenanceDataOfVehicleForAndroid:fetchMaintenanceDataOfVehicleForAndroid,
    fetchSpecificMaintenanceDataOfVehicleForAndroid:fetchSpecificMaintenanceDataOfVehicleForAndroid,
    fetchSpecificVehicleDetailsForAndroid:fetchSpecificVehicleDetailsForAndroid,
    updateVehicleDriverMapping:updateVehicleDriverMapping,
    deleteMaintenanceDataOfVehicleForAndroid:deleteMaintenanceDataOfVehicleForAndroid
  //deleteMultipleVehicleDetails:deleteMultipleVehicleDetails,
  //undoDeleteMultipleVehicleDetails:undoDeleteMultipleVehicleDetails
};
 

    function addNewVehicleForAndroid(body){
        return new Promise(async function(resolve, reject){
          // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          let checkVehicleAlreadyExist = await commonFunctionWeb.fetchSpecificVehicleDetailsWithCondition(connection,`SELECT * FROM vehicles WHERE vehicle_no = '${body.vehicle_no}'`);
          // console.log("checkVehicleAlreadyExist ==>",checkVehicleAlreadyExist)
          if(checkVehicleAlreadyExist != null){
          resolve({
            "success":false,
            "message":"Vehicle Number Already Added"
            })
          }else{
          connection.query(`INSERT INTO vehicles SET label='${body.label}',	vehicle_no = '${body.vehicle_no}',driver_code='${body.driver_code}',owner_name = '${body.owner_name}',created_by='${body.database_code}'`, async(error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows > 0){
          // let vehicleData = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows.insertId);
          // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVEHICLEDATA",vehicleData)
          // let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
          // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

          connection.destroy();

          resolve({
          "success":true,
          "message":"Vehicle added",
          // "data":vehicleData
          })
          }else{

          connection.destroy();

          resolve({
          "success":false,
          "message":"Vehicle not added"
          })
          }
          }
          })
          }
          }catch(e){

          connection.destroy();

          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
         }
         }else{
         reject({"success": false, "message": "unknown db"});
        }
      })
     }


    
//`SELECT DISTINCT  route_commission.code,route_commission.short_rate,route_commission.freight_rate,route_commission.driver_commission,route_commission.mine_code,mines.name AS mine_name,route_commission.party_code,parties.name AS party_name,route_commission.created,route_commission.modified  FROM route_commission INNER JOIN parties ON parties.code = route_commission.party_code INNER JOIN mines ON route_commission.mine_code = route_commission.mine_code WHERE route_commission.status != 2 AND mines.status != 2 AND parties.status != 2 ORDER BY route_commission.created DESC `
function fetchAllVehicleForAndroid(req,res){
    return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var vehicleData = [];
    for(var i = 0;i<rows.length;i++){
    var driver_name = "";
    var driver_mobile = "";
    let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[i].driver_code);
    if(driver_details != null){
    driver_name = driver_details.name
    driver_mobile = driver_details.mobile
    }
    var item = {}
    item.code = rows[i].code,
    item.label = rows[i].label,
    item.vehicle_no = rows[i].vehicle_no,
    item.owner_name = rows[i].owner_name,
    item.driver_code = rows[i].driver_code
    item.driver_name = driver_name
    item.driver_mobile = driver_mobile
    item.created = rows[i].created
    vehicleData.push(item)
    }
    if(vehicleData.length > 0){
   
    // let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    
    connection.destroy();
    resolve({
    "success":true,
    "message":"Vehicle data fetched successfully",
    "vehicles_data":vehicleData,
    })
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Vehicle data not fetched",
      })  
    }
    }else{
      connection.destroy();
      resolve({
        "success":false,
        "message":"Vehicle data not fetched",
        })
      }
     }
    })
    }catch(e){
      console.log(e)

      connection.destroy();

      resolve({
        "success":false,
        "message":"Vehicle data not fetched",
        })
    // resolve({
    // "success":false,
    // "message":"Something went wrong"
    // })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function updateVehicleForAndroid(body){
    return new Promise(async function(resolve, reject){
        // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE vehicles SET label='${body.label}',vehicle_no = '${body.vehicle_no}',driver_code='${body.driver_code}',owner_name = '${body.owner_name}',created_by='${body.database_code}' WHERE code = '${body.vehicle_code}'`, async(error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        // let vehicleData = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,body.vehicle_code);
        // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVEHICLEDATA",vehicleData)

        // let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

        connection.destroy();

        resolve({
        "success":true,
        "message":"Vehicle details updated",
        // "data":vehicleData
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Vehicle details not updated"
        })
        }
        }
        })
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
      }
    })
   }








   function deleteSpecificVehicleDetailsForAndroid(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE vehicles SET status = 2 WHERE code = '${body.vehicle_code}'`, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows  > 0){

      connection.destroy();

      resolve({
      "success":true,
      "message":"Vehicle data deleted successfully"
      })
      }else{

      connection.destroy();  

      resolve({
      "success":false,
      "message":"Vehicle data not deleted"
      })
      }
      }
      })
      }catch(e){

      connection.destroy()

      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
    }
  
  
  
  
    // function deleteMultipleVehicleDetails(bodyEncripted){
    //   return new Promise(async function(resolve, reject){
    //     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    //     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    //     if(connection != false){
    //     try{
    //     connection.query(`UPDATE vehicles SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
    //     if(error){
    //     connection.destroy();
    //     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    //     resolve(error);
    //     }else{ 
    //     if(rows.affectedRows  > 0){
    //     connection.destroy()
    //     resolve({
    //     "success":true,
    //     "message":"Vehicle data deleted successfully"
    //     })
    //     }else{
    //     connection.destroy()      
    //     resolve({
    //     "success":false,
    //     "message":"Vehicle data not deleted"
    //     })
    //     }
    //     }
    //     })
    //     }catch(e){
    //     connection.destroy()
    //     console.log(e)
    //     resolve({
    //     "success":false,
    //     "message":"Something went wrong"
    //     })
    //     }
    //     }else{
    //     reject({"success": false, "message": "unknown db"});    
    //     }
    //   })
    //   }
    
    
  
      // function undoDeleteMultipleVehicleDetails(bodyEncripted){
      //   return new Promise(async function(resolve, reject){
      //     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      //     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      //     if(connection != false){
      //     try{
      //     connection.query(`UPDATE vehicles SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
      //     if(error){
      //     connection.destroy();
      //     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      //     resolve(error);
      //     }else{ 
      //     if(rows.affectedRows  > 0){
      //     connection.destroy()
      //     resolve({
      //     "success":true,
      //     "message":"Vehicle data undo successfully"
      //     })
      //     }else{
      //     connection.destroy()      
      //     resolve({
      //     "success":false,
      //     "message":"Vehicle data not undo"
      //     })
      //     }
      //     }
      //     })
      //     }catch(e){
      //     connection.destroy()
      //     console.log(e)
      //     resolve({
      //     "success":false,
      //     "message":"Something went wrong"
      //     })
      //     }
      //     }else{
      //     reject({"success": false, "message": "unknown db"});    
      //     }
      //   })
      //   }
      
      
    
function addNewMaintenanceDataOfVehicleForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{

    connection.query(`INSERT INTO maintenance 
    SET amount='${body.amount}',	
    created_by = '${body.database_code}',
    vehicle_code='${body.vehicle_code}',
    duration = '${body.duration}',
    duration_type = '${body.duration_type}',
    paid_date = '${body.paid_date}',
    type = '${body.type}',
    name='${body.name}'`, async(error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
      if(rows.affectedRows > 0){

        connection.destroy();

        resolve({
        "success":true,
        "message":"Vehicle maintenance data added"
        })
      }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Vehicle maintenance data not added"
       })
      }
     }
    })
    }catch(e){
    connection.destroy();
     console.log(e)
     resolve({
     "success":false,
     "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});    
  }
 })
}
  

function updateMaintenanceDataOfVehicleForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{

    connection.query(`UPDATE maintenance 
    SET amount='${body.amount}',	
    created_by = '${body.database_code}',
    vehicle_code='${body.vehicle_code}',
    duration = '${body.duration}',
    duration_type = '${body.duration_type}',
    paid_date = '${body.paid_date}',
    type = '${body.type}',
    name='${body.name}' WHERE code = '${body.maintenance_code}'`, async(error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
      if(rows.affectedRows > 0){

        connection.destroy();
        
        resolve({
        "success":true,
        "message":"Vehicle maintenance data updated successfully"
        })
      }else{
        
        connection.destroy()
        
        resolve({
        "success":false,
        "message":"Vehicle maintenance data not updated"
       })
      }
     }
    })
    }catch(e){

    connection.destroy()

     console.log(e)
     resolve({
     "success":false,
     "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});    
  }
 })
}


function fetchMaintenanceDataOfVehicleForAndroid(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
      connection.query(`SELECT * FROM maintenance WHERE status != 2 AND type = '${body.type}' AND vehicle_code = '${body.vehicle_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      connection.destroy()
      resolve(error);
      }else{ 
        if(rows.length > 0){

        connection.destroy()

        resolve({
        "success":true,
        "message":"Maintenance Data fetched successfull",
        "maintenance_data":rows
        })
        }else{

        connection.destroy()

        resolve({
        "success":false,
        "message":"No data found"
        })
        }
        }
       })
      }catch(e){

      connection.destroy()

      console.log(e)
      resolve({
      "success":false,
      "message":"Something Went Wrong"
      })
      }
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
    })
  }

 

  function fetchSpecificMaintenanceDataOfVehicleForAndroid(body){
    return new Promise(async function(resolve, reject){
      var database_code = body.database_code
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
      if(connection != false){
      try{
        connection.query(`SELECT * FROM maintenance WHERE code  = '${body.maintenance_code}' `, async (error, rows, fields)=>{
        if(error){
        connection.destroy()
        resolve(error);
        }else{ 
          if(rows.length > 0){
  
          connection.destroy()
  
          resolve({
          "success":true,
          "message":"Maintenance Data fetched successfull",
          "maintenance_data":rows[0]
          })
          }else{
  
          connection.destroy()
  
          resolve({
          "success":false,
          "message":"No data found"
          })
          }
          }
         })
        }catch(e){
  
        connection.destroy()
  
        console.log(e)
        resolve({
        "success":false,
        "message":"Something Went Wrong"
        })
        }
        }else{
        resolve({"success": false, "message": "unknown db"});    
        }
      })
    }
  

function fetchSpecificVehicleDetailsForAndroid(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    var vehicle_code = body.vehicle_code
    connection.query(`SELECT * FROM vehicles WHERE code = '${vehicle_code}' `, async (error, rows, fields)=>{
    if(error){

    connection.destroy()

    resolve(error);
    }else{ 
    if(rows.length > 0){
    var driver_name = "";
    var driver_mobile = "";
    let driver_details = await commonFunctionWeb.fetchSpecificDriverDetails(connection,rows[0].driver_code);
    if(driver_details != null){
    driver_name = driver_details.name
    driver_mobile = driver_details.mobile
    }
    var item = {}
    item.code = rows[0].code,
    item.label = rows[0].label,
    item.vehicle_no = rows[0].vehicle_no,
    item.owner_name = rows[0].owner_name,
    item.driver_code = rows[0].driver_code
    item.driver_name = driver_name
    item.driver_mobile = driver_mobile
    item.created = rows[0].created

    connection.destroy();

    resolve({
      "success":true,
      "message":"Vehicle data fetched successfully",
      "vehicles_data":item
    })
    }else{

      connection.destroy();

      resolve({
        "success":false,
        "message":"Vehicle data not fetched",
        })
      }
     }
    })
    }catch(e){
      console.log(e)

      connection.destroy();

      resolve({
        "success":false,
        "message":"Vehicle data not fetched",
        })
    // resolve({
    // "success":false,
    // "message":"Something went wrong"
    // })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});
   }
 })
}

function updateVehicleDriverMapping(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE vehicles SET driver_code = ${body.driver_code} WHERE code = '${body.vehicle_code}'`, async(error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows > 0){

      connection.destroy();

      resolve({
      "success":true,
      "message":"Vehicle mapping updated",
      })
      }else{

      connection.destroy();

      resolve({
      "success":false,
      "message":"Vehicle mapping not updated"
      })
      }
      }
      })
      }catch(e){

      connection.destroy();

      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
     }else{
     reject({"success": false, "message": "unknown db"});
    }
  })
}


function deleteMaintenanceDataOfVehicleForAndroid(body){
  return new Promise(async function(resolve, reject){
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
    if(connection != false){
    try{
    connection.query(`DELETE From maintenance WHERE code = '${body.maintenance_code}'`, async(error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
      if(rows.affectedRows > 0){

        connection.destroy();

        resolve({
        "success":true,
        "message":"Vehicle maintenance data deleted successfully"
        })
      }else{
        
        connection.destroy();
        
        resolve({
        "success":false,
        "message":"Vehicle maintenance data not deleted"
       })
      }
     }
    })
    }catch(e){

    connection.destroy()

     console.log(e)
     resolve({
     "success":false,
     "message":"Something went wrong"
    })
   }
   }else{
   resolve({"success": false, "message": "unknown db"});    
  }
 })
}


module.exports = vehicleModel;
