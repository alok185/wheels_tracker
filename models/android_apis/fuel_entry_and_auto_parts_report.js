var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');



var fuelEntryAndAutoPartsReportModel = {
    fetchFuelEntryReportForWeb:fetchFuelEntryReportForWeb,
    fetchAutoPartReportForWeb:fetchAutoPartReportForWeb,
    fetchAllNecessaryDataForFuelEntryAndAutoPartReport:fetchAllNecessaryDataForFuelEntryAndAutoPartReport
};
 



function fetchFuelEntryReportForWeb(body){
    return new Promise(async function(resolve, reject){
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var fuelEntryData = [];

        if(body.party_code != 0){
        partyQuery = `AND party_code = '${body.party_code}'`
        // partyPayment = await fetchSpecficPartyPaymentDetailsForReport(connection,body.party_code,body.from_date,body.to_date)
        }
        if(body.vehicle_code  != 0){
        vehicleQuery = `AND vehicle_code = '${body.vehicle_code}'`
        }
        var filterQuery = `SELECT code,challan_no,party_code,vehicle_code,quantity,fuel_rate,amount,bill_date,remarks,status,created,modified,image_url_1,image_url_2,image_url_1_thumbnail,image_url_2_thumbnail,created_by FROM fuel_entry WHERE status != 2 ${partyQuery} ${vehicleQuery} AND  bill_date BETWEEN '${from_date}' AND '${to_date}'  ORDER BY bill_date ASC`
        console.log(filterQuery)
        connection.query(filterQuery, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  

        connection.destroy();

        resolve(error);
        }else{ 
        try{
        if(rows.length > 0){
        for(var i = 0;i<rows.length;i++){
        var vehicle_label = '';
        var party_name = "";
        vehicle_no = ''
        let vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        if(party_details != null){
        party_name = party_details.name
        }
        var item = {}

        item.vehicle_label = vehicle_label
        item.vehicle_no = vehicle_no
        // item.slno = rows[i].slno
        item.code = rows[i].code
        item.challan_no = rows[i].challan_no
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.transporter_party_name  = party_name
        item.destination_party_name = ""
        item.bill_date = rows[i].bill_date,
        item.vehicle_code = rows[i].vehicle_code
        item.rate = rows[i].fuel_rate
        item.quantity = rows[i].quantity
        item.remarks = rows[i].remarks,
        item.amount = rows[i].amount

        // item.image_url_1 = rows[i].image_url_1,
        // item.image_url_2 = rows[i].	image_url_2,
        // item.image_url_1_thumbnail = rows[i].image_url_1_thumbnail,
        // item.image_url_2_thumbnail = rows[i].image_url_2_thumbnail,
        item.created = rows[i].created
        // item.selected = false
        fuelEntryData.push(item)
        }
        if(fuelEntryData.length > 0 || partyPayment.length > 0){
        
        connection.destroy();
        // let compressData = await commonFunctionWeb.compressDataFunction({"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment})
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);  
        resolve({
        "success":true,
        "message":"Fuel entry challan report data fetched successfully",
        "fuel_entry_data":fuelEntryData,
        // "payments_fuel_entry_data":partyPayment
        // "data":{"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment},
        })
        }else{

        connection.destroy();

        // let compressData = await commonFunctionWeb.compressDataFunction({"fuel_enrty_challan_data":fuelEntryData,"party_payment_data":partyPayment})
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);  
        resolve({
        "success":false,
        "message":"Fuel entry challan report not data fetched",
        })
        }
        }else{
        // if(partyPayment.length > 0){
        
        connection.destroy();
        
        resolve({
        "success":false,
        "message":"Fuel entry challan report data not fetched",
        })
        // }else{
        // connection.destroy();
        // resolve({
        // "success":false,
        // "message":"No data found"
        // })
        // }
        }
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })    
        } 
        }
        })
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
        resolve({"success": false, "message": "unknown db"});
       }
     })
     }




function fetchSpecficPartyPaymentDetailsForReport(connection,party_code,from_date,to_date){ 
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT DISTINCT party_code,payment_code FROM party_payments  INNER JOIN payments ON party_payments.payment_code = payments.code WHERE party_payments.status != 2 And party_code = '${party_code}' AND  payments.bill_date BETWEEN '${from_date}' AND '${to_date}' ORDER BY payments.bill_date ASC`, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        try{

        if(rows.length > 0){
        var paymentData = [];
        for(var i = 0;i<rows.length;i++){
        var party_name = '';
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        let payment_details = await commonFunctionWeb.fetchSpecificPaymentDetails(connection,rows[i].payment_code)
        if(payment_details != null){
        var item = {};
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.payment_code = rows[i].payment_code
        item.code = rows[i].payment_code
        item.bill_no = payment_details.bill_no,
        item.bill_date = payment_details.bill_date,
        item.from_date = payment_details.from_date,
        item.to_date = payment_details.to_date,
        item.amount = payment_details.amount,
        item.purpose = payment_details.purpose,
        item.payment_type = payment_details.payment_type,
        item.ref_no = payment_details.ref_no,
        item.remarks = payment_details.remarks,
        item.image_url = payment_details.image_url_1,
        // item.image_url_2 = payment_details.	image_url_2,
        // item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail,
        // item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail,
        // item.selected = false
        item.created = payment_details.created
        paymentData.push(item)
        }
        }
        if(paymentData.length > 0){
        resolve(paymentData)
        }else{
        resolve([])    
        }
        }else{
        resolve([])    
        }
        }catch(e){
        console.log(e)
        resolve([])        
        } 
        }
        })
        }catch(e){
        console.log(e)
        resolve([])
       }
     })   
    }



function fetchAutoPartReportForWeb(body){
    return new Promise(async function(resolve, reject){
        // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        var database_code = body.database_code
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
        if(connection != false){
        try{
        var from_date = body.from_date
        var to_date = body.to_date 
        var partyQuery = ""
        var vehicleQuery = ""
        var partyPayment = [];
        var autoPartsData = [];
        if(body.party_code != 0){
        partyQuery = `AND party_code = '${body.party_code}'`
        partyPayment = await fetchSpecficPartyPaymentDetailsForReport(connection,body.party_code,body.from_date,body.to_date)
        }
        if(body.vehicle_code  != 0){
        vehicleQuery = `AND vehicle_code = '${body.vehicle_code}'`
        }
        var filterQuery = `SELECT code,challan_no,party_code,vehicle_code,amount,bill_date,remarks,status,created,modified,created_by FROM auto_parts WHERE status != 2 ${partyQuery} ${vehicleQuery} AND  bill_date BETWEEN '${from_date}' AND '${to_date}'  ORDER BY bill_date ASC`
        // console.log(filterQuery)
        connection.query(filterQuery, async (error, rows, fields)=>{
        if(error){
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  

        connection.destroy();

        resolve(error);
        }else{ 
        if(rows.length > 0){
            for(var i =0;i<rows.length;i++){
            var item = {};
            var party_name = ''
            var vehicle_label = ''
            var  vehicle_no = ''
            var party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection, rows[i].party_code)
            if(party_details != null){
            party_name = party_details.name
            }
            var vehicle_details = await commonFunctionWeb.fetchSpecificVehicleDetails(connection,rows[i].vehicle_code)
            if(vehicle_details != null){
            vehicle_label = vehicle_details.label
            vehicle_no = vehicle_details.vehicle_no
            }
            item.auto_part_code = rows[i].code,
            item.challan_no = rows[i].challan_no,
            item.party_code = rows[i].party_code,
            item.party_name = party_name,
            item.transporter_party_name = party_name,
            item.vehicle_code = rows[i].vehicle_code,
            item.vehicle_label = vehicle_label,
            item.vehicle_no = vehicle_no,
            item.amount = rows[i].amount,
            item.bill_date = rows[i].bill_date,
            item.remarks = rows[i].remarks,
            item.created = rows[i].created
            // item.selected = false
            autoPartsData.push(item);
            }

            connection.destroy();

            // let compressData = await commonFunctionWeb.compressDataFunction({"auto_part_data":autoPartsData,"party_payment_data":partyPayment})
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
            if(autoPartsData.length > 0 || partyPayment.length > 0){
                resolve({
                "success":true,
                "message":"Auto part data report fetched successfully",
                // "data":{"auto_part_data":autoPartsData,"party_payment_data":partyPayment}
                "auto_parts_data":autoPartsData,
                "payments_auto_parts_data":partyPayment
              })
            }else{
                resolve({
                "success":false,
                "message":"Auto part data report not fetched",
                });
             }
           }else{

            connection.destroy();

            // let compressData = await commonFunctionWeb.compressDataFunction({"auto_part_data":autoPartsData,"party_payment_data":partyPayment})
            // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
            resolve({
            "success":false,
            "message":"Auto part data report not fetched",
            });
          }
         }
        })
        }catch(e){

        connection.destroy();
        
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })    
        } 
       }else{
       resolve({"success": false, "message": "unknown db"});
       }
     })
    }


function fetchAllNecessaryDataForFuelEntryAndAutoPartReport(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    // console.log(req.params)
    //var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    let vehicle_list = await fetchAllVehicle(connection);
    let party_list = await fetchAllParty(connection);
    var item = {"party_list":party_list,"vehicle_list":vehicle_list}
    connection.destroy()
    // let compressData = await commonFunctionWeb.compressDataFunction(item)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    resolve({
    "success":true,
    "message":"Data fetched successfully",
    "data":item,
    // "data":encriptedData
    })
    }else{
    resolve({
    "success":false,
    "message":"Unknown db"
    })
  }
})
}



function fetchAllVehicle(connection){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT code,label,vehicle_no FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      resolve(error);
      }else{ 
      //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
      if(rows.length > 0){
      //connection.destroy();
      resolve(rows)
      }else{
      //connection.destroy();
      resolve([])
      }
      }
      })
      }catch(e){
     // connection.destroy();
      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
     }
   })
  }



  function fetchAllParty(connection){
      return new Promise(async function(resolve, reject){
          try{
          connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY name ASC `, async (error, rows, fields)=>{
          if(error){
          resolve(error);
          }else{ 
          //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
          if(rows.length > 0){
          resolve(rows)
          }else{
          resolve([])
          }
          }
          })
          }catch(e){
          console.log(e)
          resolve([])
         }
       })
      }
    



module.exports = fuelEntryAndAutoPartsReportModel;
