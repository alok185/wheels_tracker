var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');
var cmd = require('node-cmd');
var CronJob = require('cron').CronJob;



var otherModel = {
  fetchAllPartyFromPartyTypeForFormData:fetchAllPartyFromPartyTypeForFormData,
  fetchAllVehicleListForFormData:fetchAllVehicleListForFormData,
  fetchAllRouteForFormData:fetchAllRouteForFormData,
  fetchAllDriverListForFormData:fetchAllDriverListForFormData,
  fetchAllMineDataForFormData:fetchAllMineDataForFormData,
  restartServerForReleasingAllTheConnection:restartServerForReleasingAllTheConnection
}
 

function fetchAllPartyFromPartyTypeForFormData(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    var party_type_code = body.party_type_code
    var party_type_code_query = ""
    if(party_type_code == 0){
      party_type_code_query = ""
    }else if(party_type_code > 0){
      party_type_code_query = `AND party_type_code = '${party_type_code}'`
    }else{
      resolve({
        "success":false,
        "message":"Party Type Code Is Required"
      })
    }

    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created FROM parties WHERE status != 2 ${party_type_code_query} ORDER BY name ASC `, async (error, rows, fields)=>{
    if(error){
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
      var partyData = []
      for(var i =0;i<rows.length;i++){
        var items = {}
        items["code"] = rows[i].code,
        items["name"] = rows[i].name
        items["mobile"] = rows[i].mobile
        items["party_type_code"] = rows[i].party_type_code,
        items["opening_balance"] = rows[i].opening_balance,
        items["type_of_ob"] =  rows[i].type_of_ob == 0 ? "Debit":"Credit",
        items["address"] = rows[i].address,
        items["created"] = rows[i].created,
        items["party_type_label"] = await commonFunctionAndroid.fetchPartyTypeLabel(rows[i].party_type_code)
        partyData.push(items)
      }
      if(partyData.length > 0){
        connection.destroy();
        resolve({
          "success":true,
          "message":"Party data fetched successfully",
          // "data":encriptedData,
          "parties_data":partyData,
          })
      }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Party data not fetched",
        })
      }
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    }else{
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Party data not fetched",
    })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
      "success":false,
      "message":"Party data not fetched",
      })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}

function fetchAllVehicleListForFormData(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM vehicles WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var vehicleData = [];
    for(var i = 0;i<rows.length;i++){
    var driver_name = "";
    var driver_mobile = "";
    let driver_details = await commonFunctionAndroid.fetchSpecificDriverDetails(connection,rows[i].driver_code);
    if(driver_details != null){
    driver_name = driver_details.name
    driver_mobile = driver_details.mobile
    }
    var item = {}
    item.code = rows[i].code,
    item.label = rows[i].label,
    item.vehicle_no = rows[i].vehicle_no,
    item.owner_name = rows[i].owner_name,
    item.driver_code = rows[i].driver_code
    item.driver_name = driver_name
    item.driver_mobile = driver_mobile
    item.created = rows[i].created
    vehicleData.push(item)
    }
    if(vehicleData.length > 0){
   
    // let compressData = await commonFunctionWeb.compressDataFunction(vehicleData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    
    connection.destroy();
    resolve({
    "success":true,
    "message":"Vehicle data fetched successfully",
    "vehicles_data":vehicleData,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Vehicle data not fetched",
    })  
    }
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Vehicle data not fetched",
      })
      }
     }
    })
    }catch(e){
      console.log(e)
      connection.destroy();
      resolve({
        "success":false,
        "message":"Vehicle data not fetched",
        })
    // resolve({
    // "success":false,
    // "message":"Something went wrong"
    // })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}

function fetchAllRouteForFormData(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM route_commission WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var routeData = [];
    for(var i = 0;i<rows.length;i++){
        var mine_name = ""
        var party_name = ""
        let mine_details = await commonFunctionWeb.fetchSpecificMineDetails(connection,rows[i].mine_code);
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(mine_details != null){
        mine_name = mine_details.name
        }
        if(party_details != null){
        party_name = party_details.name
        }
        
        var item = {}
        item.code = rows[i].code,
        item.short_amount = rows[i].short_rate,
        item.freight_rate = rows[i].freight_rate,
        item.commision = rows[i].driver_commission,
        item.mine_code = rows[i].mine_code
    
        item.mine_name = mine_name
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.created = rows[i].created
        routeData.push(item)
    }
    if(routeData.length > 0){
      connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction(routeData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Route data fetched successfully",
    // "data":encriptedData,
    "route_commision_data":routeData,
    })
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Route data not fetched",
      })
    }
    }else{
    connection.destroy();
    resolve({
      "success":false,
      "message":"Route data not fetched",
      })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function fetchAllDriverListForFormData(body){
  return new Promise(async function(resolve, reject){
    var database_code = body.database_code
    // var index = req.params.index
    // var limit = req.params.limit
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,name,mobile,address FROM drivers WHERE status != 2 ORDER BY name ASC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy();
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData)
    connection.destroy();
    resolve({
    "success":true,
    "message":"Drivers data fetched successfully",
    // "data":encriptedData,
    "drivers_data":rows,
    })
    }else{
    connection.destroy();
    resolve({
    "success":false,
    "message":"Drivers data not fetched",
    })
    }
    }
    })
    }catch(e){
    console.log(e)
    connection.destroy();
    resolve({
    "success":false,
    "message":"Something went wrong",
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
  }
 })  
}


function fetchAllMineDataForFormData(req,res){
  return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    //console.log(`SELECT * FROM parties WHERE status != 2 ORDER BY created DESC `)
    connection.query(`SELECT code,name,expense,address,created FROM mines WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    connection.destroy();
    // let compressData = await commonFunctionWeb.compressDataFunction(rows)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
      resolve({
      "success":true,
      "message":"Mine data fetched successfully",
      // "data":encriptedData,
      "mines_data":rows,
      })
    }else{
      connection.destroy();
      resolve({
      "success":false,
      "message":"Mine data not fetched",
      })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    // let compressData = await commonFunctionWeb.compressDataFunction([])
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":false,
    "message":"Something went wrong",
    // "data":encriptedData,
    })
   }
   }else{
    resolve({"success": false, "message": "unknown db"});
   }
 })
}

new CronJob('00 05 * * *',async function() {//
  try{
  var commands = "pm2 restart app.js"
  restartServerForReleasingAllTheConnection()
  }catch(e){
   console.log(e)
  }
}, null, true, 'Asia/Kolkata');




new CronJob('30 14 * * *',async function() {//
  try{
  var commands = "pm2 restart app.js"
  restartServerForReleasingAllTheConnection()
  }catch(e){
   console.log(e)
  }
}, null, true, 'Asia/Kolkata');


function restartServerForReleasingAllTheConnection(body){
  return new Promise(async function(resolve, reject){
    var commands = "pm2 restart app.js"
      cmd.get(
      commands,
      function(err, data, stderr){
      commonFunctionAndroid.sendErrorsInEmail("DAAAAAAAATATATTATA"+error+"data"+data+"stderr"+stderr)
      // console.log("err==>",err, "data==>",data, "stderr==>",stderr,"commands=============>" ,commands)
      resolve(true);
      });
  })
}
//RESTART SERVER CODE............................ENDS..........



module.exports = otherModel;
