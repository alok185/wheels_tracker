var db = require("../../config/database");
var commonFunctionWeb = require('../../commons/commonFunctionWeb/commonFuncWeb');
var commonFunctionAndroid = require('../../commons/commonFunctionAndroid/commonFuncAndroid');
var routeModel = {
    fetchAllNecessaryDataForAddingRouteForAndroid:fetchAllNecessaryDataForAddingRouteForAndroid,
    addNewRouteForAndroid:addNewRouteForAndroid,
    fetchAllRoutesForAndroid:fetchAllRoutesForAndroid,
    updateRouteForAndroid:updateRouteForAndroid,
    deleteSpecificRouteDetailsForAndroid:deleteSpecificRouteDetailsForAndroid,
    updateFrieghtRate:updateFrieghtRate
    // deleteMultipleRouteDetails:deleteMultipleRouteDetails,
    // undoDeleteMultipleRouteDetails:undoDeleteMultipleRouteDetails
};
 

function fetchAllNecessaryDataForAddingRouteForAndroid(req,res){
    return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    let party_list = await fetchAllPartyList(connection)
    let mine_list = await fetchAllMineList(connection)
    if(party_list.length != 0 || mine_list.length != 0){
    var item = {"party_list":party_list,"mine_list":mine_list}

    // let compressData = await commonFunctionWeb.compressDataFunction(item)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);

    connection.destroy();

    resolve({
    "success":true,
    "message":"Data fetched sucessfully",
    "data":item,
    // "data":encriptedData,
    })
    }else{

    connection.destroy();

    resolve({
    "success":false,
    "message":"No data found"
    })
    }
    }catch(e){

    connection.destroy();

    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    } 
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
  })
}


function fetchAllPartyList(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name FROM parties WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        //connection.destroy()
        resolve(error);
        }else{ 
        if(rows.length > 0){
        //connection.destroy();
        resolve(rows)
        }else{
       // connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
       // connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }


function fetchAllMineList(connection){
    return new Promise(async function(resolve, reject){
        try{
        connection.query(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
        if(error){
        connection.destroy();
        resolve(error);
        }else{ 
        //console.log(`SELECT code,name FROM mines WHERE status != 2 ORDER BY created DESC `)
        if(rows.length > 0){
        connection.destroy();
        resolve(rows)
        }else{
        connection.destroy();
        resolve([])
        }
        }
        })
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
     })
    }


    function addNewRouteForAndroid(body){
        return new Promise(async function(resolve, reject){
          // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
          let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
          if(connection != false){
          try{
          connection.query(`INSERT INTO route_commission SET mine_code='${body.mine_code}',party_code = '${body.party_code}',short_rate='${body.short_amount}',freight_rate = '${body.freight_rate}',driver_commission = '${body.commision}',created_by='${body.database_code}'`,async(error, rows, fields)=>{
          if(error){
          connection.destroy();
          commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
          resolve(error);
          }else{ 
          if(rows.affectedRows > 0){
          // let routeData = await commonFunctionAndroid.fetchSpecificRouteDetails(connection,rows.insertId);
          // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>routeData",routeData)
          // let encriptedData = await commonFunctionWeb.encriptDataFunction(routeData);
          connection.destroy();

          resolve({
          "success":true,
          "message":"Route added",
          // "data":encriptedData,
          // "data":routeData,
          })
          }else{

          connection.destroy();

          resolve({
          "success":false,
          "message":"Route not added"
          })
          }
          }
          })
          }catch(e){

          connection.destroy();

          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
         }
         }else{
         reject({"success": false, "message": "unknown db"});
        }
      })
     }


    
//`SELECT DISTINCT  route_commission.code,route_commission.short_rate,route_commission.freight_rate,route_commission.driver_commission,route_commission.mine_code,mines.name AS mine_name,route_commission.party_code,parties.name AS party_name,route_commission.created,route_commission.modified  FROM route_commission INNER JOIN parties ON parties.code = route_commission.party_code INNER JOIN mines ON route_commission.mine_code = route_commission.mine_code WHERE route_commission.status != 2 AND mines.status != 2 AND parties.status != 2 ORDER BY route_commission.created DESC `
function fetchAllRoutesForAndroid(req,res){
    return new Promise(async function(resolve, reject){
    var database_code = req.params.code
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(database_code)
    if(connection != false){
    try{
    connection.query(`SELECT * FROM route_commission WHERE status != 2 ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    connection.destroy()
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var routeData = [];
    for(var i = 0;i<rows.length;i++){
        var mine_name = ""
        var party_name = ""
        let mine_details = await commonFunctionWeb.fetchSpecificMineDetails(connection,rows[i].mine_code);
        let party_details = await commonFunctionWeb.fetchSpecificPartyDetails(connection,rows[i].party_code)
        if(mine_details != null){
        mine_name = mine_details.name
        }
        if(party_details != null){
        party_name = party_details.name
        }
        
        var item = {}
        item.code = rows[i].code,
        item.short_amount = rows[i].short_rate,
        item.freight_rate = rows[i].freight_rate,
        item.commision = rows[i].driver_commission,
        item.mine_code = rows[i].mine_code
    
        item.mine_name = mine_name
        item.party_code = rows[i].party_code
        item.party_name = party_name
        item.created = rows[i].created
        routeData.push(item)
    }
    if(routeData.length > 0){

      connection.destroy();

    // let compressData = await commonFunctionWeb.compressDataFunction(routeData)
    // let encriptedData = await commonFunctionWeb.encriptDataFunction(compressData);
    resolve({
    "success":true,
    "message":"Route data fetched successfully",
    // "data":encriptedData,
    "route_commision_data":routeData,
    })
    }else{

      connection.destroy();

      resolve({
      "success":false,
      "message":"Route data not fetched",
      })
    }
    }else{

    connection.destroy();
    
    resolve({
      "success":false,
      "message":"Route data not fetched",
      })
    }
    }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
   }else{
   reject({"success": false, "message": "unknown db"});
   }
 })
}



function updateRouteForAndroid(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE route_commission SET mine_code='${body.mine_code}',party_code = '${body.party_code}',short_rate='${body.short_amount}',freight_rate = '${body.freight_rate}',driver_commission = '${body.commision}',created_by='${body.database_code}' WHERE code = '${body.route_commision_code}'`, async(error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){
        // let routeData = await commonFunctionAndroid.fetchSpecificRouteDetails(connection,body.route_commision_code);
        // console.log("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV===========>>>>routeData",routeData)
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(routeData);
        connection.destroy();

        resolve({
        "success":true,
        "message":"Route updated",
        // "data":encriptedData,
        // "data":routeData
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Route not updated"
        })
        }
        }
        })
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
      }
    })
   }



   function deleteSpecificRouteDetailsForAndroid(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
      let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
      if(connection != false){
      try{
      connection.query(`UPDATE route_commission SET status = 2 WHERE code = '${body.route_commision_code}'`, async (error, rows, fields)=>{
      if(error){
      connection.destroy();
      commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      if(rows.affectedRows  > 0){

      connection.destroy()

      resolve({
      "success":true,
      "message":"Route deleted successfully"
      })
      }else{

      connection.destroy()  

      resolve({
      "success":false,
      "message":"Route data not deleted"
      })
      }
      }
      })
      }catch(e){

      connection.destroy()

      console.log(e)
      resolve({
      "success":false,
      "message":"Something went wrong"
      })
      }
      }else{
     reject({"success": false, "message": "unknown db"});    
    }
   })
  }
  
  
  
  
  //   function deleteMultipleRouteDetails(bodyEncripted){
  //     return new Promise(async function(resolve, reject){
  //       var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  //       let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
  //       if(connection != false){
  //       try{
  //       connection.query(`UPDATE route_commission SET status = 2 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
  //       if(error){
  //       connection.destroy();
  //       commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  //       resolve(error);
  //       }else{ 
  //       if(rows.affectedRows  > 0){
  //       connection.destroy()
  //       resolve({
  //       "success":true,
  //       "message":"Route deleted successfully"
  //       })
  //       }else{
  //       connection.destroy()      
  //       resolve({
  //       "success":false,
  //       "message":"Route not deleted"
  //       })
  //       }
  //       }
  //       })
  //       }catch(e){
  //       connection.destroy()
  //       console.log(e)
  //       resolve({
  //       "success":false,
  //       "message":"Something went wrong"
  //       })
  //       }
  //       }else{
  //       reject({"success": false, "message": "unknown db"});    
  //       }
  //     })
  //     }
    
    
  


  // function undoDeleteMultipleRouteDetails(bodyEncripted){
  //   return new Promise(async function(resolve, reject){
  //     var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
  //     let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
  //     if(connection != false){
  //     try{
  //     connection.query(`UPDATE route_commission SET status = 0 WHERE code IN (${body.code}) `, async (error, rows, fields)=>{
  //     if(error){
  //     connection.destroy();
  //     commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
  //     resolve(error);
  //     }else{ 
  //     if(rows.affectedRows  > 0){
  //     connection.destroy()
  //     resolve({
  //     "success":true,
  //     "message":"Route data undo successfully"
  //     })
  //     }else{
  //     connection.destroy()      
  //     resolve({
  //     "success":false,
  //     "message":"Route  data not undo"
  //     })
  //     }
  //     }
  //     })
  //     }catch(e){
  //     connection.destroy()
  //     console.log(e)
  //     resolve({
  //     "success":false,
  //     "message":"Something went wrong"
  //     })
  //     }
  //     }else{
  //     reject({"success": false, "message": "unknown db"});    
  //     }
  //   })
  //   }
  
  function updateFrieghtRate(body){
    return new Promise(async function(resolve, reject){
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getSpecificDatabaseConnection(body.database_code)
        if(connection != false){
        try{
        connection.query(`UPDATE route_commission SET freight_rate = '${body.freight_rate}',created_by='${body.database_code}' WHERE code = '${body.route_commision_code}'`, async(error, rows, fields)=>{
        if(error){
        connection.destroy();
        commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
        resolve(error);
        }else{ 
        if(rows.affectedRows > 0){

        connection.destroy();

        resolve({
        "success":true,
        "message":"Freight rate updated",
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Freight rate not updated"
        })
        }
        }
        })
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
       }else{
       reject({"success": false, "message": "unknown db"});
      }
    })
  }
  


  

module.exports = routeModel;
