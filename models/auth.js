var db = require("../config/database");
var otplib = require('otplib');
var authenticator = require('otplib/authenticator');
const jwt = require('jsonwebtoken');
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';
var commonFunctionWeb = require('../commons/commonFunctionWeb/commonFuncWeb');
let date = require('date-and-time');
var commonFunctionAndroid = require('../commons/commonFunctionAndroid/commonFuncAndroid');


const msg91 = require('msg91-promise');
const API_KEY = '230364AslqNHOaR5b6932aa'; // Your API key
const SENDER_ID = 'SRCLAP'; // Your sender id 
const ROUTE = 4; // transactional route
const msg91SMS = msg91(API_KEY, SENDER_ID, ROUTE);
const SendOtp = require('sendotp');
const expireAccountInDay = 45


otplib.authenticator.options = {
  step: 60,
  window: 1,
};

var authModel = {

  appInfo:appInfo,
  sendOtpForUser:sendOtpForUser,
  verifyOtpForUser:verifyOtpForUser,
  getEncriptedDataOfJson:getEncriptedDataOfJson,
  getDecriptedDataOfJson:getDecriptedDataOfJson,
  sendOtpForChangeMobileOfUser:sendOtpForChangeMobileOfUser,
  verifyOtpForSignup:verifyOtpForSignup,
  sendOtpForSignup:sendOtpForSignup,
  checkUserExistOrNot:checkUserExistOrNot,
  
  sendOtpForUserLoginForAndroid:sendOtpForUserLoginForAndroid,
  verifyOtpForUserLoginForAndroid:verifyOtpForUserLoginForAndroid,
  sendOtpForUserSignupForAndroid : sendOtpForUserSignupForAndroid,
  verifyOtpForUserSignupForAndroid :verifyOtpForUserSignupForAndroid,
  fetchAppPriceForUser:fetchAppPriceForUser,
  adminSignin:adminSignin

}


 


function appInfo() {
  return new Promise(async(resolve, reject) => {
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
    connection.query(`SELECT * FROM app_info`, (error, rows, fields)=>{
    if(error) {
    connection.destroy();
    reject(error);
    } else {
    if(rows.length > 0){
    connection.destroy()
    resolve({
    "success": true,
    "message": "Data fetched successfully",
    "app_info": {"version":rows[0].version_code}
    });
    }else{
    connection.destroy()
    resolve({
    "success": false,
    "message": "No data found",
    });
    }
   }
  });
  }catch(e){
    connection.destroy()
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
   }
  }else{
  resolve({"success": false, "message": "unknown db"});
  }
 });
}

  


// function sendOtpFunction(mobile,otp){
//   return new Promise((resolve, reject) => {
//     console.log("MOBILE------->>>>",mobile,"OTP--------------->>>>",otp)
//     // const SendOtp = require('sendotp');
//     // const sendOtp = new SendOtp('230364AslqNHOaR5b6932aa');
//     // sendOtp.send("91"+mobile, "WTSOTP", otp, function (error, data) {
//     // console.log(data);
//     // });
//     resolve({
//     "success":true,
//     "message":"otp sent success",
//     })
//   })
// }



function sendOtpForUser(bodyEncripted) {
    return new Promise(async(resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      var mobile = body.mobile;
      const otp = authenticator.generate(secret); // 556443
      connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
      if (error) {
      connection.destroy()
      throw error;
      }else{
        if(rowsUserData.length > 0){
        var user_code = rowsUserData[0].code          
        const token1 = jwt.sign({mobile,otp,user_code},'my_secret_key');
        var data  = {"otp":otp,"token":token1}

        var dataNew = {"token":token1}
        let sendOtp = await commonFunctionAndroid.sendOtpFunction(mobile,otp);
        let encriptedData = await commonFunctionWeb.encriptDataFunction(data);

        connection.destroy();

        resolve({
        "success":true,
        "message":"otp sent success",
       // "data":data,
        "data":encriptedData
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Please signup before login"
        })
        }
        }
       })
      }else{
        resolve({
        "success":false,
        "message":"Unknow db"
        })
       }
      })
     }
  


     function sendOtpForUserLoginForAndroid(body) {
      return new Promise(async(resolve, reject) => {
      // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
      let connection = await commonFunctionWeb.getMasterDatabaseConnection()
      if(connection != false){
        var mobile = body.mobile;
        var secret_key = body.secret_key
        const otp = authenticator.generate(secret_key); // 556443
        connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
        if (error) {
        connection.destroy()
        throw error;
        }else{
          if(rowsUserData.length > 0){
          var user_code = rowsUserData[0].code          
          const token1 = jwt.sign({mobile,otp,user_code},'my_secret_key');
          var data  = {"otp":otp,"token":token1,"user_code":user_code}
  
          var dataNew = {"token":token1}
          let sendOtp = await commonFunctionAndroid.sendOtpFunction(mobile,otp);
          // let encriptedData = await commonFunctionWeb.encriptDataFunction(data);

          connection.destroy();

          resolve({
          "success":true,
          "message":"otp sent success",
          "data":data,
          // "data":encriptedData
          })
          }else{

          connection.destroy();

          resolve({
          "success":false,
          "message":"Please signup before login"
          })
          }
          }
         })
        }else{
          resolve({
          "success":false,
          "message":"Unknow db"
          })
         }
        })
       }
  


function verifyOtpForUserSignupForAndroid(body){
  return new Promise(async (resolve, reject) => {
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
      var mobile = body.mobile;
      var name = body.name
      var otp = body.otp
      var secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
      const isValid = otplib.authenticator.check(otp, secret);
      var token = body.token;
      var decoded = jwt.decode(token);
      var finalOtp = decoded.otp
    //  console.log("cccccccccccccccccccrrrrrrrrrrrrrrrrrassshh",finalOtp,"OOOOOOOOOOOOOOOOOO",otp,"isValidisValidisValid",isValid)
      if(isValid == true && otp==`${finalOtp}`){
        connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
            if (error) {
            connection.destroy()
            throw error;
             }else{
              try{
                  var add = false;
                  if(rowsUserData.length > 0){
                  let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,rowsUserData[0].code)
                  // fetchUserData.notification_array = [];
                  // fetchUserData.unseen_notification_count = 0;
                  connection.destroy();
                  // let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
                  resolve({
                  "success":true,
                  "message":"Number already in use",
                  "token":token,
                  "data":fetchUserData,
                  //  "data":encriptedData,
                  })
                  }else{
                  add = await addUserForSignUp(connection,mobile,name);
                  }
              if(add){
              let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,add)
              // fetchUserData.notification_array = [];
              // fetchUserData.unseen_notification_count = 0;
              // let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
              connection.destroy();
              resolve({
              "success":true,
              "message":"User profile created",
              "token":token,
              "data":fetchUserData,
              // "data":encriptedData
              })
              }else{
              connection.destroy();
              resolve({
              "success":false,
              "message":"User profile not created"
              })
              }
              }catch(e){
               connection.destroy();
               console.log(e)
               resolve({
              "success":false,
              "message":"Something went wrong"
               })
               }
              }
            })
        }else{
        connection.destroy();
        resolve({
        "success":false,
        "message":"Wrong otp"
        })
        }
        }catch(e){
        connection.destroy();
        console.log(e)
        resolve({
       "success":false,
       "message":"Something went wrong"
        })
        }
       }else{
       resolve({"success": false, "message": "unknown db"});
       }
      });
}

  
function sendOtpForSignup(bodyEncripted){
  return new Promise(async(resolve, reject) => {
    // console.log("ENCRYPTED DATA =============>>>>",bodyEncripted);
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      var mobile = body.mobile;
      const otp = authenticator.generate(secret); // 556443
      const token1 = jwt.sign({mobile,otp},'my_secret_key');
      var data  = {"otp":otp,"token":token1}
      connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
      if (error) {
      connection.destroy()
      throw error;
      }else{
      if(rowsUserData.length > 0){

      connection.destroy();

      resolve({
      "success":false,
      "message":"Number is already in use"
      })
      }else{
      var dataNew = {"token":token1}
      let sendOtp = await commonFunctionAndroid.sendOtpFunction(mobile,otp);
      let encriptedData = await commonFunctionWeb.encriptDataFunction(data);

      connection.destroy();

      resolve({
      "success":true,
      "message":"otp sent success",
     // "data":data,
      "data":encriptedData
      })
      }
     }
    })
    }else{
      resolve({
      "success":false,
      "message":"Unknown db"
      })
      }
    })
  }



      function verifyOtpForUser(bodyEncripted) {
        return new Promise(async (resolve, reject) => {
        var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
        let connection = await commonFunctionWeb.getMasterDatabaseConnection()
        if(connection != false){
        try{
          var mobile = body.mobile;
          var name = body.name
          var otp = body.otp
          var secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
          const isValid = otplib.authenticator.check(otp, secret);
          var token = body.token;
          var decoded = jwt.decode(token);
          var finalOtp = decoded.otp
        //  console.log("cccccccccccccccccccrrrrrrrrrrrrrrrrrassshh",finalOtp,"OOOOOOOOOOOOOOOOOO",otp,"isValidisValidisValid",isValid)
          if(isValid == true && otp==`${finalOtp}`){
            connection.query(`SELECT * FROM users WHERE mobile = '${mobile}'  `,async(error,rowsUserData, fields)=>{
                if (error) {
                connection.destroy()
                throw error;
                 }else{
                  try{
                  if(rowsUserData.length > 0){
                  let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,rowsUserData[0].code)
                  var notification_array = []//await fetchAllNotificationOfUser(rowsUserData[0].code);
                  fetchUserData.notification_array = notification_array
                  fetchUserData.unseen_notification_count =  0//await fetchUnseenNotificationCount(rowsUserData[0].code)

                  connection.destroy();

                  let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
                  resolve({
                  "success":true,
                  "message":"user already exist",
                  "token":token,
                  //"data":fetchUserData,
                   "data":encriptedData,
                  })
                  }else{

                  connection.destroy();

                  resolve({
                  "success":false,
                  "message":"Please signup first",
                  })
                  }
                  // if(add){
                  // let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,add)
                  // fetchUserData.notification_array = [];
                  // fetchUserData.unseen_notification_count = 0;
                  // let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
                  // connection.destroy();
                  // resolve({
                  // "success":true,
                  // "message":"User profile created",
                  // "token":token,
                  // //"data":fetchUserData,
                  // "data":encriptedData

                  // })
                  // }else{
                  // resolve({
                  // "success":false,
                  // "message":"User profile not created"
                  // })
                  // }
                  }catch(e){
                   connection.destroy();
                   console.log(e)
                   resolve({
                  "success":false,
                  "message":"Something went wrong"
                   })
                   }
                  }
                })
            }else{
            connection.destroy();
            resolve({
            "success":false,
            "message":"Wrong otp"
            })
            }
            }catch(e){
            connection.destroy();
            console.log(e)
            resolve({
           "success":false,
           "message":"Something went wrong"
            })
            }
           }else{
            resolve({"success": false, "message": "unknown db"});
           }
          });
         }





function addUser(connection,mobile){
    return new Promise((resolve, reject) => {
        console.log(`INSERT INTO users SET mobile = '${mobile}'`)
        connection.query(`INSERT INTO users SET mobile = '${mobile}' `,async(error,rowsUser, fields)=>{
            if (error) {
            throw error;
             }else{
                if(rowsUser.affectedRows > 0){
                // var db_name = 'wheels_tracker'+rowsUser.insertId
                // connection.query(`UPDATE users SET db_name  = '${db_name}' , code = '${rowsUser.insertId}'`,async(error,rows, fields)=>{
                // if (error) {
                // throw error;
                // }else{
                // if(rows.affectedRows > 0){
                let createDatabaseForUser  = await createDatabaseForNewUser(connection,rowsUser.insertId)
                resolve(rowsUser.insertId)
                // }else{
                // resolve(false)
                // }
                // }
                // })
                }else{
                resolve(false)
                }
                }
              })
           })    
         }



function addUserForSignUp(connection,mobile,name){
  return new Promise((resolve, reject) => {
      console.log(`INSERT INTO users SET mobile = '${mobile}'`)
      let now = new Date();
      let expiry_date = date.addDays(now, expireAccountInDay);
  //  console.log("sssssssssssssssssssssssssssssssssssssssssssssssexpiry_dateexpiry_dateexpiry_date",expiry_date.toISOString().split("T")[0])
      connection.query(`INSERT INTO users SET mobile = '${mobile}',name = '${name}',expiry_date = '${expiry_date.toISOString().split("T")[0]}' `,async(error,rowsUser, fields)=>{
          if (error) {
          throw error;
            }else{
              if(rowsUser.affectedRows > 0){
              // var db_name = 'wheels_tracker'+rowsUser.insertId
              // connection.query(`UPDATE users SET db_name  = '${db_name}' , code = '${rowsUser.insertId}'`,async(error,rows, fields)=>{
              // if (error) {
              // throw error;
              // }else{
              // if(rows.affectedRows > 0){
              let createDatabaseForUser  = await createDatabaseForNewUser(connection,rowsUser.insertId)
              resolve(rowsUser.insertId)
              // }else{
              // resolve(false)
              // }
              // }
              // })
              }else{
              resolve(false)
              }
              }
            })
          })    
        }
      



function createDatabaseForNewUser(connection,user_code){
    return new Promise((resolve, reject) => {
    var db_name = 'wheels_tracker'+user_code
        // connection.query(`UPDATE users SET db_name  = '${db_name}' , code = '${user_code}'`,async(error,rows, fields)=>{
        // if (error) {
        // throw error;
        // }else{
        //if(rows.affectedRows > 0){
        connection.query(`CREATE DATABASE ${db_name}`, async(error0, rowsDatabase, fileds0)=>{
        if (error0) {

        connection.destroy()
        
        reject(error0);
        } else {
        if(rowsDatabase.affectedRows > 0){
        let connectionNewDbWithUserCode = await commonFunctionWeb.getSpecificDatabaseConnection(user_code)
        if(connectionNewDbWithUserCode != false){
        try{
            connectionNewDbWithUserCode.query(commonFunctionWeb.createTables, (error1, rowsDatabaseOfUser, fields1)=>{
            if (error1) {

            connectionNewDbWithUserCode.destroy();

            reject(error1);
            }else{
            if(rowsDatabaseOfUser.affectedRows > 0){

            connectionNewDbWithUserCode.destroy();

            resolve(true)
            }else{

            connectionNewDbWithUserCode.destroy();

            resolve(false)
           }
         }
       })
    }catch(e){
    console.log(e)

    connectionNewDbWithUserCode.destroy();

    resolve(false)
    }
    }else{
    resolve({"success": false, "message": "unknown db"});    
    }
    }else{
    resolve(false)
    }
    }
    })
    // }else{
  //  resolve(false)
    // }
   //}
 //})
 })
}

function getEncriptedDataOfJson(body){
  return new Promise(async function(resolve, reject) {
    let compressData = await commonFunctionWeb.compressDataFunction(body)
    let encriptData = await commonFunctionWeb.encriptDataFunction(compressData)

    resolve({"data":encriptData})
  })
}


function getDecriptedDataOfJson(body){
  return new Promise(async function(resolve, reject) {
    let decriptData = await commonFunctionWeb.decriptDataFunction(body.data)
    // let decompressData = await commonFunctionWeb.decompressDataFunction(decriptData)
    resolve(decriptData)
  })
}


function sendOtpForChangeMobileOfUser(bodyEncripted){
  return new Promise(async(resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    var mobile = body.mobile;
     const otp = authenticator.generate(secret); // 556443
     const token1 = jwt.sign({mobile,otp},'my_secret_key');
     try{
      connection.query(`SELECT code,NOW() AS created FROM users WHERE mobile = '${mobile}' `,async(error,rows, fields)=>{
       if (error) {
        connection.destroy()
        throw error;
       }else{
        if(rows.length > 0){

        connection.destroy()

        resolve({
        "success":false,
        "message":"Number is already in use",
        })
        }else{

        var data  = {"otp":otp,"token":token1}
        let sendOtp = await  commonFunctionAndroid.sendOtpFunction(mobile,otp);          
        let encriptedData = await commonFunctionWeb.encriptDataFunction(data);

        connection.destroy()

          resolve({
          "success":true,
          "message":"otp sent",
          //"data":data,
          "data":encriptedData,
          })
          }
         }
       })
       }catch(e){

        connection.destroy()

        console.log(e)
        resolve({
        "success":false,
        "message":"Something went wrong"
        })
       }
      }else{
      resolve({"success": false, "message": "unknown db"});    
      }
     })
    }


    


function verifyOtpForSignup(bodyEncripted){
  return new Promise(async (resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
      var mobile = body.mobile;
      var name = body.name
      var otp = body.otp
      var secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
      const isValid = otplib.authenticator.check(otp, secret);
      var token = body.token;
      var decoded = jwt.decode(token);
      var finalOtp = decoded.otp
    //  console.log("cccccccccccccccccccrrrrrrrrrrrrrrrrrassshh",finalOtp,"OOOOOOOOOOOOOOOOOO",otp,"isValidisValidisValid",isValid)
      if(isValid == true && otp==`${finalOtp}`){
        connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
            if (error) {

            connection.destroy()

            throw error;
             }else{
              try{
              var add = false;
              if(rowsUserData.length > 0){
              let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,rowsUserData[0].code)
              fetchUserData.notification_array = [];
              fetchUserData.unseen_notification_count = 0;

              connection.destroy();

              let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);
              resolve({
              "success":true,
              "message":"Number already in use",
              "token":token,
              //"data":fetchUserData,
               "data":encriptedData,
              })
              }else{
              add = await addUserForSignUp(connection,mobile,name);
              }
              if(add){

              let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,add)
              fetchUserData.notification_array = [];
              fetchUserData.unseen_notification_count = 0;
              let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);

              connection.destroy();

              resolve({
              "success":true,
              "message":"User profile created",
              "token":token,
              //"data":fetchUserData,
              "data":encriptedData
              })
              }else{

              connection.destroy();
              
              resolve({
              "success":false,
              "message":"User profile not created"
              })
              }
              }catch(e){

               connection.destroy();

               console.log(e)
               resolve({
              "success":false,
              "message":"Something went wrong"
               })
               }
              }
            })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"Wrong otp"
        })
        }
        }catch(e){

        connection.destroy();

        console.log(e)
        resolve({
       "success":false,
       "message":"Something went wrong"
        })
        }
       }else{
       resolve({"success": false, "message": "unknown db"});
       }
      });
     }




function checkUserExistOrNot(bodyEncripted){
  return new Promise(async (resolve, reject) => {
    var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data)
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    try{
    var decoded = jwt.decode(body.token);
    // console.log("decoded===========================================>>>>TOKEENN",decoded)
    if(decoded.user_code != 0 || decoded.user_code != undefined){
    let fetchUserData = await commonFunctionWeb.fetchSpecificUserDetails(connection,decoded.user_code)
    let encriptedData = await commonFunctionWeb.encriptDataFunction(fetchUserData);

    connection.destroy();

    resolve({
    "success":true,
    "message":"User data fetched successfully",
    // "data":fetchUserData
    "data":encriptedData
    })
    }else{

    connection.destroy();

    resolve({
    "success":false,
    "message":"Wrong token provided"
    })
    }
    }catch(e){

    connection.destroy();
    
    console.log(e)
    resolve({
    "success":false,
    "message":"Something went wrong"
    })
    }
    }else{
    resolve({
    "success":false,
    "message":"Unknow db"
    })
   } 
  })
 }



 function fetchAllNotificationOfUser(user_code){
  return new Promise(async function(resolve, reject){
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(user_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,title,message,icon_url,created FROM notification ORDER BY created DESC`,async(error,rows, fields)=>{
    if (error) {
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error)
    }else{
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(rows);
        connection.destroy();
        resolve(rows)        
      }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve([])        
    }
    }else{
    resolve([]);
   }
  })
 }

 function  fetchUnseenNotificationCount(user_code){
  return new Promise(async function(resolve, reject){
    let connection = await commonFunctionWeb.getSpecificDatabaseConnection(user_code)
    if(connection != false){
    try{
    connection.query(`SELECT code,title,message,icon_url,created FROM notification WHERE seen = 0`,async(error,rows, fields)=>{
    if (error) {
    commonFunctionWeb.createErrorLogFile(error);commonFunctionWeb.sendErrorsInEmail(error);  
    connection.destroy();
    resolve(0)
    }else{
        // let encriptedData = await commonFunctionWeb.encriptDataFunction(rows);
        connection.destroy();
        resolve(rows.length)        
      }
    })
    }catch(e){
    connection.destroy();
    console.log(e)
    resolve(0)        
    }
    }else{
    resolve(0);
   }
  })
 }


 function verifyOtpForUserLoginForAndroid(body){
  return new Promise(async function(resolve, reject){
    let verifyOtp = await commonFunctionAndroid.verifyOtp(body);
    var user_code = body.user_code
    if(verifyOtp){
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
    let userDetails = await  commonFunctionAndroid.fetchSpecificUserDetails(connection,user_code)
    if(userDetails != null){

      connection.destroy()

    resolve({
      "success":true,
      "message":"Otp Verified Successfully",
      "data":userDetails
      })
    }else{

      connection.destroy()

      resolve({
      "success":false,
      "message":"No data found"
      })
    }
    }else{
      resolve({
      "success":false,
      "message":"Unknow db"
      })
    }
    }else{
      resolve({
      "success":false,
      "message":"Wrong otp"
     })
   }
  })
 }

 function sendOtpForUserSignupForAndroid(body){
  return new Promise(async(resolve, reject) => {
    // console.log("ENCRYPTED DATA =============>>>>",bodyEncripted);
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      var mobile = body.mobile;
      const otp = authenticator.generate(secret); // 556443
      const token1 = jwt.sign({mobile,otp},'my_secret_key');
      var data  = {"otp":otp,"token":token1}
      connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' `,async(error,rowsUserData, fields)=>{
      if (error) {
      connection.destroy()
      throw error;
      }else{
      if(rowsUserData.length > 0){

      connection.destroy();

      resolve({
      "success":false,
      "message":"Number is already in use"
      })
      }else{
      var dataNew = {"token":token1}
      let sendOtp = await commonFunctionAndroid.sendOtpFunction(mobile,otp);
      // let encriptedData = await commonFunctionWeb.encriptDataFunction(data);

      connection.destroy();

      resolve({
      "success":true,
      "message":"otp sent success",
      "data":data,
      // "data":encriptedData
      })
      }
     }
    })
    }else{
      resolve({
      "success":false,
      "message":"Unknown db"
      })
      }
    })
 }




 function fetchAppPriceForUser(){
  return new Promise(async(resolve, reject) => {
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      connection.query(`SELECT * FROM app_price_details`,async(error,rowsAppPrice, fields)=>{
      if (error) {

      connection.destroy()

      throw error;
      }else{
        if(rowsAppPrice.length > 0){

        connection.destroy()

        resolve({
        "success":true,
        "message":"App Price Details Fetched Successfully",
        "data":rowsAppPrice[0],
        })
        }else{

        connection.destroy();

        resolve({
        "success":false,
        "message":"App Price Details Not Fetched"
        })
        }
        }
       })
      }else{
        resolve({
        "success":false,
        "message":"Unknow db"
        })
       }
      })
     }


function adminSignin(req,res){
  return new Promise(async(resolve, reject) => {
    // var body = await commonFunctionWeb.decriptDataFunction(bodyEncripted.data);    
    let connection = await commonFunctionWeb.getMasterDatabaseConnection()
    if(connection != false){
      var body = req.body
      var mobile = body.mobile
      var password = body.password 
      connection.query(`SELECT * FROM users WHERE mobile = '${mobile}' AND password = '${password}' `,async(error,rowsUserData, fields)=>{
        if (error) {
        connection.destroy()
        throw error;
         }else{
            if(rowsUserData.length > 0){
              const token1 = jwt.sign({mobile,password},'my_secret_key');
              resolve({
                "success":true,
                "message":"User login successfully",
                "data":rowsUserData[0],
                "token":token1
              })
            }else{
              resolve({
                "success":false,
                "message":"Invalid Mobile Or Password"
              })
            }
         }
      })
    }else{
      resolve({
        "success":false,
        "message":"Unknown dp"
      })
    }
  })
}

     
module.exports = authModel;
