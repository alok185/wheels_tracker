var db = require("../../config/database");
var dbFunc = require('../../config/db-function');
var commonFunctionAdmin = require('../../commons/admin/commonFuncAdmin');

var multer = require('multer')
var ip = require('ip');
var Jimp = require("jimp");


var aboutUsModel = {
    addAboutUsDetails:addAboutUsDetails,
    fetchAboutUsDetails:fetchAboutUsDetails,
    deleteAboutUsData:deleteAboutUsData,
    updateAboutUsDetails:updateAboutUsDetails,
}


function addAboutUsDetails(body){
  return new Promise(async(resolve, reject) => {
    //var body = await commonFunctionAdmin.decriptDataFunction(bodyEncripted.data)
    try{
      db.query(` INSERT INTO about_us SET content='${await commonFunctionAdmin.mysqlRealEscapeString(body.content)}'`,async (error,rows, fields)=>{
          if (error) {
            dbFunc.connectionRelease;
            throw error;
            }else{
            if(rows.affectedRows > 0){
            resolve({
            "success":true,
            "message":"About us data added",
            })                    
            }else{
            resolve({
            "success":false,
            "message":"About us data added"
            })
            }
           }
          })
          }catch(e){
          console.log(e)
          resolve({
          "success":false,
          "message":"Something went wrong"
          })
          }
         })
        }



    
function fetchAboutUsDetails(body){
  return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM about_us ORDER BY created DESC`,async (error,rows, fields)=>{
          if (error) {
          //commonFunctionAdmin.createErrorLogFile(error);commonFunctionAdmin.sendErrorsInEmail(error);
            dbFunc.connectionRelease;
            throw error;
            }else{
              if(rows.length > 0){
              //let encriptedData = await commonFunctionAdmin.encriptDataFunction(rows[0])
              resolve({
              "success":true,
              "message":"About us data fetched success",
              "data":rows[0],
              })                    
              }else{
              resolve({
              "success":false,
              "message":"No data found"
              })
              }
              }
             })
             })
             }
          


function deleteAboutUsData(req,res){
    return new Promise((resolve, reject) => {
      db.query(`DELETE  FROM about_us WHERE code ='${req.params.code}' `,async(error,rows, fields)=>{
        if (error) {
          dbFunc.connectionRelease;
          resolve (error)
          }else{ 
          if(rows.affectedRows > 0){
          resolve({
          "success":true,
          "message":"About us data deleted success",
          })  
          }else{
          resolve({
          "success":false,
          "message":"About us data not deleted ",
          })  
          }
         }
        })
       })
      }




function updateAboutUsDetails(body){
    return new Promise(async(resolve, reject) => {
        //var body = await commonFunctionAdmin.decriptDataFunction(bodyEncripted.data)
        try{
          db.query(` UPDATE about_us SET content='${await commonFunctionAdmin.mysqlRealEscapeString(body.content)}' WHERE code = '${body.about_us_code}'`,async (error,rows, fields)=>{
              if (error) {
                dbFunc.connectionRelease;
                throw error;
                }else{
                if(rows.affectedRows > 0){
                resolve({
                "success":true,
                "message":"About us data updated",
                })                    
                }else{
                resolve({
                "success":false,
                "message":"About us data not updated"
                })
                }
               }
              })
              }catch(e){
                console.log(e)
                resolve({
                "success":false,
                "message":"Something went wrong"
                })
              }
             })
            }





          

module.exports = aboutUsModel;
