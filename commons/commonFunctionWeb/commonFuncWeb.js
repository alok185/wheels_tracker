var db = require("../../config/database");
var otplib = require('otplib');
var authenticator = require('otplib/authenticator');
const jwt = require('jsonwebtoken');
let date = require('date-and-time');
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';
const nodemailer = require('nodemailer');
var CronJob = require('cron').CronJob;
var admin = require("firebase-admin");
var log4js = require('log4js');
const fs = require('fs');
const cryptLib = require('@skavinvarnan/cryptlib');
var CryptoJS = require("crypto-js");
var io = require('socket.io-client');
var Buffer = require('buffer').Buffer;
var zlib = require('zlib');
var pako = require('pako');

// var fcm = require('fcm-notification');
// var FCM = new fcm("./models/service-clap-2-firebase-adminsdk-q0lb6-df8103146e.json"); 
// admin.initializeApp({
// credential: admin.credential.cert('./models/service-clap-2-firebase-adminsdk-q0lb6-df8103146e.json'),
// databaseURL: "https://service-clap-2.firebaseio.com"
// });

const webpush = require('web-push');
const cors = require('cors');
const vapidKeys = webpush.generateVAPIDKeys();

const PUBLIC_VAPID = 'BJuHA7hTXW3UbleA1P5HrGoRbwn2IK-GTz1UilkYc7OyoQlUdxxahOG4quiVdG3rqavQU405yCDCoafZmO76cHk';
const PRIVATE_VAPID = 'fjVCpt1x0ZaJkGxvkGqhxAtRSZd3dJ7pW3Rv_Zsy6FY';

webpush.setVapidDetails('mailto:you@domain.com', PUBLIC_VAPID, PRIVATE_VAPID);
const msg91 = require('msg91-promise');
const API_KEY = '230364AslqNHOaR5b6932aa'; // Your API key
const SENDER_ID = 'SRCLAP'; // Your sender id 
const ROUTE = 4; // transactional route

const msg91SMS = msg91(API_KEY, SENDER_ID, ROUTE);


var port = ":2022/";
var thumbPort = ":2022";




function verifyOtp(body){
  return new Promise(function(resolve, reject) {
  var mobile = body.mobile;
  var email =  body.email;
  var otp = body.otp
  var secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
  const isValid = otplib.authenticator.check(otp, secret);
  var token = body.token;
  var decoded = jwt.decode(token);
  var finalOtp = decoded.otp
  // console.log("cccccccccccccccccccrrrrrrrrrrrrrrrrrassshhTokenOTP",finalOtp,"======",otp)
  if(isValid == true && otp==`${finalOtp}`){
  resolve(true)
  }else{
  resolve(false)
  }
 })
}

    
function getSpecificDatabaseConnection(database_code){
  return new Promise(function(resolve, reject) {
    db.specificConnection('wheels_tracker'+database_code, false).getConnection((err, connection)=>{
    if(err){
    console.log(err);
    resolve(false);
    }else{
    try{
    console.log('wheels_tracker'+database_code)
    resolve(connection)
    }catch(e){
    console.log(e)
    resolve(false);
    }
    }
  })
 })
}



function getMasterDatabaseConnection(){
  return new Promise(function(resolve, reject) {
    db.specificConnection('wheels_tracker_master_final', false).getConnection((err, connection)=>{
    if(err){
    console.log(err);
    resolve(false);
    }else{
    try{
    // console.log('connected to -->>  wheels_tracker_master_final')
    resolve(connection)
    }catch(e){
    console.log(e)
    resolve(false);
    }
    }
  })
 })
}


function encriptDataFunction(string_text){
  return new Promise(async function(resolve, reject) {
  var stringOfItemObject = JSON.stringify(string_text)
  const plainText = stringOfItemObject;
  const key = "MYSECRETPASS";
  //const cipherText = CryptoJS.AES.encrypt(plainText,key)
  var cipherText = CryptoJS.AES.encrypt(plainText, key);
  //console.log('cipherText %s', cipherText.toString());
  resolve(cipherText.toString())
  })
}


function decriptDataFunction(cipherText){
  return new Promise(async function(resolve, reject) {  
  const key = "MYSECRETPASS";
//  const decryptedString = cryptLib.decryptCipherTextWithRandomIV(cipherText, key);
  //console.log("DDDDDDDDDDDDDDDDDECRTINGGG",cipherText);
  var bytes  = CryptoJS.AES.decrypt(cipherText.toString(), key);
  var decryptedString = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  //console.log('decryptedString %s', decryptedString);
  resolve(decryptedString)
  })
}

function compressDataFunction(inputData){
  return new Promise(async function(resolve, reject) {
  // const gzip = zlib.createGzip();
  // // fs.statSync(input).size
  // console.log("NormalData Data Size =====>>>>>>",input.length)
  
  // zlib.deflate(input, (err, buffer) => {
  // if (!err) {
  // // console.log("BASE_64",buffer.toString('base64'));
  // compressedData = buffer.toString('base64')
  // console.log("Compressed Data Size =====>>>>>>",compressedData.length)

  // resolve(compressedData)
  // }else {
  // resolve(null)
  //  }
  // });
  var input = JSON.stringify(inputData);
  var binaryString = pako.deflate(input, { to: 'string' });
  console.log("NormalData Data Size =====>>>>>>",input.length)
  console.log("Compressed Data Size =====>>>>>>",binaryString.length)

  // var restored = JSON.parse(pako.inflate(binaryString, { to: 'string' }));
  resolve(binaryString)
 })
}

function decompressDataFunction(binaryString){
  return new Promise(async function(resolve, reject) {
    try{
    var restored = JSON.parse(pako.inflate(binaryString, { to: 'string' }));
    resolve(restored)
    }catch(e){
      console.log(e)
      resolve("")
    }
})
}



function currentDateTime(){
  return new Promise(async function(resolve, reject) {
    let connection = await getMasterDatabaseConnection()
    if(connection != false){
    connection.query(`SELECT NOW() AS currentDateTime`, (error, rowsCurrentDateTime, fields)=>{
    if (error) {
    reject(error);
    }else {
    if(rowsCurrentDateTime.length > 0){
    resolve(rowsCurrentDateTime[0].currentDateTime);
    }else{
    resolve("0000-00-00 00:00:00");
    }
   }
  })
  }
 })
}



function createErrorLogFile(error){
  log4js.configure({
  appenders: { cheese: { type: 'file', filename: 'error.log' } },
  categories: { default: { appenders: ['cheese'], level: 'error' } }
  });             
  const logger = log4js.getLogger('cheese');
  logger.error(error);
 } 



      
 function sendErrorsInEmail(error){
  return new Promise(async (resolve, reject) => {
  // create reusable transporter object using the default SMTP transport
  try{
    let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
    user: 'alokpoddar110@gmail.com', // generated user
    pass:  "Alok@1850123"// generated password
    }
    });
    let mailOptions = {
    from: 'alokpoddar110@gmail.com' , // sender address
    to: 'alokpoddar110@gmail.com', // list of receivers,saraswatshalu1234@gmail.com
    subject: 'Error Details', // Subject line
//  text: '', // plain text body
    html: '<b>'+error+'</b>', // html body
    attachments: [    {   // file on disk as an attachment
    filename: 'error.log',
    path: './error.log' // stream this file
    }]
    };
    transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
    return console.log(error);
    }
    console.log(' Email sent: %s', info.messageId);
    fs.unlink('./error.log', (err) => {
    if (err) throw err;
    });
    resolve(true)
   });
   }catch(e){
     console.log(e)
     return e
   }
  }); 
 }

function fetchSpecificMineDetails(connection,mine_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,name,expense,address,created,modified FROM mines WHERE code = '${mine_code}' `, async (error, rows, fields)=>{
    if(error){
    connection.release();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    //connection.destroy();
    //let encriptedData = await encriptDataFunction(rows);
    resolve(rows[0])
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
 })
}



function fetchSpecificPartyDetails(connection,party_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,name,mobile,address,party_type_code,opening_balance,type_of_ob,created,modified FROM parties WHERE code = '${party_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    //connection.release();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    //connection.destroy();
    //let encriptedData = await encriptDataFunction(rows);
    resolve(rows[0])
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
 })
}


function fetchSpecificVehicleDetails(connection,vehicle_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM vehicles WHERE code = '${vehicle_code}'`, async (error, rows, fields)=>{
    if(error){
    //connection.release();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    //connection.destroy();
    var item = {}
    var driver_name = "";
    var driver_mobile = "";
    let driver_details = await fetchSpecificDriverDetails(connection,rows[0].driver_code);
    if(driver_details != null){
    driver_name = driver_details.name
    driver_mobile = driver_details.mobile
    }
    item.code = rows[0].code
    item.label = rows[0].label,
    item.vehicle_no = rows[0].vehicle_no,
    item.driver_code = rows[0].driver_code,
    item.driver_name = driver_name,
    item.driver_mobile = driver_mobile,
    item.owner_name = rows[0].owner_name
    resolve(item)
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
 })
}

function fetchSpecificVehicleDetailsWithCondition(connection,quer){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(quer, async (error, rows, fields)=>{
    if(error){
    //connection.release();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    //connection.destroy();
    var item = {}
    var driver_name = "";
    var driver_mobile = "";
    let driver_details = await fetchSpecificDriverDetails(connection,rows[0].driver_code);
    if(driver_details != null){
    driver_name = driver_details.name
    driver_mobile = driver_details.mobile
    }
    item.code = rows[0].code
    item.label = rows[0].label,
    item.vehicle_no = rows[0].vehicle_no,
    item.driver_code = rows[0].driver_code,
    item.driver_name = driver_name,
    item.driver_mobile = driver_mobile,
    item.owner_name = rows[0].owner_name
    resolve(item)
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
 })
}




function fetchSpecificDriverDetails(connection,driver_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT code,name,mobile,address,adhaar_no,license_no,salary,profile_image_url,profile_image_url_thumbnail,document_image_url_1,document_image_url_2,document_image_url_thumbnail_1,document_image_url_thumbnail_2,created FROM drivers WHERE  code = '${driver_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    //connection.destroy();
    createErrorLogFile(error);sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    //console.log(`SELECT code,name,mobile,address,adhaar_no,license_no,profile_image_url,profile_image_url_thumbnail,document_image_url,document_image_url_thumbnail,created FROM drivers WHERE status != 2 AND code = '${driver_code}' ORDER BY created DESC `,rows)
    if(rows.length > 0){
    //connection.destroy();
   // let encriptedData = await encriptDataFunction(rows);
    resolve(rows[0])
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
  })  
  }


function fetchSpecificPaymentDetails(connection,payment_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM payments WHERE  code = '${payment_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    //connection.destroy();
    createErrorLogFile(error);sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var item = {};
    item.code = rows[0].code,
    item.bill_no = rows[0].bill_no,
    item.bill_date = rows[0].bill_date,
    item.from_date = rows[0].from_date,
    item.to_date = rows[0].to_date,
    item.amount = rows[0].amount,
    item.purpose = rows[0].purpose,
    item.payment_type = rows[0].payment_type,
    item.ref_no = rows[0].ref_no,
    item.remarks = rows[0].remarks,
    item.image_url_1 = rows[0].image_url_1,
    item.image_url_2 = rows[0].	image_url_2,
    item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
    item.created = rows[0].created
    resolve(item)
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
  })  
}



function fetchSpecificRouteDetails(connection,route_commission_code){
  return new Promise(async function(resolve, reject){
    try{
    connection.query(`SELECT * FROM route_commission WHERE code = '${route_commission_code}' ORDER BY created DESC  `, async (error, rows, fields)=>{
    if(error){
    //connection.destroy();
    createErrorLogFile(error);sendErrorsInEmail(error);  
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var mine_name  = '';
    var party_name = '';
    var expense = 0
    let mine_details = await fetchSpecificMineDetails(connection,rows[0].mine_code)
    let party_details = await fetchSpecificPartyDetails(connection,rows[0].party_code)
    if(party_details != null){
      party_name = party_details.name
    }
    if(mine_details != null){
      mine_name = mine_details.name
      expense = mine_details.expense
    }
    var item = {};
    item.code = rows[0].code,
    item.mine_code = rows[0].mine_code,
    item.mine_name = mine_name
    item.party_code = rows[0].party_code,
    item.party_name = party_name
    item.short_rate = rows[0].short_rate,
    item.purpose = rows[0].purpose,
    item.freight_rate = rows[0].freight_rate,
    item.ref_no = rows[0].ref_no,
    item.driver_commission = rows[0].driver_commission,
    item.expense = expense
    item.created = rows[0].created
    resolve(item)
    }else{
    //connection.destroy();
    resolve(null)
    }
    }
    })
    }catch(e){
    //connection.destroy();
    console.log(e)
    resolve(null)
   }
  })
}


function fetchSpecificUserDetails(connection,user_code){
  return new Promise(async function(resolve, reject){
    connection.query(`SELECT * FROM users WHERE  code = '${user_code}' ORDER BY created`,async (error,rows, fields)=>{
        if (error) {
        //connection.destroy()
        resolve(error)
        }else {
              try{
              if(rows.length > 0){  
              //var delivery_boy = [];
               var item = {};
               item.code = rows[0].code
               item.name = rows[0].name,
               item.mobile = rows[0].mobile
               item.address = rows[0].address
               item.profile_image_url = rows[0].profile_image_url
               item.profile_image_url_thumbnail = rows[0].profile_image_url_thumbnail,
              // item.password = rows[0].password 
               item.db_name =  "wheels_tracker"+rows[0].code 
               item.created = rows[0].created 
               item.city = rows[0].city 
               item.email = rows[0].email,
               item.state = rows[0].state,
               item.dob = rows[0].dob,
               item.expiry_date = rows[0].expiry_date
               item.account_expiry_status = rows[0].account_expiry_status
               item.database_code = rows[0].code
              //  item.verify_status = rows[0].verify_status
              //  item.block_status = rows[0].block_status,
              // item.total_orders = await fetchDeliveryBoyOrderList(rows[0].code)
               //delivery_boy.push(item)
              //if(delivery_boy.length > 0){
              resolve(item)
              }else{
              resolve(null) 
              }
              }catch(e){
              console.log(e)
              reject(null)
              }
             }
            })
           })
         }



var createTables =
         'CREATE TABLE attendance LIKE wheels_tracker1.attendance;'+
         'CREATE TABLE account_upgrade LIKE wheels_tracker1.account_upgrade;'+
        //  'CREATE TABLE attendance LIKE wheels_tracker1.attendance;'+
         'CREATE TABLE auto_parts LIKE wheels_tracker1.auto_parts; '+
         'CREATE TABLE billty LIKE wheels_tracker1.billty; '+
         'CREATE TABLE diesel_rate LIKE wheels_tracker1.diesel_rate; '+
         'CREATE TABLE drivers LIKE wheels_tracker1.drivers;'+
         'CREATE TABLE driver_payments LIKE wheels_tracker1.driver_payments;'+
         'CREATE TABLE fuel_billty LIKE wheels_tracker1.fuel_billty;'+
         'CREATE TABLE fuel_entry LIKE wheels_tracker1.fuel_entry;'+
         'CREATE TABLE maintenance LIKE wheels_tracker1.maintenance;'+
         'CREATE TABLE mines LIKE wheels_tracker1.mines;'+
         'CREATE TABLE notification LIKE wheels_tracker1.notification;'+
         'CREATE TABLE parties LIKE wheels_tracker1.parties;'+
         'CREATE TABLE party_payments LIKE wheels_tracker1.party_payments;'+
         'CREATE TABLE party_type LIKE wheels_tracker1.party_type;'+
         'CREATE TABLE payments LIKE wheels_tracker1.payments;'+
         'CREATE TABLE route_commission LIKE wheels_tracker1.route_commission;'+
         'CREATE TABLE vehicles LIKE wheels_tracker1.vehicles;'+
         'CREATE TABLE voucher_receipt LIKE wheels_tracker1.voucher_receipt;';




function fetchSpecificBilltyDetails(connection,billty_code){
  return new Promise(async function(resolve, reject){
  try{
    connection.query(`SELECT * FROM billty  WHERE code = '${billty_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
    if(error){
    createErrorLogFile(error);sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var billtyData = [];
    var vehicle_label = '';
    var party_name = "";
    var vehicle_no = '';
    var driver_name = '';
    var mine_name = '';
    var route_details = {};
    let vehicle_details = await fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
    let party_details = await fetchSpecificPartyDetails(connection,rows[0].party_code)
    let routeDetails = await fetchSpecificRouteDetails(connection,rows[0].route_commission_code)
    let driverDetails = await fetchSpecificDriverDetails(connection,rows[0].driver_code)

    if(routeDetails != null){
    route_details = routeDetails
    }
    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    if(driverDetails != null){
      driver_name = driverDetails.name
      // driver_mobile = driverDetails.mobile
    }
    var item = {}
    item.code = rows[0].code
    item.route_code = rows[0].route_commission_code
    item.billty_no = rows[0].billty_no
    item.party_code = rows[0].party_code
    item.party_name = party_name
    item.bill_date = rows[0].bill_date,
    item.route_details = route_details
    item.vehicle_code = rows[0].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.driver_name = driver_name
    item.advance = rows[0].advance
    item.remarks = rows[0].remarks,
    item.freight_rate = rows[0].freight_rate
    item.challan_weight = rows[0].challan_weight
    item.diesel = rows[0].diesel,
    item.received_weight = rows[0].received_weight,
    item.short_weight = rows[0].short_weight,
    item.image_url_1 = rows[0].image_url_1,
    item.image_url_2 = rows[0].	image_url_2,
    item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
    item.created = rows[0].created
    resolve(item)
    }else{
    resolve(null)
    }
    }catch(e){
    console.log(e)
    resolve(null)    
    } 
    }
    })
    }catch(e){
    console.log(e)
    resolve(null)
   }
  })
}



function fetchSpecificfuelEntryDetails(connection,fuel_entry_code){
  return new Promise(async function(resolve, reject){
  try{
    connection.query(`SELECT * FROM fuel_entry WHERE code = '${fuel_entry_code}'`, async (error, rows, fields)=>{
    if(error){
    createErrorLogFile(error);sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    try{
    if(rows.length > 0){
    var vehicle_label = '';
    var party_name = "";
    var vehicle_no = '';
    let vehicle_details = await fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
    let party_details = await fetchSpecificPartyDetails(connection,rows[0].party_code)

    if(vehicle_details != null){
    vehicle_label = vehicle_details.label
    vehicle_no = vehicle_details.vehicle_no
    }
    if(party_details != null){
    party_name = party_details.name
    }
    var item = {}
    item.code = rows[0].code
    item.challan_no = rows[0].challan_no
    item.party_code = rows[0].party_code
    item.party_name = party_name
    item.bill_date = rows[0].bill_date,

    item.vehicle_code = rows[0].vehicle_code
    item.vehicle_label = vehicle_label
    item.vehicle_no = vehicle_no
    item.fuel_rate = rows[0].fuel_rate
    item.quantity = rows[0].quantity
    item.remarks = rows[0].remarks,
    item.amount = rows[0].amount
    item.image_url_1 = rows[0].image_url_1,
    item.image_url_2 = rows[0].	image_url_2,
    item.image_url_1_thumbnail = rows[0].image_url_1_thumbnail,
    item.image_url_2_thumbnail = rows[0].image_url_2_thumbnail,
    item.created = rows[0].created
    resolve(item)
    }else{
    resolve(null)
    }
    }catch(e){
    console.log(e)
    resolve(null)    
    } 
    }
  })
  }catch(e){
  console.log(e)
  resolve(null)
  }
})
}




function fetchSpecificAutoPartChallanDetails(connection,auto_part_challan_code){
  return new Promise(async function(resolve, reject){
    connection.query(`SELECT * FROM auto_parts WHERE code = '${auto_part_challan_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
    if(error){
    createErrorLogFile(error);sendErrorsInEmail(error);  
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
        var item = {};
        var party_name = ''
        var vehicle_label = ''
        var  vehicle_no = ''
        var party_details = await fetchSpecificPartyDetails(connection, rows[0].party_code)
        if(party_details != null){
        party_name = party_details.name
        }
        var vehicle_details = await fetchSpecificVehicleDetails(connection,rows[0].vehicle_code)
        if(vehicle_details != null){
        vehicle_label = vehicle_details.label
        vehicle_no = vehicle_details.vehicle_no
        }
        item.code = rows[0].code,
        item.challan_no = rows[0].challan_no,
        item.party_code = rows[0].party_code,
        item.party_name = party_name,
        item.vehicle_code = rows[0].vehicle_code,
        item.vehicle_label = vehicle_label,
        item.vehicle_no = vehicle_no,
        item.amount = rows[0].amount,
        item.bill_date = rows[0].bill_date,
        item.remarks = rows[0].remarks,
        item.created = rows[0].created
      resolve(item)
      }else{
      resolve(null)  
      }
      }
     })
   })
  }




  function fetchSpecificDriverPaymentDetails(connection,payment_code){
    return new Promise(async function(resolve, reject){
     connection.query(`SELECT * FROM driver_payments WHERE payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      if(rows.length > 0){
      var paymentData = [];
      var driver_name = '';
      let driver_details = await fetchSpecificDriverDetails(connection,rows[0].driver_code)
      if(driver_details != null){
      driver_name = driver_details.name
      }
      let payment_details = await fetchSpecificPaymentDetails(connection,rows[0].payment_code)
      if(payment_details != null){
      var item = {}
      item.driver_code = rows[0].driver_code
      item.driver_name = driver_name
      item.payment_code = rows[0].payment_code
      item.bill_no = payment_details.bill_no,
      item.bill_date = payment_details.bill_date,
      item.from_date = payment_details.from_date,
      item.to_date = payment_details.to_date,
      item.amount = payment_details.amount,
      item.purpose = payment_details.purpose,
      item.payment_type = payment_details.payment_type,
      item.ref_no = payment_details.ref_no,
      item.remarks = payment_details.remarks,
      item.image_url_1 = payment_details.image_url_1,
      item.image_url_2 = payment_details.	image_url_2,
      item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail;
      item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;
      resolve(item)
      }else{
      resolve(null)    
      }
      }else{
      resolve(null)
      }
    }
  })
})
}



function fetchSpecificPartyPaymentDetails(connection,payment_code){
  return new Promise(async function(resolve, reject){
   connection.query(`SELECT * FROM party_payments WHERE status != 2 AND payment_code = '${payment_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
    if(error){
    createErrorLogFile(error);sendErrorsInEmail(error);
    connection.destroy();
    resolve(error);
    }else{ 
    if(rows.length > 0){
    var paymentData = [];
    var party_name = '';
    let party_details = await fetchSpecificPartyDetails(connection,rows[0].party_code)
    if(party_details != null){
      party_name = party_details.name
    }
    let payment_details = await fetchSpecificPaymentDetails(connection,rows[0].payment_code)
    if(payment_details != null){
    var item = {}
    item.party_code = rows[0].party_code
    item.party_name = party_name
    item.payment_code = rows[0].payment_code
    item.bill_no = payment_details.bill_no,
    item.bill_date = payment_details.bill_date,
    item.from_date = payment_details.from_date,
    item.to_date = payment_details.to_date,
    item.amount = payment_details.amount,
    item.purpose = payment_details.purpose,
    item.payment_type = payment_details.payment_type,
    item.ref_no = payment_details.ref_no,
    item.remarks = payment_details.remarks,
    item.image_url_1 = payment_details.image_url_1,
    item.image_url_2 = payment_details.	image_url_2,
    item.image_url_1_thumbnail = payment_details.image_url_1_thumbnail;
    item.image_url_2_thumbnail = payment_details.image_url_2_thumbnail;
    resolve(item)
    }else{
    resolve(null)    
    }
    }else{
    resolve(null)
    }
  }
})
})
}




function fetchSpecificFuelEntryDieselRateDetails(connection,fuel_entry_diesel_rate_code){
  return new Promise(async function(resolve, reject){
  try{
  connection.query(`SELECT * FROM diesel_rate WHERE code = '${fuel_entry_diesel_rate_code}' ORDER BY entry_date DESC `, async (error, rows, fields)=>{
  if(error){
  createErrorLogFile(error);sendErrorsInEmail(error);  
  connection.destroy();
  resolve(error);
  }else{ 
    // console.log(`SELECT * FROM diesel_rate WHERE code = '${fuel_entry_diesel_rate_code}' ORDER BY entry_date DESC `,rows)
  if(rows.length > 0){
    var dieselRateData = [];
    var party_name = '';
    var item = {};
    let party_details = await fetchSpecificPartyDetails(connection,rows[0].party_code)
    if(party_details != null){
    party_name = party_details.name
    }
    item.code = rows[0].code
    item.party_name = party_name
    item.party_code = rows[0].party_code
    item.entry_date = rows[0].entry_date
    item.rate = rows[0].rate
    resolve(item)
  }else{
    resolve(null)
  }
  }
  })
  }catch(e){
  connection.destroy();
  console.log(e)
  resolve(null)
 }
})
}


function calculateClosingbalanceOfParty(connection,party_code){
  return new Promise(async function(resolve, reject){
    let closing_balance = 0
    let opening_balance  = await fetchPartyOpeningBalance(connection,party_code);
    let total_party_payment = await fetchPartyTotalPayments(connection,party_code);
    let total_fuel_entry_amount = await fetchTotalFuelEntryAmountOfParty(connection,party_code);
    let total_auto_parts_amount = await fetchTotalAutoPartAmountOfParty(connection,party_code)
    closing_balance = [opening_balance + total_fuel_entry_amount + total_auto_parts_amount] - total_party_payment
    // console.log("PARTY CODE",party_code)
    // console.log("opening_balance==>",opening_balance,"closing_balance ==>",closing_balance,"total_auto_parts_amount==>",total_auto_parts_amount,"total_fuel_entry_amount= >>",total_fuel_entry_amount,"total_party_payment =>",total_party_payment)

    resolve(closing_balance)
  })
}



function insertDataInLedger(connection,party_code,remarks){
 return new Promise(async function(resolve, reject){
//  console.log(",party_code,remarks,party_code,remarks================>>>>",party_code,remarks)
 let closing_balance = 0
 let opening_balance  = await fetchPartyOpeningBalance(connection,party_code);
 let total_party_payment = await fetchPartyTotalPayments(connection,party_code);
 let total_fuel_entry_amount = await fetchTotalFuelEntryAmountOfParty(connection,party_code);
 let total_auto_parts_amount = await fetchTotalAutoPartAmountOfParty(connection,party_code)
 closing_balance = [opening_balance + total_fuel_entry_amount + total_auto_parts_amount] - total_party_payment
 let opening_balance_from_ledger = await fetchOpeningBalanceFromLedger(connection,party_code)
 //,opening_balance = '${opening_balance}'
//  console.log("opening_balance_from_ledger ==>>",opening_balance_from_ledger,"opening_balance==>",opening_balance,"closing_balance ==>",closing_balance,"total_auto_parts_amount==>",total_auto_parts_amount,"total_fuel_entry_amount= >>",total_fuel_entry_amount,"total_party_payment =>",total_party_payment)
  connection.query(`INSERT INTO ledger SET party_code = '${party_code}',opening_balance = '${opening_balance_from_ledger}',closing_balance = '${closing_balance}',remarks = '${remarks}' `, async (error, rows, fields)=>{
  if(error){
  createErrorLogFile(error);sendErrorsInEmail(error);  
  connection.destroy();
  resolve(error);
  }else{ 
  // console.log(`INSERT INTO ledger SET party_code = '${party_code}',closing_balance = '${closing_balance}',remarks = '${remarks}' `,rows)
  resolve(true)
  }
 })
 })
}



function fetchPartyTotalPayments(connection,party_code){
  return new Promise(async function(resolve, reject){
      connection.query(`SELECT SUM(payments.amount) AS total_amount FROM party_payments INNER JOIN payments ON payments.code = party_payments.payment_code  WHERE party_payments.status != 2 AND party_payments.party_code = '${party_code}'`, async (error, rowsPartnerPayments, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      try{
      // console.log("+++++++++++++++++++fetchTotalFuelEntryAmountOfParty+++++++++++",rowsPartnerPayments)
      if(rowsPartnerPayments.length > 0){
      if(rowsPartnerPayments[0].total_amount != null){
      resolve(rowsPartnerPayments[0].total_amount)
      }else{
      resolve(0)
      }
      }else{
      resolve(0)
      }
      }catch(e){
      console.log(e)
      resolve(0)
      }
     }
     })
   })
  }

function fetchTotalFuelEntryAmountOfParty(connection,party_code){
  return new Promise(async function(resolve, reject){
  connection.query(`SELECT SUM(amount) AS total_amount FROM fuel_entry WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      // console.log("+++++++++++++++++++fetchTotalFuelEntryAmountOfParty+++++++++++",rows)
      if(rows.length > 0){
      if(rows[0].total_amount != null){
      resolve(rows[0].total_amount)
      }else{
      resolve(0)
      }
      }else{
      resolve(0)
      }
      }catch(e){
      console.log(e)
      resolve(0)    
      } 
    }
  })
})
 }


 function fetchTotalAutoPartAmountOfParty(connection,party_code){
  return new Promise(async function(resolve, reject){
  connection.query(`SELECT SUM(amount) AS total_amount FROM auto_parts WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      // console.log("+++++++++++++++++++fetchTotalAutoPartAmountOfParty+++++++++++",rows)
      if(rows[0].total_amount != null){
      resolve(rows[0].total_amount)
      }else{
      resolve(0)
      }
      }else{
      resolve(0)
      }
      }catch(e){
      console.log(e)
      resolve(0)    
      } 
    }
  })
})
 }




 function fetchPartyOpeningBalance(connection,party_code){
  return new Promise(async function(resolve, reject){
  connection.query(`SELECT opening_balance,type_of_ob FROM parties WHERE status != 2 AND code = '${party_code}' ORDER BY created DESC`, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      connection.destroy();
      resolve(error);
      }else{ 
      try{
      if(rows.length > 0){
      // console.log("+++++++++++++++++++fetchPartyOpeningBalance+++++++++++",rows)
      if(rows[0].opening_balance != null){
      if(rows[0].type_of_ob == 0){
      resolve(- (rows[0].opening_balance));
      }else{
      resolve(rows[0].opening_balance);
      }
      }else{
      resolve(0)
      }
      }else{
      resolve(0)
      }
      }catch(e){
      console.log(e)
      resolve(0)    
      } 
    }
  })
 })
 }



 function fetchOpeningBalanceFromLedger(connection,party_code){
  return new Promise(async function(resolve, reject){
      try{
      connection.query(`SELECT * FROM ledger WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC `, async (error, rows, fields)=>{
      if(error){
      createErrorLogFile(error);sendErrorsInEmail(error);  
      resolve(error);
      }else{ 
      // console.log(`SELECT * FROM ledger WHERE status != 2 AND party_code = '${party_code}' ORDER BY created DESC `,rows)
      if(rows.length > 0){
      resolve(rows[0].opening_balance)
      }else{
      resolve(0);  
      }
      }
      })
      }catch(e){
      console.log(e)
      resolve(0)
     }
   })
  }






  function insertNotificationFunctionForWeb( user_code,title,message,code_to_open,target_panel,icon_url,user_type){
    return new Promise(async function(resolve, reject) {
    // var date1 = dateTime.create();
    // var currentDate = date1.format('Y-m-d H:M:S').split(" ")[0];
    let connection = await getSpecificDatabaseConnection(user_code)
    if(connection != false){
    try{
      connection.query(`INSERT INTO notification SET user_code ='${user_code}',title = '${title}', message = '${message}',target_panel = '${target_panel}',code_to_open ='${code_to_open}',icon_url = '${icon_url}',seen = '0',user_type = '${user_type}' `,async (error,rows, fields)=>{
      if (error) {
      createErrorLogFile(error);sendErrorsInEmail(error);
      connection.destroy();    
      throw error;
      }else{
      // console.log(`INSERT INTO notification SET user_code ='${user_code}',title = '${title}', message = '${message}',target_panel = '${target_panel}',code_to_open ='${code_to_open}',icon_url = '${icon_url}',seen = '0',user_type = '${user_type}' `,rows)
      if(rows.affectedRows > 0){
      //console.log("ADDDDDDDDDDDDDDDDDDDDDDDDDDDMIN")
      emitDataToPerticluarRoomInSocket(user_code,title,message,icon_url)
      let sendNotification = await sendNotificationToWeb(connection,rows.insertId)
      connection.destroy();    
      resolve({
      "success":true,
      "message":"Notification added"
      })
      }else{
      connection.destroy();    
      resolve({
      "success":false,
      "message":"Notification not added"
      })
      }
     }
    })
    }catch(e){
    connection.destroy();    
    console.log(e)
    resolve(false)
    }
  }else{
  resolve({"success": false, "message": "unknown db"});
  }
  })
  }
  




      
  function sendUserQueryToAdminInEmail(admin_email,query){
    return new Promise(async (resolve, reject) => {
    // create reusable transporter object using the default SMTP transport
    try{
      let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
      user: 'alokpoddar110@gmail.com', // generated user
      pass:  "Alok@1850123"// generated password
      }
      });
      let mailOptions = {
      from: 'alokpoddar110@gmail.com' , // sender address
      to: admin_email, // list of receivers,saraswatshalu1234@gmail.com
      subject: 'User Query', // Subject line
  //  text: '', // plain text body
      html: query, // html body
      };
      transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
      return console.log(error);
      }
      console.log(' Email sent: %s', info.messageId);
      fs.unlink('./error.log', (err) => {
      if (err) throw err;
      });
      resolve(true)
     });
     }catch(e){
       console.log(e)
       return e
     }
    }); 
   }
  
  

// new CronJob('*/10 * * * * *',async function() {
//   sendNotificationToWeb()
// }, null, true, 'America/Los_Angeles');


async function sendNotificationToWeb(connection,lastInsertId){
  return new Promise(async function(resolve, reject) {
    connection.query(`SELECT code,code_to_open,title,message,target_panel,created,icon_url,user_code,user_type FROM notification WHERE code = ${lastInsertId}`, async(error, rowsNotificationData, fields)=>{
    if (error) {
    createErrorLogFile(error);sendErrorsInEmail(error);
  //dbFunc.connectionRelease;
    reject(error);
    }else {
    // console.log(`SELECT code,code_to_open,title,message,target_panel,created,icon_url,user_code,user_type FROM notification WHERE code = ${lastInsertId}`,rowsNotificationData)
    try{
    let connectionMaster = await getMasterDatabaseConnection()
    if(connectionMaster != false){
      connectionMaster.query(`SELECT * FROM web_user_subscription_id WHERE user_code = '${rowsNotificationData[0].user_code}' ` , async(error, rows, fields)=>{
    if (error) {
    // dbFunc.connectionRelease;
    reject(error);
    }else {
    
    // console.log(`SELECT * FROM web_user_subscription_id WHERE user_code = '${rowsNotificationData[0].user_code}'`,rows)
    if(rows.length > 0){
    for(var i =0;i<rows.length;i++){
    let sendPushNotification = await sendPushNotificationToWeb(rowsNotificationData[0].message,rowsNotificationData[0].title,rowsNotificationData[0].icon_url,rows[0].endpoint,rows[0].auth_key,rows[0].p256dh_key)
    }
    resolve(true)
    }else{
    resolve(false)
    }
   } 
  })
  }else{
  resolve(false)
  }
  }catch(e){
  console.log(e)
  resolve(false)
  }
  }
 })

})
}



function sendPushNotificationToWeb(message,title,icon_url,endpoint,auth_key,p256dh_key){
  return new Promise(async function(resolve, reject) {
  // console.log("*******************",message,title,icon_url,endpoint,auth_key,p256dh_key)
  try{
  const notificationPayload = {
    "notification":{
    "body":message,
    "title":title,
    "vibrate":[300,100,400,100,400,100,400],
    "icon":icon_url,
    // "tag":"push ",
    "requireInteraction":true,
    "renotify":true,
    "data":{"url":"wheelstracker.in/#/my_profile"}
     }}
    const pushSubscription = {
    endpoint: endpoint,
    expirationTime: null,
    keys: {
    auth: auth_key,
    p256dh: p256dh_key,
    }
    }
    let sendPushNotification = await webpush.sendNotification(pushSubscription, JSON.stringify(notificationPayload))
    // console.log("WWWWWWWWWWWWWEEEBBBBPUSH",sendPushNotification);
    resolve(true)
    }catch(e){
    console.log(e)
    resolve(false)
   }
  })
}

function emitDataToPerticluarRoomInSocket(user_code,title,message,icon_url){
  return new Promise(async function(resolve, reject){
  // console.log("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP",title,message);
  var data = {"user_code":user_code,"title":title,"message":message,"created":await currentDateTime()}  
  var socket = io('http://localhost:3002');
  // var socket = io();
  socket.emit('send notification',data)
  resolve(true)
  })
}




new CronJob(' 00 07 * * *',async function() {
  // sendNotificationToWeb()
  checkExpiryDateOfUser()
}, null, true, 'America/Los_Angeles');






function checkExpiryDateOfUser(){
  return new Promise(async function(resolve, reject){
  let connectionMaster = await getMasterDatabaseConnection()
  if(connectionMaster != false){
    connectionMaster.query(`SELECT * FROM users WHERE expiry_date <= NOW() AND account_expiry_status != 0  ` , async(error, rows, fields)=>{
    if (error) {
    reject(error);
    }else {
      // console.log("ACCCCCCCCCCCCCOOUNT EXOPPIRE USER LIST",rows);
      if(rows.length > 0){
      var user_code = []
        for(var i = 0;i<rows.length;i++){
        user_code.push(rows[i].code)
        }
        if(user_code.length > 0){
        let updateAllUser =  await updateAllUserAccountExpiryStatus(connectionMaster,user_code)
        connectionMaster.destroy()
        resolve(true)
        }
        }else{
        connectionMaster.destroy()
        resolve(false)
        }
       }
    })
   }else{
  resolve(false)
 }
})
}


function updateAllUserAccountExpiryStatus(connectionMaster,user_code){
  return new Promise(async function(resolve, reject){
      connectionMaster.query(`UPDATE users SET account_expiry_status = 0 WHERE code IN(${user_code})   ` , async(error, rows, fields)=>{
      if (error) {
      // dbFunc.connectionRelease;
      reject(error);
      }else {
      console.log('updateing expiry status of users',rows)
      resolve(true)
     }
    })
  })
 }



module.exports = {
  compressDataFunction,decompressDataFunction,createTables,verifyOtp,port,thumbPort,
  getSpecificDatabaseConnection,encriptDataFunction,decriptDataFunction,currentDateTime,
  createErrorLogFile,sendErrorsInEmail,fetchSpecificPartyDetails,fetchSpecificMineDetails,
  fetchSpecificDriverDetails,getMasterDatabaseConnection,fetchSpecificVehicleDetails,
  fetchSpecificPaymentDetails,fetchSpecificRouteDetails,fetchSpecificUserDetails,
  fetchSpecificBilltyDetails,fetchSpecificfuelEntryDetails,fetchSpecificAutoPartChallanDetails,
  fetchSpecificDriverPaymentDetails,fetchSpecificPartyPaymentDetails,
  fetchSpecificFuelEntryDieselRateDetails,insertDataInLedger,calculateClosingbalanceOfParty,
  insertNotificationFunctionForWeb,sendUserQueryToAdminInEmail,fetchSpecificVehicleDetailsWithCondition}
