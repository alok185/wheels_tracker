var partyModel = require('../models/web_apis/party');
var mineModel = require('../models/web_apis/mine');
var routeModel = require('../models/web_apis/route');
var driverModel = require('../models/web_apis/driver');
var vehicleModel = require('../models/web_apis/vehicle');
var contactUsModel = require('../models/web_apis/contact_us');
var autoPartsModel = require('../models/web_apis/auto_parts');
var paymentModel = require('../models/web_apis/driver_payment');
var fuelEntryModel = require('../models/web_apis/fuel_entry');
var billtyModel =  require('../models/web_apis/billty');
var partyPaymentModel = require('../models/web_apis/party_payment');
var usersModel = require('../models/web_apis/users');
var otherModel = require('../models/web_apis/other');
var attendanceModel  = require('../models/web_apis/attendance');
var fuelEntryDieselModel = require('../models/web_apis/fuel_entry_diesel');
var ledgerModel = require('../models/web_apis/ledger');
var userNotificationModel = require('../models/web_apis/notification');
var reportModel = require('../models/web_apis/report');
var driverReportModel = require('../models/web_apis/driver_report');
var transporterReportModel = require('../models/web_apis/transporter_report');




function init(router) {

//Party_model..............................................STARTS...................
router.route('/add_new_party')
.post(addNewParty);
router.route('/fetch_all_party_data/:code')
.get(fetchAllPartyData);
router.route('/update_party')
.put(updateParty);
router.route('/delete_specific_party_details')
.post(deleteSpecificPartyDetails);
router.route('/delete_multiple_party_details')
.post(deleteMultiplePartyDetails);
router.route('/fetch_all_transporter_party_data/:code')
.get(fetchAllTransporterPartyData);
router.route('/undo_delete_multiple_party_details')
.post(undoDeleteMultiplePartyDetails);

//report party
router.route('/fetch_fuel_party/:code')
.get(fetchFuelParty);
router.route('/fetch_auto_parts_party/:code')
.get(fetchAutoPartsParty);
router.route('/fetch_transporter_party/:code')
.get(fetchTransporterParty);




//Party_model..............................................ENDS.....................


//MINE_Model...............................................STARTS..................
router.route('/add_new_mine')
.post(addNewMine);
router.route('/fetch_all_mine_details/:code')
.get(fetchAllMineDetails);
router.route('/update_mine')
.put(updateMine);
router.route('/delete_specific_mine_details')
.post(deleteSpecificMineDetails);
router.route('/delete_multiple_mine_details')
.post(deleteMultipleMineDetails);

router.route('/undo_delete_multiple_mine_details')
.post(undoDeleteMultipleMineDetails);


//Mine_Model...............................................ENDS........................

//Route_model..............................................Start.....................
router.route('/fetch_all_necessary_data_for_adding_route/:code')
.get(fetchAllNecessaryDataForAddingRoute);
router.route('/add_new_route')
.post(addNewRoute);
router.route('/fetch_all_routes/:code')
.get(fetchAllRoutes);
router.route('/update_route')
.put(updateRoute);
router.route('/delete_specific_route_details')
.post(deleteSpecificRouteDetails);
router.route('/delete_multiple_route_details')
.post(deleteMultipleRouteDetails);
router.route('/undo_delete_multiple_route_details')
.post(undoDeleteMultipleRouteDetails);

router.route('/fetch_route_commision/:code')
.get(fetchRouteCommision);



//Route_model...............................................ENDS.....................



//Driver_model..............................................START....................
router.route('/add_new_driver')
.post(addNewDriver);
router.route('/fetch_all_drivers/:code')
.get(fetchAllDrivers);
router.route('/update_driver_details')
.put(updateDriverDetails);
router.route('/delete_specific_driver_details')
.post(deleteSpecificDriverDetails);
router.route('/delete_multiple_driver_details')
.post(deleteMultipleDriverDetails);
router.route('/undo_delete_multiple_driver_details')
.post(undoDeleteMultipleDriverDetails);
router.route('/fetch_specific_driver_details')
.post(fetchSpecificDriverDetails);
router.route('/fetch_drivers_data/:code')
.get(fetchDriversData);



//Driver_model...............................................ENDS....................



//Vehicle_model..............................................STARTS......................
router.route('/add_new_vehicle')
.post(addNewVehicle);
router.route('/fetch_all_vehicle/:code')
.get(fetchAllVehicle);
router.route('/update_vehicle')
.put(updateVehicle);
router.route('/delete_specific_vehicle_details')
.post(deleteSpecificVehicleDetails);
router.route('/delete_multiple_vehicle_details')
.post(deleteMultipleVehicleDetails);
router.route('/undo_delete_multiple_vehicle_details')
.post(undoDeleteMultipleVehicleDetails);

router.route('/fetch_vehicles_data/:code')
.get(fetchVehiclesData);


//Vehicel_model.............................................ENDS........................


//Contact_us_model..........................................STARTS......................
router.route('/add_contact_us_details')
.post(addContactUsDetails);
router.route('/fetch_contact_us_details')
.get(fetchContactUsDetails);
router.route('/delete_contact_us_data/:code')
.delete(deleteContactUsData);
router.route('/update_contact_us_details')
.put(updateContactUsDetails);
//Contact_us_Model..........................................ENDS.......................


//Auto_parts.................................................STARTS...................
router.route('/add_new_auto_part_challan')
.post(addNewAutoPartChallan);
router.route('/fetch_all_auto_part_challan/:code')
.get(fetchAllAutoPartChallan);
router.route('/update_auto_part_challan')
.put(updateAutoPartChallan);
router.route('/delete_specific_auto_part_challan_details')
.post(deleteSpecificAutoPartChallanDetails);
router.route('/delete_multiple_auto_part_challan_details')
.post(deleteMultipleAutoPartsChallanDetails);
router.route('/ecrypt_data_check_api')
.post(ecryptDataCheckApi);
router.route('/fetch_all_necessary_data_for_adding_and_updating_auto_part/:code')
.get(fetchAllNecessaryDataForAddingAndUpdatingAutoPart);
router.route('/undo_delete_multiple_auto_parts_challan_details')
.post(undoDeleteMultipleAutoPartsChallanDetails);




//Auto_parts.................................................ENDS....................



//Payment_model..............................................START...................
router.route('/add_new_driver_payment')
.post(addNewDriverPayment);
router.route('/fetch_all_driver_payments/:code')
.get(fetchAllDriverPayments);
router.route('/update_driver_payment_details')
.put(updateDriverPaymentDetails);
router.route('/delete_specific_driver_payment_details')
.post(deleteSpecificDriverPaymentDetails);
router.route('/delete_multiple_driver_payment_details')
.post(deleteMultipleDriverPaymentDetails);
router.route('/fetch_all_drivers_for_adding_payment/:code')
.get(fetchAllDriversForAddingPayment);

router.route('/fetch_specific_driver_payment_details')
.post(fetchSpecificDriverPaymentDetails);


router.route('/undo_delete_multiple_driver_payment_details')
.post(undoDeleteMultipleDriverPaymentDetails);



//Payment_model...............................................ENDS...................



//PARTY_Payment_model..............................................START...................
router.route('/add_new_party_payment')
.post(addNewPartyPayment);
router.route('/fetch_all_party_payments/:code')
.get(fetchAllPartyPayments);
router.route('/update_party_payment_details')
.put(updatePartyPaymentDetails);
router.route('/delete_specific_party_payment_details')
.post(deleteSpecificPartyPaymentDetails);
router.route('/delete_multiple_party_payment_details')
.post(deleteMultiplePartyPaymentDetails);
router.route('/undo_delete_multiple_party_payment_details')
.post(undoDeleteMultiplePartyPaymentDetails);


router.route('/fetch_all_party_for_adding_payment/:code')
.get(fetchAllPartyForAddingPayment);

router.route('/fetch_specific_party_payment_details')
.post(fetchSpecificPartyPaymentDetails);


//PARTY_Payment_model...............................................ENDS...................




//Fuel_entry..................................................STARTS.....................
router.route('/add_new_fuel_entry')
.post(addNewFuelEntry);
router.route('/fetch_all_fuel_entry_details/:code')
.get(fetchAllFuelEntryDetails);
router.route('/update_fuel_entry_details')
.put(updateFuelEntryDetails);
router.route('/delete_multiple_fuel_entry_details')
.post(deleteMultipleFuelEntryDetails);
router.route('/delete_specific_fuel_entry_details')
.post(deleteSpecificFuelEntryDetails);
router.route('/fetch_all_fuel_entry_party_data/:code')
.get(fetchAllFuelEntryPartyData);
router.route('/fetch_specific_fuel_entry_details')
.post(fetchSpecificFuelEntryDetails);
router.route('/filter_fuel_entry_data')
.post(filterFuelEntryData);


router.route('/undo_delete_multiple_fuel_entry_details')
.post(undoDeleteMultipleFuelEntryDetails);



//Fuel_entry..................................................ENDS..........................




//Fuel_entry_diesel..................................................STARTS.....................
router.route('/add_new_fuel_entry_diesel_rate')
.post(addNewFuelEntryDieselRate);
router.route('/fetch_all_fuel_entry_diesel_rate_details_of_specific_party')
.post(fetchAllFuelEntryDieselRateDetailsOfSpecificParty);
router.route('/update_fuel_entry_diesel_rate')
.put(updateFuelEntryDieselRate);
router.route('/delete_multiple_diesel_rate_details')
.post(deleteMultipleDieselRateDetails);
router.route('/delete_specific_fuel_entry_details')
.post(deleteSpecificFuelEntryDetails);
router.route('/fetch_all_fuel_entry_party_data/:code')
.get(fetchAllFuelEntryPartyData);

router.route('/undo_delete_multiple_diesel_rate_details')
.post(undoDeleteMultipleDieselRateDetails);


//Fuel_entry_diesel..................................................ENDS..........................


//Billty_model................................................STARTS........................
router.route('/add_new_billty')
.post(addNewBillty);
router.route('/fetch_all_billties')
.post(fetchAllBillties);
router.route('/update_billty_details')
.put(updateBilltyDetails);
router.route('/delete_specific_billty_details')
.post(deleteSpecificBilltyDetails);
router.route('/delete_multiple_billty_details')
.post(deleteMultipleBilltyDetails);
router.route('/undo_delete_multiple_billty_details')
.post(undoDeleteMultipleBilltyDetails);


router.route('/fetch_all_necessary_data_for_adding_and_updating_billty/:code')
.get(fetchAllNecessaryDataForAddingAndUpdatingBillty);

router.route('/search_billty_data')
.post(searchBilltyData);

router.route('/fetch_specific_billty_details')
.post(fetchSpecificBilltyDetails);

router.route('/filter_billty_data')
.post(filterBilltyData);






//Billty_model................................................ENDS........................



//USER_model...................................................STARTS......................
router.route('/update_user_details')
.put(updateUserDetails);
router.route('/fetch_specific_user_details_for_user/:code')
.get(fetchSpecificUserDetailsForUser);
router.route('/verify_otp_for_change_mobile_of_user')
.post(verifyOtpForChangeMobileOfUser);
router.route('/recharge_user_account')
.post(rechargeUserAccount);
router.route('/update_profile_image_of_user')
.put(updateProfileImageOfUser);



//USer_model...................................................ENDS........................


//Other_model...................................................STARTS...................
router.route('/global_search')
.post(globalSearch);
router.route('/add_users_query')
.post(addUsersQuery);
router.route('/fetch_all_queries_of_users')
.get(fetchAllQueriesOfUsers);




//Other_model....................................................ENDS...................


//Attendance_model...............................................STARTS...............
router.route('/add_new_attendance')
.post(addNewAttendance);
router.route('/fetch_attendance_month_wise')
.post(fetchAttendanceMonthWise);
router.route('/fetch_specific_attendance_details')
.post(fetchSpecificAttendanceDetails);
router.route('/update_attendance')
.put(updateAttendance);
router.route('/delete_multiple_attendance_details')
.post(deleteMultipleAttendanceDetails);
router.route('/undo_delete_multiple_attendance_details')
.post(undoDeleteMultipleAttendanceDetails);

//Attendance_model..............................................ENDS.................



//Ledger_model...............................................STARTS...............
router.route('/fetch_all_ledger_data/:code')
.get(fetchAllLedgerData);
router.route('/fetch_specific_ledger_details_of_party')
.post(fetchSpecificLedgerDetailsOfParty);
//Ledger_model..............................................ENDS.................



//NOTIFICATION_OF_USER.........................................STARTS..................
router.route('/fetch_all_notification_of_user/:code')
.get(fetchAllNotificationOfUser);
router.route('/seen_all_notification_of_user/:code')
.get(seenAllNotificationOfUser);
router.route('/seen_one_notification_of_user')
.post(seenOneNotificationOfUser);
//NOTIFICATION_OF_USER.........................................ENDS..................



//Report_Model.........................................STARTS..................
router.route('/fetch_fuel_entry_report')
.post(fetchFuelEntryReport);
router.route('/fetch_auto_part_report')
.post(fetchAutoPartReport);
//Report_Model.........................................ENDS..................


//Driver_Report........................................START................
router.route('/fetch_all_necesary_data_for_filtering_driver_report/:code')
.get(fetchAllNecesaryDataForFilteringDriverReport);
router.route('/fetch_all_driver_report')
.post(fetchAllDriverReport);
//Driver_Report..........................................ENDS...............


//Transporter_Report........................................STARTS................
router.route('/fetch_transporter_report')
.post(fetchTransporterReport);

router.route('/fetch_all_owner_list_of_vehicles/:code')
.get(fetchAllOwnerListOfVehicles);

router.route('/fetch_all_vehicle_list_of_specific_owner')
.post(fetchAllVehicleListOfSpecificOwner);

router.route('/fetch_all_necessary_data_for_filtering_transport_report_data/:code')
.get(fetchAllNecessaryDataForFilteringTransportReportData);





//Transporter_Report........................................ENDS................
}

//Party_model..............................................STARTS...................

function addNewParty(req,res) {
  partyModel.addNewParty(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllPartyData(req,res) {
  partyModel.fetchAllPartyData(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateParty(req,res) {
  partyModel.updateParty(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteSpecificPartyDetails(req,res) {
  partyModel.deleteSpecificPartyDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function deleteMultiplePartyDetails(req,res) {
  partyModel.deleteMultiplePartyDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function undoDeleteMultiplePartyDetails(req,res) {
  partyModel.undoDeleteMultiplePartyDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllTransporterPartyData(req,res) {
  partyModel.fetchAllTransporterPartyData(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function fetchFuelParty(req,res) {
  partyModel.fetchFuelParty(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAutoPartsParty(req,res) {
  partyModel.fetchAutoPartsParty(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function fetchTransporterParty(req,res) {
  partyModel.fetchTransporterParty(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



//Party_model..............................................ENDS.....................


//MINE_Model...............................................STARTS..................

function addNewMine(req,res) {
  mineModel.addNewMine(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllMineDetails(req,res) {
  mineModel.fetchAllMineDetails(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateMine(req,res) {
  mineModel.updateMine(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function deleteSpecificMineDetails(req,res) {
  mineModel.deleteSpecificMineDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function deleteMultipleMineDetails(req,res) {
  mineModel.deleteMultipleMineDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function undoDeleteMultipleMineDetails(req,res) {
  mineModel.undoDeleteMultipleMineDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


//Mine_Model...............................................ENDS........................



//Route_model..............................................Start.....................

function fetchAllNecessaryDataForAddingRoute(req,res) {
  routeModel.fetchAllNecessaryDataForAddingRoute(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function addNewRoute(req,res) {
  routeModel.addNewRoute(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllRoutes(req,res) {
  routeModel.fetchAllRoutes(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function updateRoute(req,res) {
  routeModel.updateRoute(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteSpecificRouteDetails(req,res) {
  routeModel.deleteSpecificRouteDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function deleteMultipleRouteDetails(req,res) {
  routeModel.deleteMultipleRouteDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function undoDeleteMultipleRouteDetails(req,res) {
  routeModel.undoDeleteMultipleRouteDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function fetchRouteCommision(req,res) {
  routeModel.fetchRouteCommision(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


//Route_model...............................................ENDS.....................



//Driver_model..............................................START....................

function addNewDriver(req,res) {
  driverModel.addNewDriver(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllDrivers(req,res) {
  driverModel.fetchAllDrivers(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateDriverDetails(req,res) {
  driverModel.updateDriverDetails(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function deleteSpecificDriverDetails(req,res) {
  driverModel.deleteSpecificDriverDetails(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteMultipleDriverDetails(req,res) {
  driverModel.deleteMultipleDriverDetails(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function undoDeleteMultipleDriverDetails(req,res) {
  driverModel.undoDeleteMultipleDriverDetails(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function fetchSpecificDriverDetails(req,res) {
  driverModel.fetchSpecificDriverDetails(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchDriversData(req,res) {
  driverModel.fetchDriversData(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




//Driver_model...............................................ENDS....................





//Vehicle_model..............................................STARTS......................

function addNewVehicle(req,res) {
  vehicleModel.addNewVehicle(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function fetchAllVehicle(req,res) {
  vehicleModel.fetchAllVehicle(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function updateVehicle(req,res) {
  vehicleModel.updateVehicle(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function deleteSpecificVehicleDetails(req,res) {
  vehicleModel.deleteSpecificVehicleDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteMultipleVehicleDetails(req,res) {
  vehicleModel.deleteMultipleVehicleDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function undoDeleteMultipleVehicleDetails(req,res) {
  vehicleModel.undoDeleteMultipleVehicleDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function fetchVehiclesData(req,res) {
  vehicleModel.fetchVehiclesData(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




//Vehicel_model.............................................ENDS........................


//Contact_us_model..........................................STARTS......................


function addContactUsDetails(req,res) {
  contactUsModel.addContactUsDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchContactUsDetails(req,res) {
  contactUsModel.fetchContactUsDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function deleteContactUsData(req,res) {
  contactUsModel.deleteContactUsData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateContactUsDetails(req,res) {
  contactUsModel.updateContactUsDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//Contact_us_model..........................................ENDS......................




//Auto_parts.................................................STARTS...................



function ecryptDataCheckApi(req,res) {
  autoPartsModel.ecryptDataCheckApi(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function addNewAutoPartChallan(req,res) {
  autoPartsModel.addNewAutoPartChallan(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function fetchAllAutoPartChallan(req,res) {
  autoPartsModel.fetchAllAutoPartChallan(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updateAutoPartChallan(req,res) {
  autoPartsModel.updateAutoPartChallan(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificAutoPartChallanDetails(req,res) {
  autoPartsModel.deleteSpecificAutoPartChallanDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function deleteMultipleAutoPartsChallanDetails(req,res) {
  autoPartsModel.deleteMultipleAutoPartsChallanDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function undoDeleteMultipleAutoPartsChallanDetails(req,res) {
  autoPartsModel.undoDeleteMultipleAutoPartsChallanDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllNecessaryDataForAddingAndUpdatingAutoPart(req,res) {
  autoPartsModel.fetchAllNecessaryDataForAddingAndUpdatingAutoPart(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


//Auto_parts.................................................ENDS....................




//Payment_model..............................................START...................

function addNewDriverPayment(req,res) {
  paymentModel.addNewDriverPayment(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllDriverPayments(req,res) {
  paymentModel.fetchAllDriverPayments(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updateDriverPaymentDetails(req,res) {
  paymentModel.updateDriverPaymentDetails(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificDriverPaymentDetails(req,res) {
  paymentModel.deleteSpecificDriverPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteMultipleDriverPaymentDetails(req,res) {
  paymentModel.deleteMultipleDriverPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultipleDriverPaymentDetails(req,res) {
  paymentModel.undoDeleteMultipleDriverPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function fetchAllDriversForAddingPayment(req,res) {
  paymentModel.fetchAllDriversForAddingPayment(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchSpecificDriverPaymentDetails(req,res) {
  paymentModel.fetchSpecificDriverPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


//Payment_model...............................................ENDS...................










//Party_Payment_model..............................................START...................

function addNewPartyPayment(req,res) {
  partyPaymentModel.addNewPartyPayment(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllPartyPayments(req,res) {
  partyPaymentModel.fetchAllPartyPayments(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updatePartyPaymentDetails(req,res) {
  partyPaymentModel.updatePartyPaymentDetails(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificPartyPaymentDetails(req,res) {
  partyPaymentModel.deleteSpecificPartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteMultiplePartyPaymentDetails(req,res) {
  partyPaymentModel.deleteMultiplePartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultiplePartyPaymentDetails(req,res) {
  partyPaymentModel.undoDeleteMultiplePartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllPartyForAddingPayment(req,res) {
  partyPaymentModel.fetchAllPartyForAddingPayment(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchSpecificPartyPaymentDetails(req,res) {
  partyPaymentModel.fetchSpecificPartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




//Party_Payment_model...............................................ENDS...................









//Fuel_entry..................................................STARTS.....................
function addNewFuelEntry(req,res) {
  fuelEntryModel.addNewFuelEntry(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllFuelEntryDetails(req,res) {
  fuelEntryModel.fetchAllFuelEntryDetails(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateFuelEntryDetails(req,res) {
  fuelEntryModel.updateFuelEntryDetails(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function deleteMultipleFuelEntryDetails(req,res) {
  fuelEntryModel.deleteMultipleFuelEntryDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultipleFuelEntryDetails(req,res) {
  fuelEntryModel.undoDeleteMultipleFuelEntryDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificFuelEntryDetails(req,res) {
  fuelEntryModel.deleteSpecificFuelEntryDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllFuelEntryPartyData(req,res) {
  fuelEntryModel.fetchAllFuelEntryPartyData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchSpecificFuelEntryDetails(req,res) {
  fuelEntryModel.fetchSpecificFuelEntryDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function filterFuelEntryData(req,res) {
  fuelEntryModel.filterFuelEntryData(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



//Fuel_entry..................................................ENDS..........................








//Fuel_entry_diesel..................................................STARTS.....................
function addNewFuelEntryDieselRate(req,res) {
  fuelEntryDieselModel.addNewFuelEntryDieselRate(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAllFuelEntryDieselRateDetailsOfSpecificParty(req,res) {
  fuelEntryDieselModel.fetchAllFuelEntryDieselRateDetailsOfSpecificParty(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateFuelEntryDieselRate(req,res) {
  fuelEntryDieselModel.updateFuelEntryDieselRate(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function deleteMultipleDieselRateDetails(req,res) {
  fuelEntryDieselModel.deleteMultipleDieselRateDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultipleDieselRateDetails(req,res) {
  fuelEntryDieselModel.undoDeleteMultipleDieselRateDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


//Fuel_entry_diesel_rate..................................................ENDS..........................





//Billty_model................................................STARTS........................




function addNewBillty(req,res) {
  billtyModel.addNewBillty(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAllBillties(req,res) {
  billtyModel.fetchAllBillties(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateBilltyDetails(req,res) {
  billtyModel.updateBilltyDetails(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function deleteSpecificBilltyDetails(req,res) {
  billtyModel.deleteSpecificBilltyDetails(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  }); 
  }

  function deleteMultipleBilltyDetails(req,res) {
    billtyModel.deleteMultipleBilltyDetails(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }


  function undoDeleteMultipleBilltyDetails(req,res) {
    billtyModel.undoDeleteMultipleBilltyDetails(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }

  


  function fetchAllNecessaryDataForAddingAndUpdatingBillty(req,res) {
    billtyModel.fetchAllNecessaryDataForAddingAndUpdatingBillty(req,res).then((data) => {
      if(data) {
      res.json(data);
      }
      }).catch((err) => {
      res.json(err);
    });
  }
    

function searchBilltyData(req,res) {
  billtyModel.searchBilltyData(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}


    

function fetchSpecificBilltyDetails(req,res) {
  billtyModel.fetchSpecificBilltyDetails(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}

function filterBilltyData(req,res) {
  billtyModel.filterBilltyData(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}
      


//Billty_model................................................ENDS........................




//USER_model...................................................STARTS......................

function updateUserDetails(req,res) {
  usersModel.updateUserDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchSpecificUserDetailsForUser(req,res) {
  usersModel.fetchSpecificUserDetailsForUser(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function verifyOtpForChangeMobileOfUser(req,res) {
  usersModel.verifyOtpForChangeMobileOfUser(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function rechargeUserAccount(req,res) {
  usersModel.rechargeUserAccount(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function updateProfileImageOfUser(req,res) {
  usersModel.updateProfileImageOfUser(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




//USer_model...................................................ENDS........................



//Other_model...................................................STARTS...................


function globalSearch(req,res) {
  otherModel.globalSearch(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function addUsersQuery(req,res) {
  otherModel.addUsersQuery(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllQueriesOfUsers(req,res) {
  otherModel.fetchAllQueriesOfUsers(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



//Other_model....................................................ENDS...................



//Attendance_model...............................................STARTS...............

function addNewAttendance(req,res) {
  attendanceModel.addNewAttendance(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAttendanceMonthWise(req,res) {
  attendanceModel.fetchAttendanceMonthWise(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificAttendanceDetails(req,res) {
  attendanceModel.fetchSpecificAttendanceDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function deleteMultipleAttendanceDetails(req,res) {
  attendanceModel.deleteMultipleAttendanceDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function undoDeleteMultipleAttendanceDetails(req,res) {
  attendanceModel.undoDeleteMultipleAttendanceDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





function updateAttendance(req,res) {
  attendanceModel.updateAttendance(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

//Attendance_model..............................................ENDS.................




//Ledger_model...............................................STARTS...............



function fetchAllLedgerData(req,res) {
  ledgerModel.fetchAllLedgerData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificLedgerDetailsOfParty(req,res) {
  ledgerModel.fetchSpecificLedgerDetailsOfParty(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

//Ledger_model..............................................ENDS.................






//NOTIFICATION_OF_USER.........................................STARTS..................


function fetchAllNotificationOfUser(req,res) {
  userNotificationModel.fetchAllNotificationOfUser(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function seenAllNotificationOfUser(req,res) {
  userNotificationModel.seenAllNotificationOfUser(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function seenOneNotificationOfUser(req,res) {
  userNotificationModel.seenOneNotificationOfUser(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//NOTIFICATION_OF_USER.........................................ENDS..................



//Report_Model.........................................STARTS..................

function fetchFuelEntryReport(req,res) {
  reportModel.fetchFuelEntryReport(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAutoPartReport(req,res) {
  reportModel.fetchAutoPartReport(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

 


//Report_Model.........................................ENDS................



//Driver_Report_model..................................START...............


function fetchAllNecesaryDataForFilteringDriverReport(req,res) {
  driverReportModel.fetchAllNecesaryDataForFilteringDriverReport(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllDriverReport(req,res) {
  driverReportModel.fetchAllDriverReport(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//Driver_report_model..................................ENDS..............



//Transporter_Report........................................STARTS................


function fetchTransporterReport(req,res) {
  transporterReportModel.fetchTransporterReport(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllOwnerListOfVehicles(req,res) {
  transporterReportModel.fetchAllOwnerListOfVehicles(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllVehicleListOfSpecificOwner(req,res) {
  transporterReportModel.fetchAllVehicleListOfSpecificOwner(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllNecessaryDataForFilteringTransportReportData(req,res) {
  transporterReportModel.fetchAllNecessaryDataForFilteringTransportReportData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




//Transporter_Report........................................ENDS................


module.exports.init = init;
