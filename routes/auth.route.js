var authModel = require('../models/auth');
var otherModel = require('../models/web_apis/other');
var otherModelAndroid = require('../models/android_apis/other');
var driverPaymentModel = require('../models/android_apis/driver_payment');

function init(router) {
  router.route('/fetch_app_info')
  .get(appInfo);
  router.route('/send_otp_for_user')
  .post(sendOtpForUser);
  router.route('/verify_otp_for_user')
  .post( verifyOtpForUser);
  router.route('/get_encripted_data_of_json')
  .post(getEncriptedDataOfJson);
  router.route('/get_decripted_data_of_json')
  .post(getDecriptedDataOfJson);
  router.route('/send_otp_for_change_mobile_of_user')
  .post(sendOtpForChangeMobileOfUser);
  router.route('/verify_otp_for_signup')
  .post(verifyOtpForSignup);

  
router.route('/save_subcription_id')
.post(saveSubcriptionId);  
router.route('/send_otp_for_signup')
.post(sendOtpForSignup);
router.route('/check_user_exist_or_not')
.post(checkUserExistOrNot);


router.route('/send_otp_for_user_login_for_android')
.post(sendOtpForUserLoginForAndroid);
router.route('/verify_otp_for_user_login_for_android')
.post(verifyOtpForUserLoginForAndroid);
router.route('/send_otp_for_user_signup_for_android')
.post(sendOtpForUserSignupForAndroid);
router.route('/verify_otp_for_user_signup_for_android')
.post(verifyOtpForUserSignupForAndroid);
router.route('/restartServerForReleasingAllTheConnection')
.get(restartServerForReleasingAllTheConnection);




router.route('/fetch_app_price_for_user')
.get(fetchAppPriceForUser);


  //admin signin..............

  router.route('/admin_signin')
  .post(adminSignin);
  
  


  
}




//SC website api's without token api's -----------------------------------ENDS------------------------------------


function appInfo(req,res){
   authModel.appInfo().then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
   });
}

function sendOtpForUser(req,res) {
  authModel.sendOtpForUser(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }

 

function verifyOtpForUser(req,res){
  authModel.verifyOtpForUser(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }

 
 

function getEncriptedDataOfJson(req,res) {
  authModel.getEncriptedDataOfJson(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }

 function getDecriptedDataOfJson(req,res) {
  authModel.getDecriptedDataOfJson(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }


 function sendOtpForChangeMobileOfUser(req,res) {
  authModel.sendOtpForChangeMobileOfUser(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }




 function verifyOtpForSignup(req,res) {
  authModel.verifyOtpForSignup(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  });
 }



 function saveSubcriptionId(req,res) {
  otherModel.saveSubcriptionId(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function sendOtpForSignup(req,res) {
  authModel.sendOtpForSignup(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function sendOtpForUserSignupForAndroid(req,res) {
  authModel.sendOtpForUserSignupForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function verifyOtpForUserSignupForAndroid(req,res) {
  authModel.verifyOtpForUserSignupForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}






function checkUserExistOrNot(req,res) {
  authModel.checkUserExistOrNot(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function sendOtpForUserLoginForAndroid(req,res) {
  authModel.sendOtpForUserLoginForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function restartServerForReleasingAllTheConnection(req,res) {
  otherModelAndroid.restartServerForReleasingAllTheConnection(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





function verifyOtpForUserLoginForAndroid(req,res) {
  authModel.verifyOtpForUserLoginForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAppPriceForUser(req,res) {
  authModel.fetchAppPriceForUser(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function adminSignin(req,res) {
  authModel.adminSignin(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




 
 
 
module.exports.init = init;
