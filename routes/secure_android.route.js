var vehicleModel = require('../models/android_apis/vehicle');
var partyModel = require('../models/android_apis/party');
var mineModel = require('../models/android_apis/mine');
var routeModel = require('../models/android_apis/route');
var driverModel = require('../models/android_apis/driver');
var driverPaymentModel = require('../models/android_apis/driver_payment');
var partyPaymentModel = require('../models/android_apis/party_payment');
var fuelEntryModel = require('../models/android_apis/fuel_entry');
var fuelEntryDieselModel = require('../models/android_apis/fuel_entry_diesel');
var usersModel = require('../models/android_apis/users');
var autoPartsModel = require('../models/android_apis/auto_parts');
var billtyModel =  require('../models/android_apis/billty');
var contactUsModel = require('../models/web_apis/contact_us');
var otherModel = require('../models/android_apis/other');
var attendanceModel  = require('../models/android_apis/attendance');
// var ledgerModel = require('../models/android_apis/ledger');
var userNotificationModel = require('../models/android_apis/notification');
var fuelEntryAndAutoPartsReportModel = require('../models/android_apis/fuel_entry_and_auto_parts_report');
var driverReportModel = require('../models/android_apis/driver_report');
var transporterReportModel = require('../models/android_apis/transporter_report');
var voucherModel = require('../models/android_apis/vouchers');



function init(router) {

//Party_model..............................................STARTS...................
router.route('/add_new_party_for_android')
.post(addNewPartyForAndroid);
router.route('/fetch_all_party_data_for_android/:code')
.get(fetchAllPartyDataForAndroid);
router.route('/update_party_for_android')
.put(updatePartyForAndroid);
router.route('/delete_specific_party_details_for_android')
.post(deleteSpecificPartyDetailsForAndroid);
// router.route('/delete_multiple_party_details')
// .post(deleteMultiplePartyDetails);
router.route('/fetch_all_transporter_party_data_for_android/:code')
.get(fetchAllTransporterPartyDataForAndroid);
// router.route('/undo_delete_multiple_party_details')
// .post(undoDeleteMultiplePartyDetails);


router.route('/fetch_specific_party_details_for_android')
.post(fetchSpecificPartyDetailsForAndroid);


//Party_model..............................................ENDS.....................


//MINE_Model...............................................STARTS..................
router.route('/add_new_mine_for_android')
.post(addNewMineForAndroid);
router.route('/fetch_all_mine_details_for_android/:code')
.get(fetchAllMineDetailsForAndroid);
router.route('/update_mine_for_android')
.put(updateMineForAndroid);
router.route('/delete_specific_mine_details_for_android')
.post(deleteSpecificMineDetailsForAndroid);
router.route('/delete_multiple_mine_details')
.post(deleteMultipleMineDetails);

router.route('/undo_delete_multiple_mine_details')
.post(undoDeleteMultipleMineDetails);


//Mine_Model...............................................ENDS........................

//Route_model..............................................Start.....................
router.route('/fetch_all_necessary_data_for_adding_route_for_android/:code')
.get(fetchAllNecessaryDataForAddingRouteForAndroid);
router.route('/add_new_route_for_android')
.post(addNewRouteForAndroid);
router.route('/fetch_all_routes_for_android/:code')
.get(fetchAllRoutesForAndroid);
router.route('/update_route_for_android')
.put(updateRouteForAndroid);
router.route('/delete_specific_route_details_for_android')
.post(deleteSpecificRouteDetailsForAndroid);
// router.route('/delete_multiple_route_details')
// .post(deleteMultipleRouteDetails);
// router.route('/undo_delete_multiple_route_details')
// .post(undoDeleteMultipleRouteDetails);


router.route('/update_frieght_rate')
.put(updateFrieghtRate);


//Route_model...............................................ENDS.....................



//Driver_model..............................................START....................
router.route('/add_new_driver_for_android')
.post(addNewDriverForAndroid);
router.route('/fetch_all_drivers_for_android/:code')
.get(fetchAllDriversForAndroid);
router.route('/update_driver_details_for_android')
.put(updateDriverDetailsForAndroid);
router.route('/delete_specific_driver_details_for_android')
.post(deleteSpecificDriverDetailsForAndroid);
// router.route('/delete_multiple_driver_details')
// .post(deleteMultipleDriverDetails);
// router.route('/undo_delete_multiple_driver_details')
// .post(undoDeleteMultipleDriverDetails);


router.route('/fetch_specific_driver_details_for_android')
.post(fetchSpecificDriverDetailsForAndroid);

//Driver_model...............................................ENDS....................



//Vehicle_model..............................................STARTS......................
router.route('/add_new_vehicle_for_android')
.post(addNewVehicleForAndroid);
router.route('/fetch_all_vehicle_for_android/:code')
.get(fetchAllVehicleForAndroid);
router.route('/update_vehicle_for_android')
.put(updateVehicleForAndroid);
router.route('/delete_specific_vehicle_details_for_android')
.post(deleteSpecificVehicleDetailsForAndroid);

router.route('/add_new_maintenance_data_of_vehicle_for_android')
.post(addNewMaintenanceDataOfVehicleForAndroid);

router.route('/update_maintenance_data_of_vehicle_for_android')
.put(updateMaintenanceDataOfVehicleForAndroid);

router.route('/fetch_maintenance_data_of_vehicle_for_android')
.post(fetchMaintenanceDataOfVehicleForAndroid);

router.route('/fetch_specific_maintenance_data_of_vehicle_for_android')
.post(fetchSpecificMaintenanceDataOfVehicleForAndroid);

router.route('/fetch_specific_vehicle_details_for_android')
.post(fetchSpecificVehicleDetailsForAndroid);

router.route('/update_vehicle_driver_mapping')
.put(updateVehicleDriverMapping);

router.route('/delete_maintenance_data_of_vehicle_for_android')
.post(deleteMaintenanceDataOfVehicleForAndroid);




// router.route('/delete_multiple_vehicle_details')
// .post(deleteMultipleVehicleDetails);
// router.route('/undo_delete_multiple_vehicle_details')
// .post(undoDeleteMultipleVehicleDetails);


//Vehicel_model.............................................ENDS........................


//Contact_us_model..........................................STARTS......................
router.route('/add_contact_us_details')
.post(addContactUsDetails);
router.route('/fetch_contact_us_details_for_android')
.get(fetchContactUsDetailsForAndroid);
router.route('/delete_contact_us_data/:code')
.delete(deleteContactUsData);
router.route('/update_contact_us_details')
.put(updateContactUsDetails);
//Contact_us_Model..........................................ENDS.......................


//Auto_parts.................................................STARTS...................
router.route('/add_new_auto_part_challan_for_android')
.post(addNewAutoPartChallanForAndroid);
router.route('/fetch_all_auto_part_challan_for_android')
.post(fetchAllAutoPartChallanForAndroid);
router.route('/update_auto_part_challan_for_android')
.put(updateAutoPartChallanForAndroid);
router.route('/delete_specific_auto_part_challan_details_for_android')
.post(deleteSpecificAutoPartChallanDetailsForAndroid);
// router.route('/delete_multiple_auto_part_challan_details')
// .post(deleteMultipleAutoPartsChallanDetails);
router.route('/ecrypt_data_check_api')
.post(ecryptDataCheckApi);
router.route('/fetch_all_necessary_data_for_adding_and_updating_auto_part_for_android/:code')
.get(fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid);
router.route('/filter_auto_parts_data_for_android')
.post(filterAutoPartsDataForAndroid);
router.route('/search_auto_parts_data_for_android')
.post(searchAutoPartsDataForAndroid);
router.route('/fetch_specific_auto_part_challan_details_for_android')
.post(fetchSpecificAutoPartChallanDetailsForAndroid);
router.route('/fetch_distinct_year_from_auto_parts/:database_code')
.get(fetchDistinctYearFromAutoParts);


// router.route('/undo_delete_multiple_auto_parts_challan_details')
// .post(undoDeleteMultipleAutoPartsChallanDetails);




//Auto_parts.................................................ENDS....................



//Payment_model..............................................START...................
router.route('/add_new_driver_payment_for_android')
.post(addNewDriverPaymentForAndroid);
router.route('/fetch_all_driver_payments_for_android')
.post(fetchAllDriverPaymentsForAndroid);
router.route('/update_driver_payment_details_for_android')
.put(updateDriverPaymentDetailsForAndroid);
router.route('/delete_specific_driver_payment_details_for_android')
.post(deleteSpecificDriverPaymentDetailsForAndroid);
// router.route('/delete_multiple_driver_payment_details')
// .post(deleteMultipleDriverPaymentDetails);
router.route('/fetch_all_drivers_for_adding_payment_for_android/:code')
.get(fetchAllDriversForAddingPaymentForAndroid);
router.route('/fetch_specific_driver_payment_details_for_android')
.post(fetchSpecificDriverPaymentDetailsForAndroid);
router.route('/search_driver_payment_data_for_android')
.post(searchDriverPaymentDataForAndroid);
router.route('/filter_driver_payment_data_for_android')
.post(filterDriverPaymentDataForAndroid);
router.route('/fetch_distinct_year_from_driver_payments/:database_code')
.get(fetchDistinctYearFromDriverPayments);



// router.route('/undo_delete_multiple_driver_payment_details')
// .post(undoDeleteMultipleDriverPaymentDetails);



//Payment_model...............................................ENDS...................



//PARTY_Payment_model..............................................START...................
router.route('/add_new_party_payment_for_android')
.post(addNewPartyPaymentForAndroid);
router.route('/fetch_all_party_payments_for_android')
.post(fetchAllPartyPaymentsForAndroid);
router.route('/update_party_payment_details_for_android')
.put(updatePartyPaymentDetailsForAndroid);
router.route('/delete_specific_party_payment_details_for_android')
.post(deleteSpecificPartyPaymentDetailsForAndroid);
router.route('/delete_multiple_party_payment_details')
.post(deleteMultiplePartyPaymentDetails);
router.route('/undo_delete_multiple_party_payment_details')
.post(undoDeleteMultiplePartyPaymentDetails);


// router.route('/fetch_all_party_for_adding_payment_for_android/:code')
// .get(fetchAllPartyForAddingPaymentForAndroid);
router.route('/fetch_specific_party_payment_details_for_android')
.post(fetchSpecificPartyPaymentDetailsForAndroid);
router.route('/search_party_payment_data_for_android')
.post(searchPartyPaymentDataForAndroid);
router.route('/filter_party_payment_data_for_android')
.post(filterPartyPaymentDataForAndroid);
router.route('/fetch_all_party_type/:code')
.get(fetchAllPartyType);
router.route('/fetch_distinct_year_from_party_payments/:database_code')
.get(fetchDistinctYearFromPartyPayments);



//PARTY_Payment_model...............................................ENDS...................




//Fuel_entry..................................................STARTS.....................
router.route('/add_new_fuel_entry_for_android')
.post(addNewFuelEntryForAndroid);
router.route('/fetch_all_fuel_entry_details_for_android')
.post(fetchAllFuelEntryDetailsForAndroid);
router.route('/update_fuel_entry_details_for_android')
.put(updateFuelEntryDetailsForAndroid);
// router.route('/delete_multiple_fuel_entry_details')
// .post(deleteMultipleFuelEntryDetails);
router.route('/delete_specific_fuel_entry_details_for_android')
.post(deleteSpecificFuelEntryDetailsForAndroid);
router.route('/fetch_all_fuel_entry_party_data_for_android/:code')
.get(fetchAllFuelEntryPartyDataForAndroid);
router.route('/fetch_specific_fuel_entry_details_for_android')
.post(fetchSpecificFuelEntryDetailsForAndroid);
router.route('/filter_fuel_entry_data_for_android')
.post(filterFuelEntryDataForAndroid);
router.route('/search_fuel_entry_data_for_android')
.post(searchFuelEntryDataForAndroid);

router.route('/fetch_distinct_year_from_fuel_entry_challan/:database_code')
.get(fetchDistinctYearFromFuelEntryChallan);




// router.route('/undo_delete_multiple_fuel_entry_details')
// .post(undoDeleteMultipleFuelEntryDetails);



//Fuel_entry..................................................ENDS..........................




//Fuel_entry_diesel..................................................STARTS.....................
router.route('/add_new_fuel_entry_diesel_rate_for_android')
.post(addNewFuelEntryDieselRateForAndroid);
router.route('/fetch_all_fuel_entry_diesel_rate_details_of_specific_party_for_android')
.post(fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid);
router.route('/update_fuel_entry_diesel_rate_for_android')
.put(updateFuelEntryDieselRateForAndroid);
router.route('/fetch_specific_fuel_entry_diesel_rate_details_by_party_and_entry_date_for_android')
.post(fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid);
router.route('/undo_delete_multiple_diesel_rate_details')
.post(undoDeleteMultipleDieselRateDetails);


//Fuel_entry_diesel..................................................ENDS..........................


//Billty_model................................................STARTS........................
router.route('/add_new_billty_for_android')
.post(addNewBilltyForAndroid);
router.route('/fetch_all_billties_for_android')
.post(fetchAllBilltiesForAndroid);
router.route('/update_billty_details_for_android')
.put(updateBilltyDetailsForAndroid);
router.route('/delete_specific_billty_details_for_android')
.post(deleteSpecificBilltyDetailsForAndroid);
router.route('/delete_multiple_billty_details')
.post(deleteMultipleBilltyDetails);
router.route('/undo_delete_multiple_billty_details')
.post(undoDeleteMultipleBilltyDetails);
router.route('/validate_billty_no')
.post(validateBilltyNo);
router.route('/fetch_billties_for_vehicle_date')
.post(fetchBilltiesForVehicleDate);
router.route('/fetch_all_necessary_data_for_adding_and_updating_billty_for_android/:code')
.get(fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid);
router.route('/search_billty_data_for_android')
.post(searchBilltyDataForAndroid);
router.route('/fetch_specific_billty_details_for_android')
.post(fetchSpecificBilltyDetailsForAndroid);
router.route('/filter_billty_data_for_android')
.post(filterBilltyDataForAndroid);

router.route('/fetch_distinct_year_from_billties/:database_code')
.get(fetchDistinctYearFromBillties);






//Billty_model................................................ENDS........................



//USER_model...................................................STARTS......................
router.route('/update_user_details_for_android')
.put(updateUserDetailsForAndroid);
router.route('/fetch_specific_user_details_for_android/:code')
.get(fetchSpecificUserDetailsForAndroid);
router.route('/verify_otp_for_change_mobile_of_user_for_android')
.post(verifyOtpForChangeMobileOfUserForAndroid);
router.route('/recharge_user_account')
.post(rechargeUserAccount);
router.route('/update_profile_image_of_user')
.put(updateProfileImageOfUser);
router.route('/send_otp_for_change_mobile_of_user_for_android')
.post(sendOtpForChangeMobileOfUserForAndroid);


//USer_model...................................................ENDS........................


//Other_model...................................................STARTS...................
router.route('/fetch_all_party_from_party_type_for_form_data')
.post(fetchAllPartyFromPartyTypeForFormData);
router.route('/fetch_all_vehicle_list_for_form_data')
.post(fetchAllVehicleListForFormData);

router.route('/fetch_all_route_for_form_data')
.post(fetchAllRouteForFormData);
router.route('/fetch_all_driver_list_for_form_data')
.post(fetchAllDriverListForFormData);

router.route('/fetch_all_mine_data_for_form_data/:code')
.get(fetchAllMineDataForFormData);






//Other_model....................................................ENDS...................


//Attendance_model...............................................STARTS...............
router.route('/add_new_attendance_for_android')
.post(addNewAttendanceForAndroid);
router.route('/fetch_attendance_month_wise_for_android')
.post(fetchAttendanceMonthWiseForAndroid);
router.route('/fetch_specific_attendance_details_for_android')
.post(fetchSpecificAttendanceDetailsForAndroid);
router.route('/update_attendance_for_android')
.put(updateAttendanceForAndroid);
router.route('/delete_multiple_attendance_details')
.post(deleteMultipleAttendanceDetails);
router.route('/undo_delete_multiple_attendance_details')
.post(undoDeleteMultipleAttendanceDetails);

//Attendance_model..............................................ENDS.................



//Ledger_model...............................................STARTS...............
router.route('/fetch_all_ledger_data/:code')
.get(fetchAllLedgerData);
router.route('/fetch_specific_ledger_details_of_party')
.post(fetchSpecificLedgerDetailsOfParty);
//Ledger_model..............................................ENDS.................



//NOTIFICATION_OF_USER.........................................STARTS..................
router.route('/fetch_all_notification_of_user_for_android/:index/:limit/:code')
.get(fetchAllNotificationOfUserForAndroid);
router.route('/seen_all_notification_of_user_for_android/:code')
.get(seenAllNotificationOfUserForAndroid);
router.route('/seen_one_notification_of_user')
.post(seenOneNotificationOfUser);
//NOTIFICATION_OF_USER.........................................ENDS..................



//Report_Model.........................................STARTS..................
router.route('/fetch_fuel_entry_report_for_web')
.post(fetchFuelEntryReportForWeb);
router.route('/fetch_auto_part_report_for_web')
.post(fetchAutoPartReportForWeb);
router.route('/fetch_all_necessary_data_for_fuel_entry_and_auto_part_report_for_web/:code')
.get(fetchAllNecessaryDataForFuelEntryAndAutoPartReport);

//Report_Model.........................................ENDS..................


//Driver_Report........................................START................
router.route('/fetch_all_necesary_data_for_filtering_driver_report_for_web/:code')
.get(fetchAllNecesaryDataForFilteringDriverReportForWeb);
router.route('/fetch_all_driver_report_for_web')
.post(fetchAllDriverReportForWeb);
//Driver_Report..........................................ENDS...............


//Transporter_Report........................................STARTS................
router.route('/fetch_transporter_report_for_web')
.post(fetchTransporterReportForWeb);
router.route('/fetch_all_owner_list_of_vehicles_for_web/:code')
.get(fetchAllOwnerListOfVehiclesForWeb);
router.route('/fetch_all_vehicle_list_of_specific_owner')
.post(fetchAllVehicleListOfSpecificOwner);
router.route('/fetch_all_necessary_data_for_filtering_transport_report_data_for_web/:code')
.get(fetchAllNecessaryDataForFilteringTransportReportData);
//Transporter_Report........................................ENDS................




//Voucher Receipt...........................................START..............
router.route('/add_new_voucher_receipt')
.post(addNewVoucherReceipt);
router.route('/fetch_all_voucher_receipt')
.post(fetchAllVoucherReceipt);
router.route('/fetch_specific_voucher_receipt_details')
.post(fetchSpecificVoucherReceiptDetails);
router.route('/delete_voucher_receipt')
.post(deleteVoucherReceipt);
router.route('/update_voucher_receipt')
.put(updateVoucherReceipt);

router.route('/search_voucher_receipt')
.post(searchVoucherReceipt);

router.route('/filter_voucher_receipt')
.post(filterVoucherReceipt);

router.route('/fetch_distinct_year_from_voucher_receipt/:database_code')
.get(fetchDistinctYearFromVoucherReceipt);






//Voucher Receipt...........................................ENDS..............

}

//Party_model..............................................STARTS...................

function addNewPartyForAndroid(req,res) {
  partyModel.addNewPartyForAndroid(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllPartyDataForAndroid(req,res) {
  partyModel.fetchAllPartyDataForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updatePartyForAndroid(req,res) {
  partyModel.updatePartyForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteSpecificPartyDetailsForAndroid(req,res) {
  partyModel.deleteSpecificPartyDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




// function deleteMultiplePartyDetails(req,res) {
//   partyModel.deleteMultiplePartyDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }



// function undoDeleteMultiplePartyDetails(req,res) {
//   partyModel.undoDeleteMultiplePartyDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }


function fetchAllTransporterPartyDataForAndroid(req,res) {
  partyModel.fetchAllTransporterPartyDataForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchSpecificPartyDetailsForAndroid(req,res) {
  partyModel.fetchSpecificPartyDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



//Party_model..............................................ENDS.....................


//MINE_Model...............................................STARTS..................

function addNewMineForAndroid(req,res) {
  mineModel.addNewMineForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllMineDetailsForAndroid(req,res) {
  mineModel.fetchAllMineDetailsForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateMineForAndroid(req,res) {
  mineModel.updateMineForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function deleteSpecificMineDetailsForAndroid(req,res) {
  mineModel.deleteSpecificMineDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function deleteMultipleMineDetails(req,res) {
  mineModel.deleteMultipleMineDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function undoDeleteMultipleMineDetails(req,res) {
  mineModel.undoDeleteMultipleMineDetails(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


//Mine_Model...............................................ENDS........................



//Route_model..............................................Start.....................

function fetchAllNecessaryDataForAddingRouteForAndroid(req,res) {
  routeModel.fetchAllNecessaryDataForAddingRouteForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function addNewRouteForAndroid(req,res) {
  routeModel.addNewRouteForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllRoutesForAndroid(req,res) {
  routeModel.fetchAllRoutesForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function updateRouteForAndroid(req,res) {
  routeModel.updateRouteForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteSpecificRouteDetailsForAndroid(req,res) {
  routeModel.deleteSpecificRouteDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function updateFrieghtRate(req,res) {
  routeModel.updateFrieghtRate(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}





// function deleteMultipleRouteDetails(req,res) {
//   routeModel.deleteMultipleRouteDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }



// function undoDeleteMultipleRouteDetails(req,res) {
//   routeModel.undoDeleteMultipleRouteDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }


//Route_model...............................................ENDS.....................



//Driver_model..............................................START....................

function addNewDriverForAndroid(req,res) {
  driverModel.addNewDriverForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchAllDriversForAndroid(req,res) {
  driverModel.fetchAllDriversForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateDriverDetailsForAndroid(req,res) {
  driverModel.updateDriverDetailsForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




function deleteSpecificDriverDetailsForAndroid(req,res) {
  driverModel.deleteSpecificDriverDetailsForAndroid(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


// function deleteMultipleDriverDetails(req,res) {
//   driverModel.deleteMultipleDriverDetails(req.body,res).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }


// function undoDeleteMultipleDriverDetails(req,res) {
//   driverModel.undoDeleteMultipleDriverDetails(req.body,res).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }




function fetchSpecificDriverDetailsForAndroid(req,res) {
  driverModel.fetchSpecificDriverDetailsForAndroid(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}




//Driver_model...............................................ENDS....................





//Vehicle_model..............................................STARTS......................

function addNewVehicleForAndroid(req,res) {
  vehicleModel.addNewVehicleForAndroid(req.body,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function fetchAllVehicleForAndroid(req,res) {
  vehicleModel.fetchAllVehicleForAndroid(req,res).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function updateVehicleForAndroid(req,res) {
  vehicleModel.updateVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}

function deleteSpecificVehicleDetailsForAndroid(req,res) {
  vehicleModel.deleteSpecificVehicleDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function addNewMaintenanceDataOfVehicleForAndroid(req,res) {
  vehicleModel.addNewMaintenanceDataOfVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function updateMaintenanceDataOfVehicleForAndroid(req,res) {
  vehicleModel.updateMaintenanceDataOfVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchMaintenanceDataOfVehicleForAndroid(req,res) {
  vehicleModel.fetchMaintenanceDataOfVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchSpecificMaintenanceDataOfVehicleForAndroid(req,res) {
  vehicleModel.fetchSpecificMaintenanceDataOfVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function fetchSpecificVehicleDetailsForAndroid(req,res) {
  vehicleModel.fetchSpecificVehicleDetailsForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}



function updateVehicleDriverMapping(req,res) {
  vehicleModel.updateVehicleDriverMapping(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}


function deleteMaintenanceDataOfVehicleForAndroid(req,res) {
  vehicleModel.deleteMaintenanceDataOfVehicleForAndroid(req.body).then((data) => {
  if(data) {
  res.json(data);
  }
  }).catch((err) => {
  res.json(err);
 });
}









// function deleteMultipleVehicleDetails(req,res) {
//   vehicleModel.deleteMultipleVehicleDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }



// function undoDeleteMultipleVehicleDetails(req,res) {
//   vehicleModel.undoDeleteMultipleVehicleDetails(req.body).then((data) => {
//   if(data) {
//   res.json(data);
//   }
//   }).catch((err) => {
//   res.json(err);
//  });
// }



//Vehicel_model.............................................ENDS........................


//Contact_us_model..........................................STARTS......................


function addContactUsDetails(req,res) {
  contactUsModel.addContactUsDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchContactUsDetailsForAndroid(req,res) {
  contactUsModel.fetchContactUsDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function deleteContactUsData(req,res) {
  contactUsModel.deleteContactUsData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateContactUsDetails(req,res) {
  contactUsModel.updateContactUsDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//Contact_us_model..........................................ENDS......................




//Auto_parts.................................................STARTS...................



function ecryptDataCheckApi(req,res) {
  autoPartsModel.ecryptDataCheckApi(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function addNewAutoPartChallanForAndroid(req,res) {
  autoPartsModel.addNewAutoPartChallanForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function fetchAllAutoPartChallanForAndroid(req,res) {
  autoPartsModel.fetchAllAutoPartChallanForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updateAutoPartChallanForAndroid(req,res) {
  autoPartsModel.updateAutoPartChallanForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificAutoPartChallanDetailsForAndroid(req,res) {
  autoPartsModel.deleteSpecificAutoPartChallanDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




// function deleteMultipleAutoPartsChallanDetails(req,res) {
//   autoPartsModel.deleteMultipleAutoPartsChallanDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }

// function undoDeleteMultipleAutoPartsChallanDetails(req,res) {
//   autoPartsModel.undoDeleteMultipleAutoPartsChallanDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }



function fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid(req,res) {
  autoPartsModel.fetchAllNecessaryDataForAddingAndUpdatingAutoPartForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function filterAutoPartsDataForAndroid(req,res) {
  autoPartsModel.filterAutoPartsDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function searchAutoPartsDataForAndroid(req,res) {
  autoPartsModel.searchAutoPartsDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificAutoPartChallanDetailsForAndroid(req,res) {
  autoPartsModel.fetchSpecificAutoPartChallanDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchDistinctYearFromAutoParts(req,res) {
  autoPartsModel.fetchDistinctYearFromAutoParts(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





//Auto_parts.................................................ENDS....................




//Payment_model..............................................START...................

function addNewDriverPaymentForAndroid(req,res) {
  driverPaymentModel.addNewDriverPaymentForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllDriverPaymentsForAndroid(req,res) {
  driverPaymentModel.fetchAllDriverPaymentsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updateDriverPaymentDetailsForAndroid(req,res) {
  driverPaymentModel.updateDriverPaymentDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificDriverPaymentDetailsForAndroid(req,res) {
  driverPaymentModel.deleteSpecificDriverPaymentDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



// function deleteMultipleDriverPaymentDetails(req,res) {
//   driverPaymentModel.deleteMultipleDriverPaymentDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }


// function undoDeleteMultipleDriverPaymentDetails(req,res) {
//   driverPaymentModel.undoDeleteMultipleDriverPaymentDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }




function fetchAllDriversForAddingPaymentForAndroid(req,res) {
  driverPaymentModel.fetchAllDriversForAddingPaymentForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchSpecificDriverPaymentDetailsForAndroid(req,res) {
  driverPaymentModel.fetchSpecificDriverPaymentDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function searchDriverPaymentDataForAndroid(req,res) {
  driverPaymentModel.searchDriverPaymentDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function filterDriverPaymentDataForAndroid(req,res) {
  driverPaymentModel.filterDriverPaymentDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchDistinctYearFromDriverPayments(req,res) {
  driverPaymentModel.fetchDistinctYearFromDriverPayments(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




//Payment_model...............................................ENDS...................










//Party_Payment_model..............................................START...................

function addNewPartyPaymentForAndroid(req,res) {
  partyPaymentModel.addNewPartyPaymentForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllPartyPaymentsForAndroid(req,res) {
  partyPaymentModel.fetchAllPartyPaymentsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function updatePartyPaymentDetailsForAndroid(req,res) {
  partyPaymentModel.updatePartyPaymentDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteSpecificPartyPaymentDetailsForAndroid(req,res) {
  partyPaymentModel.deleteSpecificPartyPaymentDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function deleteMultiplePartyPaymentDetails(req,res) {
  partyPaymentModel.deleteMultiplePartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultiplePartyPaymentDetails(req,res) {
  partyPaymentModel.undoDeleteMultiplePartyPaymentDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



// function fetchAllPartyForAddingPaymentForAndroid(req,res) {
//   partyPaymentModel.fetchAllPartyForAddingPaymentForAndroid(req,res).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }



function fetchSpecificPartyPaymentDetailsForAndroid(req,res) {
  partyPaymentModel.fetchSpecificPartyPaymentDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function searchPartyPaymentDataForAndroid(req,res) {
  partyPaymentModel.searchPartyPaymentDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function filterPartyPaymentDataForAndroid(req,res) {
  partyPaymentModel.filterPartyPaymentDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllPartyType(req,res) {
  partyPaymentModel.fetchAllPartyType(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchDistinctYearFromPartyPayments(req,res) {
  partyPaymentModel.fetchDistinctYearFromPartyPayments(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}






//Party_Payment_model...............................................ENDS...................









//Fuel_entry..................................................STARTS.....................
function addNewFuelEntryForAndroid(req,res) {
  fuelEntryModel.addNewFuelEntryForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllFuelEntryDetailsForAndroid(req,res) {
  fuelEntryModel.fetchAllFuelEntryDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateFuelEntryDetailsForAndroid(req,res) {
  fuelEntryModel.updateFuelEntryDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

// function deleteMultipleFuelEntryDetails(req,res) {
//   fuelEntryModel.deleteMultipleFuelEntryDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }


// function undoDeleteMultipleFuelEntryDetails(req,res) {
//   fuelEntryModel.undoDeleteMultipleFuelEntryDetails(req.body).then((data) => {
//    if(data) {
//     res.json(data);
//     }
//    }).catch((err) => {
//    res.json(err);
//   });
// }



function deleteSpecificFuelEntryDetailsForAndroid(req,res) {
  fuelEntryModel.deleteSpecificFuelEntryDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllFuelEntryPartyDataForAndroid(req,res) {
  fuelEntryModel.fetchAllFuelEntryPartyDataForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchSpecificFuelEntryDetailsForAndroid(req,res) {
  fuelEntryModel.fetchSpecificFuelEntryDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function filterFuelEntryDataForAndroid(req,res) {
  fuelEntryModel.filterFuelEntryDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function searchFuelEntryDataForAndroid(req,res) {
  fuelEntryModel.searchFuelEntryDataForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchDistinctYearFromFuelEntryChallan(req,res) {
  fuelEntryModel.fetchDistinctYearFromFuelEntryChallan(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





//Fuel_entry..................................................ENDS..........................








//Fuel_entry_diesel..................................................STARTS.....................
function addNewFuelEntryDieselRateForAndroid(req,res) {
  fuelEntryDieselModel.addNewFuelEntryDieselRateForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid(req,res) {
  fuelEntryDieselModel.fetchAllFuelEntryDieselRateDetailsOfSpecificPartyAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateFuelEntryDieselRateForAndroid(req,res) {
  fuelEntryDieselModel.updateFuelEntryDieselRateForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid(req,res) {
  fuelEntryDieselModel.fetchSpecificFuelEntryDieselRateDetailsByPartyAndEntryDateForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function undoDeleteMultipleDieselRateDetails(req,res) {
  fuelEntryDieselModel.undoDeleteMultipleDieselRateDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


//Fuel_entry_diesel_rate..................................................ENDS..........................





//Billty_model................................................STARTS........................




function addNewBilltyForAndroid(req,res) {
  billtyModel.addNewBilltyForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data); 
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAllBilltiesForAndroid(req,res) {
  billtyModel.fetchAllBilltiesForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function updateBilltyDetailsForAndroid(req,res) {
  billtyModel.updateBilltyDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function deleteSpecificBilltyDetailsForAndroid(req,res) {
  billtyModel.deleteSpecificBilltyDetailsForAndroid(req.body).then((data) => {
   if(data) {
   res.json(data);
   }
   }).catch((err) => {
   res.json(err);
  }); 
  }

  function deleteMultipleBilltyDetails(req,res) {
    billtyModel.deleteMultipleBilltyDetails(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }


  function undoDeleteMultipleBilltyDetails(req,res) {
    billtyModel.undoDeleteMultipleBilltyDetails(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }

  
  function validateBilltyNo(req,res) {
    billtyModel.validateBilltyNo(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }

    
  function fetchBilltiesForVehicleDate(req,res) {
    billtyModel.fetchBilltiesForVehicleDate(req.body).then((data) => {
     if(data) {
      res.json(data);
      }
     }).catch((err) => {
     res.json(err);
    });
  }

   

  
  
  

  function fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid(req,res) {
    billtyModel.fetchAllNecessaryDataForAddingAndUpdatingBilltyForAndroid(req,res).then((data) => {
      if(data) {
      res.json(data);
      }
      }).catch((err) => {
      res.json(err);
    });
  }
    

function searchBilltyDataForAndroid(req,res) {
  billtyModel.searchBilltyDataForAndroid(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}


    

function fetchSpecificBilltyDetailsForAndroid(req,res) {
  billtyModel.fetchSpecificBilltyDetailsForAndroid(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}

function filterBilltyDataForAndroid(req,res) {
  billtyModel.filterBilltyDataForAndroid(req.body).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}
      

function fetchDistinctYearFromBillties(req,res) {
  billtyModel.fetchDistinctYearFromBillties(req,res).then((data) => {
    if(data) {
    res.json(data);
    }
    }).catch((err) => {
    res.json(err);
  });
}
      

//Billty_model................................................ENDS........................




//USER_model...................................................STARTS......................

function updateUserDetailsForAndroid(req,res) {
  usersModel.updateUserDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchSpecificUserDetailsForAndroid(req,res) {
  usersModel.fetchSpecificUserDetailsForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function verifyOtpForChangeMobileOfUserForAndroid(req,res) {
  usersModel.verifyOtpForChangeMobileOfUserForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function sendOtpForChangeMobileOfUserForAndroid(req,res) {
  usersModel.sendOtpForChangeMobileOfUserForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function rechargeUserAccount(req,res) {
  usersModel.rechargeUserAccount(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function updateProfileImageOfUser(req,res) {
  usersModel.updateProfileImageOfUser(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




//USer_model...................................................ENDS........................



//Other_model...................................................STARTS...................


function fetchAllPartyFromPartyTypeForFormData(req,res) {
  otherModel.fetchAllPartyFromPartyTypeForFormData(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllVehicleListForFormData(req,res) {
  otherModel.fetchAllVehicleListForFormData(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllRouteForFormData(req,res) {
  otherModel.fetchAllRouteForFormData(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllDriverListForFormData(req,res) {
  otherModel.fetchAllDriverListForFormData(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllMineDataForFormData(req,res) {
  otherModel.fetchAllMineDataForFormData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





//Other_model....................................................ENDS...................



//Attendance_model...............................................STARTS...............

function addNewAttendanceForAndroid(req,res) {
  attendanceModel.addNewAttendanceForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

function fetchAttendanceMonthWiseForAndroid(req,res) {
  attendanceModel.fetchAttendanceMonthWiseForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificAttendanceDetailsForAndroid(req,res) {
  attendanceModel.fetchSpecificAttendanceDetailsForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function deleteMultipleAttendanceDetails(req,res) {
  attendanceModel.deleteMultipleAttendanceDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function undoDeleteMultipleAttendanceDetails(req,res) {
  attendanceModel.undoDeleteMultipleAttendanceDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}





function updateAttendanceForAndroid(req,res) {
  attendanceModel.updateAttendanceForAndroid(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

//Attendance_model..............................................ENDS.................




//Ledger_model...............................................STARTS...............



function fetchAllLedgerData(req,res) {
  ledgerModel.fetchAllLedgerData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificLedgerDetailsOfParty(req,res) {
  ledgerModel.fetchSpecificLedgerDetailsOfParty(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

//Ledger_model..............................................ENDS.................






//NOTIFICATION_OF_USER.........................................STARTS..................


function fetchAllNotificationOfUserForAndroid(req,res) {
  userNotificationModel.fetchAllNotificationOfUserForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function seenAllNotificationOfUserForAndroid(req,res) {
  userNotificationModel.seenAllNotificationOfUserForAndroid(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}




function seenOneNotificationOfUser(req,res) {
  userNotificationModel.seenOneNotificationOfUser(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//NOTIFICATION_OF_USER.........................................ENDS..................



//Report_Model.........................................STARTS..................

function fetchFuelEntryReportForWeb(req,res) {
  fuelEntryAndAutoPartsReportModel.fetchFuelEntryReportForWeb(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAutoPartReportForWeb(req,res) {
  fuelEntryAndAutoPartsReportModel.fetchAutoPartReportForWeb(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllNecessaryDataForFuelEntryAndAutoPartReport(req,res) {
  fuelEntryAndAutoPartsReportModel.fetchAllNecessaryDataForFuelEntryAndAutoPartReport(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


 


//Report_Model.........................................ENDS................



//Driver_Report_model..................................START...............


function fetchAllNecesaryDataForFilteringDriverReportForWeb(req,res) {
  driverReportModel.fetchAllNecesaryDataForFilteringDriverReportForWeb(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllDriverReportForWeb(req,res) {
  driverReportModel.fetchAllDriverReportForWeb(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}
//Driver_report_model..................................ENDS..............



//Transporter_Report........................................STARTS................


function fetchTransporterReportForWeb(req,res) {
  transporterReportModel.fetchTransporterReportForWeb(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllOwnerListOfVehiclesForWeb(req,res) {
  transporterReportModel.fetchAllOwnerListOfVehiclesForWeb(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllVehicleListOfSpecificOwner(req,res) {
  transporterReportModel.fetchAllVehicleListOfSpecificOwner(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchAllNecessaryDataForFilteringTransportReportData(req,res) {
  transporterReportModel.fetchAllNecessaryDataForFilteringTransportReportData(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}

//Transporter_Report........................................ENDS................




//Voucher Receipt...........................................START..............

function addNewVoucherReceipt(req,res) {
  voucherModel.addNewVoucherReceipt(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchAllVoucherReceipt(req,res) {
  voucherModel.fetchAllVoucherReceipt(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function fetchSpecificVoucherReceiptDetails(req,res) {
  voucherModel.fetchSpecificVoucherReceiptDetails(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function deleteVoucherReceipt(req,res) {
  voucherModel.deleteVoucherReceipt(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function updateVoucherReceipt(req,res) {
  voucherModel.updateVoucherReceipt(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function searchVoucherReceipt(req,res) {
  voucherModel.searchVoucherReceipt(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}


function filterVoucherReceipt(req,res) {
  voucherModel.filterVoucherReceipt(req.body).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}



function fetchDistinctYearFromVoucherReceipt(req,res) {
  voucherModel.fetchDistinctYearFromVoucherReceipt(req,res).then((data) => {
   if(data) {
    res.json(data);
    }
   }).catch((err) => {
   res.json(err);
  });
}







//Voucher Receipt...........................................ENDS..............

module.exports.init = init;
